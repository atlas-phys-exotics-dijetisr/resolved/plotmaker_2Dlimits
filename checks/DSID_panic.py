
import sys
import ROOT
ROOT.gROOT.SetBatch(1)

limitsFile = '../data/limits2016_TLA_J75yStar03_v6.txt'
fh=open(limitsFile)
limitsDict = eval(fh.read())

DSIDdict = {}

for gq in limitsDict:
    for mZ in limitsDict[gq]:
        
        DSID = limitsDict[gq][mZ]['dsid']
        if DSID == 0:
            continue

        DSIDdict[DSID] = {'gq': gq, 'mZ': mZ}

        
canv = ROOT.TCanvas()
hist = ROOT.TH2F("hist", ";m_{Z'} [GeV]; g_{q}", 100, 300, 2000, 100, 0, 0.5)
hist.SetStats(0)
hist.Draw()

markers = {1: [], 2: [], 3: [], 4: []}
doneLeg = {1: False, 2: False, 3: False, 4: False}
DSIDrange = {1: range(301859,301864+1), 2: range(301883,301886+1), 3: range(304968,304997+1), 4: range(305615,305621+1)}

leg = ROOT.TLegend(0.7, 0.8, 0.99, 0.99)

for DSID in sorted(DSIDdict):
    print DSID, DSIDdict[DSID]

    foundOne = False
    for DSIDset in DSIDrange:
        if DSID in DSIDrange[DSIDset]:
            foundOne = True
            break
    if not foundOne:
        print "failed to find a DSID set for", DSID
        sys.exit()
        
    markers[DSIDset].append(ROOT.TMarker(DSIDdict[DSID]['mZ']*1000, DSIDdict[DSID]['gq'], 20))
    # markers[-1].SetMarkerSize(20)

    if DSIDset == 1:
        col = ROOT.kRed
    elif DSIDset == 2:
        col = ROOT.kBlue
    elif DSIDset == 3:
        col = ROOT.kGreen+1
    elif DSIDset == 4:
        col = ROOT.kMagenta

    if not doneLeg[DSIDset]:
        legname = str(DSIDrange[DSIDset][0]) +'-'+str(DSIDrange[DSIDset][-1])
        print "adding legend entry for", legname
        leg.AddEntry(markers[DSIDset][-1], legname, "P")

        doneLeg[DSIDset] = True
        
    markers[DSIDset][-1].SetMarkerColor(col)
    markers[DSIDset][-1].Draw()

leg.Draw()
    
canv.SaveAs('points_by_DSID.png')
