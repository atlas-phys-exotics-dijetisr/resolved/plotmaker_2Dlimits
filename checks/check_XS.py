
# turn XS file into dictionary
XSfile = '../data/CrossSectionData.txt'
XSdict = {}

entries = ['DSID', 'DSName', 'crossSection', 'BR', 'genFiltEff', 'Xsec', 'kFactor', 'TotSampleXsec']

for line in open(XSfile).readlines():
    # DSID/I:DSName/C:crossSection/D:BR:genFiltEff:Xsec:kFactor:TotSampleXsec
    ls = line.rstrip('\n\r').split()
    try:
        DSID = int(ls[0])
    except:
        print 'skipping line', line
        continue

    XSdict[DSID] = {}
    for i in range(1,len(entries)):
        try:
            XSdict[DSID][entries[i]] = float(ls[i])
        except ValueError:
            XSdict[DSID][entries[i]] = ls[i]


limitsFile = '../data/limits2016_TLA_J75yStar03_v6.txt'
fh=open(limitsFile)
limitsDict = eval(fh.read())

DSIDdict = {}

for gq in limitsDict:
    for mZ in limitsDict[gq]:
        
        DSID = limitsDict[gq][mZ]['dsid']
        if DSID == 0:
            continue
        XS = limitsDict[gq][mZ]['theory']

        DSIDdict[DSID] = {'gq': gq, 'mZ': mZ, 'XS': XS}

print "DSID", "\t", "mZ", "\t", "gq", "\t", "AMI XS", "\t", "TLA XS", "\t", "AMI / TLA"
for DSID in sorted(DSIDdict):
    XS_AMI = XSdict[DSID]['TotSampleXsec']
    XS_limitDict = DSIDdict[DSID]['XS']

    print DSID, "\t", DSIDdict[DSID]['mZ'], "\t", DSIDdict[DSID]['gq'], "\t", '%.2f' % XS_AMI, "\t", '%.2f' % XS_limitDict, "\t", '%.2f' % (XS_AMI / XS_limitDict)
