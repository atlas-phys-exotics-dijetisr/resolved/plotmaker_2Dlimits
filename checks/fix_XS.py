
import copy
import os

limitFiles = ['../data/limits2016_TLA_J75yStar03_v6.txt', '../data/limits2016_TLA_J100yStar06_v6.txt']
morphedFiles = ['7503_morphed.py', '10006_morphed.py']

oldVersion = 'v6'
newVersion = 'v8'

badRange = range(301859,301864+1)

items = ['acc',
         'exp-2',
         'exp-1',
         'nPEs',
         'exp',
         'exp+1',
         'exp+2',
         'dsid',
         'obs',
         'theory'
         ]

for i in range(len(limitFiles)):

    oldLimitFile = limitFiles[i]
    newLimitFile = oldLimitFile.replace(oldVersion, newVersion)
    morphedFile = morphedFiles[i]

    fh=open(oldLimitFile)
    oldLimitsDict = eval(fh.read())
    fh.close()

    newLimitsDict = copy.deepcopy(oldLimitsDict)

    fh=open(morphedFile)
    morphedXSdict = eval(fh.read())
    fh.close()
    
    for gq in newLimitsDict:
        for mZ in newLimitsDict[gq]:
            # DSID = int(newLimitsDict[gq][mZ]['dsid'])
            # if DSID in badRange:
                # newLimitsDict[gq][mZ]['theory'] = newLimitsDict[gq][mZ]['theory'] * 1.25

            morphedXS = morphedXSdict[gq][mZ]['theory']
            newLimitsDict[gq][mZ]['theory'] = morphedXS
            

    newfile = open(newLimitFile,'w')
    newfile.write('{\n')
    for gq in sorted(newLimitsDict):
        newfile.write('    '+str(gq)+': {\n')

        for mZ in sorted(newLimitsDict[gq]):
            newfile.write('        '+str(mZ)+': {\n')

            for item in items:
                newfile.write("            '"+item+"': "+str(newLimitsDict[gq][mZ][item])+',\n')
            
            newfile.write('        },\n')
        
        newfile.write('    },\n')
            


    newfile.write('}\n')
