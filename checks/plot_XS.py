
import sys
import ROOT
ROOT.gROOT.SetBatch(1)

# limitsFile = '../data/limits2016_TLA_J75yStar03_v6.txt'
limitsFile = '../data/limits2016_TLA_J75yStar03_v8.txt'
fh=open(limitsFile)
limitsDict = eval(fh.read())


canv = ROOT.TCanvas()
hist = ROOT.TH1F("hist", ";m_{Z'} [GeV]; #sigma [pb]", 100, 300, 2000)
hist.SetStats(0)
hist.Draw()
hist.SetMinimum(0.1)
hist.SetMaximum(100)
canv.SetLogy()

mZs = {}
XSs = {}
mZs_bad = {}
XSs_bad = {}

isMorpheds = {}

for gq in limitsDict:
    mZs[gq] = []
    XSs[gq] = []
    mZs_bad[gq] = []
    XSs_bad[gq] = []
    isMorpheds[gq] = []
    
    for mZ in sorted(limitsDict[gq]):
        DSID = limitsDict[gq][mZ]['dsid']
        isMorphed = (DSID == 0)
        XS = limitsDict[gq][mZ]['theory']

        if DSID in range(301859,301864+1):
            mZs_bad[gq].append(mZ*1000.)
            XSs_bad[gq].append(XS)
        else:
            mZs[gq].append(mZ*1000.)
            XSs[gq].append(XS)
            
        isMorpheds[gq].append(isMorphed)

from array import array
gq = 0.1

gr = ROOT.TGraph(len(mZs[gq]), array('f', mZs[gq]), array('f', XSs[gq]) )
gr.SetMarkerStyle(ROOT.kFullCircle)
gr.Draw('P same')

gr_bad = ROOT.TGraph(len(mZs_bad[gq]), array('f', mZs_bad[gq]), array('f', XSs_bad[gq]) )
gr_bad.SetMarkerStyle(ROOT.kFullCircle)
gr_bad.SetMarkerColor(ROOT.kRed)
gr_bad.Draw('P same')

# func = ROOT.TF1("f1", "exp([0]+[1]*x+[2]*x^2)", 400, 1800)
# fit = gr.Fit("f1")
# fit.Draw()
canv.SaveAs('XS_gq0p1.png')
canv.SaveAs('XS_gq0p1.pdf')

