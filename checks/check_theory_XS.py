
inLimits = '../data/limits2016_TLA_J100yStar06_v6.txt'
minPEs = 50

limits={}
fh=open(inLimits)
limits = eval(fh.read())
fh.close()

# limits[gSM][mZ']

gSMs = sorted(limits.keys())
mZs = []

for gSM,limitsgSM in limits.items():
    for mZ in limitsgSM:
        if mZ not in mZs: mZs.append(mZ)

mZs.sort()
print gSMs, mZs

for mZ in mZs:
    print ''
    print 'mZ =', mZ
    print 'gSM \t XS \t acc \t XS/gSM**2 \t XS*acc/gSM**2'

    ref_val = 0
    for gSM in gSMs:
        if mZ not in limits[gSM]:
            continue

        if limits[gSM][mZ]['nPEs'] < minPEs:
            continue
        
        this_val = limits[gSM][mZ]['theory']*limits[gSM][mZ]['acc']/gSM**2
        if ref_val == 0:
            ref_val = this_val
        
        print gSM, '\t', '%.2f' % limits[gSM][mZ]['theory'], '\t', '%.2f' % limits[gSM][mZ]['acc'], '\t', '%.2f' % (limits[gSM][mZ]['theory']/gSM**2), '\t', '%.2f' % (limits[gSM][mZ]['theory']*limits[gSM][mZ]['acc']/gSM**2),

        if abs((this_val - ref_val)/ref_val) > 0.1:
            print "<<<<<", '%.0f' % (100*(this_val - ref_val)/ref_val) + '% diff'
        else:
            print ''
            
