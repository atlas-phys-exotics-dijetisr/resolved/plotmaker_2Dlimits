
import ROOT
ROOT.gROOT.SetBatch(1)

from prot import utiltools
from prot import canvastools
from prot import plottools

import glob
import re
import os, os.path
import numpy as np
import array
import sys

from math import *

from helpers import *


def main(inLimits, selection='', lumi=None, massrange=None, bjets=1):
    makeLimit(inLimits, selection, lumi, massrange, bjets)



def makeLimit(inLimits,selection='',lumi=None, massrange=None, bjets=1):

    # Load the limits
    limits={}
    fh=open(inLimits)
    limits = eval(fh.read())
    fh.close()

    set_palette()
    
    # Determine bins for intermediate plots
    gSMs=set()
    mRs=set()
    for gSM,limitsgSM in limits.items():
        gSMs.add(gSM)
        for mR,limit in limitsgSM.items():
            if massrange != None:
                if mR > massrange[1]: continue
            mRs.add(mR)
    gSMs=sorted(list(gSMs))
    mRs =sorted(list(mRs))
        

    minmR ,maxmR ,nbinsmR =bestbins(mRs)
    mingSM,maxgSM,nbinsgSM=bestbins(gSMs)        

    print 'mRs =', mRs
    print 'gSMs =', gSMs
    print 'plot params', minmR, maxmR, mingSM, maxgSM
    
    
    # b-jet scaling
    bjetscaling = 1.25
    if bjets==1:
        bjetscaling = 1.0

    print "I am applying a b-jet scaling of", bjetscaling

    # Make plot
    ratplots={}
    h2=ROOT.TH2F('excl',"Exclusion Plot;m_{Z'} [GeV];g_{q}",
                 nbinsmR,minmR*1e3,maxmR*1e3,
                 nbinsgSM,float('%0.3g'%mingSM),float('%0.3g'%maxgSM))

    for gSM,limitsgSM in limits.items():
        for mR,limit in limitsgSM.items():
            xslim_obs=limit.get('obs'   ,None)
            xslim_exp=limit.get('exp'   ,None)
            xstheory =limit.get('theory',None)
            binIdx=h2.FindBin(mR*1000,gSM)
            if xslim_obs==None:
                h2.SetBinContent(binIdx,0)
                continue
            h2.SetBinContent(binIdx,xslim_obs/(xstheory*bjetscaling))
            ratplots[(mR,gSM)]=(xslim_obs/(xstheory*bjetscaling),xslim_exp/(xstheory*bjetscaling))

            print "  I am putting", mR, gSM, "in bin", binIdx

    # Make graph
    gobs=ROOT.TGraph()
    gexp=ROOT.TGraph()
    gidx=0

    for mR in mRs:
        # only consider points in a certain range (i.e. for TLA y*<0.3 and y*<0.6 regions)
        if massrange != None:
            if mR < massrange[0] or mR > massrange[1]: continue
            
        # Get interesting point, where interesting = first bin where limit is mu < 1
        h=h2.ProjectionY("_py",h2.GetXaxis().FindBin(mR*1000),h2.GetXaxis().FindBin(mR*1000))
        binLim=h.GetNbinsX()
        #for binIdx in range(h.GetNbinsX(),0,-1):
        print "I think there are", binLim, "bins:"
        for binIdx in range(0, h.GetNbinsX()):
            print binIdx, "min, max =", h.GetXaxis().GetBinLowEdge(binIdx), h.GetXaxis().GetBinUpEdge(binIdx)
            mu=h.GetBinContent(binIdx)
            if mu==0: continue
            binLim=binIdx
            if mu<1:
                print "I am breaking out of the loop at bin", binIdx
                break
        gSMref=float('%0.3g'%h.GetBinCenter(binLim))
        print "mR, gSMref, mu_obs =", mR, gSMref, mu

        limit=limits.get(gSMref,{}).get(mR,{})
        #if mR==0.55:
        #    print('GOD',gSMref,mR,limit)
        xsref=limit['theory']
        xsobs=limit['obs']
        xsexp=limit['exp']
        gSMobs=0
        gSMexp=0
        if xsobs==None:
            continue
        else:
            gSMobs=sqrt(gSMref**2*xsobs/(xsref*bjetscaling))
            gSMexp=sqrt(gSMref**2*xsexp/(xsref*bjetscaling))

        print 'xsref, xsobs, xsexp, gSMobs, gSMexp', xsref, xsobs, xsexp, gSMobs, gSMexp

        gobs.SetPoint(gidx,mR*1000,gSMobs)
        gexp.SetPoint(gidx,mR*1000,gSMexp)
        gidx+=1            
        
    print "observed:"
    gobs.Print("all")
    print "expected:"
    gexp.Print("all")


    #############################################################z
    # Now to make individual plots - this is not very necessary #
    #############################################################

    FIATLAS=ROOT.TColor.CreateGradientColorTable(2,
                                                 np.array([0.00, 1.00],'d'),
                                                 np.array([1.00, 1.00],'d'),
                                                 np.array([1.00, 1.00],'d'),
                                                 np.array([1.00, 1.00],'d'),
                                                 255
                                                 )    

    ATPallete=array.array('i',range(FIATLAS,FIATLAS+255))

    ROOT.gStyle.SetPalette(255,ATPallete)
    
    c1=canvastools.canvas()
    c1.Clear()

    l=ROOT.TLegend(0.7,0.8,0.935,0.93)
    l.SetTextFont(42)

    # h2.GetXaxis().SetRangeUser(200,3500)
    plottools.plot2d(h2,text={'title':selection,'lumi':lumi,'sim':False},textpos=(0.19,0.8),zrange=(0,2),ztitle='#sigma_{obs. limit}/#sigma_{theory}')
    
    lineWidth = 100*5 + 5

    gobs.Draw('C SAME')#all was C
    gobs.SetLineWidth(2)
    gobs.SetFillStyle(3375)

    colname = 'TLA'
    if 'TLA' in selection or 'tla' in selection:
        colname = 'TLA'
    elif 'ISR' in selection or 'isr' in selection:
        if 'gamma' in selection:
            colname = 'jjg'
        if '(j' in selection:
            colname = 'jjj'
    elif 'Dijet' in selection or 'dijet' in selection:
        colname = 'dijet'

    col = cols[colname]

    gobs.SetLineColor(col)
    gobs.SetFillColor(col)

    gexp.Draw('C SAME')
    gexp.SetLineWidth(lineWidth)
    gexp.SetFillColor(col)
    gexp.SetLineColor(col)
    gexp.SetFillStyle(3003)
    gexp.SetLineStyle(ROOT.kDashed)

    l.AddEntry(gobs, "observed" ,"L")
    l.AddEntry(gexp, "expected")
    l.Draw()
    l.SetBorderSize(0)

    Tl=ROOT.TLatex()

    
    markers = []
    for (mR,gSM),(xsobs,xsexp) in ratplots.items():
        marker = ROOT.TMarker(mR*1000, gSM, ROOT.kOpenCircle)
        marker.SetMarkerColor(ROOT.kGray+2)
        markers.append(marker)
        markers[-1].Draw()

    utiltools.store.append(gobs)
    utiltools.store.append(gexp)

    ROOT.gPad.RedrawAxis()

    outname = inLimits.replace('data/','').replace('.txt','')
    canvastools.save('intermediate_plots/'+outname+'_oldBuggy.png')

    #############################################################
    # Done with making individual plots, now save to root file  #
    #############################################################
    
    outrootfile = ROOT.TFile('intermediate_rootfiles/'+outname+'_oldBuggy.root','recreate')

    gobs.Write(outname+'_obs')
    gexp.Write(outname+'_exp')

    outrootfile.Close()





if __name__ == '__main__':
    main(sys.argv)
