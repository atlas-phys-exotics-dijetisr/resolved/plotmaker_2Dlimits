#prot "quickLimitsTLA_withb_withISR_andDijets('data/limits_tla_increaseRange.txt','data/limits2016_g130_2j25_95.txt','data/limits_highmass.txt','data/limits2016_3jet_669.txt',selection='',lumi='3.4-9.5')"

import ROOT

from prot import utiltools
from prot import canvastools
from prot import plottools

import glob
import re
import os, os.path
import numpy as np
import array

from math import *

def set_palette(name="palette", ncontours=4):
    
    from array import array
    from ROOT import TColor, gStyle
    
    """Set a color palette from a given RGB list
        stops, red, green and blue should all be lists of the same length
        see set_decent_colors for an example"""
    
    if name == "gray" or name == "grayscale":
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red   = [1.00, 0.84, 0.61, 0.34, 0.00]
        green = [1.00, 0.84, 0.61, 0.34, 0.00]
        blue  = [1.00, 0.84, 0.61, 0.14, 0.00]
    # elif name == "whatever":
    # (define more palettes)
    else:
        # default palette, looks cool
        #jamaica
        # stops = [0.00, 0.20, 0.61, 0.84, 1.00]
        # red   = [0.00, 0.00, 0.87, 1.00, 0.51]
        # green = [0.00, 0.81, 1.00, 0.0, 0.00]
        # blue  = [1.00, 0.20, 0.12, 0.00, 0.00]
        stops = [0.00, 0.20, 0.61, 0.84, 1.00]
        red   = [0.00, 0.00, 0.87, 1.00, 0.51]
        green = [0.00, 0.81, 1.00, 0.20, 0.00]
        blue  = [0.51, 1.00, 0.12, 0.00, 0.00]
    
    s = array('d', stops)
    r = array('d', red)
    g = array('d', green)
    b = array('d', blue)

    npoints = len(s)
    TColor.CreateGradientColorTable(npoints, s, r, g, b, ncontours)
    
    # For older ROOT versions
    #gStyle.CreateGradientColorTable(npoints, s, r, g, b, ncontours)
    gStyle.SetNumberContours(ncontours)

def TextLabelBold(x,y,text="",color=ROOT.kBlack):
    l = ROOT.TLatex()
    #l.SetNDC()
    l.SetTextFont(42)
    l.SetTextSize(0.027)
    l.SetTextColor(color)
    l.DrawLatex(x,y,text)
    #t=ROOT.TPaveText(x+0.015,y-0.005)

def TextLabelNotBold(x,y,text2="",color=ROOT.kBlack):
    l1 = ROOT.TLatex()
    #l1.SetNDC()
    l1.SetTextFont(42)
    l1.SetTextSize(0.023)
    l1.SetTextColor(color)
    print "text2"
    l1.DrawLatex(x,y,text2)

def bestbins(values):
    mindiff=None
    for i in range(len(values)-1):
        diff=values[i+1]-values[i]
        if mindiff==None or diff<mindiff:
            mindiff=diff
    width=mindiff
    minval=min(values)-width/2
    maxval=max(values)+width/2
    nbins=int((maxval-minval)/width)
    return minval,maxval,nbins

def bestbins_dijetISR(values,defaultwidth=50):
    mindiff=None
    for i in range(len(values)-1):
        diff=values[i+1]-values[i]
        if mindiff==None or diff<mindiff:
            mindiff=diff
    width=mindiff if mindiff!=None else defaultwidth
    minval=min(values)-width/2
    maxval=max(values)+width/2
    nbins=int((maxval-minval)/width)
    return minval,maxval,nbins


def getOverlaidCurves(inLimits, below950Only=True, nobjets = False):
    
    # Load the limits
    limits={}
    fh=open(inLimits)
    limits = eval(fh.read())
    fh.close()

    # Determine bins
    gSMs=set()
    mRs=set()
    for gSM,limitsgSM in limits.items():
        gSMs.add(gSM)
        for mR,limit in limitsgSM.items():
            mRs.add(mR)
    gSMs=sorted(list(gSMs))
    mRs =sorted(list(mRs))

    print "gSMs", gSMs
    print "gSMs", mRs

    minmR ,maxmR ,nbinsmR =bestbins_dijetISR(mRs ,defaultwidth=50)
    mingSM,maxgSM,nbinsgSM=bestbins_dijetISR(gSMs,defaultwidth=0.05)
    maxgSM+=0.1
    nbinsgSM+=1

    # Make plot
    ratplots={}
    h2=ROOT.TH2F('excl','Exclusion Plot;m_{R} [GeV];g_{SM}',
                 nbinsmR,minmR*1e3,maxmR*1e3,
                 nbinsgSM,float('%0.3g'%mingSM),float('%0.3g'%maxgSM))

    for gSM,limitsgSM in limits.items():
        for mR,limit in limitsgSM.items():
            xslim_obs=limit.get('obs'   ,None)
            xslim_exp=limit.get('exp'   ,None)
            xstheory =limit.get('theory',None)
            binIdx=h2.FindBin(mR*1000,gSM)
            if xslim_obs==None:
                h2.SetBinContent(binIdx,0)
                continue
            if nobjets : xstheory = xstheory*1.25
            h2.SetBinContent(binIdx,xslim_obs/xstheory)
            ratplots[(mR,gSM)]=(xslim_obs/xstheory,xslim_exp/xstheory)

    # Make graph
    gobs=ROOT.TGraph()
    gexp=ROOT.TGraph()
    gidx=0
    for mR in mRs:
        # Get interesting point
        h=h2.ProjectionY("_py",h2.GetXaxis().FindBin(mR*1000),h2.GetXaxis().FindBin(mR*1000))
        binLim=h.GetNbinsX()
            #was for binIdx in range(h.GetNbinsX(),0,-1):
        for binIdx in range(0, h.GetNbinsX()):

            mu=h.GetBinContent(binIdx)
            print('testing',mR,binIdx,mu)
            if mu==0: continue
            binLim=binIdx
            if mu<1:
                break
        gSMref=float('%0.3g'%h.GetBinCenter(binLim))

        limit=limits.get(gSMref,{}).get(mR,{})
        xsref=limit['theory']
        xsobs=limit['obs']
        xsexp=limit['exp']
        if nobjets : xsref = xsref*1.25

        gSMobs=0
        gSMexp=0
        if xsobs==None:
            continue
        else:
            gSMobs=sqrt(gSMref**2*xsobs/xsref)
            gSMexp=sqrt(gSMref**2*xsexp/xsref)

        print(gSMobs,xsobs,xsref)
        print(gSMexp,xsexp,xsref)
      
        print 'xsref, xsobs, xsexp, gSMobs, gSMexp', xsref, xsobs, xsexp, gSMobs, gSMexp

        #avoid showing points above 950 GeV
        if (below950Only and mR*1000 >= 1000) or mR*1000 >= 3600 :
          continue
        gobs.SetPoint(gidx,mR*1000,gSMobs)
        gexp.SetPoint(gidx,mR*1000,gSMexp)
        gidx+=1

    gobs.Print("all")
    gexp.Print("all")

    utiltools.store.append(gobs)
    utiltools.store.append(gexp)

#    markers = []
#    for (mR,gSM),(xsobs,xsexp) in ratplots.items():
#        if gSM < 0.3 : continue
#        if mR < 0.3 : continue
#        if mR > 0.7 and gSM < 0.35 : continue
#        #print(mR,gSM)
#        #Tl.DrawLatex(mR*1000-25,gSM+0.005,'#color[34]{#scale[0.75]{%0.2f}}'  %(xsobs))
#        #Tl.DrawLatex(mR*1000-25,gSM-0.015,'#color[34]{#scale[0.75]{(%0.2f)}}'%(xsexp))
#        marker = ROOT.TMarker(mR*1000, gSM, ROOT.kStar)
#        marker.SetMarkerColor(ROOT.kRed-9)
#        marker.Draw("same")
#        markers.append(marker)


    return gobs, gexp#, markers


def main(inLimits,inLimits_overlay,inLimits_otheroverlay,inLimits_yetanotheroverlay,selection='dijetgamma_g130_2j25',lumi=None):
    
    ###############
    #####TLA plot
    ###############
    

    # Load the limits
    limits={}
    fh=open(inLimits)
    limits = eval(fh.read())
    fh.close()

    set_palette()
    
    # Determine bins
    gSMs=set()
    mRs=set()
    for gSM,limitsgSM in limits.items():
        gSMs.add(gSM)
        for mR,limit in limitsgSM.items():
            mRs.add(mR)
    gSMs=sorted(list(gSMs))
    mRs =sorted(list(mRs))

    minmR ,maxmR ,nbinsmR =bestbins(mRs)
    mingSM,maxgSM,nbinsgSM=bestbins(gSMs)    

    # Make plot
    ratplots={}
    h2=ROOT.TH2F('excl',"Exclusion Plot;m_{Z'} [GeV];g_{q}",
                 nbinsmR,minmR*1e3,maxmR*1e3,
                 nbinsgSM,float('%0.3g'%mingSM),float('%0.3g'%maxgSM))

    for gSM,limitsgSM in limits.items():
        for mR,limit in limitsgSM.items():
            xslim_obs=limit.get('obs'   ,None)
            xslim_exp=limit.get('exp'   ,None)
            xstheory =limit.get('theory',None)
            binIdx=h2.FindBin(mR*1000,gSM)
            if xslim_obs==None:
                h2.SetBinContent(binIdx,0)
                continue
            h2.SetBinContent(binIdx,xslim_obs/(xstheory*1.25))
            ratplots[(mR,gSM)]=(xslim_obs/(xstheory*1.25),xslim_exp/(xstheory*1.25))

    # Make graph
    gobs3=ROOT.TGraph()
    gexp3=ROOT.TGraph()
    gobs=ROOT.TGraph()
    gexp=ROOT.TGraph()
    gidx=0

    for mR in mRs:
        # Get interesting point, where interesting = first bin where limit is mu < 1
        h=h2.ProjectionY("_py",h2.GetXaxis().FindBin(mR*1000),h2.GetXaxis().FindBin(mR*1000))
        binLim=h.GetNbinsX()
        #for binIdx in range(h.GetNbinsX(),0,-1):
        for binIdx in range(0, h.GetNbinsX()):
            mu=h.GetBinContent(binIdx)
            if mu==0: continue
            binLim=binIdx
            if mu<1:
                break
        gSMref=float('%0.3g'%h.GetBinCenter(binLim))
        print "mR, gSMref", mR, gSMref

        limit=limits.get(gSMref,{}).get(mR,{})
        #if mR==0.55:
        #    print('GOD',gSMref,mR,limit)
        xsref=limit['theory']
        xsobs=limit['obs']
        xsexp=limit['exp']
        gSMobs=0
        gSMexp=0
        print "mR:", mR, xsobs, xsexp, xsref
        if xsobs==None:
            continue
        else:
            gSMobs=sqrt(gSMref**2*xsobs/(xsref*1.25))
            gSMexp=sqrt(gSMref**2*xsexp/(xsref*1.25))

        print(gSMobs,xsobs,(xsref*1.25))
        print(gSMexp,xsexp,(xsref*1.25))
        if mR>0.45 and mR < 1.05:
            gobs.SetPoint(gidx,mR*1000,gSMobs)
            gexp.SetPoint(gidx,mR*1000,gSMexp)
            gidx+=1            
        else:
            #hacked numbers by hand at this point
            #h2.SetBinContent(binIdx,xslim_obs/xstheory)
            #h=h2.ProjectionY("_py",h2.GetXaxis().FindBin(mR*1000),h2.GetXaxis().FindBin(mR*1000))
            #450, y*<0.3
            #final, 0.3, observed
            #'0.3' : { 0.45 : {"excl" :  38.4 , "theo" :  429 },
            #final, 0.3, expected
            #'0.3' : { 0.45 : {"excl" :  43.4 , "theo" :  429 },
            gSMref=0.3
            xsobs=38.4
            xsexp=43.4
            xsref=429.
            gSMobs=sqrt(gSMref**2*xsobs/(xsref*1.25))
            gSMexp=sqrt(gSMref**2*xsexp/(xsref*1.25))
            gobs3.SetPoint(0,450,gSMobs)
            gexp3.SetPoint(0,450,gSMexp)
            #final, 0.3, observed
            #0.55 : {"excl" :  18.1 , "theo" :  203 },
            #final, 0.3, expected
            #0.55 : {"excl" :  18.8 , "theo" :  203 },
            gSMref=0.3
            xsobs=18.1
            xsexp=18.8
            xsref=203.
            gSMobs=sqrt(gSMref**2*xsobs/(xsref*1.25))
            gSMexp=sqrt(gSMref**2*xsexp/(xsref*1.25))
            gobs3.SetPoint(1,550,gSMobs)
            gexp3.SetPoint(1,550,gSMexp)

    #gobs.SetPoint(gidx,mRs[-1]*1000+50,gSMobs)
    #gexp.SetPoint(gidx,mRs[-1]*1000+50,gSMexp)

    # Plot plot
    # FIATLAS=ROOT.TColor.CreateGradientColorTable(5,
    #                                              np.array([0.00, 0.16, 0.39, 0.66, 1.00],'d'),
    #                                              np.array([0.51, 1.00, 0.87, 0.00, 0.00],'d'),
    #                                              np.array([0.00, 0.20, 1.00, 0.81, 0.00],'d'),
    #                                              np.array([0.00, 0.00, 0.12, 1.00, 0.51],'d'),
    #                                              255
    #                                              )    
    FIATLAS=ROOT.TColor.CreateGradientColorTable(2,
                                                 np.array([0.00, 1.00],'d'),
                                                 np.array([1.00, 1.00],'d'),
                                                 np.array([1.00, 1.00],'d'),
                                                 np.array([1.00, 1.00],'d'),
                                                 255
                                                 )    

    ATPallete=array.array('i',range(FIATLAS,FIATLAS+255))

    ROOT.gStyle.SetPalette(255,ATPallete)
    
    c1=canvastools.canvas()
    c1.Clear()

    #l=ROOT.TLegend(0.2,0.3,0.5,0.2)
    #l=ROOT.TLegend(0.3,0.82,0.6,0.7)
    #l=ROOT.TLegend(0.468,0.43,0.82,0.68)
    l=ROOT.TLegend(0.565,0.63,0.935,0.93)
    l.SetTextFont(42)
    #l=ROOT.TLegend(0.4,0.44,0.82,0.6)


    #plottools.plot2d(h2,text={'title':selection,'lumi':lumi},textpos=(0.38,0.8),zrange=(0,2),ztitle='#sigma_{obs. limit}/#sigma_{theory}')
    plottools.plot2d(h2,text={'title':selection,'lumi':lumi,'sim':False},textpos=(0.19,0.8),zrange=(0,2),ztitle='#sigma_{obs. limit}/#sigma_{theory}')
    
    lineWidth = 100*5 + 5

    gobs.Draw('C SAME')#all was C
    gobs.SetLineWidth(2)
    gobs.SetFillStyle(3375)
    gobs.SetLineColor(ROOT.kBlue-4)
    gobs.SetFillColor(ROOT.kBlue-4)

    gexp.Draw('C SAME')
    gexp.SetLineWidth(lineWidth)
    gexp.SetFillColor(ROOT.kBlue-4)
    gexp.SetLineColor(ROOT.kBlue-4)
    gexp.SetFillStyle(3003)
    gexp.SetLineStyle(ROOT.kDashed)

    gobs3.Draw('C SAME')
    gobs3.SetLineWidth(2)
    gobs3.SetFillColor(ROOT.kBlue-4)
    gobs3.SetLineColor(ROOT.kBlue-4)
    gobs3.SetFillStyle(3375)

    gexp3.Draw('C SAME')
    gexp3.SetLineWidth(lineWidth)
    gexp3.SetFillStyle(3003)
    gexp3.SetLineStyle(ROOT.kDashed)
    gexp3.SetLineColor(ROOT.kBlue-4)
    gexp3.SetFillColor(ROOT.kBlue-4)
    gexp3.SetLineStyle(ROOT.kDashed)

    l.Draw()
    l.SetBorderSize(0)

    Tl=ROOT.TLatex()
    markers = []
    for (mR,gSM),(xsobs,xsexp) in ratplots.items():
        if mR < 0.3 : continue
        if gSM > 0.15 : continue
        if mR < 0.6 and gSM > 0.1 : continue
        if mR > 0.8 and gSM > 0.1 : continue
        #if gSM > 0.2 and mR > 0.7 : continue
        #if gSM > 0.2 and mR > 0.7 and mR < 0.6 : continue
        #if gSM > 0.1 and mR < 0.5: continue
        #if gSM < 0.12 and mR > 0.6: continue
        #if gSM > 0.15 and mR > 0.5 and mR < 0.6: continue
        #print(mR,gSM)
        #Tl.DrawLatex(mR*1000-25,gSM+0.005,'#color[34]{#scale[0.75]{%0.2f}}'  %(xsobs))
        #Tl.DrawLatex(mR*1000-25,gSM-0.015,'#color[34]{#scale[0.75]{(%0.2f)}}'%(xsexp))
        marker = ROOT.TMarker(mR*1000, gSM, ROOT.kOpenCircle)
        marker.SetMarkerColor(ROOT.kGray+2)
        #marker.Draw("same")
        markers.append(marker)

    utiltools.store.append(gobs)
    utiltools.store.append(gexp)

    TextLabelBold(350,0.28,"|y*_{12}| < 0.8", ROOT.kRed)
    TextLabelNotBold(280,0.255,"ATLAS-CONF-2016-XXX, 15.4 fb^{-1}", ROOT.kRed)

    TextLabelBold(580,0.17,"|y*_{23}| < 0.6", ROOT.kMagenta-2)
    TextLabelNotBold(580,0.145,"ATLAS-CONF-2016-XXX, 15.4 fb^{-1}", ROOT.kMagenta-2)

    TextLabelBold(400,0.055,"|y*_{12}| < 0.3", ROOT.kBlue-4)
    TextLabelBold(580,0.055,"|y*_{12}| < 0.6", ROOT.kBlue-4)
    TextLabelNotBold(380,0.035,"ATLAS-CONF-2016-030, 3.4 fb^{-1}", ROOT.kBlue-4)

    TextLabelBold(2000,0.095,"|y*_{12}| < 0.6", ROOT.kBlue-2)
    #TextLabelNotBold(1300,0.125,"Phys. Lett. B 754 302-322 (2016)", ROOT.kBlue-2)
    TextLabelNotBold(1500,0.07,"ATLAS-CONF-2016-XXX, 15.7 fb^{-1}", ROOT.kBlue-2)

    #adding the points to the legend if necessary
#    mobs3 = ROOT.TMarker(535, 0.273, ROOT.kOpenCircle)
#    mobs3.SetMarkerStyle(ROOT.kFullCircle)
#    mobs3.SetMarkerColor(ROOT.kRed)
#    mexp3 = ROOT.TMarker(535, 0.253, ROOT.kFullCircle)
#    mexp3.SetMarkerStyle(ROOT.kOpenCircle)
#    mexp3.SetMarkerColor(ROOT.kRed)
#    mobs3.PaintMarkerNDC(0.5,0.5)
#    mexp3.PaintMarkerNDC(0.5,0.5)
#    mobs3.Draw("same")
#    mexp3.Draw("same")
    #adding arrows
#    ar1 = ROOT.TArrow(450,0.035,500,0.035,0.03,"");
#    ar1.Draw("")
#    ar1.SetLineWidth(3)
#    ar1.SetLineColor(ROOT.kBlack)
#    ar2 = ROOT.TArrow(500,0.035,550,0.035,0.03,"");
#    ar2.Draw("")
#    ar2.SetLineWidth(3)
#    ar2.SetLineColor(ROOT.kBlack)

    ###############
    #####ISR plot
    ###############

    gobs_overlay, gexp_overlay = getOverlaidCurves(inLimits_overlay)

    gobs_overlay.Draw('C SAME')
    gobs_overlay.SetLineWidth(2)
    gobs_overlay.SetFillStyle(3375)
    gobs_overlay.SetLineColor(ROOT.kRed)
    gobs_overlay.SetFillColor(ROOT.kRed)
    
    gexp_overlay.Draw('C SAME')
    gexp_overlay.SetLineWidth(lineWidth)#was 2
    gexp_overlay.SetFillStyle(3003)
    gexp_overlay.SetFillColor(ROOT.kRed)
    gexp_overlay.SetLineColor(ROOT.kRed)
    gexp_overlay.SetLineStyle(ROOT.kDashed)


    ###############
    #####High mass plot
    ###############
    print "HIGH MASS STARTS HERE"

    gobs_overlay_2, gexp_overlay_2 = getOverlaidCurves(inLimits_otheroverlay,False,True)
    
    gobs_overlay_2.Draw('C SAME')
    gobs_overlay_2.SetLineWidth(2)
    gobs_overlay_2.SetFillStyle(3395)
    gobs_overlay_2.SetLineColor(ROOT.kBlue-2)
    gobs_overlay_2.SetFillColor(ROOT.kBlue-2)
    
    gexp_overlay_2.Draw('C SAME')
    gexp_overlay_2.SetLineWidth(lineWidth)#was 2
    gexp_overlay_2.SetFillStyle(3003)
    gexp_overlay_2.SetFillColor(ROOT.kBlue-2)
    gexp_overlay_2.SetLineColor(ROOT.kBlue-2)
    gexp_overlay_2.SetLineStyle(ROOT.kDashed)


    ###############
    #####Three jets plot
    ###############
    print "THREE JETS STARTS HERE"

    gobs_overlay_3, gexp_overlay_3 = getOverlaidCurves(inLimits_yetanotheroverlay)
 
    print "PRINTING THREE JETS STARTS HERE"

    gobs_overlay_3.Print("all")
    gexp_overlay_3.Print("all")
    
    gobs_overlay_3.Draw('C SAME')
    gobs_overlay_3.SetLineWidth(2)
    gobs_overlay_3.SetFillStyle(3395)
    gobs_overlay_3.SetLineColor(ROOT.kMagenta-2)
    gobs_overlay_3.SetFillColor(ROOT.kMagenta-2)
    
    gexp_overlay_3.Draw('C SAME')
    gexp_overlay_3.SetLineWidth(lineWidth)#was 2
    gexp_overlay_3.SetFillStyle(3003)
    gexp_overlay_3.SetFillColor(ROOT.kMagenta-2)
    gexp_overlay_3.SetLineColor(ROOT.kMagenta-2)
    gexp_overlay_3.SetLineStyle(ROOT.kDashed)

    l.AddEntry(gobs,'obs. limit, Low-mass dijet (TLA)','L')
    #UNCOMMENT IF LINES
    #l.AddEntry(gexp,'(#sigma_{exp. limit} / #sigma_{theory})','L')
    l.AddEntry(gexp,'exp. limit, Low-mass dijet (TLA)','FL')
    l.AddEntry(gobs_overlay_2,'obs. limit, High-mass dijet','L')
    l.AddEntry(gexp_overlay_2,'exp. limit, High-mass dijet','FL')
    l.AddEntry(gobs_overlay,'obs. limit, #gamma + X ','L')
    l.AddEntry(gexp_overlay,'exp. limit, #gamma + X','FL')
    l.AddEntry(gobs_overlay_3,'obs. limit, jet + X','L')
    l.AddEntry(gexp_overlay_3,'exp. limit, jet + X','FL')


    #line=ROOT.TLine(500,0.025TLe,500,0.325)
    line=ROOT.TLine(550,0.05,550,0.14)
    line.SetLineWidth(1)
    line.SetLineColor(ROOT.kBlack)
    line.Draw("same")

    ROOT.gPad.RedrawAxis()

    #h2_overlay.Draw("COLZ SAME")

#            gidx+=1
#        else:
#            #hacked numbers by hand at this point
#            #h2.SetBinContent(binIdx,xslim_obs/xstheory)
#            #h=h2.ProjectionY("_py",h2.GetXaxis().FindBin(mR*1000),h2.GetXaxis().FindBin(mR*1000))
#            #450, y*<0.3
#            #final, 0.3, observed
#            #'0.3' : { 0.45 : {"excl" :  38.4 , "theo" :  429 },
#            #final, 0.3, expected
#            #'0.3' : { 0.45 : {"excl" :  43.4 , "theo" :  429 },
#            gSMref=0.3
#            xsobs=38.4
#            xsexp=43.4
#            xsref=429.
#            gSMobs=sqrt(gSMref**2*xsobs/(xsref*1.25))
#            gSMexp=sqrt(gSMref**2*xsexp/(xsref*1.25))
#            gobs3.SetPoint(0,450,gSMobs)
#            gexp3.SetPoint(0,450,gSMexp)
#            #final, 0.3, observed
#            #0.55 : {"excl" :  18.1 , "theo" :  203 },
#            #final, 0.3, expected
#            #0.55 : {"excl" :  18.8 , "theo" :  203 },
#            gSMref=0.3
#            xsobs=18.1
#            xsexp=18.8
#            xsref=203.
#            gSMobs=sqrt(gSMref**2*xsobs/(xsref*1.25))
#            gSMexp=sqrt(gSMref**2*xsexp/(xsref*1.25))
#            gobs3.SetPoint(1,550,gSMobs)
#            gexp3.SetPoint(1,550,gSMexp)
#
#
    canvastools.save('test.png')

    
