
def main():
    epsFile = 'figaux_10_mod2.eps'
    limitsDict = getPointsFromEps(epsFile)
        
    import pprint
    pprint.pprint(limitsDict)
    

def getPointsFromEps(epsFile):
    epsFile = 'figaux_10_mod2.eps'

    limitsDict = {}

    for line in open(epsFile).readlines():
        if 'limit' not in line:
            continue

        name = line.split('% ')[1].split(' limit')[0]
        print name

        " 0 0 1 c 983 546 m 41 15 d 59 27 d 51 12 d 46 7 d 35 4 d s % CMS 4fb limit"

        protocoords = []
        firstcoord = line.split(' c ')[1].split(' m ')[0]
        protocoords.append(firstcoord)
        coordsline = line.split(' d s')[0].split(' m ')[1]
        protocoords += coordsline.split(' d ')
        # print protocoords

        # deal with shorthand of "d 12 X 16 24 d" in place of "d 12 0 d 16 24"
        # (didn't do find a replace in case it can appear in the first one)
        coords = []
        for coord in protocoords:
            if 'X' in coord:
                coord0 = coord.split(' X ')[0] + ' 0'
                coords.append(coord0)
                coord1 = coord.split(' X ')[1]
                coords.append(coord1)
            elif 'Y' in coord:
                coord0 = '0 ' + coord.split(' Y ')[0]
                coords.append(coord0)
                coord1 = coord.split(' Y ')[1]
                coords.append(coord1)
            else:
                coords.append(coord)


        points = []
        # need to keep track of cumulative X and Y
        cum_x = 0
        cum_y = 0
        for coord in coords:

            thisX = cum_x + int(coord.split()[0])
            thisY = cum_y + int(coord.split()[1])
            cum_x = thisX
            cum_y = thisY

            # (419,292) -> (100,0.4)
            # 1694 across = 2700
            # 1155 up = 2.2

            mZ = int(100 + (thisX-419) * (2700/1694.))
            gB = 0.4 + (thisY-292) * (2.2/1155.)
            gq = gB / 6.
            
            points.append((mZ,gq))

        # print coords
        # print points

        limitsDict[name] = points
    return limitsDict

if __name__ == '__main__':
    main()
