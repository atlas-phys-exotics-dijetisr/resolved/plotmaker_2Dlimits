eps from https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/EXOT-2013-11/ fig 10

EPS decoding

For each line, the points are relative (ie change the first one and they all move)

Start at bottom left
Coord 1: go right
Coord 2: go up

R G B c black for solid line
% 0 0 1 c[  12 12 ] 0 sd black % [ 12 12 ] makes stuff dashed; reset with c[  ]

R G B c <- line colour (maybe needs to be defined twice, with header line above)
start-coord m intermediate-coord d intermediate-coord d s


Image is how big?
(0,0.3) is at (363, 253)
- except it's not quite (0,0.3)
- safer is (100, 0.4)

(100,0.4) is (363+56,253+39) = (419,292)
100 -> 2800 is 1694
0.4 -> 2.6 is 1155

-> I have all I need :-)

getPointsFromEps.py will extract coordinates from this eps file

1fb-1 7 TeV https://arxiv.org/pdf/1108.6311.pdf
13 fb-1 8 TeV CONF https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2012-148/
20.3 fb-1 8 TeV paper https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/EXOT-2013-11/
