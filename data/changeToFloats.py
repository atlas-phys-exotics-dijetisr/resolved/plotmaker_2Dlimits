
from glob import glob
import json

limitfiles = sorted(glob("limits*txt"))

for limitfile in limitfiles:
    print limitfile

    limitf = open(limitfile)
    limitstring = ''.join(limitf.readlines())
    limitf.close()

    limitdict = eval(limitstring)

    # print limitdict

    isOK = False
    for gSM in limitdict:
        if type(gSM) == float:
            isOK = True
        break

    if isOK:
        print "all OK"
        continue
        
    else:
        print "converting"

    newlimitdict = {}    
    for gSM in limitdict:
        gSM_f = float(gSM)
        if gSM_f not in newlimitdict:
            newlimitdict[gSM_f] = {}
        for mR in limitdict[gSM]:
            newlimitdict[gSM_f][float(mR)] = limitdict[gSM][mR]

    print newlimitdict


    # write to file
    # json doesn't work since keys have to be strings

    # limitf = open(limitfile.replace('.txt','_test.py'),'w')
    limitf = open(limitfile,'w')
    limitf.write('{\n')
    for gSM in sorted(newlimitdict):
        limitf.write('    '+str(gSM)+': {\n')
        for mR in sorted(newlimitdict[gSM]):
            limitf.write('        '+str(mR)+': {\n')
            limitf.write('            '+',\n           '.join(str(newlimitdict[gSM][mR])[1:-1].split(',')) + '\n')
            limitf.write('        },\n')
        limitf.write('    },\n')
    limitf.write('}\n')
    limitf.close()
