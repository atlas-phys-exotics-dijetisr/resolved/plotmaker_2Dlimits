
import ROOT
from ROOT import TGraph
from array import array


def readDataFromFile(filename):
    lines = open(filename).readlines()
    xArr = []
    yArr = []
  
    for line in lines:
        try:
            x = float(line.split(',')[0])
            y = float(line.split(',')[1])
        except:
            continue

        xArr.append(x)
        yArr.append(y / 6000.) # rescale to gq
    
    return xArr, yArr


def getTGraphFromFile(filename):
    xArr,yArr=readDataFromFile(filename)
    tmpX=array("d",xArr)
    tmpY=array("d",yArr)
    graph =TGraph(len(tmpX),tmpX,tmpY)
    graph.SetName(filename)
    return graph

def getListFromFile(filename):
    xArr,yArr=readDataFromFile(filename)
    limitList = []
    for i in range(len(xArr)):
        limitList.append((xArr[i],yArr[i]))
    return limitList
