
These .csv files are originally from Oliver Endner, via Antonio Boveia

getData.py turns these into TGraphs (and rescales to gq from 1000*gB, gq = gB/6), which can be plotted with plotLimits.py

UA2 = UA2 10.9 pb-1 0.63 TeV 1993 [ref]
cdf2 = CDF 106 pb-1 1.8 TeV 1997 []
cdf1 = CDF 1.13 fb-1 1.96 TeV 2009 []
cms4 = CMS 0.13 fb-1 8 TeV
cms2 = CMS 4 fb-1 8 TeV
atlas1 = atlas 1 fb-1 8 TeV
cms1 = CMS 20 fb-1 8 TeV
cms3 = CMS 5 fb-1 8 TeV