
from getData import getTGraphFromFile, getListFromFile
from glob import glob

import ROOT
ROOT.gROOT.SetBatch(1)

graphs = sorted(glob("*.csv"))

name_map = {
    'ua2': 'UA2 10.9 pb-1 0.63 TeV 1993',
    'cdf2': 'CDF 106 pb-1 1.8 TeV 1997',
    'cdf1': 'CDF 1.13 fb-1 1.96 TeV 2009',
    'cms4': 'CMS 0.13 fb-1 8 TeV',
    'cms2': 'CMS 4 fb-1 8 TeV',
    'atlas1': 'atlas 1 fb-1 8 TeV',
    'cms1': 'CMS 20 fb-1 8 TeV',
    'cms3': 'CMS 5 fb-1 8 TeV',
    }

canv = ROOT.TCanvas()

# canv.SetLogy()
# canv.SetLogx()
minX = 0
maxX = 3000
minY = 0.3 / 6.
maxY = 2.7 / 6.

hist = ROOT.TH1F("hist", ";m_{Z'};g_{q}", 100, minX, maxX)
hist.SetMinimum(minY)
hist.SetMaximum(maxY)
hist.SetStats(0)
hist.Draw()

leg = ROOT.TLegend(0.6,0.6,0.9,0.9)
leg.SetFillStyle(4000)

cols = [ROOT.kBlack, ROOT.kBlue+2, ROOT.kBlue, ROOT.kRed, ROOT.kRed+2, ROOT.kOrange, ROOT.kOrange+9, ROOT.kGreen+2]

i = 0
tgraphs = []
limitsDict = {}
for graph in graphs:
    print graph
    name = graph.split(".csv")[0]
    tgraphs.append(getTGraphFromFile(graph))

    tgraphs[-1].SetLineColor(cols[i])
    tgraphs[-1].SetLineWidth(2)
    tgraphs[-1].Draw("same")
    # tgraphs[-1].Draw()
    leg.AddEntry(tgraphs[-1], name, "L")
    i += 1

    limitsDict[name_map[name]] = getListFromFile(graph)
    
leg.Draw()

canv.SaveAs('allGraphs.png')

import pprint
pprint.pprint(limitsDict)
