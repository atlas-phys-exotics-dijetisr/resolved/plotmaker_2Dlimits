#!/usr/bin/env python

import ROOT
import os,sys
#import pprint
from art.morisot import Morisot
from array import array
from pprint import pprint
from decimal import Decimal
import SignalDictionaries
import MorphedDictionaries
from ROOT import TFile

#========= these are constant. don't change them======

SignalTitles = {  "ZPrime0p05": "Z' (0.05)"
                , "ZPrime0p10": "Z' (0.10)"
                , "ZPrime0p15": "Z' (0.15)"
                , "ZPrime0p20": "Z' (0.20)"
                , "ZPrime0p30": "Z' (0.30)"
                , "ZPrime0p40": "Z' (0.40)"
                , "ZPrime0p50": "Z' (0.50)"
                , "ZPrime0p60": "Z' (0.60)"
                }
SignalCouplings ={"ZPrime0p05": "0.05"
                , "ZPrime0p10": "0.10"
                , "ZPrime0p15": "0.15"
                , "ZPrime0p20": "0.20"
                , "ZPrime0p30": "0.30"
                , "ZPrime0p40": "0.40"
                , "ZPrime0p50": "0.50"
                , "ZPrime0p60": "0.60"
                }

#SignalTitles = {"ZPrime0p30": "Z' (0.30)"}
SignalCouplings ={"ZPrime0p30": "0.30", "ZPrime0p10": "0.10", "ZPrime0p20": "0.20", "ZPrime0p40": "0.40"}

def rreplace(s, old, new, occurrence):
  li = s.rsplit(old, occurrence)
  return new.join(li)

def makeBand(graph1, graph2):
  points = []
  for i in range(graph1.GetN()):
    points += [(i,graph1.GetX()[i],graph1.GetY()[i])]
  for i in range(graph2.GetN()-1,-1,-1):
    points += [(i,graph2.GetX()[i],graph2.GetY()[i])]
  graph_band = ROOT.TGraph();
  for i in range (len(points)): graph_band.SetPoint(i,points[i][1],points[i][2])
  return graph_band

def GetCenterAndSigmaDeviations(inputs) :
  inputs = sorted(inputs)

  statVals = []
  quantiles = [0.02275,0.1587,0.5,0.8413,0.9772]
  for q in quantiles:
    wantEvents = len(inputs)*q
    statVals.append(inputs[int(wantEvents)])
  return statVals

def getModelLimits(phCut, name, signal, these_massvals,individualLimitFiles, sig_dict, limitsDictOut, \
  theoryLineRootFile="", limitHistogram="", theoryRootcutstring = '', xname = "M_{Z'} [GeV]", yname = "#sigma #times #it{A} #times #epsilon #times BR [pb]" , makePlot = True):
  print signal

  # Initialize painter
  myPainter = Morisot()
  myPainter.setColourPalette("ATLAS")
  myPainter.cutstring = cutstring
  myPainter.setEPS(True)
  myPainter.setLabelType(2) # Sets label type i.e. Internal, Work in progress etc.
                            # See below for label explanation
  # 0 Just ATLAS
  # 1 "Preliminary"
  # 2 "Internal"
  # 3 "Simulation Preliminary"
  # 4 "Simulation Internal"
  # 5 "Simulation"
  # 6 "Work in Progress"

  thisobserved = ROOT.TGraph()
  thisexpected = ROOT.TGraph()
  thisexpected_plus1  = ROOT.TGraph()
  thisexpected_minus1 = ROOT.TGraph()
  thisexpected_plus2  = ROOT.TGraph()
  thisexpected_minus2 = ROOT.TGraph()
  thistheory = ROOT.TGraph()
  print("htese massval: ", these_massvals)

  import glob
  template=individualLimitFiles
  file_list_allmass = glob.glob(template.format(signal,"*"))
  if len(file_list_allmass)==0:
      print "no files for this signal and ph cut"
      return
  for mass in these_massvals:

    file_list = glob.glob(individualLimitFiles.format(signal,mass))
    print ("file searching format: ", individualLimitFiles.format(signal,mass))
    if len(file_list) == 0:
        print("no files found")
        continue
    allCLs = []
    PE_CLs = []
    for f in file_list:
      file = ROOT.TFile.Open(f)
      if not file or not file.Get("CLOfRealLikelihood"): continue
      CL = file.Get("CLOfRealLikelihood")[0]
      PE_tree = file.Get("ensemble_test")

      if not PE_tree or not CL:
          print("no pe")
          continue

      for event in PE_tree:
          PE_CLs.append( event.GetBranch("95quantile_marginalized_0").GetListOfLeaves().At(0).GetValue() )
      allCLs.append(CL)
    if len(allCLs) == 0:
        print("allCLs has a size of 0")
        continue
    expCLs = GetCenterAndSigmaDeviations(PE_CLs)
    print mass, allCLs[0], expCLs[2], len(PE_CLs)
    m = float(mass)/1000
    obsCL = allCLs[0]/luminosity
    print ("obsCL: ", obsCL)
    expCLs = [e/luminosity for e in expCLs]
    thisobserved.SetPoint(thisobserved.GetN(),m,obsCL)
    thisexpected_minus2.SetPoint(thisexpected_minus2.GetN(),m,expCLs[0])
    thisexpected_minus1.SetPoint(thisexpected_minus1.GetN(),m,expCLs[1])
    thisexpected.SetPoint(       thisexpected.GetN(),m,expCLs[2])
    thisexpected_plus1.SetPoint( thisexpected_plus1.GetN(),m,expCLs[3])
    thisexpected_plus2.SetPoint( thisexpected_plus2.GetN(),m,expCLs[4])

    print("signal: ", signal)
    c = SignalCouplings[signal]
    print("signal coupling: ", SignalCouplings[signal])
    print "signaldictionary", sig_dict[c]
    print("mass: %1.2f"%m)
    # y hack
    signal_info={}
    #signal_info        = sig_dict[c]['%1.2f'%m]
    #signal_acc         = signal_info['acc']
    #signal_thxsec      = signal_info['theory']
    signal_info['exp'] = expCLs[2]
    signal_info['obs'] = obsCL
    signal_info['exp+1'] = expCLs[3]
    signal_info['exp+2'] = expCLs[4]
    signal_info['exp-1'] = expCLs[1]
    signal_info['exp-2'] = expCLs[0]
    signal_info['nPEs'] = len(PE_CLs)

    print("limitsDictOut: ", limitsDictOut)
    if c not in limitsDictOut: limitsDictOut[c] = {}
    limitsDictOut[c]['%1.2f'%m] = signal_info
    print(limitsDictOut[c])
    #thistheory.SetPoint(thistheory.GetN(),m,signal_acc*signal_thxsec)

    #if not c in ZPrimeLimits: ZPrimeLimits[c] = {}
    #ZPrimeLimits[c][m] = {'obs':obsCL,'exp':expCLs[2],'th':signal_acc*signal_thxsec}

  if thisobserved.GetN() == 0:
    print "No limits found for %s"%signal
    return limitsDictOut

  thisexpected1 = makeBand(thisexpected_minus1,thisexpected_plus1)
  thisexpected2 = makeBand(thisexpected_minus2,thisexpected_plus2)
  outputName = folderextension+"Limits_"+name+"_Ph"+phCut+"_"+signal+'_'+dataset+plotextension
  print("this expected:")
  thisexpected1.Print()


  xlow  = 'automatic'# (int(masses[signal][0]) - 100)/1000.
  xhigh = 'automatic'#(int(masses[signal][-1]) + 100)/1000.
  #placeholder=ROOT.TGraph()

  #theoryRoot= TFile.Open("/lustre/SCRATCH/atlas/ywng/WorkSpace/r21/r21SwiftCat/BayesianFramework/source/Bayesian/inputs/theoryLine/Limit_Setting_single.root")
  print("theoryRootFile: ", theoryLineRootFile)
  if theoryLineRootFile is not "":
      theoryRoot= TFile.Open(theoryLineRootFile)
  else:
      print("missing theory root name in function getModelLimits")
      os._exit(1)

  #placeholder=theoryRoot.Get("Limit_Setting_Sp2_Ph100")
  if limitHistogram is not "":
      placeholder=theoryRoot.Get(limitHistogram)
  else:
      print("missing theory histogram name in function getModelLimits")
      os._exit(1)


  print("thoery RootFile: ", theoryLineRootFile)
  print("theory histogram: ", limitHistogram)
  try:
      xList=placeholder.GetX()
      print("xList: ", list(xList))
      yList=placeholder.GetY()
      print("yList: ", list(yList))
      print("limitsDictOut: ", limitsDictOut)
      for i in range(placeholder.GetN()):
          x=xList[i]/1000
          y=yList[i]
          try:

              limitsDictOut[c]['%1.2f'%x]["theory"] = y
          except Exception as e:
              print("error, limt might be missing certain points")
              print("error:", e)
              continue

          placeholder.SetPoint(i, x, y)
      #placeholder.SetPoint(0,10,1e6)
      #placeholder.SetPoint(1,1000, 1e6)
      placeholder.Print()

      if makePlot:
        ROOT.gROOT.SetBatch(True)
        print("thisobserved: ", thisobserved)
        yMean=thisobserved.GetMean(2)
        yMax=yMean*15
        yMin=yMean/10
        myPainter.drawLimitSettingPlotObservedExpected(thisobserved,thisexpected,thisexpected1, thisexpected2, [placeholder],SignalTitles[signal],\
         outputName, xname,yname,luminosity,Ecm,xlow,xhigh,yMin,yMax,False)

        thisobserved.Print()
  except Exception as e:
      print("error:", e)
      print("theory histogram may be missing")
      return
    #Y: the name needs to be changed everytime you make a new plot
    #myPainter.drawSeveralObservedLimits([thisobserved], ["zprime0p3"], "limitsetting-inclusive-newSignal", "mass[TeV]", "xs x A x E", 79800, 13, 0.2, 1.2,'automatic', 'automatic')
    #ROOT.gROOT.SetBatch(True)
    # stay away from using this
    #myPainter.drawSeveralObservedAndExpected([thisobserved], [thisexpected1], [thisexpected2],["zprime0p3"],"limitSetting-inclusive100PE", "mass[TeV]", "xs x A x E", 79800, 13, 0.2, 1.2, 0.001, 2.0)
  print("returning limitsDictOut: ", limitsDictOut)
  return limitsDictOut


def printPEs(limitsDict):
  for c,massdict in limitsDict.iteritems():
    print "Coupling = ", c
    mlist = []
    for m, siginfo in massdict.iteritems(): mlist.append( (m, siginfo['nPEs']))
    mlist = sorted(mlist, key=lambda e: e[0])
    print [m for m in mlist]
    print ["%g"%(float(m[0])*1000) for m in mlist if m[1] < 500]


def writeLimitsDict(folderextension, dataset,limitsDictOut):
  limitsDictFileName = folderextension+'LimitsDict_'+dataset+'.py'
  print 'Writing signal limit info to', limitsDictFileName
  limitsDictFile=open(limitsDictFileName, 'w')
  import pprint
  print("got here2")
  pp = pprint.PrettyPrinter(indent=4,stream=limitsDictFile)
  print("lumisDictFile: ", limitsDictFile)
  print("got here 3")
  pp.pprint(limitsDictOut)
  limitsDictFile.close()

if __name__ == "__main__":
  #====================================================================================
  # User Options
  #====================================================================================


  # Options
  folderextension = "./plots/"
  plotextension = ""
  # make plots folder i.e. make folder extension
  if not os.path.exists(folderextension):
      os.makedirs(folderextension)
#===============================================================================================
# single 2 btagged
  # Define necessary quantities.
  #Ecm = 13
  #indir = "/lustre/SCRATCH/atlas/ywng/WorkSpace/r21/r21SwiftCat/BayesianFramework/source/Bayesian/"
  ##signalInputFileTypes = ["ZPrime0p05","ZPrime0p10","ZPrime0p20","ZPrime0p30","ZPrime0p40"]
  #signalInputFileTypes=["ZPrime0p30", "ZPrime0p20"]

  ##signalInputFileTypes += ["ZPrime0p15"]

  ##''' Yvonne : make a dictionary for the different spectra

  #limitsDictOut = {}
  #(dataset, luminosity, cutstring, sig_dict) = ("photon_single_2btagged", 76.6*1000, "photon single 2btagged",MorphedDictionaries.J7503_Dict)
  ##masses_Zprime = ["450","500","550","600","650","700","725","750","800","850","900","950","1000","1050","1100","1150","1200","1250","1300","1350","1400","1450","1500","1600","1700","1800"]
  #masses_Zprime=["400", "500", "750"]
  #individualLimitFiles= indir+"results/testPhotonSingle2Btagged/Step2_setLimitsOneMassPoint_{0}_mZ{1}*.root"


  ## Loop over signals in signalInputFileTypes
  #for signal in signalInputFileTypes :
  #  limitsDictOut = getModelLimits(signal, masses_Zprime,individualLimitFiles, sig_dict, limitsDictOut, cutstring, makePlot = True)

  #writeLimitsDict(folderextension, dataset,limitsDictOut)
  #====================================================================================
# single inclusive
#  Ecm = 13
#  indir = "/lustre/SCRATCH/atlas/ywng/WorkSpace/r21/r21SwiftCat/BayesianFramework/source/Bayesian/"
#  #signalInputFileTypes = ["ZPrime0p05","ZPrime0p10","ZPrime0p20","ZPrime0p30","ZPrime0p40"]
#  signalInputFileTypes=["ZPrime0p30"]
#
#  #''' Yvonne : make a dictionary for the different spectra
#
#  limitsDictOut = {}
#  (dataset, luminosity, cutstring, sig_dict) = ("photon_single_inclusive", 79.8*1000, "photon single inclusive",MorphedDictionaries.J7503_Dict)
#  #masses_Zprime = ["450","500","550","600","650","700","725","750","800","850","900","950","1000","1050","1100","1150","1200","1250","1300","1350","1400","1450","1500","1600","1700","1800"]
#  #masses_Zprime=["400", "500", "750"]
## official
#  masses_Zprime=["200", "250", "350", "400", "450", "550", "750", "950"]
#  #masses_Zprime=["750"]
## test
#  #masses_Zprime=["200", "750"]
#  #individualLimitFiles= indir+"results/DictionaryPhotonInclusiveBtagged-withPE/PE/Step2_setLimitsOneMassPoint_{0}_mZ{1}*.root"
#  #individualLimitFiles= indir+"results/DictionaryPhotonSingleInclusive-withPE100/Step2_setLimitsOneMassPoint_{0}_mZ{1}*.root"
#  individualLimitFiles= indir+"results/DictionaryPhotonSingleInclusive-noPE-newSignal/Step2_setLimitsOneMassPoint_{0}_mZ{1}*.root"
#
#  # Loop over signals in signalInputFileTypes
#  for signal in signalInputFileTypes :
#    #limitsDictOut = getModelLimits(signal, masses_Zprime,individualLimitFiles, sig_dict, limitsDictOut, cutstring, makePlot = True)
#
#    limitsDictOut = getModelLimits(phCut,dirName, signal, masses_Zprime,individualLimitFiles, sig_dict, limitsDictOut, theoryLineRootFile="/lustre/SCRATCH/atlas/ywng/WorkSpace/r21/r21SwiftCat/BayesianFramework/source/Bayesian/inputs/theoryLine/Limit_Setting_single.root", limitHistogram=histName, theoryRootcutstring=cutstring,makePlot = True)
#
#
#  writeLimitsDict(folderextension, dataset,limitsDictOut)

#================================================================
# Compound inclusive with new signal
 # Ecm = 13
 # indir = "/lustre/SCRATCH/atlas/ywng/WorkSpace/r21/r21SwiftCat/BayesianFramework/source/Bayesian/"
 # #signalInputFileTypes = ["ZPrime0p05","ZPrime0p10","ZPrime0p20","ZPrime0p30","ZPrime0p40"]
 # signalInputFileTypes=["ZPrime0p20"]
 # #signalInputFileTypes=["ZPrime0p20", "ZPrime0p30", "ZPrime0p10"]

 # #''' Yvonne : make a dictionary for the different spectra

 # limitsDictOut = {}
 # (dataset, luminosity, cutstring, sig_dict) = ("photon_compound_inclusive", 76.6*1000, "Combined trigger, flavour inclusive",MorphedDictionaries.J7503_Dict)
 # #masses_Zprime = ["450","500","550","600","650","700","725","750","800","850","900","950","1000","1050","1100","1150","1200","1250","1300","1350","1400","1450","1500","1600","1700","1800"]kkkkk
 # #masses_Zprime=["400", "500", "750"]
## official
 # #masses_Zprime=["400", "450", "500", "550", "750", "950"]

 # masses_Zprime=[ "350", "400", "450", "500", "550", "750", "950"]

 ##---
 # #dirName="2018-8-9-SpectrumAll-MinPE-SigZP-compoundInclusive-NoSyst"
 # #dirName="2018-9-25-newSig-SpectrumPilot-AllPE-SigZP-singleInclusive-fitFunctionChoice-RunAll"
 # dirName="2018-9-28-newSig-AllPE-SigZP-compoundInclusive-RunAll-Zprime0p20Only"
 # templateOri= indir+"results/"+dirName+"/Step2_setLimitsOneMassPoint_Ph{0}_{1}_mZ{2}*.root"

 # for phCut in ["50"]:
 #     print("phCut: ", phCut)
 #     individualLimitFiles=templateOri
 #     individualLimitFiles=individualLimitFiles.format(phCut, "{0}", "{1}")
 #     print("individualLimitFiles: ", individualLimitFiles)

 #     for signal in signalInputFileTypes :
 #       limitsDictOut = {}
 #       print("limitsDictOut: ", limitsDictOut)
 #       #histNameTemp="Limit_Setting_S%s_Ph100"
 #       histNameTemp="Limit_Setting_S%s_Ph"+phCut
 #       for substring in ["p1", "p2", "p3"]:
 #           if substring in signal:
 #               histName=histNameTemp%substring
 #               print("histName: ", histName)

 #       limitsDictOut = getModelLimits(phCut,dirName, signal, masses_Zprime,individualLimitFiles, sig_dict, limitsDictOut, theoryLineRootFile="/lustre/SCRATCH/atlas/ywng/WorkSpace/r21/r21SwiftCat/BayesianFramework/source/Bayesian/inputs/theoryLine/Limit_Setting_compound.root", limitHistogram=histName, theoryRootcutstring=cutstring,makePlot = True)
 # print(limitsDictOut)
 # writeLimitsDict(folderextension, dataset,limitsDictOut)
 # #print("done!")

##  limitsDictOutTotal={}
##  for phCut in ["100"]:
##      print("phCut: ", phCut)
##      individualLimitFiles=templateOri
##      individualLimitFiles=individualLimitFiles.format(phCut, "{0}", "{1}")
##      print("individualLimitFiles: ", individualLimitFiles)
##
##      for signal in signalInputFileTypes :
##        limitsDictOut = {}
##        print("testsignal: ", signal)
##        print("limitsDictOut: ", limitsDictOut)
##        #histNameTemp="Limit_Setting_S%s_Ph100"
##        histNameTemp="Limit_Setting_S%s_Ph"+phCut
##        for substring in ["p1", "p2", "p3"]:
##            if substring in signal:
##                histName=histNameTemp%substring
##                print("histName: ", histName)
##        #limitsDictOut = getModelLimits(phCut,dirName, signal, masses_Zprime,individualLimitFiles, sig_dict, limitsDictOutTotal, theoryLineRootFile="/lustre/SCRATCH/atlas/ywng/WorkSpace/r21/r21SwiftCat/BayesianFramework/source/Bayesian/inputs/theoryLine/Limit_Setting_compound_btag.root", limitHistogram=histName, theoryRootcutstring=cutstring,makePlot = True)
##
##        limitsDictOut = getModelLimits(phCut,dirName, signal, masses_Zprime,individualLimitFiles, sig_dict, limitsDictOutTotal, theoryLineRootFile="/lustre/SCRATCH/atlas/ywng/WorkSpace/r21/r21SwiftCat/BayesianFramework/source/Bayesian/inputs/theoryLine/Limit_Setting_compound.root", limitHistogram=histName, theoryRootcutstring=cutstring,makePlot = True)
##        print("limitDictOut:", limitsDictOut)
##        if limitsDictOut is not None:
##            limitsDictOutTotal.update(limitsDictOut)
##
##
##
##  #print("got here")
##  print("limitsDictOut",limitsDictOutTotal)
##
##  writeLimitsDict(folderextension, dataset,limitsDictOutTotal)
##


###====================================================================
#
## Compound btagged with new signal
  Ecm = 13
  indir = "/lustre/SCRATCH/atlas/ywng/WorkSpace/r21/r21SwiftCat/BayesianFramework/source/Bayesian/"
  #signalInputFileTypes = ["ZPrime0p05","ZPrime0p10","ZPrime0p20","ZPrime0p30","ZPrime0p40"]
  #signalInputFileTypes=["ZPrime0p20", "ZPrime0p10"]
  signalInputFileTypes=["ZPrime0p20"]

  #''' Yvonne : make a dictionary for the different spectra


  limitsDictOut = {}
  (dataset, luminosity, cutstring, sig_dict) = ("photon_compound_btagged", 76.6*1000, "Combined trigger, 2 b-tags",MorphedDictionaries.J7503_Dict)
  #masses_Zprime = ["450","500","550","600","650","700","725","750","800","850","900","950","1000","1050","1100","1150","1200","1250","1300","1350","1400","1450","1500","1600","1700","1800"]
  #masses_Zprime=["400", "500", "750"]
# official
  #masses_Zprime=["250", "300", "350", "400", "450", "500", "550", "750", "950"]
  #masses_Zprime=["350", "400", "450", "500"]
  #masses_Zprime=["350", "450", "550", "750", "950"]
  #masses_Zprime=["400", "450", "500", "550", "750", "950"]

  masses_Zprime=["250", "300", "350", "400", "450", "500", "550", "750", "950"]
  #individualLimitFiles= indir+"results/DictionaryPhotonCompoundbtagged-withPE100-noSys/Step2_setLimitsOneMassPoint_{0}_mZ{1}*.root"

  # Loop over signals in signalInputFileTypes
  #dirName="2018-8-9-SpectrumAll-MinPE-SigZP-compoundbtagged-NoSyst"
  dirName="2018-9-26-newSig-AllPE-SigZP-compoundbtagged-RunAll-ZPrime0p20"

  templateOri= indir+"results/"+dirName+"/Step2_setLimitsOneMassPoint_Ph{0}_{1}_mZ{2}*.root"

  for phCut in ["50"]:
      print("phCut: ", phCut)
      individualLimitFiles=templateOri
      individualLimitFiles=individualLimitFiles.format(phCut, "{0}", "{1}")
      print("individualLimitFiles: ", individualLimitFiles)

      for signal in signalInputFileTypes :
        limitsDictOut = {}
        print("limitsDictOut: ", limitsDictOut)
        #histNameTemp="Limit_Setting_S%s_Ph100"
        histNameTemp="Limit_Setting_S%s_Ph"+phCut
        for substring in ["p1", "p2", "p3"]:
            if substring in signal:
                histName=histNameTemp%substring
                print("histName: ", histName)

        limitsDictOut = getModelLimits(phCut,dirName, signal, masses_Zprime,individualLimitFiles, sig_dict, limitsDictOut, theoryLineRootFile="/lustre/SCRATCH/atlas/ywng/WorkSpace/r21/r21SwiftCat/BayesianFramework/source/Bayesian/inputs/theoryLine/Limit_Setting_compound_btag.root", limitHistogram=histName, theoryRootcutstring=cutstring,makePlot = True)
  print(limitsDictOut)

  writeLimitsDict(folderextension, dataset,limitsDictOut)

  #writeLimitsDict(folderextension, dataset,limitsDictOut)
#-----------------
  #limitsDictOutTotal={}
  #for phCut in ["100"]:
  #    print("phCut: ", phCut)
  #    individualLimitFiles=templateOri
  #    individualLimitFiles=individualLimitFiles.format(phCut, "{0}", "{1}")
  #    print("individualLimitFiles: ", individualLimitFiles)

  #    for signal in signalInputFileTypes :
  #      limitsDictOut = {}
  #      print("testsignal: ", signal)
  #      print("limitsDictOut: ", limitsDictOut)
  #      #histNameTemp="Limit_Setting_S%s_Ph100"
  #      histNameTemp="Limit_Setting_S%s_Ph"+phCut
  #      for substring in ["p1", "p2", "p3"]:
  #          if substring in signal:
  #              histName=histNameTemp%substring
  #              print("histName: ", histName)
  #      limitsDictOut = getModelLimits(phCut,dirName, signal, masses_Zprime,individualLimitFiles, sig_dict, limitsDictOutTotal, theoryLineRootFile="/lustre/SCRATCH/atlas/ywng/WorkSpace/r21/r21SwiftCat/BayesianFramework/source/Bayesian/inputs/theoryLine/Limit_Setting_compound_btag.root", limitHistogram=histName, theoryRootcutstring=cutstring,makePlot = True)

  #      print("limitDictOut:", limitsDictOut)
  #      if limitsDictOut is not None:
  #          limitsDictOutTotal.update(limitsDictOut)



  ##print("got here")
  #print("limitsDictOut",limitsDictOutTotal)

  #writeLimitsDict(folderextension, dataset,limitsDictOutTotal)
##=#====================================================================
#
#### Single inclusive with new signal
  Ecm = 13
  indir = "/lustre/SCRATCH/atlas/ywng/WorkSpace/r21/r21SwiftCat/BayesianFramework/source/Bayesian/"
  #signalInputFileTypes = ["ZPrime0p05","ZPrime0p10","ZPrime0p20","ZPrime0p30","ZPrime0p40"]
  signalInputFileTypes=["ZPrime0p20", "ZPrime0p10", "ZPrime0p30"]
  #signalInputFileTypes=["ZPrime0p20"]

  #''' Yvonne : make a dictionary for the different spectra

  limitsDictOut = {}
  (dataset, luminosity, cutstring, sig_dict) = ("photon_single_inclusive", 79.8*1000, "Single photon trigger, flavour inclusive",MorphedDictionaries.J7503_Dict)
  #masses_Zprime = ["450","500","550","600","650","700","725","750","800","850","900","950","1000","1050","1100","1150","1200","1250","1300","1350","1400","1450","1500","1600","1700","1800"]
  #masses_Zprime=["400", "500", "750"]
# official
  masses_Zprime=["250", "300", "350", "400", "450", "500", "550", "750", "950"]

#---
  #dirName="2018-9-4-newSig-SpectrumPilot-AllPE-SigZP-compoundbtagged-noSyst"
  #dirName="2018-8-29-newSig-SpectrumPilot-MinPE-SigZP-singleInclusive-All"
  #dirName="2018-9-25-newSig-SpectrumPilot-AllPE-SigZP-singleInclusive-fitFunctionChoice-RunAll"
  dirName="2018-9-25-newSig-SpectrumPilot-AllPE-SigZP-singleInclusive-fitFunctionChoice-RunAll"

  templateOri= indir+"results/"+dirName+"/Step2_setLimitsOneMassPoint_Ph{0}_{1}_mZ{2}*.root"

  limitsDictOutTotal={}
  for phCut in ["100"]:
      print("phCut: ", phCut)
      individualLimitFiles=templateOri
      individualLimitFiles=individualLimitFiles.format(phCut, "{0}", "{1}")
      print("individualLimitFiles: ", individualLimitFiles)

      for signal in signalInputFileTypes :
        limitsDictOut = {}
        print("testsignal: ", signal)
        print("limitsDictOut: ", limitsDictOut)
        #histNameTemp="Limit_Setting_S%s_Ph100"
        histNameTemp="Limit_Setting_S%s_Ph"+phCut
        for substring in ["p1", "p2", "p3"]:
            if substring in signal:
                histName=histNameTemp%substring
                print("histName: ", histName)
        limitsDictOut = getModelLimits(phCut,dirName, signal, masses_Zprime,individualLimitFiles, sig_dict, limitsDictOutTotal, theoryLineRootFile="/lustre/SCRATCH/atlas/ywng/WorkSpace/r21/r21SwiftCat/BayesianFramework/source/Bayesian/inputs/theoryLine/Limit_Setting_single.root", limitHistogram=histName, theoryRootcutstring=cutstring,makePlot = True)
        print("limitDictOut:", limitsDictOut)
        if limitsDictOut is not None:
            limitsDictOutTotal.update(limitsDictOut)



  #print("got here")
  print("limitsDictOut single Incluive",limitsDictOutTotal)

  writeLimitsDict(folderextension, dataset,limitsDictOutTotal)
#=####=========================================================================

# ###Single btagged with new signal
  #Ecm = 13
  #indir = "/lustre/SCRATCH/atlas/ywng/WorkSpace/r21/r21SwiftCat/BayesianFramework/source/Bayesian/"
  ##signalInputFileTypes = ["ZPrime0p05","ZPrime0p10","ZPrime0p20","ZPrime0p30","ZPrime0p40"]
  ##signalInputFileTypes=["ZPrime0p20", "ZPrime0p10", "ZPrime0p30"]
  #signalInputFileTypes=["ZPrime0p20", "ZPrime0p30", "ZPrime0p10"]

  ##''' Yvonne : make a dictionary for the different spectra

  #limitsDictOut = {}
  #(dataset, luminosity, cutstring, sig_dict) = ("photon_single_btagged", 79.8*1000, "Single photon trigger, 2 b-tags",MorphedDictionaries.J7503_Dict)
  ##masses_Zprime = ["450","500","550","600","650","700","725","750","800","850","900","950","1000","1050","1100","1150","1200","1250","1300","1350","1400","1450","1500","1600","1700","1800"]
  ##masses_Zprime=["400", "500", "750"]
# #official
  #masses_Zprime=["250", "300", "350", "400", "450", "500", "550", "750", "950"]

  ##dirName="2018-8-9-SpectrumAll-MinPE-SigZP-singlebtagged-NoSyst"

  ##dirName="2018-8-9-SpectrumAll-MinPE-SigZP-singlebtagged-OneJESSyst-3"
  ##dirName="2018-8-9-SpectrumAll-MinPE-SigZP-singlebtagged-NoSyst"
  #dirName="2018-10-1-newSig-SpectrumPilot-AllPE-SigZP-singlebtagged-RunAll"
  ##dirName="2018-8-29-newSig-SpectrumPilot-MinPE-SigZP-singlebtagged-noSyst"
  #templateOri= indir+"results/"+dirName+"/Step2_setLimitsOneMassPoint_Ph{0}_{1}_mZ{2}*.root"
  #limitsDictOutTotal={}

  #for phCut in ["50", "100"]:
  #    print("phCut: ", phCut)
  #    individualLimitFiles=templateOri
  #    individualLimitFiles=individualLimitFiles.format(phCut, "{0}", "{1}")
  #    print("individualLimitFiles: ", individualLimitFiles)

  #    for signal in signalInputFileTypes :
  #      limitsDictOut = {}
  #      print("limitsDictOut: ", limitsDictOut)
  #      #histNameTemp="Limit_Setting_S%s_Ph100"
  #      histNameTemp="Limit_Setting_S%s_Ph"+phCut
  #      for substring in ["p1", "p2", "p3"]:
  #          if substring in signal:
  #              histName=histNameTemp%substring
  #              print("histName: ", histName)

  #      limitsDictOut = getModelLimits(phCut,dirName, signal, masses_Zprime,individualLimitFiles, sig_dict, limitsDictOut, theoryLineRootFile="/lustre/SCRATCH/atlas/ywng/WorkSpace/r21/r21SwiftCat/BayesianFramework/source/Bayesian/inputs/theoryLine/Limit_Setting_single_btag.root", limitHistogram=histName, theoryRootcutstring=cutstring,makePlot = True)
  #      print("limitDictOut:", limitsDictOut)
  #      if limitsDictOut is not None:
  #          limitsDictOutTotal.update(limitsDictOut)

  ##print("got here")
  #print("limitsDictOut single btagged",limitsDictOutTotal)
  #writeLimitsDict(folderextension, dataset,limitsDictOutTotal)
