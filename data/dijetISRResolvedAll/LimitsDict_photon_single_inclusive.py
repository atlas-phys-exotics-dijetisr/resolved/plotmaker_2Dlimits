{   '0.10': {   '0.25': {   'exp': 0.03638449381243476,
                            'exp+1': 0.04847592665875214,
                            'exp+2': 0.052212887218045095,
                            'exp-1': 0.026948920937155174,
                            'exp-2': 0.021691211191643946,
                            'nPEs': 100,
                            'obs': 0.03035180563003191,
                            'theory': 0.020577574483702867},
                '0.35': {   'exp': 0.0257734770486441,
                            'exp+1': 0.03536134710134917,
                            'exp+2': 0.04642744568468915,
                            'exp-1': 0.019749770344753045,
                            'exp-2': 0.01421077161197598,
                            'nPEs': 100,
                            'obs': 0.04900199589558772,
                            'theory': 0.015300701004153405},
                '0.45': {   'exp': 0.019879359246288163,
                            'exp+1': 0.02857538570043198,
                            'exp+2': 0.03274883644716359,
                            'exp-1': 0.012783574255818246,
                            'exp-2': 0.00944367741293697,
                            'nPEs': 100,
                            'obs': 0.016915376358381234,
                            'theory': 0.010705202452440407},
                '0.55': {   'exp': 0.015780755009814212,
                            'exp+1': 0.022263573626008146,
                            'exp+2': 0.027500654505593174,
                            'exp-1': 0.010979756589416984,
                            'exp-2': 0.008695925014196223,
                            'nPEs': 100,
                            'obs': 0.014559915188381579,
                            'theory': 0.007188826902426635}},
    '0.20': {   '0.25': {   'exp': 0.03984756107956967,
                            'exp+1': 0.049630295978040334,
                            'exp+2': 0.05586663981834322,
                            'exp-1': 0.02978007733060056,
                            'exp-2': 0.022601731025158844,
                            'nPEs': 100,
                            'obs': 0.03217763397958495,
                            'theory': 0.0785956583384805},
                '0.35': {   'exp': 0.026752243100029254,
                            'exp+1': 0.03692384676214743,
                            'exp+2': 0.05419810259721325,
                            'exp-1': 0.019929808112566432,
                            'exp-2': 0.01447075661960835,
                            'nPEs': 100,
                            'obs': 0.047967859338906155,
                            'theory': 0.06268918960814321},
                '0.45': {   'exp': 0.02010008520306731,
                            'exp+1': 0.02975640054537191,
                            'exp+2': 0.03819155187361526,
                            'exp-1': 0.013301213627723352,
                            'exp-2': 0.009832946547460525,
                            'nPEs': 100,
                            'obs': 0.019273142857142878,
                            'theory': 0.04202791177351506},
                '0.55': {   'exp': 0.017170226969292386,
                            'exp+1': 0.023625340549419727,
                            'exp+2': 0.02900376531182458,
                            'exp-1': 0.012438087866725634,
                            'exp-2': 0.009172383850953402,
                            'nPEs': 100,
                            'obs': 0.015567894079423172,
                            'theory': 0.028650209641896903},
                '0.75': {   'exp': 0.011577556559560335,
                            'exp+1': 0.01505993431855501,
                            'exp+2': 0.019649543308903287,
                            'exp-1': 0.007987287810944174,
                            'exp-2': 0.0061021309955808815,
                            'nPEs': 100,
                            'obs': 0.01449796111901376,
                            'theory': 0.014602503825728856}}}
