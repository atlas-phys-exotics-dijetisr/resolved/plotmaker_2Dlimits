scaleFactors = {
    1.25: 4.29473,
    1.5: 4.1365,
    1.75: 3.9478,
    2: 3.74003,
    2.25: 3.52251,
    2.5: 3.31041,
    2.75: 3.11205,
    3: 2.9319,
    3.25: 2.76672,
    3.5: 2.60906,
    3.75: 2.44508,
    4: 2.26238,
    4.25: 2.03821,
    4.5: 1.7612,
    4.75: 1.40081,
    5: 1.02386
}


# From Rui Wang
# Here are the SFs for limit correction (limt_Zqq=SF*limt_Zbb).
# These numbers are the eff(Z'->bb)/eff(Z'->qq). Ideally if eff(Z'->ll) and eff(Z'->cc) are 0, then eff(Z'->bb)/eff(Z'->qq) = 5.
# you can find the curved here: https://indico.cern.ch/event/738399/ on slide 13 (Z'->bb) and 15 (Z'->qq)
# The light jet tagging efficiency is ~7% while the c-jet tagging efficiency is ~17%

# these should only be applied to the high mass limits, the low mass ones have them
