
# high-mass:
# acceptances are all 0.25, they should be 0.2 and scale up XS by 5
highMassLimitsFile = 'limits2016_dibjet_Zbb.txt'
newHighMassLimitsFile = 'limits2016_dibjet_highMass.txt'

# low-mass:
# acceptances are all 1, they should be 0.2
lowMassLimitsFile = 'limits2016_dibjet_Zqq.txt'
newLowMassLimitsFile = 'limits2016_dibjet_lowMass.txt'


fh=open(highMassLimitsFile)
highMassLimitsDict = eval(fh.read())
fh.close()

for gq in highMassLimitsDict:
    for mZ in highMassLimitsDict[gq]:
        highMassLimitsDict[gq][mZ]['acc'] = 0.2
        highMassLimitsDict[gq][mZ]['theory'] = highMassLimitsDict[gq][mZ]['theory'] * 5

        
fh=open(lowMassLimitsFile)
lowMassLimitsDict = eval(fh.read())
fh.close()
for gq in lowMassLimitsDict:
    for mZ in lowMassLimitsDict[gq]:
        lowMassLimitsDict[gq][mZ]['acc'] = 0.2


# write to new files
items = ['acc',
         'exp-2',
         'exp-1',
         'exp',
         'exp+1',
        'exp+2',
         'obs',
         'theory',
]

newLimitsFiles = [newHighMassLimitsFile, newLowMassLimitsFile]
newLimitsDicts = [highMassLimitsDict, lowMassLimitsDict]
for i in range(len(newLimitsFiles)):
    newLimitFile = newLimitsFiles[i]
    newLimitsDict = newLimitsDicts[i]
    newfile = open(newLimitFile,'w')
    newfile.write('{\n')
    for gq in sorted(newLimitsDict):
        newfile.write('    '+str(gq)+': {\n')

        for mZ in sorted(newLimitsDict[gq]):
            newfile.write('        '+str(mZ)+': {\n')

            for item in items:
                newfile.write("            '"+item+"': "+str(newLimitsDict[gq][mZ][item])+',\n')
            
            newfile.write('        },\n')
        
        newfile.write('    },\n')

    newfile.write('}\n')
