names = """dataLikeHistograms.m0.35_g0.10.root
dataLikeHistograms.m0.65_g0.10.root
dataLikeHistograms.m0.85_g0.20.root
dataLikeHistograms.m1.05_g0.40.root
dataLikeHistograms.m0.35_g0.15.root
dataLikeHistograms.m0.65_g0.15.root
dataLikeHistograms.m0.85_g0.30.root
dataLikeHistograms.m1.15_g0.40.root
dataLikeHistograms.m0.45_g0.05.root
dataLikeHistograms.m0.65_g0.20.root
dataLikeHistograms.m0.85_g0.40.root
dataLikeHistograms.m1.50_g0.10.root
dataLikeHistograms.m0.45_g0.10.root
dataLikeHistograms.m0.65_g0.30.root
dataLikeHistograms.m0.95_g0.05.root
dataLikeHistograms.m1.50_g0.20.root
dataLikeHistograms.m0.45_g0.15.root
dataLikeHistograms.m0.65_g0.40.root
dataLikeHistograms.m0.95_g0.10.root
dataLikeHistograms.m1.50_g0.30.root
dataLikeHistograms.m0.45_g0.20.root
dataLikeHistograms.m0.75_g0.05.root
dataLikeHistograms.m0.95_g0.15.root
dataLikeHistograms.m1.70_g0.10.root
dataLikeHistograms.m0.45_g0.30.root
dataLikeHistograms.m0.75_g0.10.root
dataLikeHistograms.m0.95_g0.20.root
dataLikeHistograms.m1.70_g0.20.root
dataLikeHistograms.m0.55_g0.05.root
dataLikeHistograms.m0.75_g0.15.root
dataLikeHistograms.m0.95_g0.30.root
dataLikeHistograms.m1.70_g0.30.root
dataLikeHistograms.m0.55_g0.10.root
dataLikeHistograms.m0.75_g0.20.root
dataLikeHistograms.m0.95_g0.40.root
dataLikeHistograms.m1.70_g0.40.root
dataLikeHistograms.m0.55_g0.15.root
dataLikeHistograms.m0.75_g0.30.root
dataLikeHistograms.m1.00_g0.10.root
dataLikeHistograms.m2.00_g0.10.root
dataLikeHistograms.m0.55_g0.20.root
dataLikeHistograms.m0.75_g0.40.root
dataLikeHistograms.m1.00_g0.20.root
dataLikeHistograms.m2.00_g0.20.root
dataLikeHistograms.m0.55_g0.30.root
dataLikeHistograms.m0.85_g0.05.root
dataLikeHistograms.m1.00_g0.30.root
dataLikeHistograms.m2.00_g0.30.root
dataLikeHistograms.m0.65_g0.05.root
dataLikeHistograms.m0.85_g0.10.root
dataLikeHistograms.m1.05_g0.30.root
dataLikeHistograms.m2.00_g0.40.root"""

points = []
for name in sorted(names.split('\n')):
    mZ = float(name.split('.m')[1].split('_g')[0])
    gq = float(name.split('_g')[1].split('.root')[0])
    # print mZ, gq
    points.append((mZ, gq))

outfile = open('TLAsignalPoints.py','w')
outfile.write('points = [\n')
for point in points:
    outfile.write('    '+str(point)+',\n')
outfile.write(']\n')


