{
    0.05: {
        0.7: {
            'acc': 0.39481195502260136,
            'exp-2': 0.34231026805805914,
            'exp-1': 0.47100539802961894,
            'nPEs': 640,
            'exp': 0.6661626395760561,
            'exp+1': 0.9406717664120869,
            'exp+2': 1.1962256645385474,
            'dsid': 0,
            'obs': 0.7175640769390303,
            'theory': 2.7802620443918253
        },
        0.72: {
            'acc': 0.4095817082569328,
            'exp-2': 0.40009580228947506,
            'exp-1': 0.5243689092693281,
            'nPEs': 640,
            'exp': 0.7335638832157602,
            'exp+1': 0.9662527561568491,
            'exp+2': 1.1215228376426776,
            'dsid': 0,
            'obs': 0.5152369332254582,
            'theory': 2.4267829262783027
        },
        0.75: {
            'acc': 0.405,
            'exp-2': 0.32388915178290983,
            'exp-1': 0.43302404511996817,
            'nPEs': 1140,
            'exp': 0.6011336447784198,
            'exp+1': 0.8238997570828377,
            'exp+2': 0.9782819752679497,
            'dsid': 305618,
            'obs': 0.37995872522695917,
            'theory': 2.1285
        },
        0.8: {
            'acc': 0.42902773555594237,
            'exp-2': 0.2955781151127184,
            'exp-1': 0.3881599481195412,
            'nPEs': 640,
            'exp': 0.5400746444896551,
            'exp+1': 0.7379520086229127,
            'exp+2': 0.9054906208906689,
            'dsid': 0,
            'obs': 0.33502730375426587,
            'theory': 1.651591885954214
        },
        0.85: {
            'acc': 0.433,
            'exp-2': 0.2705884550167081,
            'exp-1': 0.3531766018525668,
            'nPEs': 1140,
            'exp': 0.49319404960735286,
            'exp+1': 0.6404518009988939,
            'exp+2': 0.7337483960250364,
            'dsid': 305619,
            'obs': 0.2542113003830585,
            'theory': 1.299125
        },
        0.9: {
            'acc': 0.4342790540941985,
            'exp-2': 0.2982189629465689,
            'exp-1': 0.3981393532991761,
            'nPEs': 640,
            'exp': 0.5463653220242309,
            'exp+1': 0.706546773109182,
            'exp+2': 0.7996801077345088,
            'dsid': 0,
            'obs': 0.6338856368646548,
            'theory': 1.0346207040609268
        },
        0.95: {
            'acc': 0.448,
            'exp-2': 0.24423217615282908,
            'exp-1': 0.3244583857191359,
            'nPEs': 1140,
            'exp': 0.4493508487849414,
            'exp+1': 0.6308649439297903,
            'exp+2': 0.885142779247531,
            'dsid': 305620,
            'obs': 1.1207790082258668,
            'theory': 0.8290999
        },
    },
    0.1: {
        0.7: {
            'acc': 0.39131722743778924,
            'exp-2': 0.3986673766260132,
            'exp-1': 0.4827044987665083,
            'nPEs': 140,
            'exp': 0.6922126350036681,
            'exp+1': 0.9261277578252191,
            'exp+2': 1.286015722356471,
            'dsid': 0,
            'obs': 0.7175620947259482,
            'theory': 11.040239210312015
        },
        0.72: {
            'acc': 0.4114263471984716,
            'exp-2': 0.36841531957564383,
            'exp-1': 0.47172642053316105,
            'nPEs': 140,
            'exp': 0.6869585687105251,
            'exp+1': 0.9261992372082002,
            'exp+2': 1.1776872559450609,
            'dsid': 0,
            'obs': 0.4701693115236055,
            'theory': 9.614020179830584
        },
        0.75: {
            'acc': 0.405,
            'exp-2': 0.32129254833185134,
            'exp-1': 0.45168306312130996,
            'nPEs': 640,
            'exp': 0.6171067700640954,
            'exp+1': 0.8393729352838496,
            'exp+2': 1.001366855465991,
            'dsid': 304981,
            'obs': 0.3951412583948029,
            'theory': 8.51
        },
        0.8: {
            'acc': 0.44261763316836433,
            'exp-2': 0.35568304324073474,
            'exp-1': 0.48403218746019155,
            'nPEs': 140,
            'exp': 0.6387724693244516,
            'exp+1': 0.8193437218945828,
            'exp+2': 0.9376315127249198,
            'dsid': 0,
            'obs': 0.37172010780215964,
            'theory': 6.488673499318954
        },
        0.85: {
            'acc': 0.437,
            'exp-2': 0.30279551438322766,
            'exp-1': 0.4144353653371864,
            'nPEs': 640,
            'exp': 0.554927037262144,
            'exp+1': 0.6903995651216558,
            'exp+2': 0.7564292977698817,
            'dsid': 304986,
            'obs': 0.2887312078581531,
            'theory': 5.193125
        },
        0.9: {
            'acc': 0.4536822268533362,
            'exp-2': 0.2568413127889421,
            'exp-1': 0.3304496847294243,
            'nPEs': 140,
            'exp': 0.4723722387817771,
            'exp+1': 0.6316113912222225,
            'exp+2': 0.7746965313623022,
            'dsid': 0,
            'obs': 0.30587177736938825,
            'theory': 4.009204222410281
        },
        0.95: {
            'acc': 0.448,
            'exp-2': 0.29680998725973884,
            'exp-1': 0.3840287715728583,
            'nPEs': 120,
            'exp': 0.5421245652034099,
            'exp+1': 0.7544869468791794,
            'exp+2': 0.9826455942702107,
            'dsid': 305621,
            'obs': 1.236553460249245,
            'theory': 3.3175
        },
        1.0: {
            'acc': 0.459,
            'exp-2': 0.1948217506121483,
            'exp-1': 0.3102948698419494,
            'nPEs': 1180,
            'exp': 0.42964414608804036,
            'exp+1': 0.61099730149666,
            'exp+2': 0.7985698645148533,
            'dsid': 301859,
            'obs': 1.0588205570069156,
            'theory': 2.1488
        },
        1.05: {
            'acc': 0.45607903845171044,
            'exp-2': 0.1924433871002804,
            'exp-1': 0.2421084631767911,
            'nPEs': 640,
            'exp': 0.3367983029958285,
            'exp+1': 0.4628336513310578,
            'exp+2': 0.6234436631775998,
            'dsid': 0,
            'obs': 0.4892283156777956,
            'theory': 2.089962169929553
        },
        1.1: {
            'acc': 0.4561986636683851,
            'exp-2': 0.21112922299890144,
            'exp-1': 0.2848037235266529,
            'nPEs': 660,
            'exp': 0.3900044640399647,
            'exp+1': 0.5090447269385682,
            'exp+2': 0.5797504906143344,
            'dsid': 0,
            'obs': 0.29697913138629056,
            'theory': 1.707611878658188
        },
        1.15: {
            'acc': 0.4562503174041164,
            'exp-2': 0.17205993978341877,
            'exp-1': 0.21651047322629013,
            'nPEs': 640,
            'exp': 0.3018807221891586,
            'exp+1': 0.40042211223996893,
            'exp+2': 0.4706842712153009,
            'dsid': 0,
            'obs': 0.2059673596598628,
            'theory': 1.4040955264839148
        },
        1.2: {
            'acc': 0.45627263831000836,
            'exp-2': 0.12130969373284221,
            'exp-1': 0.1642737243242111,
            'nPEs': 640,
            'exp': 0.21963539676486538,
            'exp+1': 0.2929559343255852,
            'exp+2': 0.37052909351379687,
            'dsid': 0,
            'obs': 0.1595782304303053,
            'theory': 1.16120085699549
        },
        1.25: {
            'acc': 0.4562823046118384,
            'exp-2': 0.13625475201635404,
            'exp-1': 0.17882468238400706,
            'nPEs': 640,
            'exp': 0.24457395840041207,
            'exp+1': 0.3308919157963167,
            'exp+2': 0.3968701819972128,
            'dsid': 0,
            'obs': 0.21003443968374813,
            'theory': 0.9653799977634909
        },
        1.3: {
            'acc': 0.4562865122549095,
            'exp-2': 0.09521580932273821,
            'exp-1': 0.1285538219696222,
            'nPEs': 640,
            'exp': 0.17161200562136109,
            'exp+1': 0.23880127053264633,
            'exp+2': 0.3221313993174061,
            'dsid': 0,
            'obs': 0.2161139693957069,
            'theory': 0.8064405021549201
        },
        1.35: {
            'acc': 0.4562883653982214,
            'exp-2': 0.09029210020388832,
            'exp-1': 0.11944930730889829,
            'nPEs': 640,
            'exp': 0.166873065488127,
            'exp+1': 0.22333220181218671,
            'exp+2': 0.31513158490966225,
            'dsid': 0,
            'obs': 0.1908301933337742,
            'theory': 0.6766345214800162
        },
        1.4: {
            'acc': 0.45628920299294257,
            'exp-2': 0.08317447133997248,
            'exp-1': 0.10783089638140458,
            'nPEs': 640,
            'exp': 0.14751752179215777,
            'exp+1': 0.20241807031453624,
            'exp+2': 0.2839752506482512,
            'dsid': 0,
            'obs': 0.20232695009233684,
            'theory': 0.5700161021537391
        },
        1.45: {
            'acc': 0.456289602563401,
            'exp-2': 0.07320988989748388,
            'exp-1': 0.09282695106671106,
            'nPEs': 640,
            'exp': 0.13370653231240587,
            'exp+1': 0.18477243135087357,
            'exp+2': 0.2563416389175418,
            'dsid': 0,
            'obs': 0.14878289805316952,
            'theory': 0.481981810217602
        },
        1.5: {
            'acc': 0.467,
            'exp-2': 0.07362834554565423,
            'exp-1': 0.09523561053271352,
            'nPEs': 1140,
            'exp': 0.13231782554651575,
            'exp+1': 0.18470379090643846,
            'exp+2': 0.2278908886024959,
            'dsid': 301862,
            'obs': 0.10684241327418831,
            'theory': 0.36360000000000003
        },
        1.6: {
            'acc': 0.4562900363738176,
            'exp-2': 0.0602103393284562,
            'exp-1': 0.0818772528591102,
            'nPEs': 140,
            'exp': 0.11292381285103863,
            'exp+1': 0.15783738315136542,
            'exp+2': 0.20006801686200845,
            'dsid': 0,
            'obs': 0.10672436317609557,
            'theory': 0.29710874145302135
        },
        1.7: {
            'acc': 0.469,
            'exp-2': 0.05117697841726619,
            'exp-1': 0.06856676132662896,
            'nPEs': 640,
            'exp': 0.09466185056121457,
            'exp+1': 0.1270976558497347,
            'exp+2': 0.16854343518794238,
            'dsid': 301883,
            'obs': 0.12111545160943883,
            'theory': 0.2493125
        },
    },
    0.2: {
        0.7: {
            'acc': 0.3878160084797745,
            'exp-2': 0.36920922703040154,
            'exp-1': 0.5336201959217175,
            'nPEs': 140,
            'exp': 0.7043419194894999,
            'exp+1': 0.9647002976607912,
            'exp+2': 1.1805989426487316,
            'dsid': 0,
            'obs': 0.7685324463885644,
            'theory': 44.31562743773046
        },
        0.72: {
            'acc': 0.4082239714356984,
            'exp-2': 0.45536784665449637,
            'exp-1': 0.5803316524763447,
            'nPEs': 140,
            'exp': 0.8067762259505672,
            'exp+1': 1.0882570363511437,
            'exp+2': 1.2253678477450956,
            'dsid': 0,
            'obs': 0.5526972632650035,
            'theory': 38.57735272399319
        },
        0.75: {
            'acc': 0.401,
            'exp-2': 0.3395480486971033,
            'exp-1': 0.45142747706792274,
            'nPEs': 640,
            'exp': 0.6346831673950881,
            'exp+1': 0.8508029592471025,
            'exp+2': 0.9990874078180674,
            'dsid': 304983,
            'obs': 0.3910145982349823,
            'theory': 34.13125
        },
        0.8: {
            'acc': 0.44019593071255275,
            'exp-2': 0.34231812341522183,
            'exp-1': 0.43149295023414547,
            'nPEs': 160,
            'exp': 0.5553574161431272,
            'exp+1': 0.7370490603356057,
            'exp+2': 0.92976082747092,
            'dsid': 0,
            'obs': 0.3413690180665678,
            'theory': 25.99705394227856
        },
        0.85: {
            'acc': 0.442,
            'exp-2': 0.30068830392909535,
            'exp-1': 0.4011198212399944,
            'nPEs': 640,
            'exp': 0.5471775844985136,
            'exp+1': 0.7052369480065095,
            'exp+2': 0.7873120386343485,
            'dsid': 304988,
            'obs': 0.3275253965067258,
            'theory': 20.8375
        },
        0.9: {
            'acc': 0.4517170946884542,
            'exp-2': 0.22247127755180485,
            'exp-1': 0.2830212911926602,
            'nPEs': 140,
            'exp': 0.38750523830625144,
            'exp+1': 0.5025055277589814,
            'exp+2': 0.6291829067121728,
            'dsid': 0,
            'obs': 0.30257096146766005,
            'theory': 16.015132696195927
        },
        0.95: {
            'acc': 0.453,
            'exp-2': 0.25686754880667745,
            'exp-1': 0.3392627352742776,
            'nPEs': 120,
            'exp': 0.45807599598663806,
            'exp+1': 0.6149154483283201,
            'exp+2': 0.7761768291855333,
            'dsid': 304992,
            'obs': 0.9914826192059435,
            'theory': 13.30375
        },
        1.0: {
            'acc': 0.455,
            'exp-2': 0.2736146705087898,
            'exp-1': 0.35593282414166966,
            'nPEs': 660,
            'exp': 0.5126511017782993,
            'exp+1': 0.7055571502138019,
            'exp+2': 0.9721741781268821,
            'dsid': 301860,
            'obs': 1.22619494648905,
            'theory': 8.624
        },
        1.05: {
            'acc': 0.45425792497431516,
            'exp-2': 0.24788857980582416,
            'exp-1': 0.3239069914388554,
            'nPEs': 140,
            'exp': 0.47429999057190797,
            'exp+1': 0.6677515088916829,
            'exp+2': 0.8329706900756584,
            'dsid': 0,
            'obs': 0.7899804524368166,
            'theory': 8.297284377394705
        },
        1.1: {
            'acc': 0.4543871770260036,
            'exp-2': 0.2031540834779131,
            'exp-1': 0.2843746950611636,
            'nPEs': 140,
            'exp': 0.37188059083533803,
            'exp+1': 0.476665576744075,
            'exp+2': 0.6063787776615656,
            'dsid': 0,
            'obs': 0.3728350961020309,
            'theory': 6.762839360398977
        },
        1.15: {
            'acc': 0.45444340563122976,
            'exp-2': 0.18107150458994561,
            'exp-1': 0.25799121096256,
            'nPEs': 140,
            'exp': 0.3419040820180969,
            'exp+1': 0.4384369504381433,
            'exp+2': 0.49371290255780953,
            'dsid': 0,
            'obs': 0.2518391508946118,
            'theory': 5.54632025063134
        },
        1.2: {
            'acc': 0.45446788304870156,
            'exp-2': 0.20779144280225853,
            'exp-1': 0.24490390630567851,
            'nPEs': 140,
            'exp': 0.3133330517352076,
            'exp+1': 0.39662618683299217,
            'exp+2': 0.4323074580512055,
            'dsid': 0,
            'obs': 0.23336988407186993,
            'theory': 4.574191629234284
        },
        1.25: {
            'acc': 0.4544785593594998,
            'exp-2': 0.1282048215421993,
            'exp-1': 0.16893326053375918,
            'nPEs': 140,
            'exp': 0.24092565929424378,
            'exp+1': 0.33081115547863754,
            'exp+2': 0.42666642186148757,
            'dsid': 0,
            'obs': 0.19791799960968304,
            'theory': 3.7917360095861277
        },
        1.3: {
            'acc': 0.45448323768511417,
            'exp-2': 0.13683433045573176,
            'exp-1': 0.18360662134146888,
            'nPEs': 140,
            'exp': 0.2508810470223439,
            'exp+1': 0.3426123139662339,
            'exp+2': 0.4452000972194717,
            'dsid': 0,
            'obs': 0.33622480780501296,
            'theory': 3.15778262478341
        },
        1.35: {
            'acc': 0.4544853094387664,
            'exp-2': 0.10130211888509669,
            'exp-1': 0.12709519143040582,
            'nPEs': 140,
            'exp': 0.1784328755546657,
            'exp+1': 0.25317347199696516,
            'exp+2': 0.33242346651541593,
            'dsid': 0,
            'obs': 0.24677129774270204,
            'theory': 2.6410358298301126
        },
        1.4: {
            'acc': 0.45448624847324526,
            'exp-2': 0.08326124023354864,
            'exp-1': 0.10477687209295027,
            'nPEs': 140,
            'exp': 0.1350115531867367,
            'exp+1': 0.1991110829562323,
            'exp+2': 0.2693218975688581,
            'dsid': 0,
            'obs': 0.20593595451774074,
            'theory': 2.2174820710570162
        },
        1.45: {
            'acc': 0.45448669527594093,
            'exp-2': 0.0792422443100546,
            'exp-1': 0.10737224902906907,
            'nPEs': 140,
            'exp': 0.14941919485248528,
            'exp+1': 0.19734115211958808,
            'exp+2': 0.2446270754732894,
            'dsid': 0,
            'obs': 0.14478492295326795,
            'theory': 1.868535108653373
        },
        1.5: {
            'acc': 0.463,
            'exp-2': 0.07705157762763806,
            'exp-1': 0.10166583097259686,
            'nPEs': 640,
            'exp': 0.1373777473165026,
            'exp+1': 0.18542493374263536,
            'exp+2': 0.2441921204268984,
            'dsid': 301863,
            'obs': 0.09191674123049036,
            'theory': 1.4625
        },
        1.6: {
            'acc': 0.45448716768129965,
            'exp-2': 0.06390292447327557,
            'exp-1': 0.09016472383790797,
            'nPEs': 140,
            'exp': 0.12420935289761939,
            'exp+1': 0.15558840097320312,
            'exp+2': 0.20553295823582307,
            'dsid': 0,
            'obs': 0.11303649650370208,
            'theory': 1.1391090190763618
        },
        1.7: {
            'acc': 0.467,
            'exp-2': 0.055436492819053856,
            'exp-1': 0.07743153945802003,
            'nPEs': 640,
            'exp': 0.10367346017204557,
            'exp+1': 0.14464166549925675,
            'exp+2': 0.18262806726747102,
            'dsid': 301884,
            'obs': 0.1271643284593067,
            'theory': 1.000724
        },
    },
    0.3: {
        0.7: {
            'acc': 0.37109159049878043,
            'exp-2': 0.4650436629115451,
            'exp-1': 0.6515929913403243,
            'nPEs': 640,
            'exp': 0.9079554629346542,
            'exp+1': 1.22640597033705,
            'exp+2': 1.5384495190816008,
            'dsid': 0,
            'obs': 0.920769370571387,
            'theory': 100.95785537642725
        },
        0.72: {
            'acc': 0.39326624028782875,
            'exp-2': 0.371938668220142,
            'exp-1': 0.49970097398615354,
            'nPEs': 640,
            'exp': 0.7048301737720428,
            'exp+1': 0.9824525933828584,
            'exp+2': 1.247239060940686,
            'dsid': 0,
            'obs': 0.5467571561364079,
            'theory': 87.96176267511339
        },
        0.75: {
            'acc': 0.387,
            'exp-2': 0.3544869922046187,
            'exp-1': 0.4806583715691371,
            'nPEs': 1136,
            'exp': 0.6633002088628701,
            'exp+1': 0.9100575938566552,
            'exp+2': 1.1092112948668025,
            'dsid': 304984,
            'obs': 0.4233704065678823,
            'theory': 77.75375
        },
        0.8: {
            'acc': 0.43045474034969433,
            'exp-2': 0.37632699391957963,
            'exp-1': 0.5047709436915861,
            'nPEs': 640,
            'exp': 0.7000061061461345,
            'exp+1': 0.9082574140880236,
            'exp+2': 1.0437938256282966,
            'dsid': 0,
            'obs': 0.42624349274229956,
            'theory': 59.450285849408274
        },
        0.85: {
            'acc': 0.428,
            'exp-2': 0.29881049401868504,
            'exp-1': 0.39968747212741745,
            'nPEs': 1140,
            'exp': 0.5638703995657269,
            'exp+1': 0.7414025431435693,
            'exp+2': 0.8586947190972123,
            'dsid': 304989,
            'obs': 0.29849129298382016,
            'theory': 47.55
        },
        0.9: {
            'acc': 0.44542709140897924,
            'exp-2': 0.3194438572176614,
            'exp-1': 0.4449880647221507,
            'nPEs': 640,
            'exp': 0.636230179773112,
            'exp+1': 0.8410205012135694,
            'exp+2': 0.9529993529141059,
            'dsid': 0,
            'obs': 0.4654413016036268,
            'theory': 36.78949974728807
        },
        0.95: {
            'acc': 0.441,
            'exp-2': 0.299676299704886,
            'exp-1': 0.44285501108054254,
            'nPEs': 1140,
            'exp': 0.6567773209007625,
            'exp+1': 0.9715463963059625,
            'exp+2': 1.1268350806673977,
            'dsid': 304993,
            'obs': 1.1295511806364078,
            'theory': 30.4225
        },
        1.0: {
            'acc': 0.445,
            'exp-2': 0.21901846265192468,
            'exp-1': 0.2768747480425616,
            'nPEs': 20,
            'exp': 0.5405670030779539,
            'exp+1': 0.8920627028740165,
            'exp+2': 1.9765391748239385,
            'dsid': 301861,
            'obs': 1.419647712828837,
            'theory': 19.733
        },
        1.05: {
            'acc': 0.454,
            'exp-2': 0.28119888150686484,
            'exp-1': 0.39060306906260783,
            'nPEs': 1160,
            'exp': 0.5695874290802625,
            'exp+1': 0.7671282789969366,
            'exp+2': 0.8829562069446848,
            'dsid': 304995,
            'obs': 0.7410597711088037,
            'theory': 20.16
        },
        1.1: {
            'acc': 0.44940222284942155,
            'exp-2': 0.26021656464026377,
            'exp-1': 0.33587828671814035,
            'nPEs': 660,
            'exp': 0.47316483947411375,
            'exp+1': 0.6093747449164838,
            'exp+2': 0.6797926360815889,
            'dsid': 0,
            'obs': 0.4196865032578344,
            'theory': 15.70393870198958
        },
        1.15: {
            'acc': 0.4495027063606648,
            'exp-2': 0.18228482733061377,
            'exp-1': 0.23555817533123138,
            'nPEs': 640,
            'exp': 0.3228387708939648,
            'exp+1': 0.4322817113538313,
            'exp+2': 0.5281336526438392,
            'dsid': 0,
            'obs': 0.2501991190685682,
            'theory': 12.917860599102376
        },
        1.2: {
            'acc': 0.44954885834023406,
            'exp-2': 0.16955054266727937,
            'exp-1': 0.2263736981646135,
            'nPEs': 640,
            'exp': 0.31377007186781125,
            'exp+1': 0.41262541671021474,
            'exp+2': 0.47737188909095446,
            'dsid': 0,
            'obs': 0.24050312598067938,
            'theory': 10.686959526284836
        },
        1.25: {
            'acc': 0.44957009340959625,
            'exp-2': 0.13382639442888244,
            'exp-1': 0.18657666678193963,
            'nPEs': 640,
            'exp': 0.2552582039430516,
            'exp+1': 0.3577387605027619,
            'exp+2': 0.4389728098776919,
            'dsid': 0,
            'obs': 0.22141586975182587,
            'theory': 8.887464689231129
        },
        1.3: {
            'acc': 0.4495799043072084,
            'exp-2': 0.12837919774154125,
            'exp-1': 0.17000053894849368,
            'nPEs': 640,
            'exp': 0.2286876080210737,
            'exp+1': 0.32645359817396846,
            'exp+2': 0.4260327492710356,
            'dsid': 0,
            'obs': 0.27933290726433563,
            'theory': 7.426187997280449
        },
        1.35: {
            'acc': 0.44958447794585027,
            'exp-2': 0.13355524850292955,
            'exp-1': 0.18621079516593161,
            'nPEs': 640,
            'exp': 0.25303915255328735,
            'exp+1': 0.3337436119354426,
            'exp+2': 0.4328586082323065,
            'dsid': 0,
            'obs': 0.2518016922950537,
            'theory': 6.232240478796702
        },
        1.4: {
            'acc': 0.4495866507628613,
            'exp-2': 0.10714707965741513,
            'exp-1': 0.14268188913790772,
            'nPEs': 640,
            'exp': 0.19486079473427592,
            'exp+1': 0.28051012728230457,
            'exp+2': 0.3634621473218525,
            'dsid': 0,
            'obs': 0.23616492615865511,
            'theory': 5.251182320082347
        },
        1.45: {
            'acc': 0.449587723042056,
            'exp-2': 0.09543931079069623,
            'exp-1': 0.1261324780615942,
            'nPEs': 640,
            'exp': 0.1814204447999527,
            'exp+1': 0.2501300244573102,
            'exp+2': 0.3292079667398047,
            'dsid': 0,
            'obs': 0.17695765683931405,
            'theory': 4.440838263732002
        },
        1.5: {
            'acc': 0.468,
            'exp-2': 0.09337841922472548,
            'exp-1': 0.12587979331864743,
            'nPEs': 1140,
            'exp': 0.17340094648323415,
            'exp+1': 0.23597876944119228,
            'exp+2': 0.2838294619259146,
            'dsid': 301864,
            'obs': 0.1494291004425783,
            'theory': 3.3825
        },
        1.6: {
            'acc': 0.44958885793109954,
            'exp-2': 0.08801196149140314,
            'exp-1': 0.1167187610981695,
            'nPEs': 640,
            'exp': 0.16008967859702522,
            'exp+1': 0.2149917964262225,
            'exp+2': 0.25200326579599497,
            'dsid': 0,
            'obs': 0.1351268144428758,
            'theory': 2.738149310905767
        },
        1.7: {
            'acc': 0.466,
            'exp-2': 0.06352455620073089,
            'exp-1': 0.08482582087795688,
            'nPEs': 1140,
            'exp': 0.1178097259348672,
            'exp+1': 0.16431298393354205,
            'exp+2': 0.21792598280208733,
            'dsid': 301885,
            'obs': 0.11891140753770124,
            'theory': 2.335375
        },
    },
    0.4: {
        0.7: {
            'acc': 0.33475683483289437,
            'exp-2': 0.5650177753443157,
            'exp-1': 0.7300044807221139,
            'nPEs': 640,
            'exp': 0.9933924142292336,
            'exp+1': 1.3852214191499523,
            'exp+2': 1.6305599792087873,
            'dsid': 0,
            'obs': 0.898288700393367,
            'theory': 185.41625400765832
        },
        0.72: {
            'acc': 0.3504207892638651,
            'exp-2': 0.4881996061292424,
            'exp-1': 0.6470550252013155,
            'nPEs': 640,
            'exp': 0.9060340775452512,
            'exp+1': 1.1848534958203492,
            'exp+2': 1.3519747954744854,
            'dsid': 0,
            'obs': 0.6449520056389629,
            'theory': 162.10002638302728
        },
        0.75: {
            'acc': 0.364,
            'exp-2': 0.38840185902793023,
            'exp-1': 0.546852089713112,
            'nPEs': 1140,
            'exp': 0.7902105624070853,
            'exp+1': 1.0536906445061578,
            'exp+2': 1.2181414411154794,
            'dsid': 304985,
            'obs': 0.475872671189923,
            'theory': 142.1875
        },
        0.8: {
            'acc': 0.3845147102453196,
            'exp-2': 0.3246099707632793,
            'exp-1': 0.45514514664198524,
            'nPEs': 640,
            'exp': 0.6038439987894542,
            'exp+1': 0.8400879924357719,
            'exp+2': 1.0521688025430045,
            'dsid': 0,
            'obs': 0.36712256708280017,
            'theory': 110.74927441780166
        },
        0.85: {
            'acc': 0.397,
            'exp-2': 0.35279005339578695,
            'exp-1': 0.45722128740376206,
            'nPEs': 1140,
            'exp': 0.6333914430513404,
            'exp+1': 0.8289932233343297,
            'exp+2': 0.9652184893629643,
            'dsid': 304990,
            'obs': 0.36390140723153835,
            'theory': 87.29125
        },
        0.9: {
            'acc': 0.4077972329880811,
            'exp-2': 0.31367027465186215,
            'exp-1': 0.41931468573662156,
            'nPEs': 640,
            'exp': 0.5745129257692285,
            'exp+1': 0.7766549504315443,
            'exp+2': 0.991309347835424,
            'dsid': 0,
            'obs': 0.41883357932503745,
            'theory': 69.6143132181253
        },
        0.95: {
            'acc': 0.409,
            'exp-2': 0.3361092647108516,
            'exp-1': 0.4542481795347771,
            'nPEs': 1140,
            'exp': 0.6060292251295661,
            'exp+1': 0.8175892362676284,
            'exp+2': 1.099259855415369,
            'dsid': 304994,
            'obs': 1.2982721843003415,
            'theory': 56.0975
        },
        1.05: {
            'acc': 0.43,
            'exp-2': 0.3794851145886948,
            'exp-1': 0.5071708073957721,
            'nPEs': 520,
            'exp': 0.7035790456260952,
            'exp+1': 0.9765736048286928,
            'exp+2': 1.2286722131713805,
            'dsid': 304996,
            'obs': 0.9323440650808934,
            'theory': 37.35
        },
        1.15: {
            'acc': 0.424,
            'exp-2': 0.2549809106324689,
            'exp-1': 0.32868222027762595,
            'nPEs': 1160,
            'exp': 0.4633725415712125,
            'exp+1': 0.5728553061096151,
            'exp+2': 0.627578817056703,
            'dsid': 304997,
            'obs': 0.39503282060872347,
            'theory': 25.60375
        },
        1.2: {
            'acc': 0.42286098366281677,
            'exp-2': 0.19179505285777956,
            'exp-1': 0.25673045948209133,
            'nPEs': 640,
            'exp': 0.35406181509440304,
            'exp+1': 0.45710746985090517,
            'exp+2': 0.546429751728315,
            'dsid': 0,
            'obs': 0.28520733949385024,
            'theory': 21.33440913889354
        },
        1.25: {
            'acc': 0.4232181441249381,
            'exp-2': 0.19284572762037913,
            'exp-1': 0.2428875092263725,
            'nPEs': 640,
            'exp': 0.3363647973469681,
            'exp+1': 0.4630916781642582,
            'exp+2': 0.5251414475541545,
            'dsid': 0,
            'obs': 0.19940516654444404,
            'theory': 17.915920706258888
        },
        1.3: {
            'acc': 0.42343982524921536,
            'exp-2': 0.1424582322488814,
            'exp-1': 0.19847754386140237,
            'nPEs': 640,
            'exp': 0.25776157433835767,
            'exp+1': 0.360339137622153,
            'exp+2': 0.4677775715652305,
            'dsid': 0,
            'obs': 0.33685683492006463,
            'theory': 15.120092784438873
        },
        1.35: {
            'acc': 0.42357735483232034,
            'exp-2': 0.16850343131858045,
            'exp-1': 0.2270583555930074,
            'nPEs': 640,
            'exp': 0.30837494317730846,
            'exp+1': 0.4116578099246922,
            'exp+2': 0.4932878137671866,
            'dsid': 0,
            'obs': 0.25544480135626435,
            'theory': 12.818874055812318
        },
        1.4: {
            'acc': 0.42366265860724706,
            'exp-2': 0.12873247028362952,
            'exp-1': 0.1703897419209997,
            'nPEs': 640,
            'exp': 0.22571167727243455,
            'exp+1': 0.31376750080372007,
            'exp+2': 0.4106708318266094,
            'dsid': 0,
            'obs': 0.2351264637071782,
            'theory': 10.913589706573037
        },
        1.45: {
            'acc': 0.423715567129273,
            'exp-2': 0.1127496014202088,
            'exp-1': 0.14898806083348895,
            'nPEs': 640,
            'exp': 0.20611697807601087,
            'exp+1': 0.28535881084964965,
            'exp+2': 0.36878976109215006,
            'dsid': 0,
            'obs': 0.2179107351898563,
            'theory': 9.327519359316529
        },
        1.5: {
            'acc': 0.42374838764111283,
            'exp-2': 0.10272170983842645,
            'exp-1': 0.13534165470870924,
            'nPEs': 620,
            'exp': 0.1788758290939532,
            'exp+1': 0.25800757347693276,
            'exp+2': 0.32058684507404284,
            'dsid': 0,
            'obs': 0.16493648005875197,
            'theory': 8.000516193420344
        },
        1.6: {
            'acc': 0.42378140083007443,
            'exp-2': 0.08579520389050571,
            'exp-1': 0.1163171249645585,
            'nPEs': 640,
            'exp': 0.16307285315216144,
            'exp+1': 0.22315810481677945,
            'exp+2': 0.28024361277836624,
            'dsid': 0,
            'obs': 0.18324705166762414,
            'theory': 5.943347043283576
        },
        1.7: {
            'acc': 0.436,
            'exp-2': 0.06811384331525785,
            'exp-1': 0.09769226748407633,
            'nPEs': 1140,
            'exp': 0.13611805721614745,
            'exp+1': 0.1892508389021137,
            'exp+2': 0.2553024501466778,
            'dsid': 301886,
            'obs': 0.1307235585823773,
            'theory': 4.521875
        },
        1.8: {
            'acc': 0.4237991326845593,
            'exp-2': 0.06792213818340324,
            'exp-1': 0.09034055715174015,
            'nPEs': 640,
            'exp': 0.12179905450456512,
            'exp+1': 0.16820492733154402,
            'exp+2': 0.22240720029426958,
            'dsid': 0,
            'obs': 0.19292243786208396,
            'theory': 3.3889656077106993
        },
    },
}
