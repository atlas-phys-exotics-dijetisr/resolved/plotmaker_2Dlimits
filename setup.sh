
# export WORKDIR=${HOME}/Zprime
# export PYTHONPATH=$(pwd)/DijetHelpers/scripts:$(pwd)/DijetHelpers/bkgFit:${PYTHONPATH}

# Emma
# setupATLAS
# localSetupSFT "external/pyanalysis/1.4_python2.7"


if [ "$HOSTNAME" = "lxplus*" ]; then
    setupATLAS
    lsetup git
    # Used to need to set up root then python
    # Now don't need to set up either?
    # lsetup root
    # localSetupSFT --cmtConfig=x86_64-slc6-gcc49-opt "releases/LCG_78root6/pyanalysis/1.5_python2.7"
fi

# alias prot='python -i prot.py --'
# alias prot='./prot.sh'
# don't use prot any more because it's just an annoying wrapper at the top level
# alias prot='python prot.py'
