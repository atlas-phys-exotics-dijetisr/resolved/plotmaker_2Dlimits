#!/bin/bash

if [ "$USER" = "ckaldero" ]; then
    python -i prot.py -- ${@}
elif [ "x$(command -v ipython)x" != "xx" ]; then
    ipython -i prot.py -- ${@}
else
    python -i prot.py -- ${@}
fi
