

import ROOT
from bisect import bisect
import math
from array import array

def getTGraphIntersections(graphA, graphB):
    # pythonified from https://root-forum.cern.ch/t/a-tgraph-line-intersection-method/11774    
    # Return a TGraph with the points of intersection
    intersectionPoints = []
    i = 0

    xVals_a = graphA.GetX()
    yVals_a = graphA.GetY()
    xVals_b = graphB.GetX()
    yVals_b = graphB.GetY()
    
    # Loop over all points in this TGraph
    for a_i in range(0, graphA.GetN()-1):
        
        # Loop over all points in the other TGraph
        for b_i in range(0, graphB.GetN()-1):
            
            # Get the current point, and the next point for each of the objects
            x1 = xVals_a[a_i]
            y1 = yVals_a[a_i]
            x2 = xVals_a[a_i+1]
            y2 = yVals_a[a_i+1]
            ax1 = xVals_b[b_i]
            ay1 = yVals_b[b_i]
            ax2 = xVals_b[b_i+1]
            ay2 = yVals_b[b_i+1]
            
            # Calculate the intersection between two straight lines, each axis in turn
            if x1==x2 and ax1==ax2:
                continue
            
            else:
                x = (ax1 *(ay2 *(x1-x2)+x2 * y1 - x1 * y2 )+ ax2 * (ay1 * (-x1+x2)- x2 * y1+x1 * y2)) / (-(ay1-ay2) * (x1-x2)+(ax1-ax2)* (y1-y2))
                y = (ax1 * ay2 * (y1-y2)+ax2 * ay1 * (-y1+y2)+(ay1-ay2) * (x2 * y1-x1 * y2))/(-(ay1-ay2) * (x1-x2)+(ax1-ax2) * (y1-y2))
                # x1 = x2 = x0 =>
                # x = x0
                # y = (ax1*ay2 - ax2*ay1 + (ay1-ay2)*x0) / (ax1-ax2)
                
            # Find the tightest interval along the x-axis defined by the four points
            xrange_min = max(min(x1, x2), min(ax1, ax2));
            xrange_max = min(max(x1, x2), max(ax1, ax2));

            # If points from the two lines overlap, they are trivially intersecting
            if x1 == ax1 and y1 == ay1:
                interPoint.SetPoint(i, x1, y1)
            elif x2 == ax2 and y2 == ay2:
                interPoint.SetPoint(i, x2, y2)

            # If the intersection between the two lines is within the tight range, add it to the list of intersections.

            elif x >= xrange_min and x <= xrange_max:

                # in the case of vertical lines, can be wrong
                # check the interesection is possible with the y values
                if x1==x2:
                    if min(y1,y2) > y: continue
                    if max(y1,y2) < y: continue
                if ax1==ax2:
                    if min(ay1,ay2) > y: continue
                    if max(ay1,ay2) < y: continue

                # get the line slope - accounting also for vertical lines
                try:
                    slopeA = (y2-y1)/(x2-x1)
                except ZeroDivisionError:
                    slopeA = 99999
                    if y1>y2: slopeA = -99999
                try:
                    slopeB = (ay2-ay1)/(ax2-ax1)
                except ZeroDivisionError:
                    slopeB = 99999
                    if ay1>ay2: slopeB = -99999

                # each entry of form [x, y, (graphA>graphB to left of intersection - i.e. A is better than B after this point, as opposed to vice versa)]
                # can have some weirdness with the vertical intersections leading to nearly-degenerate (floating point errors???) intersections
                if len(intersectionPoints) > 0:
                    if intersectionPoints[-1][0] == x:
                        if abs((intersectionPoints[-1][1] - y) / y) < 0.0001:
                            continue
                                  
                intersectionPoints.append([x,y,(slopeA<slopeB)])
                i += 1

    return intersectionPoints


def merge_intersections(intersections, analysis, obsgraphs, limits_to_plot):
    mergedIntersections = []
    # fill mergedIntersections, ie all of them anticlockwise
    for otheranalysis in limits_to_plot:
        if otheranalysis == analysis: continue

        for intersection in intersections[analysis][otheranalysis]:
            xVal = intersection[0]
            yVal = intersection[1]
            intersections_x = [inters[0] for inters in mergedIntersections]
            intersections_y = [inters[1] for inters in mergedIntersections]
            # print "   new intersection", xVal, yVal
            pos = bisect(intersections_x, xVal)

            if xVal not in intersections_x:
                pos = pos

            # I am unsure about this one
            elif pos == len(intersections_x):
                pos = pos

            # on the left edge put the higher y value first, on the right edge put it second
            elif xVal == list(obsgraphs[analysis+'_fill'].GetX())[0]:
                if yVal > intersections_y[pos]:
                    pos = pos
                else:
                    pos = pos + 1

            elif xVal == list(obsgraphs[analysis+'_fill'].GetX())[-1]:
                if yVal > intersections_y[pos]:
                    pos = pos + 1
                else:
                    pos = pos

            #this means that it was placed there by another analysis - ie the split between signal regions
            elif xVal == list(obsgraphs[otheranalysis+'_fill'].GetX())[0]:
                pos = pos
            elif xVal == list(obsgraphs[otheranalysis+'_fill'].GetX())[-1]:
                pos = pos - 1


            else:
                print "the sorting of intersections with the vertical hasn't worked"
                print analysis, otheranalysis, xVal, yVal
                print 'this analysis xvals', list(obsgraphs[analysis+'_fill'].GetX())
                print 'other analysis xvals', list(obsgraphs[otheranalysis+'_fill'].GetX())
                print 'all intersections with this analysis:', intersections[analysis][otheranalysis]
                print 'already known all intersections', intersections_x, mergedIntersections,
                sys.exit()

            mergedIntersections.insert(pos, [xVal, yVal, intersection[2], otheranalysis])

    return mergedIntersections



# Eval() is a thing and IT DOES THE SPLINES!!!!!!!!!!!! AGHHHH
# can do it all with intersections (not very necessary) and eval
# just eval every graph in range every 1% of the range (ie in log steps)
# draw each segment between evals
# easy peasy!!!!

def get_fills_with_eval(obsgraphs, limits_to_plot, intersections, curved, canv, xmin, xmax, top_gq):

    xVals = []
    # I want graph points for all
    for analysis in limits_to_plot:
        these_xs = list(obsgraphs[analysis].GetX())
        for x in these_xs:
            pos = bisect(xVals, x)
            xVals.insert(pos, x)

    # If I trust intersections, ie straight line, then that is all else I need
    if not curved:
        for analysis in limits_to_plot:
            for intersection in intersections[analysis]['all']:
                x = intersection[0]
                if x not in xVals:
                    pos = bisect(xVals, x)
                    xVals.insert(pos, x)

    # If I don't trust intersections, just ping at short intervals
    
    # get canvas coords
    nPoints = 10000
    start = canv.GetLeftMargin()
    end = 1 - canv.GetRightMargin()
    width = (end-start)/nPoints
    NDCpoints = [start + i*width for i in range(0,nPoints+1)]

    # get real coordinates
    if canv.GetLogx() == 1:
        xminlog = math.log10(xmin)
        xmaxlog = math.log10(xmax)
        width = (xmaxlog-xminlog)/nPoints
        xValslog = [xminlog + i*width for i in range(0,nPoints+1)]
        extra_xVals = [math.pow(10,x) for x in xValslog]
    else:
        width = (xmax-xmin)/nPoints
        extra_xVals = [xmin + i*width for i in range(0,nPoints+1)]

    for x in extra_xVals:
        if x not in xVals:
            pos = bisect(xVals, x)
            xVals.insert(pos, x)

    # clean duplicates, not sure why they arise but hey ho
    to_zap = []
    for i in range(1, len(xVals)):
        if abs(xVals[i]-xVals[i-1])/xVals[i-1] < 0.000001:
            to_zap.append(i)
    for i in to_zap[::-1]: # go in reverse order since I'm removing elements as I go
        del xVals[i]

    # now we have all the x values we want to Eval() at
    # for i in range(len(xVals)):
        # print i, xVals[i]

    
    # make a dictionary of the exclusions for each limit (top-down) at each mass point
    excluded_per_mass = {}
    for x in xVals:
        excluded_per_mass[x] = [[], []]

        for analysis in limits_to_plot:
            # get the x values of the points in the graph
            these_xs = list(obsgraphs[analysis].GetX())

            if x < these_xs[0] or x > these_xs[-1]:
                continue

            if curved:
                yVal = obsgraphs[analysis].Eval(x, 0, "S")
            else:
                yVal = obsgraphs[analysis].Eval(x, 0, "")

            # where in order does this line fit
            pos = bisect(excluded_per_mass[x][0], yVal)
            excluded_per_mass[x][0].insert(pos, yVal)
            excluded_per_mass[x][1].insert(pos, analysis)

        # print x, [excluded_per_mass[x][1][i] + ' ' + str(excluded_per_mass[x][0][i]) for i in range(len(excluded_per_mass[x][0]))]

        
    # now I have the dictionary. excluded_per_mass[x][0] is the "ladder" of limits, [1] is the analyses they came from
    fillareas = {}
    for analysis in limits_to_plot:
        # print analysis
        fillareas_top = []
        fillareas_bottom = []
        for i in range(len(xVals)):
            x = xVals[i]
            if analysis not in excluded_per_mass[x][1]:
                continue
            level = excluded_per_mass[x][1].index(analysis)
            fillareas_bottom.append([ x, excluded_per_mass[x][0][level] ])

            
            # if I'm at an intersection, top = bottom
            # print intersections[analysis]['all']
            isIntersection = False
            otherTakesOver = False
            for jj in range(len(intersections[analysis]['all'])):
                x_int = intersections[analysis]['all'][jj][0]
                if abs(x-x_int)/x < 0.00001:
                    isIntersection = True
                    otherTakesOver = intersections[analysis]['all'][jj][2]
                    otherAnalysis = intersections[analysis]['all'][jj][3]
                    break

            if isIntersection:
                index = level + 1
                try:
                    if excluded_per_mass[x][1][index] == otherAnalysis:
                        index = level + 2
                except:
                    index = -1

                if index >= len(excluded_per_mass[x][1]):
                    index = -1

                if otherTakesOver:
                    if index == -1:
                        fillareas_top.append([ x, top_gq ])
                    else:
                        fillareas_top.append([ x, excluded_per_mass[x][0][index] ])
                        
                        
                fillareas_top.append([ x, excluded_per_mass[x][0][level] ])
                
                if not otherTakesOver:
                    if index == -1:
                        fillareas_top.append([ x, top_gq ])
                    else:
                        fillareas_top.append([ x, excluded_per_mass[x][0][index] ])

                        
                    
            # if it's the highest one, the put the ceiling at top_gq
            elif level == len(excluded_per_mass[x][0])-1:
                fillareas_top.append([ x, top_gq ])

            # otherwise, want the point above
            else:
                
                # in the case that I am at the edge of a line, need two points for a given x
                analysisabove = excluded_per_mass[x][1][level+1]
                x_below = xVals[i-1]
                x_above = xVals[i+1]
                if analysisabove not in excluded_per_mass[x_below][1]:
                    try:
                        fillareas_top.append([ x, excluded_per_mass[x][0][level+2] ])
                    except:
                        fillareas_top.append([ x, top_gq ])
                    fillareas_top.append([ x, excluded_per_mass[x][0][level+1] ])
            
                elif analysisabove not in excluded_per_mass[x_above][1]:
                    fillareas_top.append([ x, excluded_per_mass[x][0][level+1] ])
                    try:
                        fillareas_top.append([ x, excluded_per_mass[x][0][level+2] ])
                    except:
                        fillareas_top.append([ x, top_gq ])
            

                else:
                    fillareas_top.append([ x, excluded_per_mass[x][0][level+1] ])        

                    
        # print 'bottom:', fillareas_bottom
        # print 'top:', fillareas_top
        fillarea_xs = [f[0] for f in fillareas_bottom] + [f[0] for f in fillareas_top[::-1]] + [fillareas_bottom[0][0]]
        fillarea_ys = [f[1] for f in fillareas_bottom] + [f[1] for f in fillareas_top[::-1]] + [fillareas_bottom[0][1]]
        fillareas[analysis] = ROOT.TGraph(len(fillarea_xs), array('f', fillarea_xs), array('f', fillarea_ys))

        # print list(fillareas[analysis].GetX())
        # print list(fillareas[analysis].GetY())
        
    return fillareas
