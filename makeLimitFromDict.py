

def makeLimitFromDict(limitsDictPath, entry, outname, overwrite=True):

    import sys
    from array import array

    import ROOT
    ROOT.gROOT.SetBatch(1)

    path = '/'.join(limitsDictPath.split('/')[:-1])
    sys.path.append(path)
    name = limitsDictPath.split('/')[-1].split('.py')[0]

    # import the module 'name' from path 'path' and get limitsDict[entry] from it
    thisLimitList = __import__(name).limitsDict[entry]
    print thisLimitList

    
    # now I want to make a graph
    xVals = [lim[0] for lim in thisLimitList]
    yVals = [lim[1] for lim in thisLimitList]
    xArr = array('f', xVals)
    yArr = array('f', yVals)
    gobs = ROOT.TGraph(len(xArr), xArr, yArr)

    # plot graph
    canv = ROOT.TCanvas()
    gobs.Draw()
    canv.SaveAs('intermediate_plots/'+outname+'.png')
    canv.SetLogx(1)
    canv.SetLogy(1)
    canv.SaveAs('intermediate_plots/'+outname+'_logXlogY.png')
    canv.SetLogx(0)
    canv.SetLogy(0)
    
    # write to root file
    outrootfile = ROOT.TFile('intermediate_rootfiles/'+outname+'.root','recreate')
    if overwrite:
        outrootfile = ROOT.TFile('intermediate_rootfiles/'+outname+'.root','recreate')
    else:
        outrootfile = ROOT.TFile('intermediate_rootfiles/'+outname+'.root','update')
    gobs.Write(outname+'_obs', ROOT.TObject.kOverwrite)
    # gexp.Write(outname+'_'+expname, ROOT.TObject.kOverwrite)
    outrootfile.Close()


def main():
    limitsDictPath = 'data/limits_UA2_etc/limitsDict.py'
    entry = 'CDF 1.13 fb-1 1.96 TeV 2009'
    outname = 'CDFrun2'
    makeLimitFromDict(limitsDictPath, entry, outname)

if __name__=='__main__':
    main()
