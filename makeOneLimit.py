
import ROOT
ROOT.gROOT.SetBatch(1)

class Quiet:
    """Context manager for silencing certain ROOT operations.  Usage:
    with Quiet(level = ROOT.kInfo+1):
       foo_that_makes_output
    You can set a higher or lower warning level to ignore different
    kinds of messages.  After the end of indentation, the level is set
    back to what it was previously.
    """
    def __init__(self, level=ROOT.kInfo + 1):
        self.level = level

    def __enter__(self):
        self.oldlevel = ROOT.gErrorIgnoreLevel
        ROOT.gErrorIgnoreLevel = self.level

    def __exit__(self, type, value, traceback):
        ROOT.gErrorIgnoreLevel = self.oldlevel



from prot import utiltools
from prot import canvastools
from prot import plottools

import glob
import re
import os, os.path
import numpy as np
import array
import sys

from math import *

from helpers import *


def main():
    print 'this is now deprecated, do "from makeOneLimit import makeLimit"'
    sys.exit()


def makeLimit(
        inLimits,             # the limits dictionary, eg 'data/limits2016_TLA_J75yStar03_v6.py'
        explim = 0,           # which expected limit line to plot, 0 for nominal, 1 for +1 sigma, -2 for -2 sigma, etc
        bjets = True,         # Does the theory cross-section account for b-quarks? Should usually be True, if False then theory XS is rescaled by 5/4
        acc_scaling = False,  # Should the theory cross-section be rescaled by the provided acceptance? If False, 'acc' is not checked for in dictionary
        scaleFactors = None,  # scale the XS upper limits by this much. Equivalent to 1/acc_scaling
        massrange = None,     # Restrict Z' mass range to include, in TeV. Does propagate to next stage, but can be reduced further there.
        interp = 'mR',        # What interpolation / extrapolation should be used? Must include one or both of 'mR' and 'gSM'. Default 'mR' -> finds mu=1 point for each Z' mass.
        upAndDown = True,     # extrapolate from both the lowest gq excluded point (previous default) and highest gq non-excluded point, and take the midpoint
        minPEs = 0,           # Require a minimum number of pseudoexperiments for a point to be included
        hack = False,         # Various changes, out-of-date. Leave False
        save = True,          # Save outputs? Leave True
        ATLASx = 'Internal',  # ATLAS label
        selection='',         # some text to be printed on the plot, and controls some default colouring etc. eg 'TLA, J75 y*<0.3' (doesn't propagate to next step)
        lumi = None,          # Luminosity label to be printed on the plot (doesn't propagate to next step)
        TeV = 0,              # mZ' axis labels in TeV? Default GeV
        draw_interp = True,   # Should the interpolation plots be drawn? If True, makes a set of plots in interpolation_plots/<limitDict>/
        logy = True,          # log y-axis on interpolation plots
        curvy = False,        # Should the contours be drawn with straight or curved limes? (doesn't propagate to next step)
        points = True,        # Should the results of interpolation be drawn on the plots? (doesn't propagate to next step)
        noMarkers = False,    # Should the signal points be drawn on the plot? If True, they won't be
        verbose = False,      # print verbose output

    ):

    expname = 'exp'
    extralegname = ''
    extraoutname = ''
    overwrite = True

    if explim != 0:
        overwrite = False
        updown = 'up'
        expname = 'exp+'
        if explim < 0:
            updown = 'down'
            expname = 'exp-'
        expname += str(abs(explim))
        extraoutname = '_' + str(abs(explim)) + updown
        extralegname = ' ' + str(abs(explim)) + '#sigma ' + updown

    if draw_interp:
        canv = ROOT.TCanvas('c','c',1000,1000)
        if logy:
            canv.SetLogy()

    # Options for pretty plots
    eV = 1000.0
    units = 'GeV'
    if TeV:
        eV = 1.0
        units = 'TeV'



    # make output dir
    outname = inLimits.replace('data/','').replace('.txt','')

    print("outname: ", outname)
    if "/" in outname:
        d=outname.split("/")[0]
        print("d: ", d)
        if not os.path.isdir("intermediate_plots/"+d):
            os.mkdir("intermediate_plots/"+d)

    if not os.path.isdir('intermediate_plots/'+outname):
        os.mkdir('intermediate_plots/'+outname)


    if draw_interp:
        if not os.path.isdir("interpolation_plots/"+d):
            os.mkdir("interpolation_plots/"+d)
        if not os.path.isdir('interpolation_plots/'+outname):
            os.mkdir('interpolation_plots/'+outname)



    # Load the limits
    limits={}
    fh=open(inLimits)
    print("fh: ", fh)
    orig_limits = eval(fh.read())
    fh.close()

    # make a new dictionary to sory out float vs string in input
    limit = {}
    for gq in orig_limits:
        limits[float(gq)] = {}
        for mZ in orig_limits[gq]:
            limits[float(gq)][float(mZ)] = orig_limits[gq][mZ]

    set_palette()

    # Determine bins for intermediate plots
    gSMs=set()
    mRs=set()
    ignorePoints = []
    for gSM,limitsgSM in limits.items():
        gSMs.add(gSM)
        for mR,limit in limitsgSM.items():
            if massrange != None:
                if mR > massrange[1]:
                    continue
            if 'nPEs' in limit:
                if limit['nPEs'] < minPEs:
                    if verbose: print "skipping", gSM, mR, "because fails nPEs >", minPEs, "with nPEs =", limit['nPEs']
                    ignorePoints.append((mR,gSM))
                    continue
            mRs.add(mR)
    gSMs=sorted(list(gSMs))
    mRs =sorted(list(mRs))


    minmR ,maxmR ,nbinsmR =bestbins(mRs)
    mingSM,maxgSM,nbinsgSM=bestbins(gSMs)

    print 'mRs =', mRs
    print 'gSMs =', gSMs
    print 'ignorePoints =', ignorePoints
    print 'plot params', minmR, maxmR, mingSM, maxgSM


    # b-jet scaling
    print "b-jets in theory XS?", bjets
    print "acceptance in theory XS?", acc_scaling
    print "scaling by imported scale factors?", (scaleFactors == None)

    # Make plot
    ratplots={}

    # h2=ROOT.TH2F('excl',"Exclusion Plot;m_{Z'} ["+units+"];g_{q}",
                 # nbinsmR,minmR*eV,maxmR*eV,
                 # nbinsgSM,float('%0.3g'%mingSM),float('%0.3g'%maxgSM))

    # h2=ROOT.TH2F('excl',"Exclusion Plot;m_{Z'} ["+units+"];g_{q}",
                 # nbinsmR,1.4*eV,3.6*eV,
                 # nbinsgSM,0.04,0.31)

    h2=ROOT.TH2F('excl'+extraoutname+selection,"Exclusion Plot;m_{Z'} ["+units+"];g_{q}",
                 nbinsmR,minmR*eV,maxmR*eV,
                 nbinsgSM, 0, 0.5)

    mRfirst = {}
    gSMfirst = {}

    for gSM,limitsgSM in limits.items():
        for mR,limit in limitsgSM.items():

            if (mR,gSM) in ignorePoints:
                continue

            xslim_obs=limit.get('obs'   ,None)
            xslim_exp=limit.get(expname   ,None)
            try:
                xstheory =limit.get('theory',None)
            except Exception as e:
                print("can't findtheory line")
                print("error:", e)

            # scale theory XS to account for bjets and acceptance, if required
            if acc_scaling:
                acc = limit.get('acc',None)
                xstheory = xstheory * acc
            if not bjets:
                xstheory = xstheory * 1.25

            if scaleFactors != None:
                try:
                    thisSF = scaleFactors[mR]
                except:
                    print 'could not find SF for mR = ', mR
                    thisSF = 1
                xslim_obs = xslim_obs * thisSF
                xslim_exp = xslim_exp * thisSF

            binIdx=h2.FindBin(mR*eV,gSM)
            if xslim_obs==None:
                h2.SetBinContent(binIdx,0)
                continue
            print("xslim_obs: ", xslim_obs)
            print("xstheory: ", xstheory)
            if xstheory==None:
                continue
            h2.SetBinContent(binIdx, xslim_obs/xstheory)
            ratplots[(mR,gSM)]=(xslim_obs/xstheory, xslim_exp/xstheory)

            if mR not in mRfirst:
                mRfirst[mR] = {}
            mRfirst[mR][gSM] = {
                'xslim_obs': xslim_obs,
                'xslim_exp': xslim_exp,
                'xs_theory': xstheory,
                'mu_obs'   : xslim_obs/xstheory,
                'mu_exp'   : xslim_exp/xstheory
                }

            if gSM not in gSMfirst:
                gSMfirst[gSM] = {}
            gSMfirst[gSM][mR] = {
                'xslim_obs': xslim_obs,
                'xslim_exp': xslim_exp,
                'xs_theory': xstheory,
                'mu_obs'   : xslim_obs/xstheory,
                'mu_exp'   : xslim_exp/xstheory
                }



    limitpoints_obs = {}
    limitpoints_exp = {}

    usedPoints = {}

    if 'mR' in interp:
        usedPoints['mR'] = {}
        for mR in mRs:
            # only consider points in a certain range (i.e. for TLA y*<0.3 and y*<0.6 regions)
            if massrange != None:
                if mR < massrange[0] or mR > massrange[1]: continue

            # Get interesting point, where interesting = first bin where limit is mu < 1
            # do separately for exp and obs

            gSMrefObsLow = None
            muref_obs_low = None
            print(mRfirst)
            try:
                print(mRfirst[mR])
            except Exception as e:
                print("this mass point may not exist")
                print(e)
                #mRfirst.pop(mR)
                continue

            for gSM in sorted(mRfirst[mR]):
                mu = mRfirst[mR][gSM]['mu_obs']
                if verbose:
                    print '  gSM:', gSM, '- muObs:', mu
                gSMrefObs = gSM
                muref_obs = mu
                if mu < 1:
                    if upAndDown:
                        index = sorted(mRfirst[mR]).index(gSM)-1
                        if index >= 0:
                            gSMrefObsLow = sorted(mRfirst[mR])[index]
                            muref_obs_low = mRfirst[mR][gSMrefObsLow]['mu_obs']
                    break

            gSMrefExpLow = None
            muref_exp_low = None
            for gSM in sorted(mRfirst[mR]):
                mu = mRfirst[mR][gSM]['mu_exp']
                if verbose:
                    print '  gSM:', gSM, '- muExp:', mu
                gSMrefExp = gSM
                muref_exp = mu
                if mu < 1:
                    if upAndDown:
                        index = sorted(mRfirst[mR]).index(gSM)-1
                        if index >= 0:
                            gSMrefExpLow = sorted(mRfirst[mR])[index]
                            muref_exp_low = mRfirst[mR][gSMrefExpLow]['mu_exp']
                    break


            if verbose:
                print "mR, gSMrefObs, mu_obs, (low, low) =", mR, gSMrefObs, muref_obs, gSMrefObsLow, muref_obs_low
                print "mR, gSMrefExp, mu_exp, (low, low) =", mR, gSMrefExp, muref_exp, gSMrefExpLow, muref_exp_low


            # get gSMobs
            xsref = mRfirst[mR][gSMrefObs]['xs_theory']
            xsobs = mRfirst[mR][gSMrefObs]['xslim_obs']
            gSMobs=0
            if xsobs==None:
                continue
            else:
                gSMobs=sqrt(gSMrefObs**2*xsobs/xsref)

            gSMobs_low = None
            if upAndDown and gSMrefObsLow != None:
                xsref_low = mRfirst[mR][gSMrefObsLow]['xs_theory']
                xsobs_low = mRfirst[mR][gSMrefObsLow]['xslim_obs']
                gSMobs_low = sqrt(gSMrefObsLow**2 * xsobs_low / xsref_low)

            # get gSMexp
            xsref = mRfirst[mR][gSMrefExp]['xs_theory']
            xsexp = mRfirst[mR][gSMrefExp]['xslim_exp']
            gSMexp=0
            if xsobs==None:
                continue
            else:
                gSMexp=sqrt(gSMrefExp**2*xsexp/xsref)

            gSMexp_low = None
            if upAndDown and gSMrefExpLow != None:
                xsref_low = mRfirst[mR][gSMrefExpLow]['xs_theory']
                xsexp_low = mRfirst[mR][gSMrefExpLow]['xslim_exp']
                gSMexp_low = sqrt(gSMrefExpLow**2 * xsexp_low / xsref_low)


            if verbose:
                print 'xsref, xsobs, xsexp, gSMobs, gSMexp', xsref, xsobs, xsexp, gSMobs, gSMexp
                if upAndDown:
                    print '  low: xsref, xsobs, xsexp, gSMobs, gSMexp', xsref_low, xsobs_low, xsexp_low, gSMobs_low, gSMexp_low

            usedPoints['mR'][mR] = {'exp': gSMrefExp, 'obs': gSMrefObs}
            limitpoints_obs[mR*eV] = gSMobs
            limitpoints_exp[mR*eV] = gSMexp

            if upAndDown:
                usedPoints['mR'][mR]['exp_low'] = gSMrefExpLow
                usedPoints['mR'][mR]['obs_low'] = gSMrefObsLow
                if gSMobs_low != None:
                    # 1. take average from low and high extrapolations
                    limitpoints_obs[mR*eV] = 0.5 * (gSMobs + gSMobs_low)
                    # 2. don't let it be below a not excluded point
                    limitpoints_obs[mR*eV] = max(gSMrefObsLow, limitpoints_obs[mR*eV])
                if gSMexp_low != None:
                    limitpoints_exp[mR*eV] = 0.5 * (gSMexp + gSMexp_low)
                    limitpoints_exp[mR*eV] = max(gSMrefExpLow, limitpoints_exp[mR*eV])
            else:
                usedPoints['mR'][mR]['exp_low'] = None
                usedPoints['mR'][mR]['obs_low'] = None


            # now to make the mR interpolation plots

            if draw_interp:

                thesegSMs = sorted(mRfirst[mR].keys())
                hist = ROOT.TH1F('h'+str(mR),"m_{Z'} = "+str(mR)+' TeV;g;#mu', 80, 0, 0.4)

                gSM_arr = array.array('f',thesegSMs)
                mus_obs = array.array('f',[mRfirst[mR][gSM]['mu_obs'] for gSM in thesegSMs])
                mus_exp = array.array('f',[mRfirst[mR][gSM]['mu_exp'] for gSM in thesegSMs])
                # hist.SetMaximum(max(mus_obs + mus_exp)*1.1)
                # hist.SetMinimum(min(mus_obs + mus_exp)*0.9)
                # hist.SetMaximum(3)
                # hist.SetMinimum(0)
                if logy:
                    hist.SetMaximum(10)
                    hist.SetMinimum(0.05)
                else:
                    hist.SetMaximum(2)
                    hist.SetMinimum(0)

                hist.SetStats(0)
                hist.Draw()

                l = ROOT.TLegend(0.5,0.5,0.89,0.89)
                l.SetHeader('m_{Z} = '+str(mR)+ ' TeV' + extralegname)

                if verbose:
                    print 'thesegSMs =', thesegSMs
                    print 'mus_obs =', mus_obs


                funcstr_exp = "("+str(gSMrefExp)+"/x)**2 * "+str(muref_exp)
                funcExp = ROOT.TF1("fexp", funcstr_exp)
                funcExp.SetLineColor(ROOT.kRed)
                funcExp.SetLineWidth(2)
                funcExp.SetLineStyle(ROOT.kDashed)
                funcExp.Draw("same")
                l.AddEntry(funcExp, "exp extrapolation", "L")

                funcstr_obs = "("+str(gSMrefObs)+"/x)**2 * "+str(muref_obs)
                funcObs = ROOT.TF1("fobs", funcstr_obs)
                funcObs.SetLineColor(ROOT.kRed)
                funcObs.SetLineWidth(2)
                funcObs.Draw("same")
                l.AddEntry(funcObs, "obs extrapolation", "L")

                dummy = ROOT.TMarker()
                dummy.SetMarkerColor(ROOT.kWhite)
                dummy.SetMarkerStyle(ROOT.kOpenCircle)
                dummy.SetMarkerSize(0)
                l.AddEntry(dummy, "#kern[-0.19]{ext: #mu(g) = (g_{0}/g)^{2} #times (#sigma_{lim} / #sigma_{theory}(g_{0}))}", "P")

                if upAndDown:
                    if gSMrefExpLow != None:
                        funcstr_exp = "("+str(gSMrefExpLow)+"/x)**2 * "+str(muref_exp_low)
                        funcExpLow = ROOT.TF1("fexp_low", funcstr_exp)
                        funcExpLow.SetLineColor(ROOT.kBlue)
                        funcExpLow.SetLineWidth(2)
                        funcExpLow.SetLineStyle(ROOT.kDashed)
                        funcExpLow.Draw("same")
                        l.AddEntry(funcExpLow, "low g_{0}", "L")
                    if gSMrefObsLow != None:
                        funcstr_obs = "("+str(gSMrefObsLow)+"/x)**2 * "+str(muref_obs_low)
                        funcObsLow = ROOT.TF1("fobs_low", funcstr_obs)
                        funcObsLow.SetLineColor(ROOT.kBlue)
                        funcObsLow.SetLineWidth(2)
                        funcObsLow.Draw("same")
                        if gSMrefExpLow == None:
                            l.AddEntry(funcObsLow, "low g_{0}", "L")

                gr_muexp = ROOT.TGraph(len(mus_exp), gSM_arr, mus_exp)
                gr_muexp.SetLineWidth(2)
                gr_muexp.SetLineStyle(ROOT.kDashed)
                gr_muexp.SetMarkerStyle(ROOT.kOpenCircle)
                gr_muexp.Draw('LP same')
                l.AddEntry(gr_muexp, "exp points", "LP")

                gr_muobs = ROOT.TGraph(len(mus_obs), gSM_arr, mus_obs)
                gr_muobs.SetLineWidth(2)
                gr_muobs.SetMarkerStyle(ROOT.kFullCircle)
                gr_muobs.Draw('LP same')
                l.AddEntry(gr_muobs, "obs points", "LP")


                markerExp = ROOT.TMarker(limitpoints_exp[mR*eV], 1, ROOT.kOpenCross)
                markerExp.SetMarkerColor(ROOT.kRed)
                markerExp.SetMarkerSize(2)
                markerExp.Draw()

                markerObs = ROOT.TMarker(limitpoints_obs[mR*eV], 1, ROOT.kFullCross)
                markerObs.SetMarkerColor(ROOT.kRed)
                markerObs.SetMarkerSize(2)
                markerObs.Draw()
                l.AddEntry(markerObs, "g (#mu = 1) extrapolated", "P")

                refMarkerExp = ROOT.TMarker(gSMrefExp, muref_exp, ROOT.kFullCircle)
                refMarkerExp.SetMarkerColor(ROOT.kRed)
                refMarkerExp.SetMarkerSize(1.5) # was 1.2
                refMarkerExp.Draw()
                l.AddEntry(refMarkerExp, "g_{0}", "P")

                refMarkerObs = ROOT.TMarker(gSMrefObs, muref_obs, ROOT.kFullCircle)
                refMarkerObs.SetMarkerColor(ROOT.kRed)
                refMarkerObs.SetMarkerSize(1.5) # was 1.2
                refMarkerObs.Draw()

                if upAndDown:
                    if gSMrefExpLow != None:
                        refMarkerExpLow = ROOT.TMarker(gSMrefExpLow, muref_exp_low, ROOT.kFullCircle)
                        refMarkerExpLow.SetMarkerColor(ROOT.kBlue)
                        refMarkerExpLow.SetMarkerSize(1.5)
                        refMarkerExpLow.Draw()
                    if gSMrefObsLow != None:
                        refMarkerObsLow = ROOT.TMarker(gSMrefObsLow, muref_obs_low, ROOT.kFullCircle)
                        refMarkerObsLow.SetMarkerColor(ROOT.kBlue)
                        refMarkerObsLow.SetMarkerSize(1.5)
                        refMarkerObsLow.Draw()


                l.SetTextFont(42)
                l.SetTextSize(0.03)
                l.SetBorderSize(0)
                l.Draw()

                outplotname = 'interpolation_plots/'+outname+'/mu_vs_gSM_mR'+str(mR)+extraoutname
                with Quiet():
                    canv.SaveAs(outplotname + '.png')
                    canv.SaveAs(outplotname + '.pdf')




    # now do the same for gSM
    if 'gSM' in interp:
        gSMpoints = []

        for gSM in gSMs:

            # Get points where obs and exp cross theory
            # print "finding points for gSM =", gSM
            # print gSMfirst[gSM]

            lastobs = None
            lastexp = None
            lastthe = None
            lastmR = None

            first = True

            for mR in sorted(gSMfirst[gSM]):
                if massrange != None:
                    if mR < massrange[0] or mR > massrange[1]: continue

                # limit is where theory and obs/exp cross, log interpolation
                limit = limits.get(gSM,{}).get(mR,{})

                if first:
                    lastobs = limit['obs']
                    lastexp = limit[expname]
                    lastthe = limit['theory']
                    lastmR = mR
                    first = False
                    continue

                xsobs=limit['obs']
                xsexp=limit[expname]
                xsthe=limit['theory']

                if (xsobs > xsthe and lastobs < lastthe) or (xsobs < xsthe and lastobs > lastthe):
                    # lim = where lines of log(XS) cross
                    y1 = log(lastthe)
                    y2 = log(xsthe)
                    y3 = log(lastobs)
                    y4 = log(xsobs)
                    x1 = lastmR
                    x2 = mR

                    m1 = (y2-y1)/(x2-x1)
                    m2 = (y4-y3)/(x2-x1)

                    X = x1 + (y3-y1)/(m1-m2)
                    limitpoints_obs[X*eV] = gSM

                if (xsexp > xsthe and lastexp < lastthe) or (xsexp < xsthe and lastexp > lastthe):
                    # lim = where lines of log(XS) cross
                    y1 = log(lastthe)
                    y2 = log(xsthe)
                    y3 = log(lastexp)
                    y4 = log(xsexp)
                    x1 = lastmR
                    x2 = mR

                    m1 = (y2-y1)/(x2-x1)
                    m2 = (y4-y3)/(x2-x1)

                    X = x1 + (y3-y1)/(m1-m2)
                    limitpoints_exp[X*eV] = gSM

                lastobs = xsobs
                lastexp = xsexp
                lastthe = xsthe
                lastmR = mR


    if hack:
        print "I am hacking a mass point in the observed limit plot"
        # limitpoints_obs[3.3*eV] = 0.265

        # limitpoints_obs[3.17*eV] = 0.2592
        # limitpoints_obs[3.33*eV] = 0.265

        limitpoints_obs[3.1*eV] = 0.259
        limitpoints_obs[3.2*eV] = 0.264
        limitpoints_obs[3.4*eV] = 0.2675

        # limitpoints_obs[3.33*eV] = 0.265

    # Make graph
    gobs=ROOT.TGraph()
    gidx=0
    for mass in sorted(limitpoints_obs):
        gobs.SetPoint(gidx,mass,limitpoints_obs[mass])
        gidx += 1

    gexp=ROOT.TGraph()
    gidx=0
    for mass in sorted(limitpoints_exp):
        gexp.SetPoint(gidx,mass,limitpoints_exp[mass])
        gidx += 1

    if verbose:
        print "observed:"
        gobs.Print("all")
        print "expected:"
        gexp.Print("all")


    ###################################################
    # Now to make individual plots for cross-checking #
    ###################################################

    FIATLAS=ROOT.TColor.CreateGradientColorTable(2,
                                                 np.array([0.00, 1.00],'d'),
                                                 np.array([1.00, 1.00],'d'),
                                                 np.array([1.00, 1.00],'d'),
                                                 np.array([1.00, 1.00],'d'),
                                                 255
                                                 )

    ATPallete=array.array('i',range(FIATLAS,FIATLAS+255))

    ROOT.gStyle.SetPalette(255,ATPallete)

    c1=canvastools.canvas()
    c1.Clear()

    legend = ROOT.TLegend(0.58,0.6,0.85,0.9)
    legend.SetFillColorAlpha(ROOT.kWhite,0.8)
    legend.SetTextFont(42)
    legend.SetTextSize(0.05)
    legend.SetTextSize(0.04) # with "95% CL upper" added

    # h2.GetXaxis().SetRangeUser(200,3500)
    # plottools.plot2d(h2,text={'title':selection,'lumi':lumi,'sim':False},textpos=(0.19,0.8),zrange=(0,2),ztitle='#sigma_{obs. limit}/#sigma_{theory}')
    plottools.plot2d(h2,text={'IntPrelim': ATLASx, 'title': '','lumi':lumi,'sim':False},textpos=(0.2,0.78),zrange=(0,2),ztitle='#sigma_{obs. limit}/#sigma_{theory}')
    # plottools.plot2d(h2,text={'title': '','lumi':lumi,'sim':False},textpos=(0.2,0.78),zrange=(0,2),ztitle='#sigma_{obs. limit}/#sigma_{theory}')


    textToDraw = selection
    if textToDraw != '':
        Tl=ROOT.TLatex()
        Tl.SetTextSize(0.04)
        Tl.DrawLatexNDC(0.2,0.72, textToDraw)

    lineWidth = 100*5 + 5
    lineWidth = 2

    drawoption = 'SAME'
    if curvy: drawoption = 'C '+drawoption
    if points:
        drawoption = 'P'+drawoption
        if not curvy:
            drawoption = 'L'+drawoption



    colname = 'TLA'
    if 'TLA' in selection or 'tla' in selection:
        colname = 'TLA'
    elif 'ISR' in selection or 'isr' in selection:
        if 'gamma' in selection:
            colname = 'jjg'
        if '(j' in selection:
            colname = 'jjj'
    elif 'Dijet' in selection or 'dijet' in selection:
        colname = 'dijet'

    col = cols[colname]

    gobs.SetMarkerSize(0.5)
    gobs.SetMarkerColor(col)
    gobs.SetMarkerStyle(ROOT.kFullCircle)
    gobs.Draw(drawoption)
    gobs.SetLineWidth(2)
    gobs.SetLineColor(col)

    gexp.SetMarkerSize(0.5)
    gexp.SetMarkerColor(col)
    gexp.SetMarkerStyle(ROOT.kOpenCircle)
    gexp.SetLineColor(col)
    gexp.SetLineWidth(lineWidth)
    gexp.Draw(drawoption)
    gexp.SetLineStyle(ROOT.kDashed)

    ROOT.gStyle.SetHatchesLineWidth(2)


    if not noMarkers:

        signalPoints = None
        markMorphing = False
        if 'TLA' in selection:
            markMorphing = True
            sys.path.append('data/TLApoints')
            from TLAsignalPoints import points as signalPoints

        markers = []
        for (mR,gSM),(xsobs,xsexp) in ratplots.items():
            isMorphed = False
            if signalPoints != None:
                if (mR,gSM) not in signalPoints:
                    isMorphed = True

            markerSize = 1.2
            if isMorphed:
                markerSize = 0.9

            marker = ROOT.TMarker(mR*eV, gSM, ROOT.kOpenCircle)
            marker.SetMarkerSize(markerSize)
            marker.SetMarkerColor(ROOT.kGray+2)
            markers.append(marker)
            markers[-1].Draw()

            # highlight ones used in interpolation
            if mR in usedPoints['mR']:
                if gSM in [usedPoints['mR'][mR]['obs'], usedPoints['mR'][mR]['obs_low']]:
                    markerUsedObs = ROOT.TMarker(mR*eV, gSM, ROOT.kFullCircle)
                    markerUsedObs.SetMarkerSize(markerSize)
                    markerUsedObs.SetMarkerColor(ROOT.kGray+2)
                    markers.append(markerUsedObs)
                    markers[-1].Draw()
                if gSM in [usedPoints['mR'][mR]['exp'], usedPoints['mR'][mR]['exp_low']]:
                    markerUsedExp = ROOT.TMarker(mR*eV, gSM, ROOT.kOpenCircle)
                    markerUsedExp.SetMarkerColor(ROOT.kRed+1)
                    markerUsedExp.SetMarkerSize(markerSize)
                    markers.append(markerUsedExp)
                    markers[-1].Draw()
                    markerUsedExp2 = ROOT.TMarker(mR*eV, gSM, ROOT.kOpenCircle)
                    markerUsedExp2.SetMarkerSize(markerSize)
                    markerUsedExp2.SetMarkerColor(ROOT.kRed+1)
                    markerUsedExp2.SetMarkerSize(markers[-1].GetMarkerSize()*0.9)
                    markers.append(markerUsedExp2)
                    markers[-1].Draw()


            if isMorphed:
                morphedMarker = marker
            else:
                normalMarker = marker
                try:     normalMarkerUsedExp = markerUsedExp
                except:  pass
                try:     normalMarkerUsedObs = markerUsedObs
                except:  pass

    # legend.AddEntry(gobs, "Observed limit" ,"L")
    # legend.AddEntry(gexp, "Expected limit", "LF")
    if points:
        legoption = "PL"
    else:
        legoption = "L"
    legend.AddEntry(gobs, "Observed limit" , legoption)
    legend.AddEntry(gexp, "Expected limit" + extralegname, legoption)

    if not noMarkers:
        legend.AddEntry(normalMarker, "Signal points", "P")
        if markMorphing:
            legend.AddEntry(morphedMarker, "Morphed points", "P")
        try:
            legend.AddEntry(normalMarkerUsedObs, "Points used for obs limit", "P")
            legend.AddEntry(normalMarkerUsedExp, "Points used for exp limit", "P")
        except:
            pass

    legend.Draw()
    legend.SetBorderSize(0)


    utiltools.store.append(gobs)
    utiltools.store.append(gexp)


    ROOT.gPad.RedrawAxis()

    outplotname = outname + '/' + 'limit' + extraoutname
    # name to match dijet paper
    if not save and 'limits_dijets_full2016' in outname:
        outplotname = outplotname.replace('limits_dijets_full2016','ZPrime_FancyPlot')

    if noMarkers:
        outplotname += '_noMarkers'
    if 'gSM' in interp:
        outplotname += '_interpX'
    if hack:
        outplotname += '_hack'
    if not curvy:
        outplotname += '_noC'
    if points:
        outplotname += '_P'
    if ATLASx=='Preliminary':
        outplotname += '_prelim'
    if ATLASx=='':
        outplotname += '_final'

    with Quiet():
        canvastools.save('intermediate_plots/'+outplotname+'.png')

    #############################################################
    # Done with making individual plots, now save to root file  #
    #############################################################

    if save:

        d=outname.split("/")[0]
        d2=outname.split("/")[1]

        print("d: ", d)
        if not os.path.isdir("intermediate_rootfiles/"+d):
            os.mkdir("intermediate_rootfiles/"+d)

        #if not os.path.isdir('intermediate_rootfiles/'+outname):
        #    os.mkdir('intermediate_rootfiles/'+outname)
        if overwrite:

            outrootfile = ROOT.TFile('intermediate_rootfiles/'+outname[:-3]+'.root','recreate')

            outrootfile.cd()
        else:
            outrootfile = ROOT.TFile('intermediate_rootfiles/'+outname[:-3]+'.root','update')
            outrootfile.cd()

        gobs.Write(d2[:-3]+'_obs', ROOT.TObject.kOverwrite)
        print ("writing "+ outname+"_obs")
        print("outrootName: ", outrootfile)
        gexp.Write(d2[:-3]+'_'+expname, ROOT.TObject.kOverwrite)

        #gobs.Write(outname+'_obs')
        #gexp.Write(outname+'_'+expname)

        #gobs.Write()
        #gexp.Write()

        outrootfile.Close()

if __name__ == '__main__':
    main(sys.argv)
