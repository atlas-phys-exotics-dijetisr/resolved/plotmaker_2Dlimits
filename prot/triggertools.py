import ROOT

from prot import canvastools
from prot import utiltools
from prot import style

def trigeff(h_pass,h_total,**kwargs):
    h_pass =utiltools.Get(h_pass)
    h_total=utiltools.Get(h_total)

    fitrange=(h_total.GetXaxis().GetXmin(),h_total.GetXaxis().GetXmax())
    fitrange=kwargs.get('xrange',fitrange)    
    fitrange=kwargs.get('fitrange',fitrange)

    fermi=ROOT.TF1('fermi','1./(1.+exp(([0]-x)/[1]))',fitrange[0],fitrange[1])
    print(fitrange)
    fermi.SetParameter(0,110)
    fermi.SetParameter(1,1)
    fermi.SetLineColor(ROOT.kRed)
    
    c1=canvastools.canvas()

    eff = ROOT.TEfficiency(h_pass,h_total);

    # Initial guesses from simple fit
    g=eff.CreateGraph()
    g.Fit(fermi,'BN')
    eff.Fit(fermi,'r')
    
    style.style.apply_style(g,kwargs,'trigeff')
    g.Draw("AP")
    fermi.DrawCopy("SAME")

    xeff=fermi.GetX(0.99)
    print(xeff)

    utiltools.store.append(eff)
    c1.SaveAs('test.pdf')
