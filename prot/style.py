import ROOT
import os.path

from prot import utiltools

dir=os.path.dirname(__file__)
ROOT.gROOT.LoadMacro("%s/atlasstyle/AtlasStyle.C"%dir)
ROOT.gROOT.LoadMacro("%s/atlasstyle/AtlasUtils.C"%dir)
ROOT.SetAtlasStyle()

palette=[ROOT.kRed,ROOT.kGreen+3,ROOT.kBlue,ROOT.kYellow+1,ROOT.kMagenta,ROOT.kAzure+1,ROOT.kOrange+9,ROOT.kGreen-2,ROOT.kRed-1]

def ATLAS(title=[],xlabel=0.2,ylabel=0.2,fontsize=0.05,lumi=None,sim=True, ATLAS_x='Internal'):

    text=['#font[72]{ATLAS} '+ATLAS_x]

    if sim:
        text.append("Simulation, #sqrt{s} = 13 TeV")
    else:
        text.append("#sqrt{s} = 13 TeV")
    if lumi!=None and type(lumi)==str: text[-1]+=", "+lumi+" fb^{-1}"
    if lumi!=None and type(lumi)==float: text[-1]+=', %0.2g fb^{-1}'%lumi
    if type(title)==list: text+=title
    elif title!=None: text.append(title)

    latext=None
    for i in range(len(text)):
        if latext==None: latext=text[i]
        else: latext='#splitline{%s}{%s}'%(latext,text[i])

    Tl=ROOT.TLatex()
    #Tl.SetTextFont(43); Tl.SetTextSize(20);
    Tl.SetNDC()
    Tl.DrawLatex(xlabel, ylabel, latext);
    utiltools.store.append(Tl)

def textbox(text,xlabel=0.2,ylabel=0.2,color=ROOT.kBlack,size=0.04):
    if type(text)!=list:
        ROOT.myText(xlabel,ylabel-0.06,color,size,text)
    else:
        for line in text:
            ylabel-=0.06
            ROOT.myText(xlabel,ylabel,color,size,line)

def extract_extra_info(input):
    parts=input.split(':')
    input=parts[0]
    extrainfo={}
    for extra in parts[1:]:
        extraparts=extra.split('=')
        key=extraparts[0]
        value='='.join(extraparts[1:])
        extrainfo[key]=value
    return input,extrainfo

def torange(data):
    if type(data)==str:
        return [float(x) for x in data.split(':')]
    return data

def toposition(data):
    if type(data)==str:
        return [float(x) for x in data.split(',')]
    return data

def kwargs_canvas(kwargs):
    kwargs=dict(kwargs)
    if 'fillcolor' in kwargs: del kwargs['fillcolor']
    return kwargs

class Style:
    def __init__(self,infile=None):
        self.data={}

        if infile!=None: self.parse(infile)

    def parse(self,infile):
        fh=open(infile)
        category=None
        for line in fh:
            line=line.strip()
            if line=='': continue
            if line[0]=='#': continue

            # New category
            if line[0]=='[':
                name=line[1:-1]
                if name not in self.data: self.data[name]={}
                category=self.data[name]
                continue

            # Load into current category
            parts=line.split('=')
            key=parts[0]
            value='='.join(parts[1:])

            try:
                category[key]=eval(value)
            except SyntaxError:
                category[key]=value

    def attr(self,name,extra,attr,default=None,thetype=None):
        data=self.get_data(extra,name)
        if attr in data:
            if thetype=='range':
                return [float(x) for x in data[attr].split(':')]
            elif thetype!=None: # basic type
                return thetype(data[attr])
            else: # Existing type
                return data[attr]

        else:
            return default

    def exists(self,name):
        return name in self.data

    def get_data(self,extra,name=None):
        data={}
        if 'style' in extra and extra['style'] in self.data:
            data.update(self.data[extra['style']])
        elif name!=None:
            if name in self.data:
                data.update(self.data[name])

        data.update(extra)

        # Close categories
        if 'color' in data:
            if 'linecolor' not in data: data['linecolor']=data['color']
            if 'markercolor' not in data: data['markercolor']=data['color']

        return data
        
    def apply_style(self,obj,extra={},name=None):
        data=self.get_data(extra,name)

        if obj.InheritsFrom(ROOT.TVirtualPad.Class()):
            self.apply_style_TVirtualPad(obj,extra,name)

        if obj.InheritsFrom(ROOT.TAttLine.Class()):
            self.apply_style_TAttLine(obj,extra,name)

        if obj.InheritsFrom(ROOT.TAttMarker.Class()):
            self.apply_style_TAttMarker(obj,extra,name)

        if obj.InheritsFrom(ROOT.TAttFill.Class()):
            self.apply_style_TAttFill(obj,extra,name)

        if obj.InheritsFrom(ROOT.TH1.Class()):
            self.apply_style_TH1(obj,extra,name)

        if obj.InheritsFrom(ROOT.TH2.Class()):
            self.apply_style_TH2(obj,extra,name)

        if obj.InheritsFrom(ROOT.THStack.Class()):
            self.apply_style_THStack(obj,extra,name)

        if obj.InheritsFrom(ROOT.TGraph.Class()):
            self.apply_style_TGraph(obj,extra,name)

        if obj.InheritsFrom(ROOT.TMultiGraph.Class()):
            self.apply_style_TMultiGraph(obj,extra,name)

        if 'title' in data and obj.InheritsFrom("TNamed"):
            obj.SetTitle(data['title'])

    def apply_style_TVirtualPad(self,pad,extra,name=None):
        data=self.get_data(extra,name)

        pad.SetLogy(bool(data.get('logy',False)))
        pad.SetLogz(bool(data.get('logz',False)))

    def apply_style_TAttLine(self,line,extra,name=None):
        data=self.get_data(extra,name)

        if 'linecolor' in data:
            line.SetLineColor(data['linecolor'])

        if 'linestyle' in data:
            line.SetLineStyle(data['linestyle'])

        if 'linewidth' in data:
            line.SetLineWidth(data['linewidth'])

    def apply_style_TAttMarker(self,marker,extra,name=None):
        data=self.get_data(extra,name)

        if 'markercolor' in data:
            marker.SetMarkerColor(data['markercolor'])

        if 'markerstyle' in data:
            marker.SetMarkerStyle(data['markerstyle'])

        if 'markersize' in data:
            marker.SetMarkerSize(data['markersize'])

    def apply_style_TAttFill(self,fill,extra,name=None):
        data=self.get_data(extra,name)

        if 'fillcolor' in data:
            fill.SetFillColor(data['fillcolor'])

        if 'fillstyle' in data:
            fill.SetFillStyle(data['fillstyle'])

    def apply_style_TH1(self,hist,extra,name=None):
        data=self.get_data(extra,name)

        if 'scale' in data:
            hist.Scale(data['scale'])

        if data.get('dijetard',False):
            for binIdx in range(1,hist.GetNbinsX()+1):
                width=hist.GetBinWidth(binIdx)
                hist.SetBinContent(binIdx,hist  .GetBinContent(binIdx)/width)
                hist.SetBinError  (binIdx,hist  .GetBinError  (binIdx)/width)                
            data['nounits']=True
            data['ytitle']=data.get('ytitle',hist.GetYaxis().GetTitle())+' [ / GeV]'

        if 'xtitle' in data:
            hist.GetXaxis().SetTitle(data['xtitle'])

        if 'xrange' in data:
            hist.GetXaxis().SetRangeUser(*torange(data['xrange']))

        if 'ytitle' in data:
            hist.GetYaxis().SetTitle(data['ytitle'])

#        if 'yrange' in data:
#            yrange=torange(data['yrange'])
#            if yrange[0]!=None: hist.SetMinimum(yrange[0])
#            if yrange[1]!=None: hist.SetMaximum(yrange[1])

        
        # units
        if not hist.InheritsFrom(ROOT.TH2.Class()) and not data.get('nounits',False): # protection
            xtitle=hist.GetXaxis().GetTitle()
            defxunits=''
            if xtitle.endswith(']'):
                defxunits=xtitle.split('[')[-1][:-1]

            ytitle=hist.GetYaxis().GetTitle()
            units=[data.get('xunits',defxunits),data.get('yunits','')]
            if ytitle.endswith(']'): # already have some units
                parts=ytitle.split('[')

                oldunits=parts[-1][:-1]
                oldunits=oldunits.split('/')
                if len(oldunits)<2: oldunits=(oldunits[0],'')
                oldunits=[' '.join(oldunits[1].strip().split()[1:]),oldunits[0].strip()]

                units[1]=oldunits[1] if oldunits[1]!='' else units[1]
                units[0]=oldunits[0] if oldunits[0]!='' else units[0]
                ytitle='['.join(parts[:-1]).strip()

            # the unit settings
            width=hist.GetBinWidth(1)
            hist.GetYaxis().SetTitle('%s [%s / %0.3g %s]'%(ytitle,units[1],width,units[0]))

        if 'normalize' in data:
            integral=hist.Integral()
            if integral>0: hist.Scale(1./integral)
            hist.GetYaxis().SetTitle("a.u.")

    def apply_style_TH2(self,hist,extra,name=None):
        data=self.get_data(extra,name)

        if 'ztitle' in data:
            hist.GetZaxis().SetTitle(data['ztitle'])

        if 'zrange' in data:
            hist.GetZaxis().SetRangeUser(*data['zrange'])

    def apply_style_THStack(self,hstack,extra,name=None):
        data=self.get_data(extra,name)

        if 'xtitle' in data:
            hstack.GetXaxis().SetTitle(data['xtitle'])

        if 'xrange' in data:
            hstack.GetXaxis().SetRangeUser(*torange(data['xrange']))

        if 'ytitle' in data:
            hstack.GetYaxis().SetTitle(data['ytitle'])

        if 'yrange' in data:
            yrange=torange(data['yrange'])
            if yrange[0]!=None: hstack.SetMinimum(yrange[0])
            if yrange[1]!=None: hstack.SetMaximum(yrange[1])

    def apply_style_TGraph(self,g,extra,name=None):
        data=self.get_data(extra,name)

        if 'xtitle' in data:
            g.GetXaxis().SetTitle(data['xtitle'])

        if 'xrange' in data:
            g.GetXaxis().SetLimits(*data['xrange'])

        if 'ytitle' in data:
            g.GetYaxis().SetTitle(data['ytitle'])

#        if 'yrange' in data:
#            g.GetYaxis().SetRangeUser(*data['yrange'])

    def apply_style_TMultiGraph(self,mg,extra,name=None):
        data=self.get_data(extra,name)

        if 'xtitle' in data:
            mg.GetXaxis().SetTitle(data['xtitle'])

        if 'xrange' in data:
            mg.GetXaxis().SetLimits(*data['xrange'])

        if 'ytitle' in data:
            mg.GetYaxis().SetTitle(data['ytitle'])

#        if 'yrange' in data:
#            mg.GetYaxis().SetRangeUser(*data['yrange'])

style=Style()
