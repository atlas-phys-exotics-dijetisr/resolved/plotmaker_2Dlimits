import ROOT

from prot import utiltools
from prot import style
from prot import canvastools
from prot import histtools
from prot import systools

import copy

def plot(hist,**kwargs):
    # Get histogram and clone
    histname=hist
    hist=utiltools.Get(hist)
    if hist==None:
        print('Missing histogram',histname)
        return
    histclone=hist.Clone()
    utiltools.store.append(histclone)
    orighist=hist
    hist=histclone


    # Rebin
    rebin=style.style.attr(hist.GetName(),kwargs,'rebin')
    if rebin!=None:
        if type(rebin)==int:
            hist.Rebin(rebin)
        else:
            hist=histtools.variable_rebin(hist,rebin)
            utiltools.store.append(hist)

    # Systematics
    sys=kwargs.get('sys',False)
    gsys=None
    opt=''
    syslist=[]
    if sys!=False:
        syslist=systools.get_syslist(orighist,sys,rebin)
        gsys=systools.apply_systematics(hist,syslist)
        utiltools.store.append(gsys)
        opt='hist'

    # Draw
    c1=canvastools.canvas()
    c1.Clear()
    style.style.apply_style(hist,kwargs,hist.GetName())

    opt=style.style.attr(hist.GetName(),kwargs,'opt',default=opt)
    hist.Draw(opt)
    if gsys!=None: gsys.Draw("SAME 3")
    style.style.apply_style(c1,style.kwargs_canvas(kwargs),'c1')    
    c1.Update()

def plots(hists,**kwargs):
    # Convert hist paths to hists
    newhists=[]
    for hist in hists:
        extrainfo=copy.copy(kwargs)
        if type(hist)==tuple:
            extrainfo.update(hist[1])
            hist=hist[0]
        if type(hist)==str:
            histname=hist
            hist=utiltools.Get(histname)
            if hist==None:
                print('Missing histogram',histname)
                continue
        histclone=hist.Clone()
        histclone.SetDirectory(0)
        newhists.append((histclone,extrainfo))
    hists=newhists

    #
    # Prepare default settings

    # Comparing two histograms
    if kwargs.get('default',True):
        if len(hists)==2:
            if 'opt' not in hists[0][1]: hists[0][1]['opt']='hist'
            if 'fillcolor' not in hists[0][1]: hists[0][1]['fillcolor']=ROOT.UCYellowLight
            if 'hsopt' not in kwargs: kwargs['hsopt']='nostack'
            if 'ratio' not in kwargs: kwargs['ratio']=0

    # Prepare stack
    c1=canvastools.canvas()
    c1.Clear()
    hstack=ROOT.THStack()
    utiltools.store.append(hstack)

    # Add entries
    for hist,extrainfo in hists:
        # Rebin
        rebin=style.style.attr(hist.GetName(),extrainfo,'rebin')
        if rebin!=None:
            if type(rebin)==int:
                hist.Rebin(rebin)
            else:
                hist=histtools.variable_rebin(hist,rebin)

        # Style
        style.style.apply_style(hist,extrainfo,hist.GetName())

        # Add
        hstack.Add(hist,extrainfo.get('opt',''))

        # Copy titles
        hstack.SetTitle(';%s;%s'%(hist.GetXaxis().GetTitle(),hist.GetYaxis().GetTitle()))

    # Prepare ratio plot, if requested
    ratioBaseIdx=kwargs.get('ratio',None) if len(hists)!=1 else None
    ratiomode=kwargs.get('ratiomode','ratio')
    h_ratios=None
    if ratioBaseIdx!=None:
        h_ratioBase=hstack.GetHists().At(ratioBaseIdx)
        h_ratios=ROOT.THStack()
        h_ratios.SetName(hstack.GetName()+"_ratio")
        utiltools.store.append(h_ratios)

        ratiolist=kwargs.get('ratiolist',range(len(hstack.GetHists())))
        for ratioIdx in ratiolist:
            h_ratio=hstack.GetHists().At(ratioIdx).Clone()
            h_ratios.Add(h_ratio)

            if ratiomode=='significance':
                for binIdx in range(h_ratioBase.GetNbinsX()+2):
                    valRef=h_ratioBase.GetBinContent(binIdx)*h_ratioBase.GetBinWidth(binIdx)
                    valSec=h_ratio.GetBinContent(binIdx)*h_ratioBase.GetBinWidth(binIdx)
                    if valSec!=0:
                        h_ratio.SetBinContent(binIdx,(valRef-valSec)/ROOT.TMath.Sqrt(valSec))
                    else:
                        h_ratio.SetBinContent(binIdx,0)
                    h_ratio.SetBinError(binIdx,0)
                h_ratio.GetYaxis().SetTitle('Significance')
            else:
                h_ratio.Divide(h_ratioBase)
                h_ratio.GetYaxis().SetTitle("1/%s"%h_ratioBase.GetTitle())

        #Update ratio title object title
        title=h_ratios.GetTitle();
        parts=title.split(';')
        ratiotitle=''
        if ratiomode=='significance':
            ratiotitle='Significance'
        else:
            ratiotitle='1/%s'%h_ratioBase.GetTitle() if len(h_ratios.GetHists())!=2 else '#frac{%s}{%s}'%(h_ratios.GetHists()[(ratioBaseIdx+1)%2].GetTitle(),h_ratioBase.GetTitle())
        ratiotitle=kwargs.get('ratiotitle',ratiotitle)
        
        title="%s;%s;%s"%(parts[0],h_ratioBase.GetXaxis().GetTitle(),ratiotitle)
        h_ratios.SetTitle(title)
        h_ratios.GetHists().RemoveAt(ratioBaseIdx)

    # Draw
    c1=canvastools.canvas()
    c1.Clear()

    pad=c1
    pad_ratio=None
    if h_ratios!=None:
        pad,pad_ratio=canvastools.apply_ratio_canvas_style(c1)
    pad.cd()
    hstack.Draw(kwargs.get('hsopt',''))
    style.style.apply_style(hstack,kwargs,'hstack')    
    style.style.apply_style(pad,kwargs,'c1')
    canvastools.apply_canvas(pad,kwargs)
    pad.Update()

    if h_ratios!=None:
        pad_ratio.cd()
        h_ratios.Draw('nostack')
        canvastools.apply_ratio_axis_style(h_ratios)

        if 'ratiorange' in kwargs:
            h_ratios.SetMinimum(kwargs['ratiorange'][0])
            h_ratios.SetMaximum(kwargs['ratiorange'][1])

        h_ratios.GetXaxis().SetRangeUser(pad.GetUxmin(),pad.GetUxmax())

        # reference line
        l=ROOT.TLine(pad.GetUxmin(),1,pad.GetUxmax(),1)
        l.SetLineWidth(2)
        l.DrawClone()

        pad_ratio.Update()

    c1.cd()
    c1.Update()

def plotsf(paths,hist,**kwargs):
    hists=[]
    for path in paths:
        if type(path)!=tuple: path=(path,{})
        histpath='%s:/%s'%(path[0],hist)
        hists.append((histpath,path[1]))
    plots(hists,**kwargs)

def plot2d(hist,**kwargs):
    hist=utiltools.Get(hist).Clone()
    utiltools.store.append(hist)

    # Draw
    c1=canvastools.canvas()
    c1.Clear()

    opt=kwargs.get('opt','col').lower()
    canvastools.canvasPrepCOLZ(c1,'colz' in opt)
    style.style.apply_style(hist,kwargs,hist.GetName())
    hist.Draw(opt)

    style.style.apply_style(c1,kwargs,'c1')
    canvastools.apply_canvas(c1,kwargs)
    
    """
    #by hand for now
    hist.GetYaxis().SetBit(ROOT.TAxis.kAxisRange);
    hist.GetYaxis().SetRange(1,3)
    hist.GetYaxis().SetRangeUser(0.05,0.15)
    #hist.GetYaxis().SetBit(ROOT.TAxis.kAxisRange);
    c1.Update()
    #hist.GetYaxis().SetRangeUser(0.025,0.7)
    #hist.SetMaximum(0.7)
    #single plot
    #hist.GetXaxis().SetRangeUser(400,3500)
    #combined plot
    hist.GetXaxis().SetRangeUser(400,1000)
    """
    c1.Update()

def graphs(graphs,**kwargs):
    # Convert hist paths to hists
    newgraphs=[]
    for graph in graphs:
        extrainfo=copy.copy(kwargs)
        if type(graph)==tuple:
            extrainfo.update(graph[1])
            graph=graph[0]
        graphclone=graph.Clone()
        newgraphs.append((graphclone,extrainfo))
    graphs=newgraphs

    # Prepare stack
    mg=ROOT.TMultiGraph()
    utiltools.store.append(mg)

    # Add entries
    for graph,extrainfo in graphs:
        graph.Sort()
        mg.Add(graph,extrainfo.get('opt',''))
        style.style.apply_style(graph,extrainfo,graph.GetName())

        # Copy titles
        mg.SetTitle(';%s;%s'%(graph.GetXaxis().GetTitle(),graph.GetYaxis().GetTitle()))

    # Draw
    c1=canvastools.canvas()
    c1.Clear()

    mg.Draw(kwargs.get('mgopt','AP'))
    style.style.apply_style(mg,kwargs,'mg')
    style.style.apply_style(c1,kwargs,'c1')
    canvastools.apply_canvas(c1,kwargs)

    c1.cd()
    c1.Update()
