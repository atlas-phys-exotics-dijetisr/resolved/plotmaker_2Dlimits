import ROOT

from prot import utiltools
from prot import legendtools
from prot import style

import os, os.path

batchc1=None
savepath='.'

def canvas(forceNew=False, coords=[]):
    global batchc1

    if ROOT.gROOT.IsBatch():
        if batchc1==None:
            if len(coords)==2:
                batchc1=ROOT.TCanvas("c1","",coords[0], coords[1])
            else:
                batchc1=ROOT.TCanvas()
        return batchc1
    
    c1=ROOT.gROOT.GetSelectedPad()
    if c1==None or forceNew:
        if len(coords)==2:
            c1=ROOT.TCanvas("c1","",coords[0], coords[1])
        else:
            c1=ROOT.TCanvas()            
        utiltools.store.append(c1)
    else:
        c1=c1.GetCanvas()

    return c1

def apply_canvas(c1,data):
    #
    # Legend!
    if 'legend' in data:
        l=legendtools.make(c1)
        legendtools.add(l,c1.GetListOfPrimitives())
        l.SetNColumns(data.get('legendncol',1))
        legendtools.resize(l,pos=style.toposition(data['legend']),width=data.get('legendwidth',0.4))
        l.Draw()

    #
    # Text!
    if 'text' in data:
        pos=(data.get('textpos',(0.2,0.2))[0],data.get('textpos',(0.2,0.2))[1])

        text=data['text']
        if type(text)!=dict: text={'title':text}
        print "I am about to apply this", data['text']
        if 'IntPrelim' in data['text']: ATLAS_x = data['text']['IntPrelim']
        else: ATLAS_x = 'Internal'
        style.ATLAS(text.get('title','').split('\n'),xlabel=pos[0],ylabel=pos[1],sim=text.get('sim',True),lumi=text.get('lumi',None), ATLAS_x=ATLAS_x)

def apply_ratio_canvas_style(c):
    c.Clear()
    c.Divide(1,2)
    pad=c.cd(1)
    pad.SetPad(0.01,0.17,0.99,0.99)
    pad_ratio=c.cd(2)
    pad_ratio.SetBorderSize(0)
    pad_ratio.SetTopMargin(0)
    pad_ratio.SetBottomMargin(0.35)
    pad_ratio.SetPad(0.01,0.0,.99,0.3)
    pad_ratio.SetGridy()

    return pad,pad_ratio

def apply_ratio_axis_style(frame):
    frame.GetXaxis().SetTitleSize(0.17)
    frame.GetXaxis().SetTitleOffset(0.9)

    frame.GetYaxis().SetTitleSize(0.17)
    frame.GetYaxis().SetTitleOffset(0.25)

    frame.GetYaxis().SetNdivisions(5,2,1)

    frame.GetXaxis().SetLabelFont(63);
    frame.GetXaxis().SetLabelSize(14);
    frame.GetYaxis().SetLabelFont(63);
    frame.GetYaxis().SetLabelSize(14);

def save(path,formats=['pdf','C','png','eps']):
    global savepath

    outpath=savepath+'/'+path
    outdir=os.path.dirname(outpath)
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    c1=canvas()

    for outformat in formats:
        newoutpath='.'.join(outpath.split('.')[:-1])+'.'+outformat
        c1.SaveAs(newoutpath)

def fopen(path,mode='r'):
    global savepath

    outpath=savepath+'/'+path
    outdir=os.path.dirname(outpath)
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    return open(outpath,mode)

def canvasPrepCOLZ(pad,isCOLZ=True):
    pad.SetRightMargin(0.15 if isCOLZ else 0.05)
