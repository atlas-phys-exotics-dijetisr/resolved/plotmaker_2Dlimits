import ROOT

class FileManager:
    def __init__(self):
        self.fhs={}
        self.map={}
        pass

    def open(self,path,mode='READ'):
        # Check if file opened by interpreter
        if(path.startswith("_file")):
            fidx=int(path[5:])
            return ROOT.gROOT.GetListOfFiles().At(fidx)

        # Check remap and open
        finalpath=self.map.get(path,path)

        # Check existing files
        if finalpath in self.fhs:
            if self.fhs[finalpath].IsOpen():
                return self.fhs[finalpath]

        # Must open new!
        fh=ROOT.TFile.Open(finalpath,mode)
        self.fhs[finalpath]=fh
        return fh

    def filemap(self,path,name):
        self.map[name]=path

fm=FileManager()

def open(path,mode='READ'):
    if type(path)==str:
        return fm.open(path,mode)
    else:
        return path

def filemap(path,name):
    fm.filemap(path,name)
