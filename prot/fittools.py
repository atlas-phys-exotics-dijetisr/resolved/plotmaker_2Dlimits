import ROOT

from prot import utiltools
from prot import style
from prot import canvastools
from prot import histtools

ROOT.Math.MinimizerOptions.SetDefaultMinimizer("Minuit2","Minimize")
ROOT.Math.MinimizerOptions.SetDefaultMaxIterations(1000000)
ROOT.Math.MinimizerOptions.SetDefaultMaxFunctionCalls(100000)
ROOT.Math.MinimizerOptions.SetDefaultTolerance(0.1)

def fit(hist,func,**kwargs):
    # Convert hist paths to hists
    newhists=[]
    if type(hist)==str:
        histname=hist
        hist=utiltools.Get(histname)
        if hist==None:
            print('Missing histogram',histname)
            return

    hist=hist.Clone()    
    hist.SetDirectory(0)
    utiltools.store.append(hist)

    # Rebin
    rebin=style.style.attr(hist.GetName(),kwargs,'rebin')
    if rebin!=None:
        if type(rebin)==int:
            hist.Rebin(rebin)
        else:
            hist=histtools.variable_rebin(hist,rebin)

    # Style
    if 'xrange' not in kwargs and 'fitrange' in kwargs: kwargs['xrange']=kwargs['fitrange']
    style.style.apply_style(hist,kwargs,hist.GetName())

    # Fit
    fitresult=None
    if 'fitrange' in kwargs:
        fitresult=hist.Fit(func,'sr%s'%kwargs.get('fitopt','i'),'',kwargs.get('fitrange')[0],kwargs.get('fitrange')[1])
    else:
        fitresult=hist.Fit(func,'s%s'%kwargs.get('fitopt','i'))
    if fitresult.Get()==None: return None

    fitfunc=hist.GetListOfFunctions()[0]
    fitfunc.SetLineColor(ROOT.kRed)    

    # Chi2
    hdiff=hist.Clone()
    utiltools.store.append(hdiff)
    hdiff.GetListOfFunctions().Clear()
    ratiomode=kwargs.get('ratiomode','diff')
    for binIdx in range(0,hdiff.GetNbinsX()+2):
        if hdiff.GetBinContent(binIdx)==0: continue
        x=hdiff.GetBinCenter(binIdx)
        y=fitfunc.Integral(hdiff.GetBinLowEdge(binIdx),hdiff.GetBinLowEdge(binIdx+1))/hdiff.GetBinWidth(binIdx) #Eval(x)
        if ratiomode=='diff':
            hdiff.GetYaxis().SetTitle('Data-Fit')
            diff=hdiff.GetBinContent(binIdx)-y
            hdiff.SetBinContent(binIdx,diff)
        elif ratiomode=='chi2':
            hdiff.GetYaxis().SetTitle('#chi^{2}')            
            if hdiff.GetBinError(binIdx)==0: diff=0
            else: diff=(hdiff.GetBinContent(binIdx)-y)**2/hdiff.GetBinError(binIdx)**2
            hdiff.SetBinContent(binIdx,diff)
            hdiff.SetBinError(binIdx,0)
        elif ratiomode=='significance':
            hdiff.GetYaxis().SetTitle('Significance')
            if hdiff.GetBinError(binIdx)==0: diff=0
            else: diff=(hdiff.GetBinContent(binIdx)-y)/hdiff.GetBinError(binIdx)
            hdiff.SetBinContent(binIdx,diff)
            hdiff.SetBinError(binIdx,0)

    # Draw
    c1=canvastools.canvas()
    c1.Clear()
    pad,pad_ratio=canvastools.apply_ratio_canvas_style(c1)

    pad.cd()
    hist.Draw(kwargs.get('opt',''))
    style.style.apply_style(pad,kwargs,'c1')
    canvastools.apply_canvas(pad,kwargs)
    style.textbox('#chi^{2}/ndof=%0.1f/%0.1f, prob=%0.2f'%(fitresult.Chi2(),fitresult.Ndf(),fitresult.Prob()),xlabel=0.6,ylabel=0.95)
    
    pad.Update()

    pad_ratio.cd()
    hdiff.Draw()
    canvastools.apply_ratio_axis_style(hdiff)

    if 'ratiorange' in kwargs:
        hdiff.SetMinimum(kwargs['ratiorange'][0])
        hdiff.SetMaximum(kwargs['ratiorange'][1])

    hdiff.GetXaxis().SetRangeUser(pad.GetUxmin(),pad.GetUxmax())

    # reference line
    l=ROOT.TLine(pad.GetUxmin(),1,pad.GetUxmax(),1)
    l.SetLineWidth(2)
    l.SetLineColor(ROOT.kRed)
    l.DrawClone()

    pad_ratio.Update()

    c1.cd()
    c1.Update()

    return fitresult

def fitg(graph,func,**kwargs):
    # Convert graph paths to hists
    newgraphs=[]
    if type(graph)==str:
        graphname=graph
        graph=utiltools.Get(graphname)
        if graph==None:
            print('Missing graph',graphname)
            return

    graph=graph.Clone()    
    utiltools.store.append(graph)

    # Style
    if 'xrange' not in kwargs and 'fitrange' in kwargs: kwargs['xrange']=kwargs['fitrange']
    style.style.apply_style(graph,kwargs,graph.GetName())

    # Fit
    fitresult=None
    if 'fitrange' in kwargs:
        fitresult=graph.Fit(func,'sr%s'%kwargs.get('fitopt',''),'',kwargs.get('fitrange')[0],kwargs.get('fitrange')[1])
    else:
        fitresult=graph.Fit(func,'s%s'%kwargs.get('fitopt',''))
    if fitresult.Get()==None: return None

    fitfunc=graph.GetListOfFunctions()[0]
    fitfunc.SetLineColor(ROOT.kRed)    

    # Ratios
    gdiff=graph.Clone()
    utiltools.store.append(gdiff)
    gdiff.GetListOfFunctions().Clear()
    for gIdx in range(0,gdiff.GetN()):
        x=gdiff.GetX()[gIdx]
        y=fitfunc.Eval(x)

        gdiff.GetYaxis().SetTitle('Data-Fit')
        diff=gdiff.GetY()[gIdx]-y
        gdiff.SetPoint(gIdx,x,diff)

    # Draw
    c1=canvastools.canvas()
    c1.Clear()
    pad,pad_ratio=canvastools.apply_ratio_canvas_style(c1)

    pad.cd()
    graph.Draw(kwargs.get('opt','ape'))
    style.style.apply_style(pad,kwargs,'c1')
    canvastools.apply_canvas(pad,kwargs)
    style.textbox('#chi^{2}/ndof=%0.1f/%0.1f, prob=%0.2f'%(fitresult.Chi2(),fitresult.Ndf(),fitresult.Prob()),xlabel=0.6,ylabel=0.95)
    
    pad.Update()

    pad_ratio.cd()
    gdiff.Draw("APE")
    canvastools.apply_ratio_axis_style(gdiff)

    if 'ratiorange' in kwargs:
        gdiff.SetMinimum(kwargs['ratiorange'][0])
        gdiff.SetMaximum(kwargs['ratiorange'][1])

    gdiff.GetXaxis().SetLimits(pad.GetUxmin(),pad.GetUxmax())
    gdiff.GetXaxis().SetTitle(graph.GetXaxis().GetTitle())
    gdiff.GetYaxis().SetTitle('Data-Fit')

    # reference line
    l=ROOT.TLine(pad.GetUxmin(),0,pad.GetUxmax(),0)
    l.SetLineWidth(2)
    l.SetLineColor(ROOT.kRed)
    l.DrawClone()

    pad_ratio.Update()

    c1.cd()
    c1.Update()

    return fitresult
