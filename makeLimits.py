
from makeOneLimit import makeLimit
from makeLimitFromDict import makeLimitFromDict

# the following is some of what you need to make the summary plot for the DMsummary paper -- various input is still needed from the analysers -- plus the TLA contours below

# For dijet+ISR
# """
# you need one "makeLimit" command for each line, each of which corresponds to a text file in data/
# let's say it's called dijetISR2017_channel[1,2,...].txt
selections = {}
selections['compound_inclusive'] = 'dijet + ISR (photon) compound inclusive'
selections["compound_btagged"]="dijet + ISR (photon) compound btagged"
selections["single_inclusive"]="dijet + ISR (photon) single inclusive"
selections["single_btagged"]="dijet + ISR (photon) single btagged"

withSyst=True

if withSyst:
    d="dijetISRResolvedAll"
else:
    d="dijetISRResolvedNoSyst"


# selections['2'] = 'dijet + ISR (photon), two b-tags'
lumi = '79.8'
for channel in selections.keys():
    limitTextFileName = 'data/'+d+'/LimitsDict_photon_'+channel+'.py'
    print("limitTextFileName: ", limitTextFileName)
    makeLimit( limitTextFileName,  selection = selections[channel],  lumi = lumi,
               acc_scaling = False, curvy = False, upAndDown = False, logy = False)
    for explim in [1,-1,2,-2]:
        makeLimit( limitTextFileName,  selection = selections[channel],  lumi = lumi,
                   acc_scaling = False, curvy = False, explim = explim, upAndDown =False, logy = False)

# this will give you some plots in intermediate_plots/[limitTextFileName.replace('.txt','')]
# and some TGraphs in intermediate_rootfiles/[limitTextFileName.replace('.txt','.root')]
# You can then take these and plot as you see fit, or experiment with combine_limits.py
# maybe taking the TLA config - finalPlot_configs/TLA2017Paper.py - as inspiration, though it doesn't need to be as complicated
# """

# """
# Dijet high-mass: needs exp band to be sent by analysers
# makeLimit( 'data/limits_dijets_full2016.txt', selection = 'Dijet full 2016',      lumi = '37.0', massrange=[1.5,5.5], curvy = False, upAndDown = False )
# makeLimit( 'data/limits_dijets_full2016.txt', selection = 'Dijet full 2016',      lumi = '37.0', massrange=[1.5,5.5], curvy = False, upAndDown = True )

# Dijet+ISR: needs exp band to be sent by analysers
# makeLimit( 'data/limits_gjet_DS2p1.txt',      selection = 'Dijet + ISR (#gamma)', lumi = '15.5', curvy = False, upAndDown = False )
# makeLimit( 'data/limits_3jet_DS2p1.txt',      selection = 'Dijet + ISR (jet)',    lumi = '15.5', curvy = False, upAndDown = False )
# makeLimit( 'data/limits_gjet_DS2p1.txt',      selection = 'Dijet + ISR (#gamma)', lumi = '15.5', curvy = False, upAndDown = True )
# makeLimit( 'data/limits_3jet_DS2p1.txt',      selection = 'Dijet + ISR (jet)',    lumi = '15.5', curvy = False, upAndDown = True )

# Dijet angular - hacked together something by hand. Need proper limits from analysers.
# makeLimit( 'data/limits_dijets_full2016_angular.txt', selection = 'Dijet angular full 2016',      lumi = '37.0', massrange = [2.0,5.0] )

# """

# various old lines
"""
# from Antonio in .csv
# makeLimitFromDict('data/limits_UA2_etc/limitsDict.py', 'UA2 10.9 pb-1 0.63 TeV 1993', 'UA2')
# makeLimitFromDict('data/limits_UA2_etc/limitsDict.py', 'CDF 106 pb-1 1.8 TeV 1997',   'CDFrun1')
# makeLimitFromDict('data/limits_UA2_etc/limitsDict.py', 'CDF 1.13 fb-1 1.96 TeV 2009', 'CDFrun2')
# makeLimitFromDict('data/limits_UA2_etc/limitsDict.py', 'CMS 0.13 fb-1 8 TeV',         'CMS8dijet0.13fb')
# makeLimitFromDict('data/limits_UA2_etc/limitsDict.py', 'CMS 4 fb-1 8 TeV',            'CMS8dijet4fb')
# makeLimitFromDict('data/limits_UA2_etc/limitsDict.py', 'CMS 5 fb-1 8 TeV',            'CMS8dijet5fb')
# makeLimitFromDict('data/limits_UA2_etc/limitsDict.py', 'CMS 20 fb-1 8 TeV',           'CMS8dijet20fb')
# makeLimitFromDict('data/limits_UA2_etc/limitsDict.py', 'atlas 1 fb-1 8 TeV',          'ATLAS8dijet1fb')

# hacked from eps file in 8 TeV ATLAS dijet paper
# makeLimitFromDict('data/eps/ATLAS_8TeV_limits.py', 'UA2',          'UA2_eps')
# makeLimitFromDict('data/eps/ATLAS_8TeV_limits.py', 'CDF 1',        'CDFrun1_eps')
# makeLimitFromDict('data/eps/ATLAS_8TeV_limits.py', 'CDF 1.1',      'CDFrun2_eps')
# makeLimitFromDict('data/eps/ATLAS_8TeV_limits.py', 'CMS 0.13fb',   'CMS8dijet0.13fb_eps')
# makeLimitFromDict('data/eps/ATLAS_8TeV_limits.py', 'CMS 4fb',      'CMS8dijet4fb_eps')
# makeLimitFromDict('data/eps/ATLAS_8TeV_limits.py', 'CMS 5fb',      'CMS8dijet5fb_eps')
# makeLimitFromDict('data/eps/ATLAS_8TeV_limits.py', 'CMS 20fb',     'CMS8dijet20fb_eps')
# makeLimitFromDict('data/eps/ATLAS_8TeV_limits.py', 'ATLAS 1fb',    'ATLAS8dijet1fb_eps')
# makeLimitFromDict('data/eps/ATLAS_8TeV_limits.py', 'ATLAS 13fb',   'ATLAS8dijet12fb_eps')
# makeLimitFromDict('data/eps/ATLAS_8TeV_limits.py', 'ATLAS 20.3fb', 'ATLAS8dijet20.3fb_eps')

# manually from CMS plots
# makeLimitFromDict('data/manual/CMS_limits.py', 'CMS_scouting_8TeV_18.8fb',  'CMS8scouting18.8fb_manual')
# makeLimitFromDict('data/manual/CMS_limits.py', 'CMS_scouting_13TeV_12.9fb', 'CMS13scouting12.9_manual')
# makeLimitFromDict('data/manual/CMS_limits.py', 'CMS_dijet_13TeV_12.9fb',    'CMS13dijet12.9_manual')
makeLimitFromDict('data/manual/CMS_limits.py', 'CMS_scouting_13TeV_27fb_prelim', 'CMS13scouting27_prelim_manual')
makeLimitFromDict('data/manual/CMS_limits.py', 'CMS_dijet_13TeV_36fb_prelim',    'CMS13dijet36_prelim_manual')
makeLimitFromDict('data/manual/CMS_limits.py', 'CMS_scouting_13TeV_27fb', 'CMS13scouting27_manual')
makeLimitFromDict('data/manual/CMS_limits.py', 'CMS_dijet_13TeV_36fb',    'CMS13dijet36_manual')
# makeLimitFromDict('data/manual/CMS_limits.py', 'CMS_boosted_13TeV_35.9fb',    'CMS13boosted35.9_manual')
"""

# di-b
"""
import sys
sys.path.append('data')
from dibjet2016scaleFactors import scaleFactors

makeLimit( 'data/limits2016_dibjet_highMass.txt',  selection = 'high mass',  lumi = '0', acc_scaling = False, curvy = False, upAndDown = True, scaleFactors = scaleFactors )
# makeLimit( 'data/limits2016_dibjet_highMass.txt',  selection = 'high mass',  lumi = '0', acc_scaling = True, curvy = False, upAndDown = True ) # wrong
# makeLimit( 'data/limits2016_dibjet_lowMass.txt',  selection = 'low mass',  lumi = '0', acc_scaling = True, curvy = False, upAndDown = True ) # has SFs rolled in already apparently

# makeLimit( 'data/limits2016_dibjet_highMass.txt',  selection = 'high mass',  lumi = '0', acc_scaling = False, curvy = False, upAndDown = True )
# makeLimit( 'data/limits2016_dibjet_lowMass.txt',  selection = 'low mass',  lumi = '0', acc_scaling = False, curvy = False, upAndDown = True )

# makeLimit( 'data/limits_dijets_full2016.txt', selection = 'Dijet full 2016',      lumi = '37.0', acc_scaling = False, curvy = False, upAndDown = True )
"""

# the following makes the necessary contours for the TLA figure 4
"""
acc_scaling = True
curvy = False
version = '_v6'
minPEs = 50
range_yStar03 = [0.45,1.8]
range_yStar06 = [0.7,1.8]
upAndDown = False
logy = False # interpolation plotting
upAndDown = True

version = '_v7' # hacked theory XS
version = '_v8' # corrected theory XS - 301859-301886 *= 1.25
version = '_v9' # hacking so only gq=0.4
upAndDown = False

# makeLimit( 'data/limits2016_TLA_J75yStar03'+version+'.txt',  selection = 'TLA, J75 y*<0.3',  lumi = '3.6', massrange = range_yStar03,
           # acc_scaling = acc_scaling, curvy = curvy, minPEs = minPEs, upAndDown = upAndDown, logy = logy)
makeLimit( 'data/limits2016_TLA_J100yStar06'+version+'.txt', selection = 'TLA, J100 y*<0.6', lumi = '29.3', massrange = range_yStar06,
           acc_scaling = acc_scaling, curvy = curvy, minPEs = minPEs, upAndDown = upAndDown, logy = logy)
# for explim in [1,-1,2,-2]:
    # makeLimit( 'data/limits2016_TLA_J75yStar03'+version+'.txt',  selection = 'TLA, J75 y*<0.3',  lumi = '3.6', massrange = range_yStar03,
               # acc_scaling = acc_scaling, curvy = curvy, explim = explim, minPEs = minPEs, upAndDown = upAndDown, logy = logy)
    # makeLimit( 'data/limits2016_TLA_J100yStar06'+version+'.txt', selection = 'TLA, J100 y*<0.6', lumi = '29.3', massrange = range_yStar06,
               # acc_scaling = acc_scaling, curvy = curvy, explim = explim, minPEs = minPEs, upAndDown = upAndDown, logy = logy)
"""
