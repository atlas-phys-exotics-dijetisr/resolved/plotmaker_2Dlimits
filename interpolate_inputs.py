
import os

import ROOT
from ROOT import TCanvas, TGraph, TH1F, TF1, TLegend
ROOT.gROOT.SetBatch(1)

from array import array
import math

# inputfiles =   ['data/limits_dijets_full2016.txt']
# bjetscalings = [1.0]
# interpolation_plot_dir = 'interpolation_plots_dijet2016'
acc_scaling = False

inputfiles =   ['data/limits2017_TLA_J75.txt']
bjetscalings = [1.0]
interpolation_plot_dir = 'interpolation_plots_TLA2017J75'
acc_scaling = True

if not os.path.isdir(interpolation_plot_dir):
    os.mkdir(interpolation_plot_dir)


canv = TCanvas('c','c',600,600)

for inputfile in inputfiles:
    bjetscaling = bjetscalings[inputfiles.index(inputfile)]

    outputfile = inputfile.replace('.txt','_interpolated.txt')

    limits={}
    fh=open(inputfile)
    limits = eval(fh.read())
    fh.close()

    # I want to extrapolate from one mass to the next
    mRs = []
    for gSM in limits:
        thesemRs = limits[gSM].keys()
        for mass in thesemRs:
            if mass not in mRs:
                mRs.append(mass)
    mRs.sort()


    # let's make some TGraphs in each dimension to see what the scaling looks like
    # 1. For each mass point, plot

    mRfirst = {}
    gSMfirst = {}

    for gSM,limitsgSM in limits.items():
        for mR,limit in limitsgSM.items():
            xslim_obs=limit.get('obs'   ,None)
            xslim_exp=limit.get('exp'   ,None)
            xstheory =limit.get('theory',None)
            acc =limit.get('acc',None)
            if not acc_scaling:
                acc = 1

            xstheory = xstheory * acc * bjetscaling

            if mR not in mRfirst:
                mRfirst[mR] = {}
            mRfirst[mR][gSM] = {
                'xslim_obs': xslim_obs,
                'xslim_exp': xslim_exp,
                'xs_theory': xstheory,
                'mu_obs'   : xslim_obs/xstheory,
                'mu_exp'   : xslim_exp/xstheory
                }

            if gSM not in gSMfirst:
                gSMfirst[gSM] = {}
            gSMfirst[gSM][mR] = {
                'xslim_obs': xslim_obs,
                'xslim_exp': xslim_exp,
                'xs_theory': xstheory,
                'mu_obs'   : xslim_obs/xstheory,
                'mu_exp'   : xslim_exp/xstheory
                }
            
    # Now I have two nice dicts, mRfirst and gSMfirst

    for mR in sorted(mRfirst):
        print mR

        # I am trying to pick out where mu = 1
        # inherited idea: take the lowest gSM where mu < 1, this is gSMref

        # get gSM ref NB!!! This seems to always come from obs
        for gSM in sorted(mRfirst[mR]):
            mu = mRfirst[mR][gSM]['mu_obs']
            print '  gSM:', gSM, '- mu:', mu
            gSMref = gSM
            muref_obs = mu
            muref_exp = mRfirst[mR][gSM]['mu_exp']
            if mu < 1:
                break


        print '  gSMref =' , gSMref
        if mu >=1:
            print "  there was no appropriate one, going to extrapolate the other way..."

        xsref = mRfirst[mR][gSMref]['xs_theory']
        xsobs = mRfirst[mR][gSMref]['xslim_obs']
        xsexp = mRfirst[mR][gSMref]['xslim_exp']

        
        gSMobs=math.sqrt(gSMref**2*xsobs/xsref)
        gSMexp=math.sqrt(gSMref**2*xsexp/xsref)


        print "mR, gSMref =", mR, gSMref
        print 'xsref, xsobs, xsexp, gSMobs, gSMexp', xsref, xsobs, xsexp, gSMobs, gSMexp
        
        # I want to make a TGraph with the TF1 on it
        
        # Step 1, basis hist
        thesegSMs = sorted(mRfirst[mR].keys())
        hist = TH1F('h',"m_{Z'} = "+str(mR)+' TeV;g_{SM};#mu', 100, 0, 0.6)

        gSM_arr = array('f',thesegSMs)
        mus_obs = array('f',[mRfirst[mR][gSM]['mu_obs'] for gSM in thesegSMs])
        mus_exp = array('f',[mRfirst[mR][gSM]['mu_exp'] for gSM in thesegSMs])
        # hist.SetMaximum(max(mus_obs + mus_exp)*1.1)
        # hist.SetMinimum(min(mus_obs + mus_exp)*0.9)
        hist.SetMaximum(5)
        hist.SetMinimum(0)
        hist.SetStats(0)
        hist.Draw()

        l = TLegend(0.5,0.5,0.89,0.89)
    
        print thesegSMs
        print mus_obs

        funcstr_exp = "("+str(gSMref)+"/x)**2 * "+str(muref_exp)
        funcExp = TF1("fexp", funcstr_exp)
        funcExp.SetLineColor(ROOT.kRed)
        funcExp.SetLineStyle(ROOT.kDashed)
        funcExp.Draw("same")
        l.AddEntry(funcExp, "exp extrapolation", "L")

        funcstr_obs = "("+str(gSMref)+"/x)**2 * "+str(muref_obs)
        funcObs = TF1("fobs", funcstr_obs)
        funcObs.SetLineColor(ROOT.kRed)
        funcObs.Draw("same")
        l.AddEntry(funcObs, "obs extrapolation", "L")

        gr_muexp = TGraph(len(mus_exp), gSM_arr, mus_exp)
        gr_muexp.SetLineWidth(2)
        gr_muexp.SetLineStyle(ROOT.kDashed)
        gr_muexp.SetMarkerStyle(ROOT.kOpenCircle)
        gr_muexp.Draw('LP same')
        l.AddEntry(gr_muexp, "exp points", "LP")

        gr_muobs = TGraph(len(mus_obs), gSM_arr, mus_obs)
        gr_muobs.SetLineWidth(2)
        gr_muobs.SetMarkerStyle(ROOT.kFullCircle)
        gr_muobs.Draw('LP same')
        l.AddEntry(gr_muobs, "obs points", "LP")
        
        markerExp = ROOT.TMarker(gSMexp, 1, ROOT.kOpenCross)
        markerExp.SetMarkerColor(ROOT.kRed)
        markerExp.SetMarkerSize(2)
        markerExp.Draw()
        l.AddEntry(markerExp, "g_{SM} extrap. points", "P")

        markerObs = ROOT.TMarker(gSMobs, 1, ROOT.kFullCross)
        markerObs.SetMarkerColor(ROOT.kRed)
        markerObs.SetMarkerSize(2)
        markerObs.Draw()

        refMarkerExp = ROOT.TMarker(gSMref, muref_exp, ROOT.kOpenCircle)
        refMarkerExp.SetMarkerColor(ROOT.kRed)
        refMarkerExp.SetMarkerSize(1.2)
        refMarkerExp.Draw()
        l.AddEntry(refMarkerExp, "g_{SM}^{ref} points", "P")

        refMarkerObs = ROOT.TMarker(gSMref, muref_obs, ROOT.kOpenCircle)
        refMarkerObs.SetMarkerColor(ROOT.kRed)
        refMarkerObs.SetMarkerSize(1.2)
        refMarkerObs.Draw()


        l.SetTextFont(42)
        l.SetTextSize(0.03)
        l.SetBorderSize(0)
        l.Draw()


        
        canv.SaveAs(interpolation_plot_dir+'/mu_vs_gSM_mR'+str(mR)+'.png')


        
    print '\nNow for gSM :-)'

    for gSM in sorted(gSMfirst):
        print gSM

        # I want to make a TGraph and do a TF1 on it
        
        # Step 1, basis hist
        thesemRs = sorted(gSMfirst[gSM].keys())
        hist.Reset()
        hist = TH1F('h',"g_{SM} = "+str(gSM)+" TeV;m_{Z'} [TeV];#mu", 100, 1, 4.5)

        mRs_arr = array('f',thesemRs)
        mus_obs = array('f',[gSMfirst[gSM][mR]['mu_obs'] for mR in thesemRs])
        mus_exp = array('f',[gSMfirst[gSM][mR]['mu_exp'] for mR in thesemRs])

        # hist.SetMaximum(max(mus_obs + mus_exp)*1.1)
        # hist.SetMinimum(min(mus_obs + mus_exp)*0.9)
        hist.SetMaximum(5)
        hist.SetMinimum(0)
        hist.SetStats(0)
        hist.Draw()

        l = TLegend(0.15,0.5,0.4,0.89)
    
        print thesemRs

        gr_muexp = TGraph(len(mus_exp), mRs_arr, mus_exp)
        gr_muexp.SetLineWidth(2)
        gr_muexp.SetLineStyle(ROOT.kDashed)
        gr_muexp.SetMarkerStyle(ROOT.kOpenCircle)
        gr_muexp.Draw('LP same')
        l.AddEntry(gr_muexp, "exp points", "LP")

        gr_muobs = TGraph(len(mus_obs), mRs_arr, mus_obs)
        gr_muobs.SetLineWidth(2)
        gr_muobs.SetMarkerStyle(ROOT.kFullCircle)
        gr_muobs.Draw('LP same')
        l.AddEntry(gr_muobs, "obs points", "LP")
        
        gr_muexp.Fit("pol2")
        funcExp = gr_muexp.GetFunction("pol2")
        funcExp.SetLineColor(ROOT.kRed)
        funcExp.SetLineStyle(ROOT.kDashed)
        funcExp.Draw("same")
        l.AddEntry(funcExp, "exp fit", "L")

        gr_muobs.Fit("pol2")
        funcObs = gr_muobs.GetFunction("pol2")
        funcObs.SetLineColor(ROOT.kRed)
        funcObs.Draw("same")
        l.AddEntry(funcObs, "obs fit", "L")

        l.SetTextFont(42)
        l.SetTextSize(0.03)
        l.SetBorderSize(0)
        l.Draw()

        canv.SaveAs(interpolation_plot_dir+'/mu_vs_mR_gSM_'+str(gSM)+'.png')

        canv.Clear()

        xs_theory = array('f',[gSMfirst[gSM][mR]['xs_theory'] for mR in thesemRs])
        xs_obs    = array('f',[gSMfirst[gSM][mR]['xslim_obs'] for mR in thesemRs])
        xs_exp    = array('f',[gSMfirst[gSM][mR]['xslim_exp'] for mR in thesemRs])

        print "XS arrays:"
        print xs_theory
        print xs_obs
        print xs_exp

        hist = TH1F('h',"g_{SM} = "+str(gSM)+" TeV;m_{Z'} [TeV];#mu", 100, 1, 4.5)

        hist.SetMaximum(0.5)
        hist.SetMinimum(0)
        hist.SetStats(0)
        hist.Draw()

        l = TLegend(0.5,0.5,0.89,0.89)

        gr_xs_the = TGraph(len(xs_theory), mRs_arr, xs_theory)
        gr_xs_the.SetLineWidth(2)
        gr_xs_the.SetMarkerStyle(ROOT.kOpenCircle)
        gr_xs_the.SetLineColor(ROOT.kBlack)
        gr_xs_the.Draw('LP same')
        l.AddEntry(gr_xs_the, "theory XS", "LP")

        gr_xs_obs = TGraph(len(xs_obs), mRs_arr, xs_obs)
        gr_xs_obs.SetLineWidth(2)
        gr_xs_obs.SetMarkerStyle(ROOT.kOpenCircle)
        gr_xs_obs.SetLineColor(ROOT.kBlue)
        gr_xs_obs.Draw('LP same')
        l.AddEntry(gr_xs_obs, "obs XS", "LP")

        gr_xs_exp = TGraph(len(xs_exp), mRs_arr, xs_exp)
        gr_xs_exp.SetLineWidth(2)
        gr_xs_exp.SetMarkerStyle(ROOT.kOpenCircle)
        gr_xs_exp.SetLineColor(ROOT.kRed)
        gr_xs_exp.Draw('LP same')
        l.AddEntry(gr_xs_exp, "exp XS", "LP")

        
        gr_xs_the.Fit("expo")
        funcXSThe = gr_xs_the.GetFunction("expo")
        funcXSThe.SetLineColor(ROOT.kBlack)
        funcXSThe.SetLineStyle(ROOT.kDashed)
        funcXSThe.Draw("same")
        l.AddEntry(funcXSThe, "theory expo fit", "L")

        gr_xs_obs.Fit("expo")
        funcXSObs = gr_xs_obs.GetFunction("expo")
        funcXSObs.SetLineColor(ROOT.kBlue)
        funcXSObs.SetLineStyle(ROOT.kDashed)
        funcXSObs.Draw("same")
        l.AddEntry(funcXSObs, "obs expo fit", "L")

        gr_xs_exp.Fit("expo")
        funcXSExp = gr_xs_exp.GetFunction("expo")
        funcXSExp.SetLineColor(ROOT.kRed)
        funcXSExp.SetLineStyle(ROOT.kDashed)
        funcXSExp.Draw("same")
        l.AddEntry(funcXSExp, "exp expo fit", "L")

        l.SetTextFont(42)
        l.SetTextSize(0.03)
        l.SetBorderSize(0)
        l.Draw()

        canv.SaveAs(interpolation_plot_dir+'/XS_vs_mR_gSM_'+str(gSM)+'.png')


    # Now I'm going to do some interpolation - want to interpolate in mu

    print mRs

    spacing = 0.1

    for m in range(1,len(mRs)):
        m_low = mRs[m-1]
        m_high = mRs[m]

        width = m_high - m_low
        print m_low, m_high, width

        for gSM in limits:
            if m_low not in limits[gSM] or m_high not in limits[gSM]: continue
            print ' ', gSM

            # this gSM exists for both m_low and m_high
            obs_high = limits[gSM][m_high]['obs']
            exp_high = limits[gSM][m_high]['exp']
            theory_high = limits[gSM][m_high]['theory']

            obs_low = limits[gSM][m_low]['obs']
            exp_low = limits[gSM][m_low]['exp']
            theory_low = limits[gSM][m_low]['theory']

            mu_obs_high = obs_high / theory_high
            mu_obs_low  = obs_low / theory_low
            mu_exp_high = exp_high / theory_high
            mu_exp_low  = exp_low / theory_low

            mu_obs_step = (mu_obs_high - mu_obs_low) * (spacing / width)
            mu_exp_step = (mu_exp_high - mu_exp_low) * (spacing / width)
            theory_step = (theory_high - theory_low) * (spacing / width)

            print 'start', m_low, obs_low, exp_low, mu_obs_low, mu_exp_low, theory_low

            thisMass = m_low
            thisObsMu = mu_obs_low
            thisExpMu = mu_exp_low
            thisTheory = theory_low
            thisObs = obs_low
            thisExp = exp_low
            while thisMass < m_high-spacing:
                thisObsMu += mu_obs_step
                thisExpMu += mu_exp_step
                thisTheory += theory_step
                thisMass += spacing
                thisObs = thisObsMu * thisTheory         # mu = xslim_obs/xstheory,
                thisExp = thisExpMu * thisTheory         # mu = xslim_obs/xstheory,
                print 'this', thisMass, thisObs, thisExp, thisTheory, thisObsMu, thisExpMu
                limits[gSM][thisMass] = {'obs': thisObs, 'exp': thisExp, 'theory': thisTheory}
            print 'end', m_high, obs_high, exp_high, mu_obs_high, mu_exp_high, theory_high


    # gSMs = sorted(limits.keys())
    
    # for i in range(1,len(gSMs)):
        # gSM_low = gSMs[i-1]
        # gSM_high = gSMs[i]
        # print gSM_low, gSM_high

        # for mass in limits[gSM_low]:
            # if mass not in limits[gSM_high]: continue
            # print ' ', mass

        # print l, limits[l]

    # for gSM,limitsgSM in limits.items():

    # 0.2 {3.5: {'obs': 0.009551755787218716, 'exp': 0.008427259498500468, 'theory': 0.0062387485}
    # gSM {mZ1: {'obs': , ... }, MZ2: {}, ...}

    output = open(outputfile,'w')
    output.write(str(limits))
    output.close()


    
