
import ROOT
from ROOT import TFile, TCanvas, TMath, TH1F, TLatex, TLine, TLegend, TBox, TPolyLine, TPaveText, TText
ROOT.gROOT.SetBatch(1)

from prot import canvastools
from quickLimitsTLA_withb_withISR_andDijets_mod import set_palette

from helpers import *
from intersections import getTGraphIntersections, merge_intersections, get_fills_with_eval

from array import array
import math

import scipy.interpolate
import pprint

import sys

# default configuration options
savedir = 'summaryPlots'
limits = {}
limits_to_plot = []
mergeSets = []

useMatplotlib = False

bonusname = None       # Name to add to output image files
lumiInLegend = True    # include luminosity in legend?
lumiOnPlot = True      # draw a summary luminosity label on the plot?
drawSqrtsText = True
yearInLegend = False   # include the year in the legend?
atlasText = 'Internal' # ATLAS label
extraText = None       # additional text on plot
extraTextpos = [0.1,0.93,0.05] # x, y, spacing between lines
extraTextSize = 0.04

atlaslabelX = 0.19
atlaslabelY = 0.89
xtitle = "m_{Z'} [GeV]"
lumilabelXspacing = 0
lumilabelYspacing = 0.06

smallerText = False    # Use smaller legend text
textlabelsize = 0.037  # text size for y* labels
blackTextLabel = False # keep y* label black rather than the colour of its line
canvascoords = []      # not used
setLeftMargin = False
setRightMargin = False
setTopMargin = False
setBottomMargin = False
setYaxisOffset = False
setXaxisOffset = False
xmin = 380             # x-axis minimum
xmax = 2100            # x-axis maximum
ymin = 0.02            # y-axis minimum
ymax = 0.21            # y-axis maximum

photoshop = False      # 'photoshop' a high-mass diejt line on. To be removed / updated.
obsPoints = False      # Draw markers on the observed limit line
logx = True            # log-scale x axis
logy = False           # log-scale y axis
curved = False         # draw straight or curved lines
fillsCurved = False    # curves for fill areas. Needs hacking on a per-line basis

date = 'December 2017' # date to draw on top (also modifies some behaviour)
dateOnTop = True       # draw date?
dateInLabel = False    # include date in main label?

drawWidths = None      # draw dashed lines corresponding to particular resonance widths
precision_gq = 0.0001
numsteps_mMed = 1000

drawBand = False             # draw expected limit bands where present?
fillObs = False
fillOldObs = False
hatchedOneSidedBand = True   # draw one-sided hatched band on expected limit lines?
lineWidth = 100*5 + 5        # width of expected limit line. Needs to be changed if hatchedOneSidedBand is False.

drawLeg = True
setLegLeft = False     # Manual positioning of legend (replace False by a float)
setLegRight = False    #
setLegBot = False      #
setLegTop = False      #
drawLeg2 = True
setLeg2Left = False    # Manual positioning of secondary legend (the one with obs and exp)
setLeg2Right = False   #
setLeg2Bot = False     #
setLeg2Top = False     #

# hacking for band - draw a manual dashed line on top of the legend entry for the secondary legend
bandlinewidth = 10     # width of 1-sigma line in secondary legend
leg2LeftPlus = 0.0112  # amount to shift LH edge of dashed line to fit on legend entry
leg2BotPlus = 0.028    #
leg2RightPlus = 0.0637 #
legtextsize = 0.03     # text size in legends
leg2textsize = 0.03

textOnLines = False
noLegLines = False     # If True, don't draw lines on the legend
colourLeg2 = False     # Colour the lines and fills on the secondary legend

drawTexts = True       # draw the y* texts
bonusTexts = False     # add bonus text boxes  {'pos': [x,y], 'text': 'blah', 'size': 0.45}

addMajorTicks = None    # Add major tickmarks at the points in this list eg [2000]
addMinorTicks = None    # Add minor tickmarks between the two points in this list eg [1000,2000]
minorTickDivisions = 10 # Sets spacing between minor ticks above

obsMarkerStyle = ROOT.kFullCircle
obsMarkerCol = False
obsMarkerSize = 0.5
setRefCol = False

verticalLineStyle = 1
verticalLineColour = ROOT.kBlack
prepend_merged = False
shuffle = False


# import customised value from config file
sys.path.append('finalPlot_configs')

# default configuration name
#configname = 'SummaryPlot_mergeCollaborations_Mar18_lowMass'
configname="dijetISRResolvedPaperAtlasCirculation"

if '--config' in sys.argv:
    # get the next argument
    try:
        configname = sys.argv[sys.argv.index('--config')+1]
    except:
        print "You asked for a config file but I couldn't find it"

    # strip the directory
    if 'finalPlot_configs/' in configname:
        configname = configname.split('/')[-1]
    # strip the ".py"
    if configname.endswith('.py'):
        configname = configname.split('.py')[0]

print "I will import the config", configname

if configname == "TLA2017Paper":                                               from TLA2017Paper import *
elif configname == "SummaryPlotDec17":                                         from SummaryPlotDec17 import *
elif configname == "SummaryPlotJan18":                                         from SummaryPlotJan18 import *
elif configname == "SummaryPlotJan18_noTLA":                                   from SummaryPlotJan18_noTLA import *
elif configname == "SummaryPlotJan18_noTLA_preRun2":                           from SummaryPlotJan18_noTLA_preRun2 import *
elif configname == "SummaryPlotJan18_ATLASTLA":                                from SummaryPlotJan18_ATLASTLA import *
elif configname == "SummaryPlot_mergeCollaborations_Feb18":                    from SummaryPlot_mergeCollaborations_Feb18 import *
elif configname == "SummaryPlot_mergeCollaborations_Feb18_boldTLA":            from SummaryPlot_mergeCollaborations_Feb18_boldTLA import *
elif configname == "SummaryPlot_mergeCollaborations_Feb18_noTLAcomb_boldTLA":  from SummaryPlot_mergeCollaborations_Feb18_boldTLA import *
elif configname == "SummaryPlot_Feb18CMScomparison":                           from SummaryPlot_Feb18CMScomparison import *
elif configname == "SummaryPlot_allDijet":                                     from SummaryPlot_allDijet import *
elif configname == "SummaryPlot_allATLASdijet":                                from SummaryPlot_allATLASdijet import *
elif configname == "SummaryPlot_Dijet_ISR_noTLA":                              from SummaryPlot_Dijet_ISR_noTLA import *
elif configname == "SummaryPlot_Dijet_ISR_withTLA":                            from SummaryPlot_Dijet_ISR_withTLA import *
elif configname == "SummaryPlot_oldStyle_Mar18":                               from SummaryPlot_oldStyle_Mar18 import *
elif configname == "SummaryPlot_oldStyle_Mar18_withDib":                       from SummaryPlot_oldStyle_Mar18_withDib import *
elif configname == "SummaryPlot_EmmaStyle_Mar18_withDib":                      from SummaryPlot_EmmaStyle_Mar18_withDib import *
elif configname == "SummaryPlot_mergeCollaborations_Mar18_lowMass":            from SummaryPlot_mergeCollaborations_Mar18_lowMass import *
elif configname == "SummaryPlot_mergeCollaborations_Mar18_lowMass_v2":         from SummaryPlot_mergeCollaborations_Mar18_lowMass_v2 import *
elif configname == "SummaryPlot_DMsummary_April18":                            from SummaryPlot_DMsummary_April18 import *
elif configname == "SummaryPlot_DMsummary_May18":                              from SummaryPlot_DMsummary_May18 import *
elif configname == "SummaryPlot_DMsummary_May18_v2":                           from SummaryPlot_DMsummary_May18_v2 import *
elif configname == "SummaryPlot_DMsummary_Jul18":                              from SummaryPlot_DMsummary_Jul18 import *
elif configname == "DijetAngular":                                             from DijetAngular import *
elif configname == "SummaryPlot_mergeCollaborations_forCaterina_Jul18":        from SummaryPlot_mergeCollaborations_forCaterina_Jul18 import *
elif configname =="dijetISRResolvedPaper":                                     from dijetISRResolvedPaper import *
elif configname =="dijetISRResolvedPaperAtlasCirculation":                     from dijetISRResolvedPaperAtlasCirculation import *
else:
    sys.exit("I'm afraid I did not recognise your config file, please add it")

# configuration ends


if not useMatplotlib:
    canv=canvastools.canvas(forceNew=False, coords=canvascoords)
    canv.Clear()
    if logx:
        canv.SetLogx()
    if logy:
        canv.SetLogy()

    if setRightMargin:
        canv.SetRightMargin(setRightMargin)
    if setLeftMargin:
        canv.SetLeftMargin(setLeftMargin)
    if setBottomMargin:
        canv.SetBottomMargin(setBottomMargin)
    if setTopMargin:
        canv.SetTopMargin(setTopMargin)


    hist = TH1F("hist",";"+xtitle+";g_{q}", xmax-xmin,xmin,xmax)

    # plottools.plot2d(hist,text={'title':'','lumi':'3.4-15.7','sim':False},textpos=(0.19,0.8),zrange=(0,2),ztitle='')

    hist.Draw()
    hist.SetMaximum(ymax)
    hist.SetMinimum(ymin)
    hist.GetXaxis().SetNoExponent(True)
    hist.GetYaxis().SetNoExponent(True)

    if logx:
        hist.GetXaxis().SetMoreLogLabels()
    if logy:
        hist.GetYaxis().SetMoreLogLabels()

    if setXaxisOffset:
        hist.GetXaxis().SetTitleOffset(setXaxisOffset)
    if setYaxisOffset:
        hist.GetYaxis().SetTitleOffset(setYaxisOffset)

if yearInLegend:
    legLeft = 0.57
    legBot = 0.61
    legRight = 0.89
    legTop = 0.93

else:
    legLeft = 0.58
    legBot = 0.61
    legRight = 0.90
    legTop = 0.93

if setLegLeft:
    legLeft = setLegLeft
if setLegRight:
    legRight = setLegRight
if setLegBot:
    legBot = setLegBot
if setLegTop:
    legTop = setLegTop

if noLegLines:
    legRight = legLeft

if not useMatplotlib:
    leg = TLegend(legLeft, legBot, legRight, legTop)
    leg.SetTextSize(legtextsize)


# Draw lines and fills at given widths
# function to get width(gq)
from width import swCalc
# def swCalc(type, gQ, gDM, mMed, mDM):
    # type: indicates type of the mediator (scalar, pseudo, vector, axial)
    # gQ: coupling of mediator to quarks
    # gDM: coupling of mediator to dark matter
    # mMed: mass of mediator in GeV
    # mDM: mass of dark matter in GeV

if drawWidths != None and not useMatplotlib:
    width_type = "axial"
    width_gDM = 1.0
    width_mDM = 10000

    # I want the gDM corresponding to this width for each mZ
    widthGraphs = []
    for width in drawWidths:
        xVals = []
        yVals = []
        # divide mMed into evenly spaced intervals, linear or log
        for i_mMed in range(numsteps_mMed+1):
            if logx:
                log_mMed = math.log10(xmin) + i_mMed*((math.log10(xmax)-math.log10(xmin))/float(numsteps_mMed))
                width_mMed = 10**log_mMed
                pass
            else:
                width_mMed = xmin + i_mMed*((xmax-xmin)/float(numsteps_mMed))
            xVals.append(width_mMed)

            # "binary sort" convergence on the value until above-below beats precision
            above_gq = ymax
            below_gq = ymin
            while (above_gq - below_gq) / (above_gq + below_gq) > precision_gq*0.5:
                width_gQ = 0.5*(above_gq + below_gq)
                thiswidth = swCalc(width_type, width_gQ, width_gDM, width_mMed, width_mDM)
                if thiswidth/float(width_mMed) > width:
                    above_gq = width_gQ
                else:
                    below_gq = width_gQ

            yVals.append(width_gQ)
        widthGraphs.append(ROOT.TGraph(len(xVals), array('f',xVals), array('f',yVals)))

expgraphs = {}
obsgraphs = {}
obssplines = {}
sublimits = {}
hasExpBand = {}

for analysis in limits_to_plot:
    limit = limits[analysis]
    if not setRefCol:
        refcol = limits[analysis]['col']
    else:
        refcol = setRefCol

    if 'manuallimit' in limit:
        rootfile = None
    elif 'rootfile' in limit:
        rootfile = limit['rootfile']
    else:
        rootfile = limit['grname'] + '.root'

    if "/" in limit["grname"]:
        limit['grname']=limit['grname'].split("/")[1]
    if rootfile != None:
        print("limit[grname]:", limit["grname"])
        print("full rootFile", "intermediate_rootfiles/"+rootfile)
        tfile = TFile('intermediate_rootfiles/' + rootfile)

    hasExpBand[analysis] = True
    if 'manuallimit' in limit:
        hasExpBand[analysis] = False
        xvals = limit['manuallimit'][0]
        yvals = limit['manuallimit'][1]
        obsgraphs[analysis] = ROOT.TGraph(len(xvals), array('f',xvals), array('f',yvals))
        expgraphs[analysis] = ROOT.TGraph(len(xvals), array('f',xvals), array('f',yvals))

    elif type(limit['grname']) == list:
        expgraphs[analysis] = tfile.Get(limit['grname'][0])
        obsgraphs[analysis] = tfile.Get(limit['grname'][1])
        try:
            expgraphs[analysis+'+1'] = tfile.Get(limit['grname'][2])
            expgraphs[analysis+'-1'] = tfile.Get(limit['grname'][3])
            expgraphs[analysis+'+2'] = tfile.Get(limit['grname'][4])
            expgraphs[analysis+'-2'] = tfile.Get(limit['grname'][5])
        except:
            hasExpBand[analysis] = False

    else:
        expgraphs[analysis] = tfile.Get(limit['grname']+'_exp')
        obsgraphs[analysis] = tfile.Get(limit['grname']+'_obs')
        expgraphs[analysis+'+1'] = tfile.Get(limit['grname']+'_exp+1')
        expgraphs[analysis+'-1'] = tfile.Get(limit['grname']+'_exp-1')
        expgraphs[analysis+'+2'] = tfile.Get(limit['grname']+'_exp+2')
        expgraphs[analysis+'-2'] = tfile.Get(limit['grname']+'_exp-2')


    try:
        x = expgraphs[analysis+'+1'].GetX()
    except:
        hasExpBand[analysis] = False

    obsOnly = False
    if 'obsOnly' in limit:
        obsOnly = limit['obsOnly']

    expOnly = False
    if 'expOnly' in limit:
        expOnly = limit['expOnly']

    # scale to lumi
    if 'scaleLumi' in limit:
        lumiRatio = float(limit['scaleLumi']) / float(limit['lumi'])
        limitRatio = lumiRatio**(-0.25)
        if not expOnly:
            xvals = list(obsgraphs[analysis].GetX())
            yvals = list(obsgraphs[analysis].GetY())
            newyvals = [y*limitRatio for y in yvals]
            obsgraphs[analysis] = ROOT.TGraph(len(xvals), array('f',xvals), array('f',newyvals))
        if not obsOnly:
            xvals = list(expgraphs[analysis].GetX())
            yvals = list(expgraphs[analysis].GetY())
            newyvals = [y*limitRatio for y in yvals]
            expgraphs[analysis] = ROOT.TGraph(len(xvals), array('f',xvals), array('f',newyvals))

    if 'inTeV' in limit:
        if limit['inTeV']:
            if not expOnly:
                xvals = list(obsgraphs[analysis].GetX())
                yvals = list(obsgraphs[analysis].GetY())
                newxvals = [x*1000 for x in xvals]
                obsgraphs[analysis] = ROOT.TGraph(len(xvals), array('f',newxvals), array('f',yvals))

            if not obsOnly:
                xvals = list(expgraphs[analysis].GetX())
                yvals = list(expgraphs[analysis].GetY())
                newxvals = [x*1000 for x in xvals]
                expgraphs[analysis] = ROOT.TGraph(len(xvals), array('f',newxvals), array('f',yvals))



    # now to truncate TGraphs to limit['xrange']
    if 'xrange' in limit:
        for analysisplus in expgraphs:
            if analysis not in analysisplus:
                continue
            try:
                xvals = list(expgraphs[analysisplus].GetX())
                yvals = list(expgraphs[analysisplus].GetY())
            except: # doesn't have exp line
                continue
            newxvals = []
            newyvals = []
            for i in range(len(xvals)):
                if xvals[i] < limit['xrange'][0]: continue
                if xvals[i] > limit['xrange'][1]: continue
                newxvals.append(xvals[i])
                newyvals.append(yvals[i])

            print("xvals: ", xvals)
            print("yvals: ", yvals)
            print("newxvals: ", newxvals)
            print("newyvals: ", newyvals)
            expgraphs[analysisplus] = ROOT.TGraph(len(newxvals), array('f',newxvals), array('f',newyvals))

        for analysisplus in obsgraphs:
            if analysis not in analysisplus:
                continue

            print("obsgraphs", obsgraphs)
            xvals = list(obsgraphs[analysisplus].GetX())
            yvals = list(obsgraphs[analysisplus].GetY())
            newxvals = []
            newyvals = []
            for i in range(len(xvals)):
                if xvals[i] < limit['xrange'][0]: continue
                if xvals[i] > limit['xrange'][1]: continue
                newxvals.append(xvals[i])
                newyvals.append(yvals[i])

            obsgraphs[analysisplus] = ROOT.TGraph(len(newxvals), array('f',newxvals), array('f',newyvals))


# at this point, I have all the individual TGraphs
if useMatplotlib:
    import matplotlib.pyplot as plt
    import numpy as np
    import mpld3

    obsarrays = {}
    exparrays = {}
    for key in obsgraphs:
        obsarrays[key] = (list(obsgraphs[key].GetX()), list(obsgraphs[key].GetY()))
    for key in expgraphs:
        try:
            exparrays[key] = (list(expgraphs[key].GetX()), list(expgraphs[key].GetY()))
        except:
            pass


    # print obsarrays

    # plt.figure()
    fig, ax = plt.subplots(figsize=(10,5))
    # ax.grid(True, alpha=0.2)

    obslines = []
    explines = []
    legnames = []

    for key in limits_to_plot: #obsarrays:

        doExp = True
        if key not in exparrays:
            doExp = False

        # legend name
        legname = limits[key]['analysisname']
        if 'SRextra' in limits[key]:
            legname = legname + ', ' + limits[key]['SRextra']
        legname = legname.replace('#it{R}','R')
        legname = legname.replace('#bar{t}','tbar')

        if 'lumi' in limits[key]:
            lumi = limits[key]['lumi']
            if '&' in lumi:
                lumi = lumi.split('&')[-1].lstrip()
            legname = legname + ', '+lumi+' fb-1'

        if 'ref' in limits[key]:
            legname = legname + ', ' + limits[key]['ref']
        elif 'UA2' in key:
            legname = legname + ', ' + 'Phys. Rev. D 88, 035021 (2013)'

        if legname in legnames:
            sys.exit("non-unique legend name")
        legnames.append(legname)

        # colour
        col = getHex( getCol(limits[key]['col']) )
        if key in ['CDFrun1', 'CDFrun2', 'UA2']:
            col = getHex( getCol('grey') )

        obsline, = ax.plot(obsarrays[key][0], obsarrays[key][1], label=legname, color=col)
        obslines.append(obsline)
        if doExp:
            expline, = ax.plot(exparrays[key][0], exparrays[key][1], '--', label=None, color=col)
            explines.append(expline)
        else:
            explines.append(None)

        # ax.fill_between(val.index,
                        # val.values * .5, val.values * 1.5,
                        # color=l.get_color(), alpha=.4)

    # ax.set_aspect(1.0/ax.get_data_ratio()*0.3)


    handles, labels = ax.get_legend_handles_labels() # return lines and labels
    # print len(handles), len(labels)

    print ax.collections

    # http://mpld3.github.io/examples/interactive_legend.html
    interactive_legend = mpld3.plugins.InteractiveLegendPlugin(zip(handles, explines), # make both obs and exp be affected by clicking on legend
                                                         labels,
                                                         alpha_unsel=0.2,
                                                         alpha_over=1.5,
                                                         start_visible=True)
    mpld3.plugins.connect(fig, interactive_legend)

    # ability to zoom
    mpld3.plugins.BoxZoom(button=True, enabled=None)


    # set axis and label fonts and styles
    hfont = {'fontname':'Helvetica'}
    ax.set_xlabel("m(Z') [GeV]", fontsize=18, **hfont)
    ax.set_ylabel('gq', fontsize=18, **hfont)
    ax.tick_params(axis='both', which='major', labelsize=16)


    class xAxisFormat(mpld3.plugins.PluginBase):  # inherit from PluginBase
        """x axis format plugin, from http://nbviewer.jupyter.org/gist/aflaxman/988cb466117430e8ba1b"""

        JAVASCRIPT = """
        mpld3.register_plugin("xaxisformat", xAxisFormat);
        xAxisFormat.prototype = Object.create(mpld3.Plugin.prototype);
        xAxisFormat.prototype.constructor = xAxisFormat;
        function xAxisFormat(fig, props){
            mpld3.Plugin.call(this, fig, props);
        };

        xAxisFormat.prototype.draw = function(){
            // FIXME: this is a very brittle way to select the x-axis element
            var ax = this.fig.axes[0].elements[0];

            // see https://github.com/mbostock/d3/wiki/Formatting#d3_format
            // for d3js formating documentation
            ax.axis.tickFormat(d3.format("d"));
            ax.axis.tickFormat(d3.format("d"));

            // TODO: use a function for tick values that
            // updates when values pan and zoom
            // ax.axis.tickValues([1,100,1000]);

            // HACK: use reset to redraw figure
            this.fig.reset();
        }
        """
        def __init__(self):
            self.dict_ = {"type": "xaxisformat"}


    class ATLASlabel(mpld3.plugins.PluginBase):  # inherit from PluginBase
        """ATLAS label plugin"""

        JAVASCRIPT = """
        mpld3.register_plugin("ATLASlabel", ATLASlabel);
        ATLASlabel.prototype = Object.create(mpld3.Plugin.prototype);
        ATLASlabel.prototype.constructor = ATLASlabel;
        ATLASlabel.prototype.requiredProps = ["atlastext"];
        function ATLASlabel(fig, props){
            mpld3.Plugin.call(this, fig, props);
        };

        ATLASlabel.prototype.draw = function(){
            this.fig.canvas.append("text")
                .text("ATLAS")
                .style("font-size", 18)
                .style("font-weight", "bold")
                .style("text-anchor", "left")
                .attr("x", 0.1 * this.fig.width)
                .attr("y", 0.1 * this.fig.height)

            this.fig.canvas.append("text")
                .text(this.props.atlastext)
                .style("font-size", 18)
                .style("text-anchor", "left")
                .attr("x", 0.17 * this.fig.width)
                .attr("y", 0.1 * this.fig.height)

        }
        """
        def __init__(self, text):
            self.dict_ = {"type": "ATLASlabel",
                          "atlastext" : text}


    class lumilabel(mpld3.plugins.PluginBase):  # inherit from PluginBase
        """lumi label plugin"""

        JAVASCRIPT = """
        mpld3.register_plugin("lumilabel", lumilabel);
        lumilabel.prototype = Object.create(mpld3.Plugin.prototype);
        lumilabel.prototype.constructor = lumilabel;
        lumilabel.prototype.requiredProps = ["lumi"];
        function lumilabel(fig, props){
            mpld3.Plugin.call(this, fig, props);
        };

        lumilabel.prototype.draw = function(){
            this.fig.canvas.append("text")
                .text("sqrt(s) = 13 TeV, " + this.props.lumi + " fb")
                .style("font-size", 18)
                .style("text-anchor", "right")
                .attr("x", 0.35 * this.fig.width)
                .attr("y", 0.1 * this.fig.height)

            this.fig.canvas.append("text")
                .text("-1")
                .style("font-size", 11)
                .style("text-anchor", "right")
                .attr("x", 0.555 * this.fig.width)
                .attr("y", 0.085 * this.fig.height)

        }
        """
        def __init__(self, lumi):
            self.dict_ = {"type": "lumilabel",
                          "lumi" : lumi}


    # "boos = " + this.props.lumi)
    # .text("sqrt(s) = 13 TeV, " + lumi + " fb-1")
    ax.set_xscale('log')

    ax_fmt = xAxisFormat()
    mpld3.plugins.connect(fig, ax_fmt)

    mpld3.plugins.connect(fig, ATLASlabel("Internal July 2018"))
    mpld3.plugins.connect(fig, lumilabel("3.6-37.0"))

    # can't manage to change 1,000 to 1000 etc
    # import matplotlib.ticker as mtick
    # ax.get_xaxis().set_major_formatter( mtick.FuncFormatter(lambda x, pos: str(x).replace(',','.')) )

    # plt.xticks([100,200,300,400,500,600,700,800,900,1000,2000,3000,4000,5000], ['100','200','300','','500','','','','','1000','2000','3000','','5000'])
    plt.xticks([100,200,300,500,1000,2000,3000,5000])
    # ax.axes.xaxis.set_ticklabels([100,200,300,500,1000,2000,3000,5000])

    for tick in ax.get_xticklabels():
        tick.set_fontname("Helvetica")

    for tick in ax.get_yticklabels():
        tick.set_fontname("Helvetica")

    xticks = ax.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[1].label1.set_visible(False)
    xticks[2].label1.set_visible(False)

    # make bit space for legend
    fig.subplots_adjust(right=0.5)


    # draw ATLAS label
    # plt.title("ATLAS Internal July 2018, sqrt(s) = 13 TeV, 3.6-37.0 fb-1", fontsize=18, **hfont)

    # save
    fig.savefig("test.pdf")
    mpld3.save_html(fig, "test.html")
    # mpld3.fig_to_html(fig)
    # mpld3.show()


    sys.exit("done with matplotlib")



# merge lines
for mergeSet in mergeSets:

    # find overlaps
    # turn all into eval-able things
    # eval all of them in 1 GeV steps
    # take the lowest, at switchover point make new entry

    y_interps = {}
    for analysis in mergeSet['analyses']:
        xvals = list(obsgraphs[analysis].GetX())
        yvals = list(obsgraphs[analysis].GetY())
        if 'extraPointsHack' in limits[analysis]:
            for point in limits[analysis]['extraPointsHack']:
                print "I am hacking in the point", point, "to", analysis
                pos = 0
                if point[0] < min(xvals):
                    pos = 0
                elif point[0] > max(xvals):
                    pos = -1
                else:
                    for i in range(len(xvals)):
                        if point[0] > xvals[i]:
                            pos = i
                            break
                # print point, pos, xvals
                if pos == 0:
                    xvals = [point[0]] + xvals
                    yvals = [point[1]] + yvals
                elif pos == -1:
                    xvals = xvals + [point[0]]
                    yvals = yvals + [point[1]]
                else:
                    sys.exit("implement this when necessary...")

            print analysis, xvals, yvals

        y_interps[analysis] = scipy.interpolate.interp1d(xvals, yvals)

    # make a new graph in 1 GeV steps
    new_graphs = []
    lastLowestAnalysis = None
    lastLowestY = 999
    for X in range(xmin, xmax+1,1):
        lowestAnalysis = None
        lowestY = 999
        for analysis in mergeSet['analyses']:
            try: yinterp = y_interps[analysis](X)
            except ValueError: yinterp = 1000
            if yinterp < lowestY:
                lowestY = yinterp
                lowestAnalysis = analysis
        # print X, lowestY, lowestAnalysis
        if lowestAnalysis != None:
            if lastLowestAnalysis == None:
                new_graphs.append({'name': mergeSet['name']+'_'+str(len(new_graphs)), 'x': [], 'y': []})

            # if it's a jump rather than a crossing, start a new tgraph and draw a vertical line
            if lastLowestAnalysis != None and lowestAnalysis != lastLowestAnalysis:
                try:
                    is_crossing = y_interps[lastLowestAnalysis](X) >= y_interps[lowestAnalysis](X) and y_interps[lastLowestAnalysis](X-1) < y_interps[lowestAnalysis](X-1)
                except:
                    is_crossing = False

                if not is_crossing:
                    new_graphs.append({'name': mergeSet['name']+'_'+str(len(new_graphs)), 'x': [], 'y': []})
                    new_graphs[-1]['line'] = [X, lowestY, X, lastLowestY]

                print 'transition between', lastLowestAnalysis, 'and', lowestAnalysis, 'at', X, 'is a crossing?', is_crossing

            new_graphs[-1]['x'].append(X)
            new_graphs[-1]['y'].append(lowestY)


        lastLowestAnalysis = lowestAnalysis
        lastLowestY = lowestY

    # pprint.pprint(new_graphs)

    # hack limits_to_plot
    for analysis in mergeSet['analyses']:
        limits_to_plot.remove(analysis)
    for bit in new_graphs:
        name = bit['name']
        if prepend_merged:
            limits_to_plot.insert(0,name)
        else:
            limits_to_plot.append(name)
        limits[name] = {
            'col': mergeSet['col'],
            'legname': mergeSet['legname'],
            'analysisname': mergeSet['analysisname'],
            'obsOnly': True, # change if have expected
            'fillcol': mergeSet['fillcol'],
            'fillalpha': mergeSet['fillalpha'],
            }
        if 'style' in mergeSet:
            limits[name]['style'] = mergeSet['style']
        if new_graphs.index(bit) != 0:
            limits[name]['legname'] = None
        if 'line' in bit:
            limits[name]['lines'] = [bit['line']]

        obsgraphs[name] = ROOT.TGraph(len(bit['x']), array('f',bit['x']), array('f',bit['y']))
        expgraphs[name] = None

    # set aesthetics
    # for analysis in mergeSet['analyses']:
        # limits[analysis]['col'] = mergeSet['col']
        # if analysis == mergeSet['analyses'][0]:
            # limits[analysis]['legname'] = mergeSet['legname']
        # else:
            # limits[analysis]['legname'] = None

    # print

# sys.exit()

for analysis in limits_to_plot:
    limit = limits[analysis]
    if not setRefCol:
        refcol = limits[analysis]['col']
    else:
        refcol = setRefCol

    # for everything I have, move points along straight lines to match drawinterval
    if 'drawinterval' in limit:
        for analysisplus in obsgraphs:
            if analysis not in analysisplus:
                continue

            xvals = list(obsgraphs[analysisplus].GetX())
            print analysis, xvals, limit['drawinterval']
            if limit['drawinterval'][0] < xvals[0]:
                sys.exit("lower edge of draw interval for " + analysis + " is lower than minimum x value of " + str(xvals[0]))
            if limit['drawinterval'][1] > xvals[-1]:
                sys.exit("upper edge of draw interval for " + analysis + " is higher than maximum x value of " + str(xvals[-1]))

            yvals = list(obsgraphs[analysisplus].GetY())

            y_interp = scipy.interpolate.interp1d(xvals, yvals)
            inside = False
            above = False
            newxvals = []
            newyvals = []
            print limit['drawinterval']
            for i in range(len(xvals)):
                xval = xvals[i]
                yval = yvals[i]

                if xval < limit['drawinterval'][0]:
                    pass

                elif xval >= limit['drawinterval'][0] and not inside and xval <= limit['drawinterval'][1]:
                    inside = True
                    newxval = limit['drawinterval'][0]
                    newyval = y_interp(newxval)

                    newxvals.append(newxval)
                    newyvals.append(newyval)
                    newxvals.append(xval)
                    newyvals.append(yval)

                elif inside and xval >= limit['drawinterval'][1]:
                    inside = False
                    above = True
                    newxval = limit['drawinterval'][1]
                    newyval = y_interp(newxval)
                    newxvals.append(newxval)
                    newyvals.append(newyval)

                elif inside:
                    newxvals.append(xval)
                    newyvals.append(yval)

                elif xval >= limit['drawinterval'][0] and not inside and  xval >= limit['drawinterval'][1] and not above:
                    newxval = limit['drawinterval'][0]
                    newyval = y_interp(newxval)
                    newxvals.append(newxval)
                    newyvals.append(newyval)
                    newxval = limit['drawinterval'][1]
                    newyval = y_interp(newxval)
                    newxvals.append(newxval)
                    newyvals.append(newyval)

                # print xval, inside

            obsgraphs[analysisplus] = ROOT.TGraph(len(newxvals), array('f',newxvals), array('f',newyvals))


        # ridiculous repetition, move to function
        for analysisplus in expgraphs:
            if analysis not in analysisplus:
                continue

            try: xvals = list(expgraphs[analysisplus].GetX())
            except: continue

            xvals = list(expgraphs[analysisplus].GetX())
            if limit['drawinterval'][0] < xvals[0]:
                sys.exit("lower edge of draw interval for " + analysis + " is lower than minimum x value")
            if limit['drawinterval'][1] > xvals[-1]:
                sys.exit("upper edge of draw interval for " + analysis + " is higher than maximum x value")

            yvals = list(expgraphs[analysisplus].GetY())

            y_interp = scipy.interpolate.interp1d(xvals, yvals)
            inside = False
            above = False
            newxvals = []
            newyvals = []
            print limit['drawinterval']
            for i in range(len(xvals)):
                xval = xvals[i]
                yval = yvals[i]

                if xval < limit['drawinterval'][0]:
                    pass

                elif xval >= limit['drawinterval'][0] and not inside and xval <= limit['drawinterval'][1]:
                    inside = True
                    newxval = limit['drawinterval'][0]
                    newyval = y_interp(newxval)

                    newxvals.append(newxval)
                    newyvals.append(newyval)
                    newxvals.append(xval)
                    newyvals.append(yval)

                elif inside and xval >= limit['drawinterval'][1]:
                    inside = False
                    above = True
                    newxval = limit['drawinterval'][1]
                    newyval = y_interp(newxval)
                    newxvals.append(newxval)
                    newyvals.append(newyval)

                elif inside:
                    newxvals.append(xval)
                    newyvals.append(yval)

                elif xval >= limit['drawinterval'][0] and not inside and  xval >= limit['drawinterval'][1] and not above:
                    newxval = limit['drawinterval'][0]
                    newyval = y_interp(newxval)
                    newxvals.append(newxval)
                    newyvals.append(newyval)
                    newxval = limit['drawinterval'][1]
                    newyval = y_interp(newxval)
                    newxvals.append(newxval)
                    newyvals.append(newyval)


            expgraphs[analysisplus] = ROOT.TGraph(len(newxvals), array('f',newxvals), array('f',newyvals))



    print "I am getting", limit
    print analysis, expgraphs[analysis], obsgraphs[analysis]
    if drawBand:
        print "you asked for an expected band, did I find one?", hasExpBand[analysis]


    # thisxminexp = TMath.MinElement(expgraphs[analysis].GetN(),expgraphs[analysis].GetX())
    # thisxminobs = TMath.MinElement(obsgraphs[analysis].GetN(),obsgraphs[analysis].GetX())

    obsOnly = False
    if 'obsOnly' in limit:
        obsOnly = limit['obsOnly']

    expOnly = False
    if 'expOnly' in limit:
        expOnly = limit['expOnly']

    if not obsOnly:
        expgraphs[analysis].SetLineWidth(lineWidth)
        if photoshop and limits.index(limit)==2:
            expgraphs[analysis].SetLineWidth(203)
        if not expOnly:
            expgraphs[analysis].SetLineStyle(ROOT.kDashed)
        expgraphs[analysis].SetLineColor(getCol(limit['col']))

        if hatchedOneSidedBand:
            expgraphs[analysis].SetFillStyle(3003)
            if photoshop and limits.index(limit)==2:
                expgraphs[analysis].SetFillStyle(3157)

        if drawBand and hasExpBand[analysis]:
            xVals = list(expgraphs[analysis].GetX())
            nPoints = len(xVals)

            yVals = {}
            for explim in ['+1','-1','+2','-2']:
                yVals[explim] = list(expgraphs[analysis+explim].GetY())


            expgraphs[analysis+'_1sigma'] = ROOT.TGraph(2*nPoints)
            expgraphs[analysis+'_2sigma'] = ROOT.TGraph(2*nPoints)
            for i in range(nPoints):
                expgraphs[analysis+'_1sigma'].SetPoint(i,xVals[i],yVals['+1'][i])
                expgraphs[analysis+'_1sigma'].SetPoint(nPoints+i,xVals[nPoints-i-1],yVals['-1'][nPoints-i-1])
                expgraphs[analysis+'_2sigma'].SetPoint(i,xVals[i],yVals['+2'][i])
                expgraphs[analysis+'_2sigma'].SetPoint(nPoints+i,xVals[nPoints-i-1],yVals['-2'][nPoints-i-1])

            expgraphs[analysis+'_2sigma'].SetFillColor(getCol(limit['col']+'b2'))
            expgraphs[analysis+'_1sigma'].SetFillColor(getCol(limit['col']+'b1'))

        expgraphs[analysis].SetFillColor(getCol(limit['col']))


    if not expOnly:
        obsgraphs[analysis].SetLineWidth(2)
        obsgraphs[analysis].SetLineColor(getCol(limit['col']))
        if 'fillcol' in limit:
            if limit['fillcol'] != None:
                obsgraphs[analysis].SetFillColorAlpha(getCol(limit['fillcol']), limit['fillalpha'])
                print analysis, "should be filled"
        else:
            obsgraphs[analysis].SetFillColor(getCol(limit['col']))
            obsgraphs[analysis].SetFillStyle(3375) # sometimes 3395...

        if photoshop:
            if limits.index(limit)==3:
                obsgraphs[analysis].SetLineColorAlpha(getCol(limit['col']), 0.5)
                if not obsOnly:
                    expgraphs[analysis].SetLineColorAlpha(getCol(limit['col']), 0.5)
                    expgraphs[analysis].SetFillColorAlpha(getCol(limit['col']), 0.5)

        if 'style' in limit:
            if 'bold' in limit['style']:
                obsgraphs[analysis].SetLineWidth(4)
            if 'thin' in limit['style']:
                obsgraphs[analysis].SetLineWidth(1)
            if 'dashed' in limit['style']:
                obsgraphs[analysis].SetLineStyle(ROOT.kDashed)

        if obsPoints:
            obsgraphs[analysis].SetMarkerSize(obsMarkerSize)
            obsgraphs[analysis].SetMarkerStyle(obsMarkerStyle)
            if obsMarkerCol:
                obsgraphs[analysis].SetMarkerColor(getCol(limit['col']))



    if limit['legname'] != None:
        if 'LUMI' in limit['legname']:
            limit['legname'] = limit['legname'].replace('LUMI', limit['lumi'])
        # Hack the legend to remove the lumi and year information
        if not yearInLegend:
            donotchange = False
            print "changing legend:\n", limit['legname']
            if ' 2015+16' in limit['legname']:
                endpart = limit['legname'].split(' 2015+16')[1]
            elif ' 2015' in limit['legname']:
                endpart = limit['legname'].split(' 2015')[1]
            elif ' 2016' in limit['legname']:
                endpart = limit['legname'].split(' 2016')[1]
            else:
                print "Don't know what to do with year info given, am going to ignore"
                donotchange = True

            if not donotchange:
                if not lumiInLegend:
                    limit['legname'] = ','.join(limit['legname'].split(',')[:-1]) + endpart
                else:
                    limit['legname'] = limit['legname'].split('minus1}')[0]+'minus1}' + endpart

            if '#scale' in limit['legname']:
                limit['legname'] = limit['legname'].split('#scale')[0] + limit['legname'].split('scale')[1].split(']{')[1].split('}')[0] + '}'.join(limit['legname'].split('scale')[1].split(']{')[1].split('}')[1:])
            #scale[0.9]{arXiv: 1703.09127 [hep-ex]}
            # limit['legname'] = limit['legname'].replace('arXiv: ','')

            print limit['legname']

        if shuffle:
            pass
        else:
            if expOnly:
                leg.AddEntry(expgraphs[analysis], limit['legname'], "L")
            else:
                if 'fillcol' in limit:
                    leg.AddEntry(obsgraphs[analysis], limit['legname'], "FL")
                else:
                    leg.AddEntry(obsgraphs[analysis], limit['legname'], "L")



# define fill areas step 1 - vertical bound at start and end
for analysis in limits_to_plot:
    top_gq = 0.4
    xVals = list(obsgraphs[analysis].GetX())
    yVals = list(obsgraphs[analysis].GetY())

    if 'hackFillPoints' in limits[analysis]:
        if limits[analysis]['hackFillPoints'][0] != None:
            for point in limits[analysis]['hackFillPoints'][0]:
                xVals = [point[0]] + xVals
                yVals = [point[1]] + yVals
                # add white box
                if 'hackWhiteBox' in limits[analysis]:
                    if limits[analysis]['hackWhiteBox']:
                        whitebox = [xVals[0], yVals[1], xVals[1], min(top_gq, ymax)]
                        if 'whiteboxes' in limits[analysis]:
                            limits[analysis]['whiteboxes'].append(whitebox)
                        else:
                            limits[analysis]['whiteboxes'] = [whitebox]

        if limits[analysis]['hackFillPoints'][1] != None:
            for point in limits[analysis]['hackFillPoints'][1]:
                xVals = xVals + [point[0]]
                yVals = yVals + [point[1]]


    xVals = [xVals[0]] + xVals + [xVals[-1]]
    yVals = [top_gq] + yVals + [top_gq]
    obsgraphs[analysis+'_fill'] = ROOT.TGraph(len(xVals), array('f', xVals), array('f',yVals))
    print analysis+'_fill'
    print xVals
    print yVals

if fillObs:

    # define fill areas step 2 - find intersections
    intersections = {}
    markers = []
    for analysis in limits_to_plot:
        intersections[analysis] = {}
        for otheranalysis in limits_to_plot:
            if otheranalysis == analysis: continue
            # each entry of form [x, y, (graphA>graphB to left of intersection - i.e. A is better than B after this point, as opposed to vice versa)]
            # intersections[analysis][otheranalysis] = getTGraphIntersections(obsgraphs[analysis+'_fill'], obsgraphs[otheranalysis+'_fill'])
            intersections[analysis][otheranalysis] = getTGraphIntersections(obsgraphs[analysis], obsgraphs[otheranalysis])

        intersections[analysis]['all'] = merge_intersections(intersections, analysis, obsgraphs, limits_to_plot)
        print analysis, intersections[analysis]['all']

        # draw intersection points
        # if True:
        # if '20.3' in analysis:
        if False:
            for intersection in intersections[analysis]['all']:
                markers.append(ROOT.TMarker(intersection[0], intersection[1], 20))
                markers[-1].SetMarkerColor(getCol(limits[analysis]['col']))
                markers[-1].Draw()


    # can ignore intersections and just use TGraph::Eval
    fillareas = get_fills_with_eval(obsgraphs, limits_to_plot, intersections, curved, canv, xmin, xmax, top_gq)

    for analysis in limits_to_plot:
        limit = limits[analysis]
        # obsgraphs[analysis+'_fill'].SetFillColorAlpha(getCol(limit['col']), 0.1)
        # obsgraphs[analysis+'_fill'].Draw("F same")
        # for i in range(len(sublimits[analysis])):
            # sublimits[analysis][i].SetFillColorAlpha(getCol(limit['col']), 0.1)
            # sublimits[analysis][i].Draw("F same")
        try:
            fillareas[analysis].SetFillColorAlpha(getCol(limit['col']), 0.2)
            if fillsCurved:
                fillareas[analysis].Draw("FC same")
            else:
                fillareas[analysis].Draw("F same")
        except:
            pass
        # canvastools.save('test.png')

else:
    for analysis in limits_to_plot:
        limit = limits[analysis]
        if 'fillcol' not in limit:
            continue
        if limit['fillcol'] != None:
            obsgraphs[analysis+'_fill'].SetFillColorAlpha(getCol(limit['fillcol']), limit['fillalpha'])
            if fillsCurved:
                obsgraphs[analysis+'_fill'].Draw("FC same")
            else:
                obsgraphs[analysis+'_fill'].Draw("F same")


# do all the drawing
for analysis in limits_to_plot:
    limit = limits[analysis]

    obsOnly = False
    if 'obsOnly' in limit:
        obsOnly = limit['obsOnly']

    expOnly = False
    if 'expOnly' in limit:
        expOnly = limit['expOnly']

    if not obsOnly:
        if drawBand:
            try:
                expgraphs[analysis+'_2sigma'].Draw("F same") # C is problematic because bends end
                expgraphs[analysis+'_1sigma'].Draw("F same") # C is problematic because bends end
            except:
                pass

        if curved: expgraphs[analysis].Draw('C same')
        else:      expgraphs[analysis].Draw('same')

    if not expOnly:
        if obsPoints:
            if curved: obsgraphs[analysis].Draw('PLC same')
            else: obsgraphs[analysis].Draw('PL same')
        else:
            if curved:
                obsgraphs[analysis].Draw('C same')
                # obssplines[analysis] = ROOT.TSpline3("gs"+analysis, obsgraphs[analysis])
                # obssplines[analysis].Draw("same");

            else: obsgraphs[analysis].Draw('same')



# draw extra text, lines, white boxes
tlines = []
tboxes = []
for analysis in limits_to_plot:
    limit = limits[analysis]
    if textOnLines:
        try:
            text = limit['analysisname']
        except:
            text = ''
        if 'text' in limit:
            if limit['text'] not in [None,'']:
                if text != '':
                    # limit['text'] = text+', '+limit['text']
                    limit['text'] = limit['text']
            else:
                limit['text'] = text
        else:
            limit['text'] = text

        if limit['text'] == 'Resolved dijet + ISR (j)':
            limit['text'] = '#splitline{  Resolved}{dijet + ISR (j)}'
        if limit['text'] == 'Resolved dijet + ISR (#gamma)':
            limit['text'] = '#splitline{  Resolved}{dijet + ISR (#gamma)}'
        if limit['text'] == 'Boosted dijet + ISR':
            limit['text'] = '#splitline{  Boosted}{dijet + ISR}'


    if 'text' in limit and drawTexts:
        col = getCol(limit['col'])
        if blackTextLabel:
            col = ROOT.kBlack
        if 'pos' not in limit or limit['text'] == None:
            print analysis, 'is missing one of pos or text'
            print limit
        else:
            TextLabelBold(limit['pos'][0], limit['pos'][1], limit['text'], col, textsize = textlabelsize)

    if 'lines' in limit:
        for l in limit['lines']:
            tlines.append(TLine(l[0], l[1], l[2], l[3]))
            tlines[-1].SetLineStyle(verticalLineStyle)
            tlines[-1].SetLineColor(verticalLineColour)
            tlines[-1].Draw()

    if 'whiteboxes' in limit:
        for box in limit['whiteboxes']:
            tboxes.append(TBox(box[0], box[1], box[2], box[3]))
            tboxes[-1].SetLineWidth(0)
            tboxes[-1].SetFillColor(ROOT.kWhite)
            tboxes[-1].Draw()


# draw bonus text boxes
ROOT.gPad.Update()
# return (x - gPad->GetX1())/(gPad->GetX2()-gPad->GetX1());
if bonusTexts not in [None, False]: # it should be None, but somehow is False...
    for bonusText in bonusTexts:
        NDC = False
        if 'NDC' in bonusText:
            if bonusText['NDC']:
                NDC = True

        if NDC:
            xNDC = bonusText['pos'][0]
            x = ROOT.gPad.GetX1() + xNDC * (ROOT.gPad.GetX2()-ROOT.gPad.GetX1())
            if logx:
                x = 10**(x)

            yNDC = bonusText['pos'][1]
            y = ROOT.gPad.GetY1() + yNDC * (ROOT.gPad.GetY2()-ROOT.gPad.GetY1())
            if logy:
                y = 10**(y)

        else:
            x = bonusText['pos'][0]
            y = bonusText['pos'][1]

        col = ROOT.kBlack
        if 'col' in bonusText:
            col = getCol(bonusText['col'])

        TextLabelBold(x, y, bonusText['text'], col, textsize = bonusText['size'])


# ATLAS label
if atlasText != None:
    atlastext = '#bf{#it{ATLAS}} '+atlasText
    if photoshop:
        atlastext = '#bf{#it{ATLAS}} Preliminary (mod)'

    atlaslabel = TLatex()
    if smallerText:
        atlaslabel.SetTextSize(0.04)
    else:
        atlaslabel.SetTextSize(0.05)
    atlaslabel.DrawLatexNDC(atlaslabelX, atlaslabelY, atlastext)

if extraText != None:
    if type(extraText) != list:
        extraText = [extraText]
    for i in range(len(extraText)):
        text = TLatex()
        text.SetTextSize(extraTextSize)
        text.DrawLatexNDC(extraTextpos[0], extraTextpos[1] - extraTextpos[2]*i, extraText[i])


# lumi label
etext = '#sqrt{s} = 13 TeV'
if lumiOnPlot:
    lumitext += ', MINLUMI-MAXLUMI fb^{#minus1}'
#minlumi = '999'
#maxlumi = '0'
#for analysis in limits_to_plot:
#    limit = limits[analysis]
#    try: lumis = [float(l) for l in limit['lumi'].split(',')]
#    except: continue
#    print lumis
#    if max(lumis) > float(maxlumi):
#        maxlumi = '%.1f' % max(lumis) # can't keep original string if multiple - was limit['lumi'] # I want to keep the original string
#    if min(lumis) < float(minlumi):
#        minlumi = '%.1f' % min(lumis) # can't keep original string if multiple - was limit['lumi'] # I want to keep the original string
#if minlumi==maxlumi:
#    lumitext = etext.replace('MINLUMI-MAXLUMI', minlumi)
#else:
#    lumitext = lumitext.replace('MINLUMI', minlumi).replace('MAXLUMI', maxlumi)


if drawSqrtsText:
    elabel = TLatex()
    if smallerText:
        elabel.SetTextSize(0.04)
        elabel.DrawLatexNDC(atlaslabelX + lumilabelXspacing, atlaslabelY - lumilabelYspacing,etext)

    else:
        elabel.SetTextSize(0.05)
        elabel.DrawLatexNDC(atlaslabelX + lumilabelXspacing, atlaslabelY - lumilabelYspacing,etext)


lumitext="79.8 fb^{#minus1}"
lumitext2="76.6 fb^{#minus1}"
lumilabel=TLatex()
lumilabel2=TLatex()
lumilabel.SetTextSize(0.05)
lumilabel.DrawLatexNDC(0.25, 0.65,lumitext)
lumilabel2.SetTextSize(0.05)
lumilabel2.DrawLatexNDC(0.5, 0.65, lumitext2)


# legends
leg.SetBorderSize(0)
leg.SetFillStyle(4000)
leg.SetTextFont(42)
if drawLeg:
    leg.Draw()

# exp/obs legend
if date == 'Summer 2016' or photoshop:
    leg2Left = 0.7
    leg2Bot = 0.18
    leg2Right = 0.9
    leg2Top = 0.26

else:
    if yearInLegend:
        leg2Left = 0.57
        leg2Bot = 0.45
        leg2Right = 0.89
        leg2Top = 0.58
    else:
        leg2Left = 0.20
        leg2Bot = 0.60
        leg2Right = 0.50
        leg2Top = 0.80

if setLeg2Left:
    leg2Right = leg2Right + setLeg2Left - leg2Left
    leg2Left = setLeg2Left
if setLeg2Right:
    leg2Right = setLeg2Right
if setLeg2Bot:
    leg2Bot = setLeg2Bot
if setLeg2Top:
    leg2Top = setLeg2Top

leg2 = TLegend(leg2Left,leg2Bot,leg2Right,leg2Top)
leg2.SetBorderSize(0)
leg2.SetFillStyle(4000)
leg2.SetTextFont(42)
leg2.SetTextSize(leg2textsize)


lobs = ROOT.TGraph()
lexp = ROOT.TGraph()
lexp.SetLineStyle(ROOT.kDashed)
lexp.SetLineWidth(lineWidth)
lobs.SetLineWidth(2)

if colourLeg2:
    lobs.SetLineColor(getCol(refcol))
    lexp.SetLineColor(getCol(refcol))

# leg2.AddEntry(lexp, "Expected 95% CL upper limit", "L")
# leg2.AddEntry(lobs, "Observed 95% CL upper limit", "L")
extralegend = ""
if shuffle:
    if withDijet:
        extralegend = "TLA "

leg2.SetHeader("95% CL upper limits")
if obsPoints:
    lobs.SetMarkerSize(obsMarkerSize)
    lobs.SetMarkerStyle(obsMarkerStyle)
    if obsMarkerCol and colourLeg2:
        lobs.SetMarkerColor(getCol(refcol))
    leg2.AddEntry(lobs, extralegend+"Observed", "LP")
else:
    leg2.AddEntry(lobs, extralegend+"Observed", "L")

if drawBand:
    lexp.SetLineStyle(1)
    lexp.SetLineWidth(bandlinewidth)
    if colourLeg2:
        lexp.SetLineColor(getCol(refcol+'b1'))
        lexp.SetFillColor(getCol(refcol+'b2'))
    else:
        lexp.SetLineColor(ROOT.kGray+1)
        lexp.SetFillColor(ROOT.kGray)
    leg2.AddEntry(lexp, extralegend+"Expected (#pm 1-2#sigma)", "LF")
else:
    leg2.AddEntry(lexp, extralegend+"Expected", "L")

if shuffle and withDijet:
    leg2.AddEntry(obsgraphs[analysis], limit['legname'], "LP")
# boxes = []
# if drawBand:
    # boxes.append(TBox(0,0,0,0))
    # boxes.append(TBox(0,0,0,0))
    # boxes[0].SetFillColor(ROOT.kGray+1)
    # boxes[1].SetFillColor(ROOT.kGray)
    # if colourLeg2:
        # boxes[0].SetFillColor(getCol(limit['col']+'b1'))
        # boxes[1].SetFillColor(getCol(limit['col']+'b2'))

    # boxes[0].SetLineWidth(0)
    # boxes[1].SetLineWidth(0)
    # leg2.AddEntry(boxes[0], "Expected #pm1 #sigma", "F")
    # leg2.AddEntry(boxes[1], "Expected #pm2 #sigma", "F")


# extra legend from yvonne
leg3 = TText(0.5, 0.6, "76.6")
leg3.Draw()
if drawLeg2:
    leg2.Draw()


if drawBand:
    expline = TLine()
    expline.SetLineStyle(ROOT.kDashed)
    expline.SetLineWidth(lineWidth)
    if colourLeg2:
        expline.SetLineColor(getCol(refcol))
    expline.DrawLineNDC(leg2Left+leg2LeftPlus, leg2Bot+leg2BotPlus, leg2Left+leg2RightPlus, leg2Bot+leg2BotPlus)

if dateOnTop:
    datetext = TLatex()
    datetext.SetTextFont(42)
    datetext.SetTextSize(0.04)
    datetext.DrawLatexNDC(0.8,0.96, date)

ROOT.gPad.RedrawAxis()

# add extra tickmarks
if addMajorTicks != None:
    addTicks(hist, addMajorTicks)
if addMinorTicks != None:
    addTicks(hist, addMinorTicks, major = False, numDivisions = minorTickDivisions)


savename = 'SummaryPlot_mZ-gq_' + date.replace(' ','')

if bonusname:
    savename += '_'+bonusname

if photoshop:
    savename += '_photoshop'

if not curved:
    savename += '_noC'

if yearInLegend:
    savename += '_year'

if logy:
    savename += '_logy'

if drawBand:
    savename += '_expBands'

if atlasText != None:
    if 'Preliminary' in atlasText:
        savename += '_prelim'

widthLabels = []
if drawWidths!=None:
    for i in range(len(drawWidths)):
        # draw graph
        g = widthGraphs[i]
        g.SetLineStyle(2)
        g.SetLineColor(ROOT.kGray)
        g.Draw("L")

        # label it
        widthLabels.append(ROOT.TLatex())
        widthLabels[-1].SetTextSize(textlabelsize)
        widthLabels[-1].SetTextColor(ROOT.kGray+1)
        widthLabels[-1].DrawLatex(g.GetX()[int(numsteps_mMed/50)], g.GetY()[1], "#Gamma/m_{Z'}="+str(drawWidths[i]))
        # print drawWidths[i], g.GetY()[1]



canvastools.save(savedir+'/'+savename+'.png')

# the png produced like this is terrible quality
print "converting pdf to png"
command = 'pdftoppm -f 1 -singlefile ' + savedir+'/'+savename + '.pdf ' + savedir+'/'+savename + ' -png -cropbox -r 300'
os.system(command)

