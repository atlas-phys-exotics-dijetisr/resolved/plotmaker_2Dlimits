#prot "quickLimitsDijetsOnly_withb('data/limits_dijets_DS1.txt',selection='',lumi='11.1')"

import ROOT

from prot import utiltools
from prot import canvastools
from prot import plottools

import glob
import re
import os, os.path
import numpy as np
import array

from math import *

def set_palette(name="palette", ncontours=4):
    
    from array import array
    from ROOT import TColor, gStyle
    
    """Set a color palette from a given RGB list
        stops, red, green and blue should all be lists of the same length
        see set_decent_colors for an example"""
    
    if name == "gray" or name == "grayscale":
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red   = [1.00, 0.84, 0.61, 0.34, 0.00]
        green = [1.00, 0.84, 0.61, 0.34, 0.00]
        blue  = [1.00, 0.84, 0.61, 0.14, 0.00]
    # elif name == "whatever":
    # (define more palettes)
    else:
        # default palette, looks cool
        #jamaica
        # stops = [0.00, 0.20, 0.61, 0.84, 1.00]
        # red   = [0.00, 0.00, 0.87, 1.00, 0.51]
        # green = [0.00, 0.81, 1.00, 0.0, 0.00]
        # blue  = [1.00, 0.20, 0.12, 0.00, 0.00]
        stops = [0.00, 0.20, 0.61, 0.84, 1.00]
        red   = [0.00, 0.00, 0.87, 1.00, 0.51]
        green = [0.00, 0.81, 1.00, 0.20, 0.00]
        blue  = [0.51, 1.00, 0.12, 0.00, 0.00]
    
    s = array('d', stops)
    r = array('d', red)
    g = array('d', green)
    b = array('d', blue)

    npoints = len(s)
    TColor.CreateGradientColorTable(npoints, s, r, g, b, ncontours)
    
    # For older ROOT versions
    #gStyle.CreateGradientColorTable(npoints, s, r, g, b, ncontours)
    gStyle.SetNumberContours(ncontours)

def TextLabelBold(x,y,text="",color=ROOT.kBlack):
    l = ROOT.TLatex()
    #l.SetNDC()
    l.SetTextFont(62)
    l.SetTextSize(0.045)
    l.SetTextColor(color)
    l.DrawLatex(x,y,text)

def bestbins(values):
    print "values", values
    mindiff=None
    for i in range(len(values)-1):
        diff=values[i+1]-values[i]
        if mindiff==None or diff<mindiff:
            mindiff=diff
    width=mindiff
    minval=min(values)-width/2
    maxval=max(values)+width/2
    nbins=int((maxval-minval)/width)
    return minval,maxval,nbins

def main(inLimits,selection='dijetgamma_g130_2j25',lumi=None):
    
    ###############
    #####TLA plot
    ###############
    

    # Load the limits
    limits={}
    fh=open(inLimits)
    limits = eval(fh.read())
    fh.close()

    set_palette()
    
    # Determine bins
    gSMs=set()
    mRs=set()
    for gSM,limitsgSM in limits.items():
        gSMs.add(gSM)
        for mR,limit in limitsgSM.items():
            mRs.add(mR)
    gSMs=sorted(list(gSMs))
    mRs =sorted(list(mRs))

    minmR ,maxmR ,nbinsmR =bestbins(mRs)
    mingSM,maxgSM,nbinsgSM=bestbins(gSMs)
    print "best bins:", mingSM, maxgSM

    # Make plot
    ratplots={}
    h2=ROOT.TH2F('excl',"Exclusion Plot;m_{Z'} [TeV];g_{q}",
                 nbinsmR,minmR,maxmR,
                 nbinsgSM,float('%0.3g'%mingSM),float('%0.3g'%maxgSM))
    #h2.GetYaxis().SetRangeUser(0.05,0.17)
    #h2.Print("all")

    for gSM,limitsgSM in limits.items():
        for mR,limit in limitsgSM.items():
            xslim_obs=limit.get('obs'   ,None)
            xslim_exp=limit.get('exp'   ,None)
            xstheory =limit.get('theory',None)
            binIdx=h2.FindBin(mR,gSM)
            if xslim_obs==None:
                h2.SetBinContent(binIdx,0)
                continue
            h2.SetBinContent(binIdx,xslim_obs/(xstheory*1.25))
            ratplots[(mR,gSM)]=(xslim_obs/(xstheory*1.25),xslim_exp/(xstheory*1.25))
            print (mR,gSM), (xslim_obs/(xstheory*1.25),xslim_exp/(xstheory*1.25)), binIdx,xslim_obs/(xstheory*1.25)

    # Make graph
    gobs3=ROOT.TGraph()
    gexp3=ROOT.TGraph()
    gobs=ROOT.TGraph()
    gexp=ROOT.TGraph()
    gidx=0

    #here we have to be very careful with what we're doing - if we go from the highest gSM to the lowest gSM,
    #it means that the interpolation has more work to do
    for mR in mRs:
        # Get interesting point, where interesting = first bin where limit is mu < 1
        h=h2.ProjectionY("_py",h2.GetXaxis().FindBin(mR),h2.GetXaxis().FindBin(mR))
        binLim=h.GetNbinsX()
        for binIdx in range(0, h.GetNbinsX()):
            #was this for binIdx in range(h.GetNbinsX(),0,-1):
            mu=h.GetBinContent(binIdx)
            print('testing for interesting point:',mR, binIdx,mu)
            if mu==0: continue
            binLim=binIdx
            if mu<1:
                break
        gSMref=float('%0.3g'%h.GetBinCenter(binLim))

        print "mR, gSMref", mR, gSMref
        limit=limits.get(gSMref,{}).get(mR,{})
        #for key,value in limits.iteritems() :
        #  print "newdict", key,value
        if mR==1.5:
            print('Dijet 1500',gSMref,mR,limit)
        xsref=limit['theory']
        xsobs=limit['obs']
        xsexp=limit['exp']
        gSMobs=0
        gSMexp=0
        if xsobs==None:
            continue
        else:
            gSMobs=sqrt(gSMref**2*xsobs/(xsref*1.25))
            gSMexp=sqrt(gSMref**2*xsexp/(xsref*1.25))

        print("obs",gSMobs,xsobs,(xsref*1.25))
        print("exp",gSMexp,xsexp,(xsref*1.25))
        gobs.SetPoint(gidx,mR,gSMobs)
        gexp.SetPoint(gidx,mR,gSMexp)
        gidx+=1
    FIATLAS=ROOT.TColor.CreateGradientColorTable(2,
                                                 np.array([0.00, 1.00],'d'),
                                                 np.array([1.00, 1.00],'d'),
                                                 np.array([1.00, 1.00],'d'),
                                                 np.array([1.00, 1.00],'d'),
                                                 255
                                                 )    

    ATPallete=array.array('i',range(FIATLAS,FIATLAS+255))

    ROOT.gStyle.SetPalette(255,ATPallete)
    
    c1=canvastools.canvas()
    c1.Clear()

    #l=ROOT.TLegend(0.2,0.3,0.5,0.2)
    #l=ROOT.TLegend(0.3,0.82,0.6,0.7)
    l=ROOT.TLegend(0.3,0.7,0.62,0.82)

    l.AddEntry(gobs,'obs. limit','L')
    #UNCOMMENT IF LINES
    #l.AddEntry(gexp,'(#sigma_{exp. limit} / #sigma_{theory})','L')
    l.AddEntry(gexp,'exp. limit','LF')

    #plottools.plot2d(h2,text={'title':selection,'lumi':lumi},textpos=(0.38,0.8),zrange=(0,2),ztitle='#sigma_{obs. limit}/#sigma_{theory}')
    plottools.plot2d(h2,text={'title':selection,'lumi':lumi,'sim':False},textpos=(0.6,0.2),zrange=(0,2),ztitle='#sigma_{obs. limit}/#sigma_{theory}')
   
    lineWidth = 100*5 + 5

    gaxis = gobs.Clone()
    gaxis.SetMarkerSize(0)
    gaxis.Draw('AP')#all was C
    gaxis.GetXaxis().SetRangeUser(1.5, 3.5)
    gaxis.GetXaxis().SetTitle("m_{Z'} [GeV]")
    gaxis.GetYaxis().SetRangeUser(0.05, 0.25)
    gaxis.GetYaxis().SetTitle("g_{q}")
    
    gobs.SetLineColor(ROOT.kBlue-2)
    #gobs.SetFillColor(ROOT.kBlue-2)
    gobs.SetLineWidth(2)
    #gobs.SetFillStyle(3375)
    gobs.Draw('C SAME')#all was C

    gexp.SetLineColor(ROOT.kBlue-2)
    gexp.SetFillColor(ROOT.kBlue-2)
    gexp.SetLineWidth(lineWidth)
    gexp.SetFillStyle(3005)
    gexp.SetLineStyle(ROOT.kDashed)
    gexp.Draw('C SAME')

    gobs3.SetLineColor(ROOT.kBlue-2)
    gobs3.SetFillColor(ROOT.kBlue-2)
    gobs3.SetLineWidth(lineWidth)
    gobs3.SetFillStyle(3375)
    gobs3.Draw('C SAME')
    gexp3.SetLineColor(ROOT.kBlue-2)
    gexp3.SetFillColor(ROOT.kBlue-2)
    gexp3.SetLineWidth(lineWidth)
    gexp3.SetFillStyle(3005)
    gexp3.SetLineStyle(ROOT.kDashed)
    gexp3.Draw('C SAME')


    #def ATLAS(title=[],xlabel=0.2,ylabel=0.2,fontsize=0.05,lumi=None,sim=True):
    text=['#font[72]{ATLAS} Internal']
    text.append("#sqrt{s} = 13 TeV; 37.4 fb^{-1}")
    
    latext=None
    for i in range(len(text)):
        if latext==None: latext=text[i]
        else: latext='#splitline{%s}{%s}'%(latext,text[i])

    Tl2=ROOT.TLatex()
    #Tl.SetTextFont(43); Tl.SetTextSize(20);
    Tl2.SetNDC()
    Tl2.DrawLatex(0.6, 0.25, latext);
    utiltools.store.append(Tl2)

    l.Draw()
    l.SetBorderSize(0)

    Tl=ROOT.TLatex()
    markers = []
    for (mR,gSM),(xsobs,xsexp) in ratplots.items():
        #print(mR,gSM)
        #Tl.DrawLatex(mR*1000-25,gSM+0.005,'#color[34]{#scale[0.75]{%0.2f}}'  %(xsobs))
        #Tl.DrawLatex(mR*1000-25,gSM-0.015,'#color[34]{#scale[0.75]{(%0.2f)}}'%(xsexp))
        marker = ROOT.TMarker(mR, gSM, ROOT.kStar)
        marker.SetMarkerColor(33)
        #marker.Draw("same")
        markers.append(marker)

    utiltools.store.append(gobs)
    utiltools.store.append(gexp)

    #adding the points to the legend if necessary
#    mobs3 = ROOT.TMarker(535, 0.273, ROOT.kOpenCircle)
#    mobs3.SetMarkerStyle(ROOT.kFullCircle)
#    mobs3.SetMarkerColor(ROOT.kRed)
#    mexp3 = ROOT.TMarker(535, 0.253, ROOT.kFullCircle)
#    mexp3.SetMarkerStyle(ROOT.kOpenCircle)
#    mexp3.SetMarkerColor(ROOT.kRed)
#    mobs3.PaintMarkerNDC(0.5,0.5)
#    mexp3.PaintMarkerNDC(0.5,0.5)
#    mobs3.Draw("same")
#    mexp3.Draw("same")
    #adding arrows
#    ar1 = ROOT.TArrow(450,0.035,500,0.035,0.03,"");
#    ar1.Draw("")
#    ar1.SetLineWidth(3)
#    ar1.SetLineColor(ROOT.kBlack)
#    ar2 = ROOT.TArrow(500,0.035,550,0.035,0.03,"");
#    ar2.Draw("")
#    ar2.SetLineWidth(3)
#    ar2.SetLineColor(ROOT.kBlack)

#    minmR ,maxmR ,nbinsmR =bestbins(mRs)
#    mingSM,maxgSM,nbinsgSM=bestbins(gSMs)    
#
#    # Make plot
#    ratplots={}
#    h2=ROOT.TH2F('excl',"Exclusion Plot;m_{Z'} [GeV];g_{q}",
#                 nbinsmR,minmR*1e3,maxmR*1e3,
#                 nbinsgSM,float('%0.3g'%mingSM),float('%0.3g'%maxgSM))
#
#    for gSM,limitsgSM in limits.items():
#        for mR,limit in limitsgSM.items():
#            xslim_obs=limit.get('obs'   ,None)
#            xslim_exp=limit.get('exp'   ,None)
#            xstheory =limit.get('theory',None)
#            binIdx=h2.FindBin(mR*1000,gSM)
#            if xslim_obs==None:
#                h2.SetBinContent(binIdx,0)
#                continue
#            h2.SetBinContent(binIdx,xslim_obs/xstheory)
#            ratplots[(mR,gSM)]=(xslim_obs/xstheory,xslim_exp/xstheory)
#
#    # Make graph
#    gobs3=ROOT.TGraph()
#    gexp3=ROOT.TGraph()
#    gobs=ROOT.TGraph()
#    gexp=ROOT.TGraph()
#    gidx=0
#
#    for mR in mRs:
#        # Get interesting point, where interesting = first bin where limit is mu < 1
#        h=h2.ProjectionY("_py",h2.GetXaxis().FindBin(mR*1000),h2.GetXaxis().FindBin(mR*1000))
#        binLim=h.GetNbinsX()
#        for binIdx in range(h.GetNbinsX(),0,-1):
#            mu=h.GetBinContent(binIdx)
#            print('testing',binIdx,mu)
#            if mu==0: continue
#            binLim=binIdx
#            if mu<1:
#                break
#        gSMref=float('%0.3g'%h.GetBinCenter(binLim))
#        print "mR, gSMref", mR, gSMref
#
#        limit=limits.get(gSMref,{}).get(mR,{})
#        if mR==0.55:
#            print('GOD',gSMref,mR,limit)
#        xsref=limit['theory']
#        xsobs=limit['obs']
#        xsexp=limit['exp']
#        gSMobs=0
#        gSMexp=0
#        if xsobs==None:
#            continue
#        else:
#            gSMobs=sqrt(gSMref**2*xsobs/(xsref*1.25))
#            gSMexp=sqrt(gSMref**2*xsexp/(xsref*1.25))
#
#        print(gSMobs,xsobs,(xsref*1.25))
#        print(gSMexp,xsexp,(xsref*1.25))
#        if mR>0.45:
#            gobs.SetPoint(gidx,mR*1000,gSMobs)
#            gexp.SetPoint(gidx,mR*1000,gSMexp)
#            gidx+=1            
#        else:
#            #hacked numbers by hand at this point
#            #h2.SetBinContent(binIdx,xslim_obs/xstheory)
#            #h=h2.ProjectionY("_py",h2.GetXaxis().FindBin(mR*1000),h2.GetXaxis().FindBin(mR*1000))
#            #450, y*<0.3
#            #final, 0.3, observed
#            #'0.3' : { 0.45 : {"excl" :  38.4 , "theo" :  429 },
#            #final, 0.3, expected
#            #'0.3' : { 0.45 : {"excl" :  43.4 , "theo" :  429 },
#            gSMref=0.3
#            xsobs=38.4
#            xsexp=43.4
#            xsref=429.
#            gSMobs=sqrt(gSMref**2*xsobs/(xsref*1.25))
#            gSMexp=sqrt(gSMref**2*xsexp/(xsref*1.25))
#            gobs3.SetPoint(0,450,gSMobs)
#            gexp3.SetPoint(0,450,gSMexp)
#            #final, 0.3, observed
#            #0.55 : {"excl" :  18.1 , "theo" :  203 },
#            #final, 0.3, expected
#            #0.55 : {"excl" :  18.8 , "theo" :  203 },
#            gSMref=0.3
#            xsobs=18.1
#            xsexp=18.8
#            xsref=203.
#            gSMobs=sqrt(gSMref**2*xsobs/(xsref*1.25))
#            gSMexp=sqrt(gSMref**2*xsexp/(xsref*1.25))
#            gobs3.SetPoint(1,550,gSMobs)
#            gexp3.SetPoint(1,550,gSMexp)
#
#
    canvastools.save('couplingMass_highMassDijets_2016_Full_TeV.png')


