## Scripts to make the DM mass -- quark coupling summary plot, and TLA and dijet individual limit plots

There are three steps to perform in order to make a lovely summary plot:
1. Ask your colleagues nicely to send you a limits dictionary in the style of `data/limits2016_TLA_J75yStar03_v6.py`.
Alternatively, you might have lucked out and been sent TGraphs directly, in which case skip step 2.
2. Process each of these limits dictionaries individually in order to perform and display the interpolation / extrapolation.
3. Combine each resulting TGraph into the finished product


### Setup
On lxplus, do `source setup.sh`


### Prepare inputs

This is an excerpt from `data/limits2016_TLA_J75yStar03_v6.py`
```
{
    0.05: {
        0.7: {
            'acc': 0.39481195502260136,
            'exp-2': 0.34231026805805914,
            'exp-1': 0.47100539802961894,
            'nPEs': 640,
            'exp': 0.6661626395760561,
            'exp+1': 0.9406717664120869,
            'exp+2': 1.1962256645385474,
            'dsid': 0,
            'obs': 0.7175640769390303,
            'theory': 2.7802620443918253
        },
```
* The first-level is g<sub>q</sub>, the second level is m<sub>Z'</sub> (in TeV)
* For each (gq,mZ') point, there are the following pieces of information:
  * 95% CL cross-section upper limits: 'obs' and 'exp' are what you might expect, 'exp+1' etc are the 1 and 2 sigma expected bands
  * 'theory' is the theory cross-section. In this case, the acceptance is calculated separately, and also stored in the dictionary under 'acc'.
  This is not necessary, the two can be combined in 'theory' and 'acc' can be omitted.
  * 'dsid' is what it says on the tin, and 'nPEs' is the number of pseudoexperiments used for the limit. Neither of these are required, but can be used with some options (see below)


In case the mZ' and gq in the dictionary are floats, you can do:
```
cd data
python changeToFloats.py
```
(really the code should cope with this, but it doesn't...)

Remember to check in the modified dictionary so others don't have to repeat this step :-)


### Make individual contours

This is done with the `makeLimit` function in `makeOneLimit.py`, steered by `makeLimits.py`. It has to be done seperately for each expected limit contour (eg nominal, +1 sigma, ...)
```
from makeOneLimit import makeLimit
makeLimit(
        inLimits,             # the limits dictionary, eg 'data/limits2016_TLA_J75yStar03_v6.py'
        explim = 0,           # which expected limit line to plot, 0 for nominal, 1 for +1 sigma, -2 for -2 sigma, etc
        bjets = True,         # Does the theory cross-section account for b-quarks? Should usually be True, if False then theory XS is rescaled by 5/4
        acc_scaling = False,  # Should the theory cross-section be rescaled by the provided acceptance? If False, 'acc' is not checked for in dictionary
        massrange = None,     # Restrict Z' mass range to include, in TeV. Does propagate to next stage, but can be reduced further there.
        interp = 'mR',        # What interpolation / extrapolation should be used? Must include one or both of 'mR' and 'gSM'. Default 'mR' -> finds mu=1 point for each Z' mass.
	upAndDown = True,     # extrapolate from both the lowest gq excluded point (previous default) and highest gq non-excluded point, and take the midpoint
        minPEs = 0,           # Require a minimum number of pseudoexperiments for a point to be included
        hack = False,         # Various changes, out-of-date. Leave False
        save = True,          # Save outputs? Leave True
        ATLASx = 'Internal',  # ATLAS label
        selection='',         # some text to be printed on the plot, and controls some default colouring etc. eg 'TLA, J75 y*<0.3' (doesn't propagate to next step)
        lumi = None,          # Luminosity label to be printed on the plot (doesn't propagate to next step)
        TeV = 0,              # mZ' axis labels in TeV? Default GeV
        draw_interp = True,   # Should the interpolation plots be drawn? If True, makes a set of plots in interpolation_plots/<limitDict>/
        curvy = False,        # Should the contours be drawn with straight or curved limes? (doesn't propagate to next step)
        points = True,        # Should the results of interpolation be drawn on the plots? (doesn't propagate to next step)
        noMarkers = False,    # Should the signal points be drawn on the plot? If True, they won't be
        verbose = False,      # print verbose output
    )
```

With interp='mR', for each Z' mass the observed and expected limit points are found by 
extrapolating mu along gq to reach mu=1 from the mu value at the lowest-gq point 
which is excluded (ie mu < 1).
If upAndDown is True (recommended behaviour, improvement as of 20.02.18),
then the same is done for the highest gq point which is not excluded, and the two
resulting values are averaged. Additionally, the limit point is not allowed to go below
the non-excluded point.

With interp='gSM' a fit (e^-x) is performed along mZ' for each gq, and the mu=1 point is extracted. Both options can be done together.
*To do* change this to e.g. take the average of an extrapolation down and 
an extrapolation up, where points exist either side of the limit line.
Maybe weighted by distance of extrapolation?

The following outputs are made, where limitDict is the name of the limits dictionary (.txt stripped):
* intermediate_plots/limitDict/ - a 'summary' plot for each contour, showing (if requested) the points used to make the contour etc
* If requested, interpolation_plots/limitDict/ - a suite of plots showing the interpolations and extrapolations performed, can be used to spot bad extrapolations
* intermediate_rootfiles/limitDict.root - a root file with the relevant TGraphs




### Combine contours
The work is done by `combine_limits.py` which imports configuration information
from finalPlot_configs, according to L70 or thereabouts. 
The relevant ones at the time of writing (December 2017) are:
* TLA2017Paper.py - for the TLA paper figure 4 and aux figures
* SummaryPlotDec17.py - for the DM summary paper

Run with `python combine_limits.py [--config myConfig]`

where `myConfig` can be in the form `finalPlot_configs/configfile.py` or `configfile`.
If `myConfig` is not one of the already recognised options, you will need to add it to the list around L130.


The key pieces are the following three
* `savedir = 'summaryPlots'` - the directory to save the plot to
* `limits = {...}` - a dictionary containing entries for each limit contour you are
  interested in. Can contain more than is plotted, the ones to plot in this instance are 
  set in
* `limits_to_plot = [...]`

The baseline limits dictionary is maintained at `limitlines.py` and can be imported for modification and use.

Example limits dictionary entries:
```
    'TLA100y06v6': {    'grname': 'limits2016_TLA_J100yStar06_v6', 'lumi': '3.6-29.3',
                        'legname': '#splitline{Dijet TLA, LUMI fb^{#minus1} 2016}{EXOT-2016-20}',
                        'text': '|y*#kern[-0.5]{_{12}}| < 0.6', 'col': 'TLA',   'pos': (900,0.125),
                        'xrange': [700,3000] },

    'dijetISRboosted' : { 'rootfile': '../data/limits_dijetISRBoosted.root', 'grname': ['Expected','Observed','ExpectedUp','ExpectedDown','Expected2Up','Expected2Down'],
                          'legname': '#splitline{Large-#it{R} jet + ISR, LUMI fb^{#minus1} 2016}{EXOT-2017-01}',
                          'lumi': '36.1', 'col': 'boosted'},

```
* The dictionary entry is what should be included in the `limits_to_plot` list.
* 'rootfile' - if specified, the name of the root file to look in for TGraphs. Otherwise, take grname + '.root'
* 'grname' - the name of the graph. If a list, these are the names of [Expected, Observed, Expected + 1 sigma, Expected - 1 sigma, Expected  + 2 sigma, Expected - 2 sigma]. Otherwise they will be autofilled according to makeOneLimit.py defaults. i..e. Only use if TGraphs came from an external source.
* 'lumi' - what will br printed in the legend to replace LUMI in 'legname'. Also gets taken into account for computation of overall lumi label.
* 'legname': legend entry (if None then no entry)
* 'col': colour name, as defined in the cols dictionary in `helpers.py`. The expected limit bands, if present, are coloured by colour+'b1' and 'b2'.
* 'pos' and 'text' - a TLatex will be drawn at pos with this text (optional)


There are many other options, mostly aesthetic. They can perhaps be rationalised a bit in future:
```
bonusname = None       # Name to add to output image files
lumiInLegend = True    # include luminosity in legend?
lumiOnPlot = True      # draw a summary luminosity label on the plot?
yearInLegend = False   # include the year in the legend?
atlasText = 'Internal' # ATLAS label
smallerText = False    # Use smaller legend text
canvascoords = []      # not used
xmin = 380             # x-axis minimum
xmax = 2100            # x-axis maximum
ymin = 0.02            # y-axis minimum
ymax = 0.21            # y-axis maximum

photoshop = False      # 'photoshop' a high-mass diejt line on. To be removed / updated.
obsPoints = False      # Draw markers on the observed limit line
logx = True            # log-scale x axis
logy = False           # log-scale y axis
curved = False         # draw straight or curved lines

date = 'December 2017' # date to draw on top (also modifies some behaviour)
dateOnTop = True       # draw date?
dateInLabel = False    # include date in main label?

drawBand = False             # draw expected limit bands where present?
hatchedOneSidedBand = True   # draw one-sided hatched band on expected limit lines?
lineWidth = 100*5 + 5        # width of expected limit line. Needs to be changed if hatchedOneSidedBand is False.

setLegLeft = False     # Manual positioning of legend (replace False by a float)
setLegRight = False    # 
setLegBot = False      # 
setLegTop = False      # 
setLeg2Left = False    # Manual positioning of secondary legend (the one with obs and exp)
setLeg2Right = False   # 
setLeg2Bot = False     # 
setLeg2Top = False     # 

# hacking for band - draw a manual dashed line on top of the legend entry for the secondary legend
bandlinewidth = 10     # width of 1-sigma line in secondary legend
leg2LeftPlus = 0.0112  # amount to shift LH edge of dashed line to fit on legend entry
leg2BotPlus = 0.028    # 
leg2RightPlus = 0.0637 # 
legtextsize = 0.03     # text size in legends
noLegLines = False     # If True, don't draw lines on the legend
colourLeg2 = False     # Colour the lines and fills on the secondary legend

addMajorTicks = None    # Add major tickmarks at the points in this list eg [2000]
addMinorTicks = None    # Add minor tickmarks between the two points in this list eg [1000,2000]
minorTickDivisions = 10 # Sets spacing between minor ticks above
```



=======
This is a readme that hopefully will cause master to exist. Yay
>>>>>>> b3f25664fe25340e9aaaba9d3e2a7a5ddb91fa98
