


# step 1, run all the limts you want, save to root file
# nice ones
python prot.py "makeOneLimit( 'data/limits_tla_all.txt',         selection='TLA',                  lumi='3.4',  bjets=False, massrange=[0.55,10])"
python prot.py "makeOneLimit( 'data/limits_tla_all_ystar03.txt', selection='TLA, y*<0.3',          lumi='3.4',  bjets=False, massrange=[0.45,0.55])"
python prot.py "makeOneLimit( 'data/limits_tla_increaseRange_ystar03.txt', selection='TLA, y*<0.3', lumi='3.4',  bjets=False, massrange=[0.45,1.0])"
python prot.py "makeOneLimit( 'data/limits_dijets_full2016.txt', selection='Dijet full 2016',      lumi='37.0', bjets=True, massrange=[1.5,3.5] )"
python prot.py "makeOneLimit( 'data/limits_gjet_DS2p1.txt',      selection='Dijet + ISR (#gamma)', lumi='15.5', bjets=True )"
python prot.py "makeOneLimit( 'data/limits_3jet_DS2p1.txt',      selection='Dijet + ISR (jet)',    lumi='15.5', bjets=True )"

python prot.py "makeOneLimit( 'data/limits2017_TLA_J75.txt',     selection='TLA, J75 y*<0.3',      lumi='3.6',   bjets=True, massrange=[0.35,0.85], acc_scaling=True)"
python prot.py "makeOneLimit( 'data/limits2017_TLA_J100.txt',    selection='TLA, J100 y*<0.6',     lumi='29.3',  bjets=True, massrange=[0.75,2.0], acc_scaling=True)"


# python prot.py "makeOneLimit( 'data/limits_tla_all.txt',         selection='TLA',                  lumi='3.4',  bjets=False, massrange=[0.55,10], interp='gSMmR', curvy=0)"
# python prot.py "makeOneLimit( 'data/limits_tla_all_ystar03.txt', selection='TLA, y*<0.3',          lumi='3.4',  bjets=False, massrange=[0.45,0.55], interp='gSMmR', curvy=0)"
# python prot.py "makeOneLimit( 'data/limits_dijets_full2016.txt', selection='Dijet full 2016',      lumi='37.0', bjets=True, massrange=[1.5,3.5], interp='gSMmR', curvy=0 )"
# python prot.py "makeOneLimit( 'data/limits_gjet_DS2p1.txt',      selection='Dijet + ISR (#gamma)', lumi='15.5', bjets=True, interp='gSMmR', curvy=0 )"
# python prot.py "makeOneLimit( 'data/limits_3jet_DS2p1.txt',      selection='Dijet + ISR (jet)',    lumi='15.5', bjets=True, interp='gSMmR', curvy=0 )"


# old and buggy
# python prot.py "makeOneLimit_oldBuggy( 'data/limits_tla_all.txt',         selection='TLA',                  lumi='3.4',  bjets=False, massrange=[0.55,10])"
# python prot.py "makeOneLimit_oldBuggy( 'data/limits_tla_all_ystar03.txt', selection='TLA, y*<0.3',          lumi='3.4',  bjets=False, massrange=[0.45,0.55])"
# python prot.py "makeOneLimit_oldBuggy( 'data/limits_dijets_full2016.txt', selection='Dijet full 2016',      lumi='37.0', bjets=True, massrange=[1.5,3.5] )"
# python prot.py "makeOneLimit_oldBuggy( 'data/limits_gjet_DS2p1.txt',      selection='Dijet + ISR (#gamma)', lumi='15.5', bjets=True )"
# python prot.py "makeOneLimit_oldBuggy( 'data/limits_3jet_DS2p1.txt',      selection='Dijet + ISR (jet)',    lumi='15.5', bjets=True )"

# step 2, combine these
# python combine_limits.py

# Standalone dijet plot, don't save to rootfile
declare -a arr=("Internal" "Preliminary" "")
for lab in "${arr[@]}"
do
    # the actual ones
    echo $lab
    # python prot.py "makeOneLimit( 'data/limits_dijets_full2016.txt', selection='Dijet full 2016', lumi='37.0', bjets=True, massrange=[1.5,3.5], TeV=1, noMarkers=1, ATLASx='$lab',    save=0)"
    # python prot.py "makeOneLimit( 'data/limits_dijets_full2016.txt', selection='Dijet full 2016', lumi='37.0', bjets=True, massrange=[1.5,3.5], TeV=1, noMarkers=0, ATLASx='$lab',    save=0)"

    # python prot.py "makeOneLimit( 'data/limits_dijets_full2016.txt', selection='Dijet full 2016', lumi='37.0', bjets=True, massrange=[1.5,3.5], TeV=1, noMarkers=0, ATLASx='Internal',    save=0, curvy=0)"

    # python prot.py "makeOneLimit( 'data/limits_dijets_full2016.txt', selection='Dijet full 2016', lumi='37.0', bjets=True, massrange=[1.5,3.5], TeV=1, noMarkers=0, ATLASx='Internal',    save=0, points=1)"
    # python prot.py "makeOneLimit( 'data/limits_dijets_full2016.txt', selection='Dijet full 2016', lumi='37.0', bjets=True, massrange=[1.5,3.5], TeV=1, noMarkers=0, ATLASx='Internal',    save=0, interp='gSMmR', points=1)"
    # python prot.py "makeOneLimit( 'data/limits_dijets_full2016.txt', selection='Dijet full 2016', lumi='37.0', bjets=True, massrange=[1.5,3.5], TeV=1, noMarkers=0, ATLASx='Internal',    save=0, interp='gSMmR', curvy=0)"
    # python prot.py "makeOneLimit( 'data/limits_dijets_full2016.txt', selection='Dijet full 2016', lumi='37.0', bjets=True, massrange=[1.5,3.5], TeV=1, noMarkers=0, ATLASx='Internal',    save=0, interp='gSMmR', curvy=0, points=1)"

    # python prot.py "makeOneLimit( 'data/limits_dijets_full2016.txt', selection='Dijet full 2016', lumi='37.0', bjets=True, massrange=[1.5,3.5], TeV=1, noMarkers=0, ATLASx='$lab',    save=0, interp='gSMmR', points=1, hack=1)"
    # python prot.py "makeOneLimit( 'data/limits_dijets_full2016.txt', selection='Dijet full 2016', lumi='37.0', bjets=True, massrange=[1.5,3.5], TeV=1, noMarkers=0, ATLASx='$lab',    save=0, interp='gSMmR', curvy=0, hack=1)"


    # python prot.py "makeOneLimit( 'data/limits_dijets_full2016.txt', selection='Dijet full 2016', lumi='37.0', bjets=True, massrange=[1.5,3.5], TeV=1, noMarkers=1, ATLASx='$lab',    save=0, interp='gSMmR', hack=1)"
    # python prot.py "makeOneLimit( 'data/limits_dijets_full2016.txt', selection='Dijet full 2016', lumi='37.0', bjets=True, massrange=[1.5,3.5], TeV=1, noMarkers=0, ATLASx='$lab',    save=0, interp='gSMmR', hack=1)"


done
