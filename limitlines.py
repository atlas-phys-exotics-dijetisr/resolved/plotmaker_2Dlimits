
limits = {
    
    # 2016 dijet
    'dijetFull': { 'grname': 'limits_dijets_full2016', 'lumi': '37.0',
                   'analysisname': 'Dijet', 'ref': 'Phys. Rev. D 96, 052004 (2017)',
                   'legname': '#splitline{Dijet, LUMI fb^{#minus1} 2015+16}{Phys. Rev. D 96, 052004 (2017)}',
                   'xrange': [0,3500],
                   'text': '|y*#kern[-0.5]{_{12}}| < 0.6', 'col': 'dijet', 'pos': (2000,0.062)},


    # partial 2017 dijet+ISR
    'jjg': {       'grname': 'limits_gjet_DS2p1', 'lumi': '15.5',
                   'analysisname': 'Dijet + ISR (#gamma)', 'ref': 'ATLAS-CONF-2016-070',
                   'legname': '#splitline{Dijet + ISR (#gamma), LUMI fb^{#minus1} 2015+16}{ATLAS-CONF-2016-070}',
                   'text': '|y*#kern[-0.5]{_{12}}| < 0.8', 'col': 'jjg',   'pos': (120,0.27)},

    '3jet': {      'grname': 'limits_3jet_DS2p1', 'lumi': '15.5',
                   'analysisname': 'Dijet + ISR (jet)', 'ref': 'ATLAS-CONF-2016-070',
                   'legname': '#splitline{Dijet + ISR (jet), LUMI fb^{#minus1} 2015+16}{ATLAS-CONF-2016-070}',
                   'text': '|y*#kern[-0.5]{_{23}}| < 0.6', 'col': 'jjj',   'pos': (580,0.17)},


    # 2016 TLA latest as of 4pm 11.12
    'TLA100y06v3': {    'grname': 'limits2016_TLA_J100yStar06_v3', 'lumi': '3.6, 29.7',
                        'legname': '#splitline{Dijet TLA, LUMI fb^{#minus1} 2016}{ATLAS-EXOT-2017-XXX}',
                        'text': '|y*#kern[-0.5]{_{12}}| < 0.6', 'col': 'TLA',   'pos': (1000,0.125)},
    'TLA75y03v3': {     'grname': 'limits2016_TLA_J75yStar03_v3',
                        'legname': None,
                        'text': '|y*#kern[-0.5]{_{12}}| < 0.3', 'col': 'TLA',   'pos': (357,0.03) },    

    # 2016 TLA latest as of 5pm 13.12
    # 'TLA100y06v6': {    'grname': 'limits2016_TLA_J100yStar06_v6', 'lumi': '3.6-29.7',
                        # 'legname': '#splitline{Dijet TLA, LUMI fb^{#minus1} 2016}{ATLAS-EXOT-2016-20}',
                        # 'text': '|y*#kern[-0.5]{_{12}}| < 0.6', 'col': 'TLA',   'pos': (1250,0.04),
                        # 'xrange': [700,3000] },
    # 'TLA75y03v6': {     'grname': 'limits2016_TLA_J75yStar03_v6', 'lumi': '3.6',
                        # 'legname':  None, 'lumi': '3.6',
                        # 'text': '|y*#kern[-0.5]{_{12}}| < 0.3', 'col': 'TLA',   'pos': (320,0.09),
                        # 'xrange': [0,700] },


    # 2016 TLA latest as of 5pm 13.12 - WITH FIXED XS
    'TLA100y06v8': {    'grname': 'limits2016_TLA_J100yStar06_v8', 'lumi': '3.6 & 29.7',
                        'analysisname': 'Dijet TLA', 'ref': 'arXiv: 1804.03496',
                        'legname': '#splitline{Dijet TLA, LUMI fb^{#minus1} 2016}{arXiv: 1804.03496}',
                        'SRextra': '|y*|<0.6', 'text': '|y*#kern[-0.5]{_{12}}| < 0.6',
                        'col': 'TLA', 'pos': (1250,0.04),
                        'xrange': [700,3000] },
    'TLA75y03v8': {     'grname': 'limits2016_TLA_J75yStar03_v8', 'lumi': '3.6',
                        'analysisname': 'Dijet TLA',
                        'legname':  None, 'lumi': '3.6',
                        'SRextra': '|y*|<0.3', 'text': '|y*#kern[-0.5]{_{12}}| < 0.3',
                        'col': 'TLA', 'pos': (320,0.09),
                        'xrange': [0,700] },


    # 2016 dijet+ISR boosted
    'dijetISRboosted' : { 'rootfile': '../data/limits_dijetISRBoosted.root', 'grname': ['Expected','Observed','ExpectedUp','ExpectedDown','Expected2Up','Expected2Down'],
                          'analysisname': 'Large-#it{R} jet + ISR', 'ref': 'arXiv: 1801.08769', 
                          'legname': '#splitline{Large-#it{R} jet + ISR, LUMI fb^{#minus1} 2016}{arXiv: 1801.08769}',
                          'lumi': '36.1', 'col': 'boosted', 'pos': (110,0.24)},


    # 2016 di-b
    'dibjetHighMass' : { 'grname': 'limits2016_dibjet_highMass', 'lumi': '24.3 & 36.1',
                         'analysisname': 'Di-b-jet', 'ref': 'arXiv: 1805.09299',
                         'legname': '#splitline{Di-b-jet, LUMI fb^{#minus1} 2016}{some ref}',
                         'col': 'tealyBlue',
                         'SRextra': 'Jet trigger', 'text': 'Jet trigger', 'pos': (1300,0.1),
    },
    
    # dibjetLowMass
    'dibjetLowMass' : { 'grname': 'limits2016_dibjet_lowMass', 'lumi': '24.3',
                        'analysisname': 'Di-b-jet', 'legname': None,
                        'col': 'tealyBlue',
                        'SRextra': 'Di-b-jet trigger', 'text': 'Di-b-jet trigger', 'pos': (600,0.11),
    },

    
    'UA2':                       {'grname': 'UA2', 'analysisname': 'UA2 dijet',
                                  'legname': '#splitline{UA2, 10.9 pb^{-1} 0.63 TeV}{Z. Phys. C 49 (1991) 17}',
                                  'col': 'UA2',
                                  'obsOnly': True},
    'CDFrun1':                   {'grname': 'CDFrun1', 'analysisname': 'CDF run 1 dijet',
                                  'legname': '#splitline{CDF run 1, 106 pb^{-1} 1.8 TeV}{arXiv: hep-ex/9702004}',
                                  'col': 'CDFrun1',
                                  'obsOnly': True},
    'CDFrun2':                   {'grname': 'CDFrun2', 'analysisname': 'CDF run 2 dijet',
                                  'legname': '#splitline{CDF run 2, 1.13 fb^{-1} 1.96 TeV}{arXiv: 0812.4036}',
                                  'col': 'CDFrun2',
                                  'obsOnly': True},
    
    'CMS8dijet20fb':             {'grname': 'CMS8dijet20fb',             'legname': '#splitline{CMS dijet, 19.7 fb^{-1} 8 TeV}{arXiv: 1501.04198}',       'col': 'CMS8dijet20fb',      'obsOnly': True},
    'CMS8scouting18.8fb_manual': {'grname': 'CMS8scouting18.8fb_manual', 'legname': '#splitline{CMS scouting, 18.8 fb^{-1} 8 TeV}{arXiv: 1604.08907}',  'col': 'CMS8scouting18.8fb', 'obsOnly': True},
    'ATLAS8dijet20.3fb_eps':     {
        'grname': 'ATLAS8dijet20.3fb_eps',
        'legname': '#splitline{ATLAS dijet, LUMI fb^{-1} 8 TeV}{arXiv: 1407.1376}',
        # 'legname': '#splitline{ATLAS dijet, 20.3 fb^{-1} 8 TeV}{arXiv: 1407.1376}',
        'analysisname': 'Dijet 8 TeV',
        'lumi': '20.3',
        'ref': 'Phys. Rev. D 91, 052007 (2015)', # arXiv:1407.1376
        'col': 'ATLAS8dijet20.3fb',  'obsOnly': True
    },
    'CMS13scouting12.9_manual': {'grname': 'CMS13scouting12.9_manual', 'legname': '#splitline{CMS scouting, 12.9 fb^{-1} 13 TeV}{arXiv: 1611.03568}', 'col': 'CMS13scouting12.9', 'obsOnly': True},
    'CMS13scouting27_manual': {'grname': 'CMS13scouting27_manual', 'legname': '#splitline{CMS scouting, 27 fb^{-1} 13 TeV}{new add}', 'col': 'CMS13scouting27', 'obsOnly': True},

    'CMS13dijet12.9_manual':     {'grname': 'CMS13dijet12.9_manual',     'legname': '#splitline{CMS dijet, 12.9 fb^{-1} 13 TeV}{arXiv: 1611.03568}',    'col': 'CMS13dijet12.9',     'obsOnly': True},
    'CMS13dijet36_manual':     {'grname': 'CMS13dijet36_manual',     'legname': '#splitline{CMS dijet, 36 fb^{-1} 13 TeV}{new add}',    'col': 'CMS13dijet36',     'obsOnly': True},
    'CMS13boosted35.9_manual':     {'grname': 'CMS13boosted35.9_manual',     'legname': '#splitline{CMS boosted, 35.9 fb^{-1} 13 TeV}{arXiv: 1710.00159}',    'col': 'CMS13boosted35.9',     'obsOnly': True},

    
    # 'UA2_eps': {'grname': 'UA2_eps', 'legname': 'UA2_eps', 'col': 'UA2mod', 'obsOnly': True},

    'dijetAngular' : { 'rootfile': '../data/limitGraph_dijetAngular.root', 'grname': ['ExpectedLimit','ObservedLimit'],
                       'analysisname': 'Dijet Angular', 'ref': 'Phys. Rev. D 96, 052004 (2017)', 
                       'legname': '#splitline{Dijet angular, LUMI fb^{#minus1} 2016}{some ref}',
                       'lumi': '37.0', 'col': 'boosted', 'pos': (110,0.24),
                       'inTeV': True,
    },

    'ttbar' : { 'rootfile': '../data/ttbar_mass_limit_dm/dm10000_coupling_limit.root', 'grname': ['expected','observed'], # dm_coupling_limit.root is old and not to be trusted
                'analysisname': 't#bar{t} resonances', 'ref': 'arXiv: 1804.10823', 
                'legname': '#splitline{t#bar{t} resonance, LUMI fb^{#minus1} 2015+16}{some ref}',
                'lumi': '36.1', 'col': 'boosted',
                'inTeV': True,
    },

    
    }
