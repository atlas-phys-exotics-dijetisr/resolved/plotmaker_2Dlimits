
# copied when split off into separate config

savedir = 'summaryPlots_Jan18_noTLA'

date = 'January 2018'
dateOnTop = True
dateInLabel = False

bonusname = None

lumiInLegend = True
yearInLegend = False

atlasText = 'Internal'
# atlasText = 'Preliminary'

logy = False
logx = True

curved = False

if dateInLabel:
    atlasText += ' '+date
    smallerText = True

setLegLeft = 0.68
setLegBot = 0.18
setLeg2Left = 0.38
legtextsize = 0.025
leg2textsize = 0.028
setLeg2Bot = 0.65
# hacking for band
leg2BotPlus = 0.033
leg2BotPlus = 0.025

# get min and max x and y
xmin = 90
xmax = 4200
# xmin = 45
xmax = 30000
ymin = 0.
ymax = 0.5

addMajorTicks = [2000,3000,4000]
addMinorTicks = [1000,4000]
minorTickDivisions = 30

# expected limit line
# lineWidth = 100*5 + 5
# hatchedOneSidedBand = True
# lineWidth = 5
hatchedOneSidedBand = False
drawBand = False
# drawBand = True
lineWidth = 2



limits = {
    
    # 2016 dijet
    'dijetFull': { 'grname': 'limits_dijets_full2016', 'lumi': '37.0',
                   'legname': '#splitline{Dijet, LUMI fb^{#minus1} 2015+16}{arXiv: 1703.09127}',
                   'text': '|y*#kern[-0.5]{_{12}}| < 0.6', 'col': 'dijet', 'pos': (2000,0.062)},



    # partial 2017 dijet+ISR
    'jjg': {       'grname': 'limits_gjet_DS2p1', 'lumi': '15.5', 
                   'legname': '#splitline{Dijet+ISR (#gamma), LUMI fb^{#minus1} 2015+16}{ATLAS-CONF-2016-070}',
                   'text': '|y*#kern[-0.5]{_{12}}| < 0.8', 'col': 'jjg',   'pos': (120,0.27)},

    '3jet': {      'grname': 'limits_3jet_DS2p1', 'lumi': '15.5',
                   'legname': '#splitline{Dijet+ISR (jet), LUMI fb^{#minus1} 2015+16}{ATLAS-CONF-2016-070}',
                   'text': '|y*#kern[-0.5]{_{23}}| < 0.6', 'col': 'jjj',   'pos': (580,0.17)},


    # 2016 TLA latest as of 4pm 11.12
    'TLA100y06v3': {    'grname': 'limits2016_TLA_J100yStar06_v3', 'lumi': '3.6, 29.7',
                        'legname': '#splitline{Dijet TLA, LUMI fb^{#minus1} 2016}{EXOT-2017-XXX}',
                        'text': '|y*#kern[-0.5]{_{12}}| < 0.6', 'col': 'TLA',   'pos': (1000,0.125)},
    'TLA75y03v3': {     'grname': 'limits2016_TLA_J75yStar03_v3',
                        'legname': None,
                        'text': '|y*#kern[-0.5]{_{12}}| < 0.3', 'col': 'TLA',   'pos': (357,0.03) },    

    # 2016 TLA latest as of 5pm 13.12
    'TLA100y06v6': {    'grname': 'limits2016_TLA_J100yStar06_v6', 'lumi': '3.6-29.7',
                        'legname': '#splitline{Dijet TLA, LUMI fb^{#minus1} 2016}{EXOT-2016-20}',
                        'text': '|y*#kern[-0.5]{_{12}}| < 0.6', 'col': 'TLA',   'pos': (900,0.125),
                        'xrange': [700,3000] },
    'TLA75y03v6': {     'grname': 'limits2016_TLA_J75yStar03_v6', 'lumi': '3.6',
                        'legname':  None,
                        'text': '|y*#kern[-0.5]{_{12}}| < 0.3', 'col': 'TLA',   'pos': (357,0.03),
                        'xrange': [0,700] },


    # 2016 dijet+ISR boosted
    'dijetISRboosted' : { 'rootfile': '../data/limits_dijetISRBoosted.root', 'grname': ['Expected','Observed','ExpectedUp','ExpectedDown','Expected2Up','Expected2Down'],
                          'legname': '#splitline{Large-#it{R} jet + ISR, LUMI fb^{#minus1} 2016}{EXOT-2017-01}',
                          'lumi': '36.1', 'col': 'boosted'},
    

    'UA2':                       {'grname': 'UA2',                       'legname': '#splitline{UA2, 10.9 pb^{-1} 0.63 TeV}{Z. Phys. C 49 (1991) 17}',                       'col': 'UA2',                'obsOnly': True},
    'CDFrun1':                   {'grname': 'CDFrun1',                   'legname': '#splitline{CDF run 1, 106 pb^{-1} 1.8 TeV}{arXiv: hep-ex/9702004}',                 'col': 'CDFrun1',            'obsOnly': True},
    'CDFrun2':                   {'grname': 'CDFrun2',                   'legname': '#splitline{CDF run 2, 1.13 fb^{-1} 1.96 TeV}{arXiv: 0812.4036}',                 'col': 'CDFrun2',            'obsOnly': True},
    'CMS8dijet20fb':             {'grname': 'CMS8dijet20fb',             'legname': '#splitline{CMS dijet, 19.7 fb^{-1} 8 TeV}{arXiv: 1501.04198}',       'col': 'CMS8dijet20fb',      'obsOnly': True},
    'CMS8scouting18.8fb_manual': {'grname': 'CMS8scouting18.8fb_manual', 'legname': '#splitline{CMS scouting, 18.8 fb^{-1} 8 TeV}{arXiv: 1604.08907}',  'col': 'CMS8scouting18.8fb', 'obsOnly': True},
    'ATLAS8dijet20.3fb_eps':     {'grname': 'ATLAS8dijet20.3fb_eps',     'legname': '#splitline{ATLAS dijet, 20.3 fb^{-1} 8 TeV}{arXiv: 1407.1376}',   'col': 'ATLAS8dijet20.3fb',  'obsOnly': True},
    'CMS13scouting12.9_manual': {'grname': 'CMS13scouting12.9_manual', 'legname': '#splitline{CMS scouting, 12.9 fb^{-1} 13 TeV}{arXiv: 1611.03568}', 'col': 'CMS13scouting12.9', 'obsOnly': True},
    'CMS13dijet12.9_manual':     {'grname': 'CMS13dijet12.9_manual',     'legname': '#splitline{CMS dijet, 12.9 fb^{-1} 13 TeV}{arXiv: 1611.03568}',    'col': 'CMS13dijet12.9',     'obsOnly': True},
    'CMS13boosted35.9_manual':     {'grname': 'CMS13boosted35.9_manual',     'legname': '#splitline{CMS boosted, 35.9 fb^{-1} 13 TeV}{arXiv: 1710.00159}',    'col': 'CMS13boosted35.9',     'obsOnly': True},

    
    # 'UA2_eps': {'grname': 'UA2_eps', 'legname': 'UA2_eps', 'col': 'UA2mod', 'obsOnly': True},
    
    }

drawTexts = False


limits_to_plot = [
    'UA2',
    'CDFrun1',
    'CDFrun2',
    # 'CMS8dijet20fb',
    # 'CMS8scouting18.8fb_manual',
    'ATLAS8dijet20.3fb_eps',
    # 'CMS13scouting12.9_manual',
    # 'CMS13dijet12.9_manual',
    # 'CMS13boosted35.9_manual',

    'dijetISRboosted',
    'jjg',
    '3jet',
    'dijetFull',
    'TLA100y06v6',
    'TLA75y03v6',
]
