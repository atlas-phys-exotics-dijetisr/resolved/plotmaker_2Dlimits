
# copied when split off into separate config

savedir = 'summaryPlots_Dec17'

date = 'December 2017'
dateOnTop = True
dateInLabel = False

bonusname = None

lumiInLegend = True
yearInLegend = False

atlasText = 'Internal'
# atlasText = 'Preliminary'

logy = False
logx = True

curved = False

if dateInLabel:
    atlasText += ' '+date
    smallerText = True

setLegLeft = 0.64
setLegBot = 0.55
setLeg2Left = 0.38
# setLeg2Bot = 

# get min and max x and y
xmin = 90
xmax = 4200
ymin = 0.
ymax = 0.5

addMajorTicks = [2000,3000,4000]
addMinorTicks = [1000,4000]
minorTickDivisions = 30

# expected limit line
# lineWidth = 100*5 + 5
# hatchedOneSidedBand = True
# lineWidth = 5
hatchedOneSidedBand = False
# drawBand = False
drawBand = True
lineWidth = 2

# hacking for band
leg2BotPlus = 0.033


limits = {
    
    # 2016 dijet
    'dijetFull': { 'grname': 'limits_dijets_full2016', 'lumi': '37.0',
                   'legname': '#splitline{Dijet, LUMI fb^{#minus1} 2015+16}{arXiv: 1703.09127}',
                   'text': '|y*#kern[-0.5]{_{12}}| < 0.6', 'col': 'dijet', 'pos': (2000,0.062)},



    # partial 2017 dijet+ISR
    'jjg': {       'grname': 'limits_gjet_DS2p1', 'lumi': '15.5', 
                   'legname': '#splitline{Dijet+ISR (#gamma), LUMI fb^{#minus1} 2015+16}{ATLAS-CONF-2016-070}',
                   'text': '|y*#kern[-0.5]{_{12}}| < 0.8', 'col': 'jjg',   'pos': (120,0.27)},

    '3jet': {      'grname': 'limits_3jet_DS2p1', 'lumi': '15.5',
                   'legname': '#splitline{Dijet+ISR (jet), LUMI fb^{#minus1} 2015+16}{ATLAS-CONF-2016-070}',
                   'text': '|y*#kern[-0.5]{_{23}}| < 0.6', 'col': 'jjj',   'pos': (580,0.17)},


    # 2016 TLA latest as of 4pm 11.12
    'TLA100y06v3': {    'grname': 'limits2016_TLA_J100yStar06_v3', 'lumi': '3.6, 29.3',
                        'legname': '#splitline{Dijet TLA, LUMI fb^{#minus1} 2016}{EXOT-2017-XXX}',
                        'text': '|y*#kern[-0.5]{_{12}}| < 0.6', 'col': 'TLA',   'pos': (1000,0.125)},
    'TLA75y03v3': {     'grname': 'limits2016_TLA_J75yStar03_v3',
                        'legname': None,
                        'text': '|y*#kern[-0.5]{_{12}}| < 0.3', 'col': 'TLA',   'pos': (357,0.03) },    

    # 2016 TLA latest as of 5pm 13.12
    'TLA100y06v6': {    'grname': 'limits2016_TLA_J100yStar06_v6', 'lumi': '3.6-29.3',
                        'legname': '#splitline{Dijet TLA, LUMI fb^{#minus1} 2016}{EXOT-2016-20}',
                        'text': '|y*#kern[-0.5]{_{12}}| < 0.6', 'col': 'TLA',   'pos': (900,0.125),
                        'xrange': [700,3000] },
    'TLA75y03v6': {     'grname': 'limits2016_TLA_J75yStar03_v6', 'lumi': '3.6',
                        'legname':  None,
                        'text': '|y*#kern[-0.5]{_{12}}| < 0.3', 'col': 'TLA',   'pos': (357,0.03),
                        'xrange': [0,700] },


    # 2016 dijet+ISR boosted
    'dijetISRboosted' : { 'rootfile': '../data/limits_dijetISRBoosted.root', 'grname': ['Expected','Observed','ExpectedUp','ExpectedDown','Expected2Up','Expected2Down'],
                          'legname': '#splitline{Large-#it{R} jet + ISR, LUMI fb^{#minus1} 2016}{EXOT-2017-01}',
                          'lumi': '36.1', 'col': 'boosted'},
    
    
    }


limits_to_plot = ['dijetISRboosted', 'jjg', '3jet', 'TLA100y06v6', 'TLA75y03v6', 'dijetFull']


