
# copied when split off into separate config

savedir = 'summaryPlots'

date = 'March 2017'
# date = 'Summer 2016'
date = 'November 2017'
bonusname = None

lumiInLegend = True
yearInLegend = False
dateOnTop = False
dateInLabel = True
atlasText = 'Internal'
# atlasText = 'Preliminary'
smallerText = False
canvascoords = []
# canvascoords = [1600,1200] # higher-resolution png, but messes up line widths and dot-filling
logy = False
logx = True
biggerRange = False

oldBuggy = False
curved = True

if dateInLabel:
    atlasText += ' '+date
    smallerText = True

if date == 'Summer 2016':
    lumiInLegend = False
    yearInLegend = False
    dateOnTop = False


photoshop = False

if photoshop:
    dateOnTop = False
    lumiInLegend = False
    yearInLegend = False


limits = {
    # 'TLA': { 'rootfile': 'limits_tla_increaseRange.root', 'grname': 'limits_tla_increaseRange', 'legname': '#splitline{Low-mass dijet (TLA) 2015}{ATLAS-CONF-2016-030}', 'col': 'TLA'},
    # 'TLAys3': { 'rootfile': 'limits_tla_increaseRange.root', 'grname': 'limits_tla_increaseRange3', 'legname': None, 'col': 'TLA'},
    # 'dijet': { 'rootfile': 'limits_tla_increaseRange.root', 'grname': 'limits_highmass', 'legname': '#splitline{High-mass dijet 2015+2016}{XXX-XXX-XXX}', 'col': 'dijet'},
    # 'jjg': { 'rootfile': 'limits_tla_increaseRange.root', 'grname': 'limits2016_g130_2j25_95', 'legname': '#splitline{Dijet+ISR (#gamma) 2015+2016}{XXX-XXX-XXX}', 'col': 'jjg'},
    # '3jet': { 'rootfile': 'limits_tla_increaseRange.root', 'grname': 'limits2016_3jet_669', 'legname': '#splitline{Dijet+ISR (jet) 2015}{ATLAS-CONF-2016-070}', 'col': 'jjj'},

    
    # 2015 TLA
    'TLA': {'rootfile': 'limits_tla_all.root',          'grname': 'limits_tla_all',
            # 'lumi': '3.4',  'legname': '#splitline{Low-mass dijet (TLA), LUMI fb^{#minus1} 2015}{ATLAS-CONF-2016-030}'},
            'lumi': '3.4',  'legname': '#splitline{Dijet TLA, LUMI fb^{#minus1} 2015}{ATLAS-CONF-2016-030}',
            # 'text': '|y*_{12}| < 0.6', 'col': 'TLA',   'pos': (580,0.055)},
            'text': 'J75 |y*_{12}| < 0.6', 'col': 'TLA',   'pos': (580,0.15)},

    'TLAys3': {    'rootfile': 'limits_tla_all_ystar03.root',  'grname': 'limits_tla_all_ystar03',  'legname': None,
                   # 'text': '|y*_{12}| < 0.3', 'col': 'TLA',   'pos': (350,0.055), 'lines': [[550,0.05, 550,0.14]]},
                   'text': 'J75 |y*_{12}| < 0.3', 'col': 'TLA',   'pos': (350,0.1), 'lines': [[550,0.05, 550,0.14]]},

    # 2016 dijet
    'dijetFull': { 'rootfile': 'limits_dijets_full2016.root',  'grname': 'limits_dijets_full2016',
                   # 'lumi': '37.0', 'legname': '#splitline{High-mass dijet, LUMI fb^{#minus1} 2015+16}{CERN-EP-2017-042}'},
                   # 'lumi': '37.0', 'legname': '#splitline{Dijet, LUMI fb^{#minus1} 2015+16}{#scale[0.85]{arXiv: }1703.09127#scale[0.85]{ [hep-ex]}}'},
                   # 'lumi': '37.0', 'legname': '#splitline{Dijet, LUMI fb^{#minus1} 2015+16}{#scale[0.9]{arXiv: 1703.09127 [hep-ex]}}'},
                   'lumi': '37.0', 'legname': '#splitline{Dijet, LUMI fb^{#minus1} 2015+16}{arXiv: 1703.09127}',
    # 'dijetFull': { 'rootfile': 'limits_dijets_full2016_interpolated.root',  'grname': 'limits_dijets_full2016_interpolated',  'col': 'dijet',
                   # 'lumi': '37.0', 'legname': '#splitline{High-mass dijet, LUMI fb^{#minus1} 2015+16}{EXOT-2016-21}'},
                   # 'text': '|y*_{12}| < 0.6', 'col': 'dijet', 'pos': (2300,0.095)},
                   'text': '|y*_{12}| < 0.6', 'col': 'dijet', 'pos': (1600,0.057)},

    # whiteboxes = [
    # [3500,0.2,4000,0.32] # cover top of dijet limit since they don't show it
    # [3500,0.24,4000,0.32] # cover top of dijet limit since they don't show it
    # ]


    # 2016 dijet angular
    'dijetAngular': {    'rootfile': 'limits_dijets_full2016_angular.root', 'grname': 'limits_dijets_full2016_angular',
                         'lumi': '37.0', 'legname': '#splitline{Dijet angular, LUMI fb^{#minus1} 2015+16}{arXiv: 1703.09127}', 
                         'text': '|y*_{12}| < 1.7', 'col': 'TLAnew', 'pos': (3000,0.34)},

    # partial 2016 dijet
    # 'dijet': {     'rootfile': 'limits_dijets_DS2p1.root',     'grname': 'limits_dijets_DS2p1', 
                   # 'lumi': '15.7', 'legname': '#splitline{High-mass dijet, LUMI fb^{#minus1} 2015+16}{ATLAS-CONF-2016-069}',
                   # 'text': '|y*_{12}| < 0.6', 'col': 'dijet', 'pos': (2000,0.095)},

    # partial 2017 dijet+ISR
    'jjg': {       'rootfile': 'limits_gjet_DS2p1.root',       'grname': 'limits_gjet_DS2p1',       'col': 'jjg',
                   'lumi': '15.5', 'legname': '#splitline{Dijet+ISR (#gamma), LUMI fb^{#minus1} 2015+16}{ATLAS-CONF-2016-070}',
                   'text': '|y*_{12}| < 0.8', 'col': 'jjg',   'pos': (350,0.25)},

    '3jet': {      'rootfile': 'limits_3jet_DS2p1.root',       'grname': 'limits_3jet_DS2p1',
                   'lumi': '15.5', 'legname': '#splitline{Dijet+ISR (jet), LUMI fb^{#minus1} 2015+16}{ATLAS-CONF-2016-070}',
                   'text': '|y*_{23}| < 0.6', 'col': 'jjj',   'pos': (580,0.17)},


    # 2016 TLA no morphing
    'TLA100y06int': {   'rootfile': 'limits2017_TLA_J100.root',          'grname': 'limits2017_TLA_J100',
                        'lumi': '3.6, 29.3',  'legname': '#splitline{Dijet TLA, LUMI fb^{#minus1} 2015}{EXOT-2017-XXX}',
                        'text': '|y*_{12}| < 0.6', 'col': 'TLAnew',   'pos': (1100,0.125)},
    'TLA75y03int': {    'rootfile': 'limits2017_TLA_J75.root',  'grname': 'limits2017_TLA_J75',  'legname': None,
                        'text': '|y*_{12}| < 0.3', 'col': 'TLAnew',   'pos': (350,0.045) },

    # 2016 TLA some morphing
    'TLA100y06': {    'rootfile': 'limits2016_TLA_J100yStar06.root',          'grname': 'limits2016_TLA_J100yStar06',
                      'lumi': '3.6, 29.3',  'legname': '#splitline{Dijet TLA, LUMI fb^{#minus1} 2015}{EXOT-2017-XXX}',
                      # 'text': '|y*_{12}| < 0.6', 'col': 'TLA',   'pos': (1100,0.125)},
                      'text': 'J100 |y*_{12}| < 0.6', 'col': 'TLA',   'pos': (1050,0.1)},
    'TLA75y06': {     'rootfile': 'limits2016_TLA_J75yStar06.root',  'grname': 'limits2016_TLA_J75yStar06',  'legname': None,
                      # 'text': '|y*_{12}| < 0.6', 'col': 'jjg',   'pos': (1300,0.125) },
                      'text': 'J75 |y*_{12}| < 0.6', 'col': 'jjg',   'pos': (580,0.053) },
    'TLA75y03': {     'rootfile': 'limits2016_TLA_J75yStar03.root',  'grname': 'limits2016_TLA_J75yStar03',  'legname': None,
                      # 'text': '|y*_{12}| < 0.3', 'col': 'TLA',   'pos': (350,0.045) },
                      'text': 'J75 |y*_{12}| < 0.3', 'col': 'TLA',   'pos': (350,0.045) },
    
    }

if photoshop:
    limits['dijetFull'] = { 'rootfile': 'limits_dijets_full2016.root',  'grname': 'limits_dijets_full2016',  'col': 'dijet',
                            'lumi': '37.0', 'legname': '#splitline{High-mass dijet, LUMI fb^{#minus1} 2015+16}{ATLAS-CONF-2016-069}'}
    limits['dijet'] = {     'rootfile': 'limits_dijets_DS2p1.root',     'grname': 'limits_dijets_DS2p1', 
                            'lumi': '15.7', 'legname': None, 'col': 'dijet'}
    

limits['TLA']['col'] = 'TLAnew'
limits['TLAys3']['col'] = 'TLAnew'
    
# get min and max x and y
xmin = 200
xmax = 4000
ymin = 0.025
ymax = 0.375

if biggerRange:
    xmin = 200
    xmax = 5000
    ymin = 0.025
    ymax = 0.5

xmin = 330
xmax = 2300
ymin = 0.025
ymax = 0.25


# limits = [limits[3], limits[4], limits[0], limits[1], limits[2], limits[-3], limits[-2], limits[-1]]
# limits = [limits[3], limits[4], limits[2], limits[-3], limits[-2], limits[-1]]
limits_to_plot = ['TLA100y06', 'TLA75y06', 'TLA75y03', 'TLA100y06int', 'TLA75y03int', 'TLA', 'TLAys3']

bonusname = 'TLA_J75y06_dijet_old'
limits_to_plot = ['TLA', 'TLAys3', 'TLA100y06', 'TLA75y06', 'TLA75y03', 'dijetFull']

bonusname = 'TLA_J75y06_dijet'
limits_to_plot = ['TLA100y06', 'TLA75y06', 'TLA75y03', 'dijetFull']

bonusname = 'TLA_dijet'
limits_to_plot = ['TLA100y06', 'TLA75y03', 'dijetFull']

bonusname = 'TLA_dijet_old'
limits_to_plot = ['TLA', 'TLAys3', 'TLA100y06', 'TLA75y03', 'dijetFull']

# expected limit line
lineWidth = 100*5 + 5
hatchedOneSidedBand = True
lineWidth = 5
hatchedOneSidedBand = False

