
# copied when split off into separate config

savedir = 'summaryPlots_Mar18'

textOnLines = False


date = 'March 2018'
date = 'April 2018'
dateOnTop = False
dateInLabel = True

bonusname = "oldStyle"

lumiInLegend = True
yearInLegend = False

atlasText = 'Internal'
atlasText = 'Preliminary'

logy = False
logx = True

curved = True
# curved = False

extraText = ['Axial vector mediator, Dirac DM', 'm_{DM} = 10 TeV']
extraTextpos = [0.2,0.21,0.033] # x, y, spacing between lines
extraTextSize = 0.034
extraText = ['Axial vector mediator', '#lower[0.1]{Dirac Dark Matter}', 'm_{DM} = 10 TeV']
extraTextpos = [0.2,0.26,0.035] # x, y, spacing between lines


if dateInLabel:
    atlasText += ' '+date
    smallerText = True

setLegLeft = 0.65
setLegBot = 0.55
setLegRight = 0.89
legtextsize = 0.028

setLeg2Top = 0.53
setLeg2Bot = 0.43
setLeg2Left = 0.65
setLeg2Right = 0.89
leg2textsize = 0.028

# hacking for band
leg2BotPlus = 0.016
leg2RightPlus = 0.05
leg2LeftPlus = 0.009

# get min and max x and y
xmin = 90
xmax = 4200
# xmin = 45
# xmax = 40000
ymin = 0.025
ymax = 0.4 # 0.375

addMajorTicks = [2000,3000,4000]
addMinorTicks = [1000,4000]
minorTickDivisions = 30

# expected limit line
# lineWidth = 100*5 + 5
# hatchedOneSidedBand = True
# lineWidth = 5
hatchedOneSidedBand = False
drawBand = False
# drawBand = True
bandlinewidth = 6
lineWidth = 2

from limitlines import limits

drawTexts = True
for analysis in ['jjg','3jet','dijetFull']:
    limits[analysis]['text'] = ''

limits_to_plot = [
    'UA2',
    'CDFrun1',
    'CDFrun2',

    'CMS8dijet20fb',
    'CMS8scouting18.8fb_manual',

    # 'ATLAS8dijet20.3fb_eps',

    'CMS13scouting27_manual',
    'CMS13dijet36_manual',
    'CMS13boosted35.9_manual',
    
    'dijetISRboosted',
    'jjg',
    '3jet',
    'TLA100y06v8',
    'TLA75y03v8',
    'dijetFull',
]

# import ROOT
# verticalLineStyle = ROOT.kDashed
# verticalLineColour = ROOT.TColor.GetColor("#CDCDCD")
# prepend_merged = True
# mergeSets = [
    # {'name': 'Other', 'legname': 'CMS / pre-LHC', 'col': 'grey',
     # 'analyses': [
         # 'CDFrun1',
         # 'CDFrun2',
         # 'UA2',
         # 'CMS8scouting18.8fb_manual',
         # 'CMS8dijet20fb',
         # 'CMS13boosted35.9_manual',
         # 'CMS13scouting27_manual',
         # 'CMS13dijet36_manual',
     # ]},
# ]
# bonusname += '_CMSandOld'

mergeSets = []
limits_to_plot = [
    'dijetISRboosted',
    'jjg',
    '3jet',
    'TLA100y06v8',
    'TLA75y03v8',
    'dijetFull',
]


# change dijet to be darker
limits['dijetFull']['col'] = 'darkBlue'

# Move TLA y* labels
textlabelsize = 0.03
limits['TLA75y03v8']['pos'] = [460,0.09]
limits['TLA100y06v8']['pos'] = [1080,0.1]

# hack dijet legend
# option 1, tiny
limits['dijetFull']['legname'] = '#splitline{Dijet, LUMI fb^{#minus1} 2015+16}{#color[0]{|}}'
bonusTexts = [{'text': 'Phys. Rev. D 96, 052004 (2017)', 'pos': [1320, 0.217], 'size': 0.017}]

# option 2, split line
legtextsize = 0.029
setLegLeft = 0.645
setLegRight = 0.89
setLegBot = 0.53

limits['dijetFull']['legname'] = '#splitline{Dijet, LUMI fb^{#minus1} 2015+16}{Phys. Rev. D 96,}'
bonusTexts = [{'text': '052004 (2017)', 'pos': [1290, 0.19], 'size': legtextsize}]

leg2textsize = 0.03
setLeg2Top = 0.78
setLeg2Bot = 0.68
setLeg2Left = 0.65-0.25
setLeg2Right = 0.89-0.25


if textOnLines:
    textlabelsize = 0.028
    drawLeg = False
    limits['dijetISRboosted']['pos'] = [100, 0.068]
    limits['jjg']['pos'] = [230, 0.29]
    limits['3jet']['pos'] = [580, 0.18]

    limits['TLA75y03v8']['legname'] = 'Dijet TLA, EXOT-2016-20'
    limits['TLA75y03v8']['pos'] = [500,0.13]
    
    # |y*#kern[-0.5]{_{12}}| < 0.3'
    limits['TLA100y06v8']['legname'] = '|y*#kern[-0.5]{_{12}}| < 0.3'
    limits['TLA100y06v8']['pos'] = [500,0.1]

    limits['dijetFull']['pos'] = [1300, 0.26]
    limits['dijetFull']['legname'] = '#splitline{Dijet, LUMI fb^{#minus1} 2015+16}{#splitline{Phys. Rev. D 96,}{  052004 (2017)}}'
    
    setLeg2Top = 0.9
    setLeg2Bot = 0.78
    setLeg2Left = 0.7
    setLeg2Right = 0.99
    leg2textsize = 0.034

    extraText = ['Axial vector mediator, Dirac DM, m_{DM} = 10 TeV']
    extraTextpos = [0.49, 0.967, 0.033]
    extraTextSize = 0.034

    
