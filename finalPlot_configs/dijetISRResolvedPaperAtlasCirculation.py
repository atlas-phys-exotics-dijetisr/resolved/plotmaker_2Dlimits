import ROOT


# Approval comments
newStyle = True
uglyCol = False
J100only = False
J75only = False
withDijet = False
withLine = True
shuffle = False # hacks to make the two legends become one in the case of both SRs with or without dijet
dummySROnly=True # added variable

# switch to get aux plots
# uglyCol = True
# withDijet = False
# shuffle = False
# J100only = True
# J75only = True

# preferred version after comments
# newStyle = True
# withLine = False
# withDijet = False
# uglyCol = False

# for first circulation
# newStyle = False
# withLine = False
# withDijet = False
# uglyCol = False


if J100only or J75only:
    withLine = False

oldStyle = not newStyle

date = 'November 2017'
#savedir = 'summaryPlots_TLA2017Paper'
savedir = 'dijetISR2017_channel1'

if newStyle:
    date = 'January 2018'
    savedir = 'summaryPlots_dijetISRResolved2017Paper_secondCirculation'

dateOnTop = False
dateInLabel = False


lumiOnPlot = True
lumiInLegend = True
yearInLegend = False

atlasText = 'Internal'
# atlasText = 'Preliminary'

logy = False
logx = True

if newStyle:
    logx = False

curved = False

if dateInLabel:
    atlasText += ' '+date
    smallerText = True


setLegLeft = 0.2
setLegRight = 0.45
setLegBot = 0.65
setLegTop = 0.8


setLeg2Left = 0.62
# setLeg2Right = 0.65
# setLeg2Bot = 0.655
setLeg2Bot = 0.75
setLeg2Top = 0.92

legtextsize = 0.04
leg2textsize = 0.04

# hacking for band
bandlinewidth = 10
leg2LeftPlus = 0.0112
leg2BotPlus = 0.028
leg2RightPlus = 0.0637

if newStyle:
    atlaslabelY = 0.88
    setLegBot = 0.68
    setLegTop = 0.80
    setLeg2Left = 0.61
    legtextsize = 0.045
    leg2textsize = 0.045

# get min and max x and y
xmin = 380
xmax = 2100
ymin = 0.06
ymax = 0.35

if oldStyle:
    addMajorTicks = [2000]
    addMinorTicks = [1000,2000]

if newStyle:
    # no log
    xmin = 150
    xmax = 1050
    if withDijet:
        xmax = 2050


# expected limit line
# lineWidth = 100*5 + 5
# hatchedOneSidedBand = True
# lineWidth = 5
hatchedOneSidedBand = False
# drawBand = False
drawBand = True
lineWidth = 2

if newStyle:
    # obsMarkerStyle = ROOT.kOpenCircle
    obsMarkerCol = True
    obsMarkerSize = 0.7


limits = {

    # 2015 TLA
    # change grname to the intermediate root file dir.
    #'dijetISRSingleBtagged': {'grname': 'dijetISR2017_channel1', 'lumi': '79.8',
    #        'legname': '#splitline{DijetISR Resolved, LUMI fb^{#minus1} 2018}',
    #        'text': '', 'col': 'TLA',   'pos': (580,0.15)},

    'dijetISRSingleBtagged': {'grname': 'dijetISRResolvedAll/LimitsDict_photon_single_btagged', 'lumi': '79.8',
            'legname': '#splitline{DijetISR Resolved, LUMI fb^{#minus1} 2018}',
            'text': '', 'col': 'TLA',   'pos': (580,0.15)},

    'dijetISRCompoundBtagged': {'grname': 'dijetISRResolvedAll/LimitsDict_photon_compound_btagged', 'lumi': '79.8',
            'legname': '#splitline{DijetISR Resolved, LUMI fb^{#minus1} 2018}',
            'text': '', 'col': 'TLA',   'pos': (250,0.10)},

    'dijetISRSingleInclusive': {'grname': 'dijetISRResolvedAll/LimitsDict_photon_single_inclusive', 'lumi': '79.8',
            'legname': '#splitline{DijetISR Resolved, LUMI fb^{#minus1} 2018}',
            'text': '', 'col': 'TLA',   'pos': (250,0.10)},

    'dijetISRCompoundInclusive': {'grname': 'dijetISRResolvedAll/LimitsDict_photon_compound_inclusive', 'lumi': '79.8',
            'legname': '#splitline{DijetISR Resolved, LUMI fb^{#minus1} 2018}',
            'text': '', 'col': 'TLA',   'pos': (250,0.10)},

    # 2016 TLA latest as of 5pm 13.12 - WITH XS FIX 13.03
    'TLA100y06v8': {    'grname': 'limits2016_TLA_J100yStar06_v8', 'lumi': '29.3',
                        'legname': 'J100, |y*| < 0.6, LUMI fb^{#minus1} 2016',
                        'col': 'dijet'},
    'TLA75y03v8': {     'grname': 'limits2016_TLA_J75yStar03_v8', 'lumi': '3.6',
                        'legname':  'J75, |y*| < 0.3, LUMI fb^{#minus1} 2016',
                        'col': 'TLA' },

    # 2016 dijet+ISR boosted
    'dijetISRboosted' : { 'rootfile': '../data/limits_dijetISRBoosted.root', 'grname': ['Expected','Observed','ExpectedUp','ExpectedDown','Expected2Up','Expected2Down'],
                          'legname': '#splitline{Large-#it{R} jet + ISR, LUMI fb^{#minus1} 2016}{EXOT-2017-01}',
                          'lumi': '36.1', 'col': 'boosted'},
    }


# version one for dijetISR
version="v1"
bonusname = 'dijetISRResolvedonly_combined_'+version
# these add extra line from the dijet limits
if withDijet:
    bonusname = 'TLAwithDijet_combined_'+version
    limits['dijetFull']['obsOnly'] = True
if uglyCol:
    bonusname += '_ugly'


#  so the key needs to be that of the limits dictionary
#limits['TLA75y03'+version]['col'] = 'TLA'
#limits['TLA100y06'+version]['col'] = 'TLA'
limits["dijetISRSingleBtagged"]["col"]="TLA"
limits["dijetISRSingleBtagged"]["col"]="TLA"
limits["dijetISRSingleInclusive"]["col"]="TLA"
limits["dijetISRCompoundInclusive"]["col"]="TLA"

if uglyCol:
    limits['TLA75y03'+version]['col'] = 'ugly'
    limits['TLA100y06'+version]['col'] = 'ugly'

#limits['TLA75y03'+version]['xrange'] = [0,700]
#limits['TLA100y06'+version]['xrange'] = [700,3000]
limits["dijetISRSingleBtagged"]["xrange"]=[150, 450]
limits["dijetISRCompoundBtagged"]["xrange"]=[450, 1050]
limits["dijetISRSingleInclusive"]["xrange"]=[150, 450]
limits["dijetISRCompoundInclusive"]["xrange"]=[450, 1050]

limits["dijetISRSingleBtagged"]['legname'] = '2 b-tags'
limits["dijetISRCompoundBtagged"]['legname'] = ' '
limits["dijetISRSingleInclusive"]['legname'] = 'Flavour-inclusive'
limits["dijetISRCompoundInclusive"]['legname'] = ' '
#limits['TLA75y03'+version]['legname'] = '|y*| < 0.3, m_{jj} < 700 GeV, LUMI fb^{#minus1} 2016'
#limits['TLA75y03'+version]['legname'] = '|y*| < 0.3, m_{jj} < 700 GeV, LUMI fb^{#minus1} 2016'
#limits['TLA100y06'+version]['legname'] = '|y*| < 0.6, m_{jj} > 700 GeV, LUMI fb^{#minus1} 2016'

if withLine:
    #limits['TLA75y03'+version]['legname'] = None
    #limits['TLA100y06'+version]['legname'] = None

    blackTextLabel = True
    limits["dijetISRSingleInclusive"]['lines'] = [[450, 0.06, 450, 0.25]]
    limits["dijetISRSingleBtagged"]['lines'] = [[450, 0.06, 450, 0.25]]
    #limits['TLA75y03'+version]['text'] = '|y*| < 0.3'
    #limits['TLA75y03'+version]['pos'] =  (420,0.13)
    #limits['TLA100y06'+version]['text'] = '|y*| < 0.6'
    #limits['TLA100y06'+version]['pos'] =  (750,0.13)

    #if withDijet:
    #    limits['TLA100y06'+version]['legname'] = 'TLA'
    #    limits['TLA100y06'+version]['lines'] = [[700, 0.02, 700, 0.14]]
    #    limits['TLA75y03'+version]['pos'] =  (420,0.125)
    #    limits['TLA100y06'+version]['pos'] =  (750,0.125)
    #textlabelsize = legtextsize
    pass

obsPoints = True
lumiOnPlot = False # yvonne turned this off for dijetISR
lumiInLegend = False
noLegLines = True
colourLeg2 = True
limits_to_plot = ['TLA75y03'+version, 'TLA100y06'+version]
if withDijet:
    noLegLines = False
    limits_to_plot += ['dijetFull']
    limits['dijetFull']['legname'] = 'Dijet, arXiv: 1703.09127'
    limits['dijetFull']['lumi'] = '29.3',
    setRefCol = 'TLA'
    del limits['dijetFull']['text']
    # limits['dijetFull']['col'] = 'darkBlue'
    limits['dijetFull']['col'] = 'darkPurple'
    limits['dijetFull']['col'] = 'ugly'

    if uglyCol:
        setRefCol = 'ugly'
        limits['dijetFull']['col'] = 'TLA'

# dummySROnly only
if dummySROnly:
    bonusname = bonusname.replace('combined','dummySR')
    # a subset of the limits dictionary key that actually contain info that you wanna plot
    #limits_to_plot=["dijetISRSingleBtagged", "dijetISRCompoundBtagged"]
    limits_to_plot=["dijetISRSingleInclusive", "dijetISRCompoundInclusive"]
    #limits['TLA100y06'+version]['xrange'] = [0,3000]
    #limits['TLA100y06'+version]['legname'] = '|y*| < 0.6, LUMI fb^{#minus1} 2016'
    #if withDijet:
    #    limits['TLA100y06'+version]['legname'] = 'TLA, |y*| < 0.6, LUMI fb^{#minus1} 2016'
    #xmin = 650
# what is shuffle? will hack of legend
if shuffle:

    lumiOnPlot = False

    # limits['TLA75y03'+version]['text'] = '#splitline{  #splitline{3.6 fb^{#minus 1}}{}}{|y*| < 0.3}'
    limits['TLA75y03'+version]['pos'] =  (420,0.135)

    # limits['TLA100y06'+version]['text'] = '#splitline{ #splitline{29.3 fb^{#minus 1}}{}}{|y*| < 0.6}'
    limits['TLA100y06'+version]['pos'] =  (750,0.135)

    bonusTexts = [
        {'pos': [420,0.154], 'text': '  3.6 fb^{#minus 1}', 'size': 0.045},
        {'pos': [750,0.154], 'text': ' 29.3 fb^{#minus 1}', 'size': 0.045},
    ]

    setLeg2Left = 0.53
    setLeg2Bot = 0.67
    if withDijet:
        leg2BotPlus = 0.094
        leg2RightPlus = 0.0637
        bandlinewidth = 12
    else:
        leg2BotPlus = 0.041
        leg2RightPlus = 0.0637
        bandlinewidth = 15

    limits['TLA75y03'+version]['legname'] = None
    limits['TLA100y06'+version]['legname'] = None
    # limits['dijetFull']['legname'] = '#splitline{Dijet Observed}{arXiv: 1703.09127}'
    limits['dijetFull']['legname'] = 'Dijet Observed'

    atlaslabelY = 0.875
    atlaslabelX = 0.22
    lumilabelYspacing = 0.08
    lumilabelXspacing = 0.02

    limits['TLA100y06'+version]['lines'] = [[700, 0.02, 700, 0.162]]

# bonusname = 'TLAonly_v3v4'
# limits['TLA100y06v3']['col'] = 'jjj'
# limits['TLA75y03v3']['col']  = 'jjj'
# limits_to_plot = ['TLA75y03v3', 'TLA100y06v3', 'TLA75y03v4', 'TLA100y06v4']


# bonusname = 'TLA_J75y06_dijet_old'
# limits_to_plot = ['TLA', 'TLAys3', 'TLA100y06', 'TLA75y06', 'TLA75y03', 'dijetFull']

# bonusname = 'TLA_J75y06_dijet'
# limits_to_plot = ['TLA100y06', 'TLA75y06', 'TLA75y03', 'dijetFull']

# bonusname = 'TLA_dijet'
# limits_to_plot = ['TLA100y06', 'TLA75y03', 'dijetFull']

# bonusname = 'TLA_dijet_old'
# limits_to_plot = ['TLA', 'TLAys3', 'TLA100y06', 'TLA75y03', 'dijetFull']

