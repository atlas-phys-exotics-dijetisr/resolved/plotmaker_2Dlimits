

savedir = 'summaryPlots_Mar18_lowMass'

date = 'March 2018'
dateOnTop = False
dateInLabel = False

extraText = ['Axial vector mediator', 'Dirac DM', 'm_{DM} = 10 TeV']
extraTextpos = [0.2,0.89,0.05] # x, y, spacing between lines
extraTextSize = 0.04
xtitle = "Mediator Mass [GeV]"


lumiInLegend = False
lumiOnPlot = False
drawSqrtsText = False
yearInLegend = False

atlasText = None

logy = False
logx = True

curved = False
# curved = True

setLegLeft = 0.55
setLegBot = 0.65
legtextsize = 0.035
drawLeg2 = False


# get min and max x and y
xmin = 90
xmax = 1500
# xmin = 45
ymin = 0.
ymax = 0.5

ymax = 0.25

# addMajorTicks = [2000,3000,4000]
addMinorTicks = [1000,1500]
minorTickDivisions = 5

# expected limit line
hatchedOneSidedBand = False
drawBand = False
lineWidth = 2
noOverlap=True

verticalLineStyle = 2
verticalLineColour = 920+2 # kGray

from limitlines import limits
limits['TLA100y06v8']['lumi'] = '29.7'

drawTexts = True


limits_to_plot = [
    'dijetISRboosted',
    'jjg',
    '3jet',
    
    'TLA100y06v8',
    'TLA75y03v8',
]

# obs
# limits['dijetISRboosted']['pos'] = [99, 0.18]
# limits['jjg']['pos'] = [800, 0.13]
# limits['3jet']['pos'] = [550, 0.11]
# limits['TLA75y03v8']['pos'] = [500, 0.075]
# limits['TLA100y06v8']['pos'] = [650, 0.01]

#exp
limits['dijetISRboosted']['pos'] = [99, 0.12]
limits['jjg']['pos'] = [700, 0.14]
limits['3jet']['pos'] = [550, 0.11]
limits['TLA75y03v8']['pos'] = [650, 0.075]
limits['TLA100y06v8']['pos'] = [650, 0.01]

for limit in ['dijetISRboosted', 'jjg', '3jet', 'TLA100y06v8', 'TLA75y03v8']:
    limits[limit]['text'] = limits[limit]['lumi'] + ' fb^{#minus 1}'

limits['TLA75y03v8']['text'] = 'J75, ' + limits['TLA75y03v8']['text']
limits['TLA100y06v8']['text'] = 'J100, ' + limits['TLA100y06v8']['text']
        


scaleLimits = ['3jet', 'jjg', 'dijetFull', 'dijetISRboosted']
for limit in scaleLimits:
    limits[limit]['scaleLumi'] = 120
    limits[limit]['text'] = str(limits[limit]['lumi']) + ' -> ' + str(limits[limit]['scaleLumi']) + ' fb^{#minus 1}'

limits['TLA100y06v8']['scaleLumi'] = 110
limits['TLA100y06v8']['text'] = 'J100, ' + str(limits['TLA100y06v8']['lumi']) + ' -> ' + str(limits['TLA100y06v8']['scaleLumi']) + ' fb^{#minus 1}'

limits['TLA75y03v8']['scaleLumi'] = 7
limits['TLA75y03v8']['text'] = 'J75, ' + str(limits['TLA75y03v8']['lumi']) + ' -> ' + str(limits['TLA75y03v8']['scaleLumi']) + ' fb^{#minus 1}'

# already have J50
# other things 0.078 3.6 fb

# limits['TLAJ50_7'] = { 'manuallimit': [[370,700], [0.066,0.08]], # lies on top of J75 extrap to 7. Was 0.07 ,0.07
                       # 'legname': None, 'lumi': '3.6', 'scaleLumi': 7, 'col': 'niceOrange', 'style': 'bold',
                       # 'text': 'J50, 7 fb^{#minus 1}', 'pos': (240,0.065)}
# limits_to_plot.append('TLAJ50_7')

limits['TLAJ50_9'] = { 'manuallimit': [[370,700], [0.066,0.08]], # 0.07 ,0.07
                       'legname': 'TLA J50', 'lumi': '3.6', 'scaleLumi': 9, 'col': 'niceOrange', 'style': 'bold',
                       'text': 'J50, 9 fb^{#minus 1}', 'pos': (240,0.05)}
limits_to_plot.append('TLAJ50_9')

limits['TLAJ50_15'] = { 'manuallimit': [[370,700], [0.066,0.08]],
                        'legname': None, 'lumi': '3.6', 'scaleLumi': 15, 'col': 'niceOrange', 'style': 'bold dashed',
                        'text': 'J50, 15 fb^{#minus 1}', 'pos': (240,0.04)}
limits_to_plot.append('TLAJ50_15')

limits['TLAJ20_0.2'] = { 'manuallimit': [[200,370], [0.058, 0.066]],
                       'legname': 'TLA J20', 'lumi': '3.6', 'scaleLumi': 0.2, 'col': 'tealyBlue', 'style': 'bold',
                       'text': 'J20, 0.2 fb^{#minus 1}', 'pos': (200,0.14)}
limits_to_plot.append('TLAJ20_0.2')

# limits['TLAJ20_4'] = { 'manuallimit': [[200,370], [0.058, 0.066]],
                       # 'legname': None, 'lumi': '3.6', 'scaleLumi': 4, 'col': 'tealyBlue', 'style': 'bold',
                       # 'text': 'J20, 4 fb^{#minus 1}', 'pos': (120,0.1)}
# limits_to_plot.append('TLAJ20_4')

# limits['TLAJ20_0.5'] = { 'manuallimit': [[200,370], [0.07,0.07]],
                         # 'legname': None, 'lumi': '3.6', 'scaleLumi': 0.5, 'col': 'tealyBlue', 'style': 'bold dashed',
                         # 'text': 'J20, 0.5 fb^{#minus 1}', 'pos': (120,0.115)}
# limits_to_plot.append('TLAJ20_0.5')

# limits['TLAJ20_2'] = { 'manuallimit': [[200,370], [0.07,0.07]],
                         # 'legname': None, 'lumi': '3.6', 'scaleLumi': 2, 'col': 'tealyBlue', 'style': 'bold dashed',
                         # 'text': 'J20, 2 fb^{#minus 1}', 'pos': (120,0.075)}
# limits_to_plot.append('TLAJ20_2')




for limit in limits_to_plot:
    if 'J50' in limit or 'J20' in limit:
        limits[limit]['obsOnly'] = True
    else:
        limits[limit]['expOnly'] = True


# limits['CMS13boosted35.9_manual']['drawinterval'] = [50,240.5]
limits['jjg']['legname'] = 'ATLAS dijet + ISR (#gamma)'
limits['3jet']['legname'] = 'ATLAS dijet + ISR (jet)'
limits['dijetISRboosted']['legname'] = 'ATLAS boosted dijet + ISR'
limits['TLA100y06v8']['legname'] = 'ATLAS TLA'
