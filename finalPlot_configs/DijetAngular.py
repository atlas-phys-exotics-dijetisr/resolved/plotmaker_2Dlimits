
# copied when split off into separate config

savedir = 'summaryPlots_DijetAngular'
with8TeV = False
withDijetISR = False
withDib = False
withPreLHC = False
newCols = True

withDijetAngular = True

textOnLines = False


date = 'May 2018'
dateOnTop = False
dateInLabel = True

bonusname = "EmmaStyle"
if not withDijetISR:
    bonusname += '_noDijetISR'
if withDib:
    bonusname += '_withDib'
if with8TeV:
    bonusname += '_with8TeVdijet'
    
lumiInLegend = True
yearInLegend = False

atlasText = 'Internal'
# atlasText = 'Preliminary'
atlaslabelX = 0.15
atlaslabelY = 0.95
lumilabelXspacing = 0.37
lumilabelYspacing = 0

logy = False
logx = True
logx = False

curved = True
curved = False
fillObs = False
fillOldObs = True

# edit canvas
setRightMargin = 0.2
setLeftMargin = 0.14
setTopMargin = 0.07
setBottomMargin = 0.14


extraText = ['Axial vector mediator', '#lower[0.1]{Dirac Dark Matter}', 'm_{DM} = 10 TeV']
extraTextpos = [0.19,0.25,0.035] # x, y, spacing between lines
extraTextSize = 0.03


if dateInLabel:
    atlasText += ' '+date
    smallerText = True

setLegLeft = 0.8
setLegTop = 0.8
setLegBot = 0.13
setLegRight = 0.99
legtextsize = 0.029

setLeg2Top = 0.92
setLeg2Bot = 0.81
setLeg2Left = 0.81
setLeg2Right = 0.99
leg2textsize = 0.03

# hacking for band
leg2BotPlus = 0.016
leg2RightPlus = 0.05
leg2LeftPlus = 0.009

# get min and max x and y
xmin = 2000
xmax = 6000
ymin = 0
ymax = 1.42

    
# expected limit line
# lineWidth = 100*5 + 5
# hatchedOneSidedBand = True
# lineWidth = 5
hatchedOneSidedBand = False
drawBand = False
# drawBand = True
bandlinewidth = 6
lineWidth = 2

from limitlines import limits

drawTexts = True
for analysis in ['jjg','3jet','dijetFull']:
    limits[analysis]['text'] = ''

    
limits_to_plot = [
    'dijetAngular',
]

mergeSets = []

limits['dijetAngular']['col'] = 'niceGreen'



bonusTexts = []


nEntries = 1
spacing = (setLegTop-setLegBot)/(nEntries+1)
ypos = setLegTop - spacing + 0.009
refxpos = 0.848
ypos = ypos - 0.04
            
ypos_0 = ypos

for analysis in limits_to_plot:
    isMergeSet = False
    for i in range(len(mergeSets)):
        if analysis in mergeSets[i]['analyses']:
            isMergeSet = True
        
    if isMergeSet:
        continue
    if 'TLA75' in analysis: continue
    if 'dibjetLowMass' in analysis: continue
    limits[analysis]['legname'] = '#lower[-0.2]{#splitline{'+limits[analysis]['analysisname']+'}{#lower[-0.15]{LUMI fb^{#minus 1}}}}'

    bonusTexts.append({'text': limits[analysis]['ref'], 'pos': [refxpos, ypos], 'NDC': True, 'size': 0.015})
    
    ypos -= spacing

for i in range(len(mergeSets)):
    mergeSets[i]['legname'] = '#lower[-0.2]{#splitline{'+mergeSets[i]['analysisname']+'}{#lower[-0.15]{0.106-1.13 fb^{#minus 1}}}}'
    thisypos = ypos_0 - spacing*(len(limits_to_plot)-len(mergeSets[i]['analyses'])-2)
    bonusTexts.append({'text': mergeSets[i]['ref'], 'pos': [refxpos, thisypos], 'NDC': True, 'size': 0.015})
        
    



if textOnLines:
    textlabelsize = 0.028
    drawLeg = False
    limits['dijetISRboosted']['pos'] = [100, 0.068]
    limits['jjg']['pos'] = [230, 0.29]
    limits['3jet']['pos'] = [580, 0.18]

    limits['TLA75y03v8']['legname'] = 'Dijet TLA, EXOT-2016-20'
    limits['TLA75y03v8']['pos'] = [500,0.13]
    
    # |y*#kern[-0.5]{_{12}}| < 0.3'
    limits['TLA100y06v8']['legname'] = '|y*#kern[-0.5]{_{12}}| < 0.3'
    limits['TLA100y06v8']['pos'] = [500,0.1]

    limits['dijetFull']['pos'] = [1300, 0.26]
    limits['dijetFull']['legname'] = '#splitline{Dijet, LUMI fb^{#minus1} 2015+16}{#splitline{Phys. Rev. D 96,}{  052004 (2017)}}'
    
    setLeg2Top = 0.9
    setLeg2Bot = 0.78
    setLeg2Left = 0.7
    setLeg2Right = 0.99
    leg2textsize = 0.034

    extraText = ['Axial vector mediator, Dirac DM, m_{DM} = 10 TeV']
    extraTextpos = [0.49, 0.967, 0.033]
    extraTextSize = 0.034

    
