

savedir = 'summaryPlots_Mar18_lowMass'

date = 'March 2018'
dateOnTop = False
dateInLabel = False

extraText = ['Axial vector mediator', 'Dirac DM', 'm_{DM} = 10 TeV']
extraTextpos = [0.2,0.89,0.05] # x, y, spacing between lines
extraTextSize = 0.04
xtitle = "Mediator Mass [GeV]"


lumiInLegend = False
lumiOnPlot = False
drawSqrtsText = False
yearInLegend = False

atlasText = None

logy = False
logx = True

curved = False

setLegLeft = 0.55
setLegBot = 0.65
legtextsize = 0.035
drawLeg2 = False


# get min and max x and y
xmin = 90
xmax = 1500
# xmin = 45
ymin = 0.
ymax = 0.5

# addMajorTicks = [2000,3000,4000]
addMinorTicks = [1000,1500]
minorTickDivisions = 5

# expected limit line
hatchedOneSidedBand = False
drawBand = False
lineWidth = 2
noOverlap=True

verticalLineStyle = 2
verticalLineColour = 920+2 # kGray

from limitlines import limits
limits['TLA100y06v6']['lumi'] = '29.7'

drawTexts = True


limits_to_plot = [
    'UA2',
    'CDFrun1',
    'CDFrun2',
    
    'CMS13boosted35.9_manual',
    'CMS8scouting18.8fb_manual',
    # 'CMS13scouting12.9_manual',
    'CMS13scouting27_manual',
    # 'CMS8dijet20fb',
    # 'CMS13dijet12.9_manual',
    # 'CMS13dijet36_manual',

    'dijetISRboosted',
    'jjg',
    '3jet',

    # 'ATLAS8dijet20.3fb_eps',
    # 'dijetFull',
    
    'TLA100y06v6',
    'TLA75y03v6',
]

limits['dijetISRboosted']['pos'] = [99, 0.18]
limits['jjg']['pos'] = [500, 0.25]
limits['3jet']['pos'] = [550, 0.18]
limits['TLA75y03v6']['pos'] = [500, 0.09]
limits['TLA100y06v6']['pos'] = [900, 0.02]

for limit in ['dijetISRboosted', 'jjg', '3jet', 'TLA100y06v6', 'TLA75y03v6']:
    limits[limit]['text'] = limits[limit]['lumi'] + ' fb^{#minus 1}'

limits['TLA75y03v6']['text'] = 'J75, ' + limits['TLA75y03v6']['text']
limits['TLA100y06v6']['text'] = 'J100, ' + limits['TLA100y06v6']['text']
        

import copy
limits['3jet_80ifb'] = copy.deepcopy(limits['3jet'])
limits['3jet_80ifb']['scaleLumi'] = 80
limits['3jet_80ifb']['style'] = 'dashed'
limits['3jet_80ifb']['pos'] = [550, 0.12]
limits['3jet_80ifb']['legname'] = None
limits_to_plot.append('3jet_80ifb')

limits['jjg_80ifb'] = copy.deepcopy(limits['jjg'])
limits['jjg_80ifb']['scaleLumi'] = 80
limits['jjg_80ifb']['style'] = 'dashed'
limits['jjg_80ifb']['pos'] = [960, 0.2]
limits['jjg_80ifb']['legname'] = None

limits_to_plot.append('jjg_80ifb')

for limit in ['3jet_80ifb', 'jjg_80ifb']:
    limits[limit]['text'] = str(limits[limit]['scaleLumi']) + ' fb^{#minus 1}'




for limit in limits_to_plot:
    limits[limit]['obsOnly'] = True

    
mergeSets = [
    {'name': 'preLHC', 'legname': 'pre-LHC', 'col': 'tealyBlue',
     'analyses': ['CDFrun1', 'CDFrun2', 'UA2']},
    # {'name': 'LHCRun1', 'legname': 'LHC Run 1 (no scouting)', 'col': 'midGreen',
     # 'analyses': [
         # 'CMS8scouting18.8fb_manual',
         # 'CMS8dijet20fb',
         # 'ATLAS8dijet20.3fb_eps',
     # ]},
    {'name': 'CMS', 'legname': 'CMS low mass', 'col': 'niceOrange',
     'analyses': [
         'CMS13boosted35.9_manual',
         'CMS8scouting18.8fb_manual',
         # 'CMS13scouting12.9_manual',
         'CMS13scouting27_manual',
         # 'CMS8dijet20fb',
         # 'CMS13dijet12.9_manual',
         # 'CMS13dijet36_manual',
     ]},
    # {'name': 'ATLAS', 'legname': 'ATLAS (no TLA)', 'col': 'tealyBlue',
     # 'analyses': [
         # 'dijetISRboosted',
         # 'jjg',
         # '3jet',
         # 'TLAys6',
         # 'TLAys3',
         # 'ATLAS8dijet20.3fb_eps',
         # 'dijetFull',
     # ]},
   # {'name': 'CMSScouting', 'legname': 'CMS scouting', 'col': 'midOrange', 'style': 'bold',
     # 'analyses': [
         # 'CMS8scouting18.8fb_manual_duplicate',
         # 'CMS13scouting12.9_manual_duplicate',
         # 'CMS13scouting27_manual_duplicate',
     # ]},
    # {'name': 'ATLASTLA', 'legname': 'ATLAS TLA', 'col': 'midBlue', 'style': 'bold',
     # 'analyses': [
         # 'TLAys6_duplicate',
         # 'TLAys3_duplicate',         
     # ]},
 ]

mergeSets = mergeSets[::-1]
prepend_merged = True


# mergeSets = []

# extra points hacked in - have to be in this order
limits['UA2']['extraPointsHack'] = [(260,0.45)]
limits['UA2']['extraPointsHack'] += [(270,0.5)]
limits['CDFrun1']['extraPointsHack'] = [(260,0.45)]
limits['CDFrun1']['extraPointsHack'] += [(250,0.5)]



# limits['CMS13boosted35.9_manual']['drawinterval'] = [50,240.5]
limits['jjg']['legname'] = 'ATLAS dijet + ISR (#gamma)'
limits['3jet']['legname'] = 'ATLAS dijet + ISR (jet)'
limits['dijetISRboosted']['legname'] = 'ATLAS boosted dijet + ISR'
limits['TLA100y06v6']['legname'] = 'ATLAS TLA'
