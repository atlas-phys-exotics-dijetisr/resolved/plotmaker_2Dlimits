
# copied when split off into separate config

savedir = 'summaryPlots_Mar18_withDiB'
with8TeV = True
with8TeV = False
withDijetISR = True
withDijetISR = False

withPreLHC = False
withPreLHC = True

textOnLines = False


date = 'March 2018'
dateOnTop = False
dateInLabel = True

bonusname = "EmmaStyle"
if not withDijetISR:
    bonusname += '_noDijetISR'
if with8TeV:
    bonusname += '_with8TeVdijet'

lumiInLegend = True
yearInLegend = False

atlasText = 'Internal'
# atlasText = 'Preliminary'
atlaslabelX = 0.15
atlaslabelY = 0.95
lumilabelXspacing = 0.37
lumilabelYspacing = 0

logy = False
logx = True

curved = True
curved = False
fillObs = True

# edit canvas
setRightMargin = 0.2
setLeftMargin = 0.14
setTopMargin = 0.07
setBottomMargin = 0.14


extraText = ['Axial vector mediator', '#lower[0.1]{Dirac Dark Matter}', 'm_{DM} = 10 TeV']
extraTextpos = [0.19,0.25,0.035] # x, y, spacing between lines
extraTextSize = 0.03


if dateInLabel:
    atlasText += ' '+date
    smallerText = True

setLegLeft = 0.8
setLegTop = 0.8
setLegBot = 0.15
setLegRight = 0.99
legtextsize = 0.029

setLeg2Top = 0.92
setLeg2Bot = 0.81
setLeg2Left = 0.81
setLeg2Right = 0.99
leg2textsize = 0.03

# hacking for band
leg2BotPlus = 0.016
leg2RightPlus = 0.05
leg2LeftPlus = 0.009

# get min and max x and y
xmin = 90
xmax = 4200
# xmin = 45
# xmax = 40000
ymin = 0.025
ymax = 0.4 # 0.375

addMajorTicks = [2000,3000,4000]
addMinorTicks = [1000,4000]
minorTickDivisions = 30

ymax = 0.35
xmax = 4000

# expected limit line
# lineWidth = 100*5 + 5
# hatchedOneSidedBand = True
# lineWidth = 5
hatchedOneSidedBand = False
drawBand = False
# drawBand = True
bandlinewidth = 6
lineWidth = 2

from limitlines import limits

drawTexts = True
for analysis in ['jjg','3jet','dijetFull']:
    limits[analysis]['text'] = ''

limits['dibjetHighMass']['xrange'] = [0,2500]
    
limits_to_plot = [
    'UA2',
    'CDFrun1',
    'CDFrun2',

    'CMS8dijet20fb',
    'CMS8scouting18.8fb_manual',

    # 'ATLAS8dijet20.3fb_eps',

    'CMS13scouting27_manual',
    'CMS13dijet36_manual',
    'CMS13boosted35.9_manual',
    
    'dijetISRboosted',
    'jjg',
    '3jet',
    'TLA100y06v8',
    'TLA75y03v8',
    'dijetFull',
]

# import ROOT
# verticalLineStyle = ROOT.kDashed
# verticalLineColour = ROOT.TColor.GetColor("#CDCDCD")
# prepend_merged = True
# mergeSets = [
    # {'name': 'Other', 'legname': 'CMS / pre-LHC', 'col': 'grey',
     # 'analyses': [
         # 'CDFrun1',
         # 'CDFrun2',
         # 'UA2',
         # 'CMS8scouting18.8fb_manual',
         # 'CMS8dijet20fb',
         # 'CMS13boosted35.9_manual',
         # 'CMS13scouting27_manual',
         # 'CMS13dijet36_manual',
     # ]},
# ]
# bonusname += '_CMSandOld'

mergeSets = []
limits_to_plot = [
    'dijetISRboosted',
    'jjg',
    '3jet',
    'dibjetHighMass',
    'dibjetLowMass',
    'TLA100y06v8',
    'TLA75y03v8',
    'dijetFull',
]

if with8TeV:
    limits_to_plot = ['ATLAS8dijet20.3fb_eps'] + limits_to_plot

if not withDijetISR:
    limits_to_plot.remove('jjg')
    limits_to_plot.remove('3jet')

if withPreLHC:
    import ROOT
    verticalLineStyle = ROOT.kDashed
    verticalLineColour = ROOT.TColor.GetColor("#CDCDCD")
    mergeSets = [
        {'name': 'Other', 'analysisname': 'pre-LHC', 'col': 'grey',
         'lumi': 'None', 'ref': 'arXiv: 1306.2629',
         'analyses': [
             'CDFrun1',
             'CDFrun2',
             'UA2',
             # 'CMS8scouting18.8fb_manual',
             # 'CMS8dijet20fb',
             # 'CMS13boosted35.9_manual',
             # 'CMS13scouting27_manual',
             # 'CMS13dijet36_manual',
         ],
        },
    ]
    bonusname += '_plusPreLHC'
    limits_to_plot += [
        'CDFrun1',
        'CDFrun2',
        'UA2',
    ]
        


    
    
# change dijet to be darker
limits['dijetFull']['col'] = 'darkBlue'

# Move TLA y* labels
textlabelsize = 0.03
limits['TLA75y03v8']['pos'] = [440,0.05]
limits['TLA100y06v8']['pos'] = [980,0.04]
limits['dibjetHighMass']['pos'] = [1300,0.3]
limits['dibjetLowMass']['pos'] = [600,0.115]


bonusTexts = []
ypos = 0.26

spacing = 0.0445
if with8TeV:
    setLegBot = 0.13
    ypos = 0.263
    spacing = 0.0391
    if not withDijetISR:
        setLegBot = 0.324
        ypos = 0.263
        spacing = 0.0391
    elif withPreLHC:
        spacing = 0.0345
        ypos = 0.267
else:
    if not withDijetISR:
        if withPreLHC:
            setLegBot = 0.263

ypos_0 = ypos

for analysis in limits_to_plot:
    isMergeSet = False
    for i in range(len(mergeSets)):
        if analysis in mergeSets[i]['analyses']:
            isMergeSet = True
        
    if isMergeSet:
        continue
    if 'TLA75' in analysis: continue
    if 'dibjetLowMass' in analysis: continue
    limits[analysis]['legname'] = '#lower[-0.2]{#splitline{'+limits[analysis]['analysisname']+'}{#lower[-0.15]{LUMI fb^{#minus 1}}}}'

    bonusTexts.append({'text': limits[analysis]['ref'], 'pos': [5300, ypos], 'size': 0.015})
    ypos -= spacing

for i in range(len(mergeSets)):
    mergeSets[i]['legname'] = '#lower[-0.2]{#splitline{'+mergeSets[i]['analysisname']+'}{#lower[-0.15]{Some fb^{#minus 1}}}}'
    thisypos = ypos_0 - spacing*(len(limits_to_plot)-len(mergeSets[i]['analyses'])-2)
    bonusTexts.append({'text': mergeSets[i]['ref'], 'pos': [5300, thisypos], 'size': 0.015})
        
    



if textOnLines:
    textlabelsize = 0.028
    drawLeg = False
    limits['dijetISRboosted']['pos'] = [100, 0.068]
    limits['jjg']['pos'] = [230, 0.29]
    limits['3jet']['pos'] = [580, 0.18]

    limits['TLA75y03v8']['legname'] = 'Dijet TLA, EXOT-2016-20'
    limits['TLA75y03v8']['pos'] = [500,0.13]
    
    # |y*#kern[-0.5]{_{12}}| < 0.3'
    limits['TLA100y06v8']['legname'] = '|y*#kern[-0.5]{_{12}}| < 0.3'
    limits['TLA100y06v8']['pos'] = [500,0.1]

    limits['dijetFull']['pos'] = [1300, 0.26]
    limits['dijetFull']['legname'] = '#splitline{Dijet, LUMI fb^{#minus1} 2015+16}{#splitline{Phys. Rev. D 96,}{  052004 (2017)}}'
    
    setLeg2Top = 0.9
    setLeg2Bot = 0.78
    setLeg2Left = 0.7
    setLeg2Right = 0.99
    leg2textsize = 0.034

    extraText = ['Axial vector mediator, Dirac DM, m_{DM} = 10 TeV']
    extraTextpos = [0.49, 0.967, 0.033]
    extraTextSize = 0.034

    
