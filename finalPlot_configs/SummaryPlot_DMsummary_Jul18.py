
# copied when split off into separate config

# savedir = 'summaryPlots_DMsummary_May18'
# savedir = 'summaryPlots_DMsummary_June18'
savedir = 'summaryPlots_DMsummary_July18'
CMScomparison = False
with8TeV = True
# with8TeV = False
withDijetISR = True
# withDijetISR = False
withDib = True
# withDib = False
withPreLHC = True
withPreLHC = False
withTTbar = True
# withTTbar = False
withDijetAngular = True
# withDijetAngular = False
drawWidths = None


zoomOut = True

useMatplotlib = False

newCols = True
textOnLines = False
textOnLines = True

import time
date = time.strftime("%B %Y") # 'June 2018'
dateOnTop = False
dateInLabel = True
# added 
dateInLabel = False
smallerText = True

bonusname = "EmmaStyle"
if not withDijetISR:
    bonusname += '_noDijetISR'
if withDib:
    bonusname += '_withDib'
if with8TeV:
    bonusname += '_with8TeVdijet'
if withTTbar:
    bonusname += '_withTTbar'
if withDijetAngular:
    bonusname += '_withDijetAngular'
if zoomOut:
    bonusname += '_zoomedOut'
if textOnLines:
    bonusname += '_textOnPlot'


    
lumiInLegend = True
yearInLegend = False

atlasText = 'Internal'
# atlasText = 'Preliminary'
# atlaslabelX = 0.15
# atlaslabelY = 0.95
# lumilabelXspacing = 0.37
# lumilabelYspacing = 0
atlaslabelX = 0.11
atlaslabelY = 0.91
lumilabelXspacing = 0.31
lumilabelYspacing = 0
if zoomOut and withTTbar:
    lumilabelXspacing = 0.27

logy = False
logx = True
# logx = False


curved = True
curved = False
fillsCurved = True
fillObs = False
fillOldObs = True
if zoomOut:
    fillOldObs = False

# edit canvas
# setRightMargin = 0.2
# setLeftMargin = 0.14
# setTopMargin = 0.07
# setBottomMargin = 0.14
# changes based on https://cernbox.cern.ch/index.php/s/tig6M1zrtUF9pIX#pdfviewer
setRightMargin = 0.26
setLeftMargin = 0.08
setTopMargin = 0.03
setBottomMargin = 0.11
setXaxisOffset = 0.95
setYaxisOffset = 0.7

# extraText = ['Axial vector mediator', '#lower[0.1]{Dirac Dark Matter}', 'm_{DM} = 10 TeV']
# extraTextpos = [0.19,0.25,0.035] # x, y, spacing between lines
extraText = ['Axial vector mediator', '#lower[0.1]{Dirac DM}', 'm_{\chi} = 10 TeV, g_{\chi} = 1.0']
extraTextpos = [0.12,0.25,0.035] # x, y, spacing between lines
extraTextSize = 0.03
xtitle = "m_{Z'_{A}} [GeV]"

if dateInLabel:
    atlasText += ' '+date
    smallerText = True

setLegLeft = 0.8
setLegTop = 0.82
setLegBot = 0.13
setLegRight = 0.99
legtextsize = 0.029

setLeg2Top = 0.93
setLeg2Bot = 0.82
setLeg2Left = 0.81
setLeg2Right = 0.99
leg2textsize = 0.03

offset = 0.055
setLegLeft = setLegLeft - offset
setLeg2Left = setLeg2Left - offset
setLegRight = setLegRight - offset
setLeg2Right = setLeg2Right - offset


# hacking for band
leg2BotPlus = 0.016
leg2RightPlus = 0.05
leg2LeftPlus = 0.009

# get min and max x and y
xmin = 90
xmax = 4200
# xmin = 45
# xmax = 40000
ymin = 0.025
ymax = 0.4 # 0.375

addMajorTicks = [2000,3000,4000]
addMinorTicks = [1000,4000]
minorTickDivisions = 30

ymax = 0.35
xmax = 4000

if zoomOut:
    xmax = 5500
    addMajorTicks = [2000,3000,4000,5000]
    addMinorTicks = [1000,5500]
    minorTickDivisions = 45
    # ymax = 0.4
    
    ymin = 0.03
    ymax = 1.0
    logy = True 

    drawWidths = [0.07, 0.1, 0.15]
    # shadeWidths = None

    
# expected limit line
# lineWidth = 100*5 + 5
# hatchedOneSidedBand = True
# lineWidth = 5
hatchedOneSidedBand = False
drawBand = False
# drawBand = True
bandlinewidth = 6
lineWidth = 2


# CMS comparison
if CMScomparison:
    logx = False
    logy = False
    curved = False
    extraTextpos = [0.6,0.25,0.035] # x, y, spacing between lines

    addMajorTicks = None
    addMinorTicks = None
    minorTickDivisions = None

    # CMS comp v2
    logy = True
    ymin = 0.03
    ymax = 2
    xmin = 0
    xmax = 6000
    

from limitlines import limits

drawTexts = True

if not textOnLines:
    for analysis in ['jjg','3jet','dijetFull']:
        limits[analysis]['text'] = ''
        
limits['dibjetHighMass']['xrange'] = [0,2500]
    
limits_to_plot = [
    'UA2',
    'CDFrun1',
    'CDFrun2',

    'CMS8dijet20fb',
    'CMS8scouting18.8fb_manual',

    # 'ATLAS8dijet20.3fb_eps',

    'CMS13scouting27_manual',
    'CMS13dijet36_manual',
    'CMS13boosted35.9_manual',
    
    'dijetISRboosted',
    'jjg',
    '3jet',
    'TLA100y06v8',
    'TLA75y03v8',
    'dijetFull',
    'dijetAngular',
]

# import ROOT
# verticalLineStyle = ROOT.kDashed
# verticalLineColour = ROOT.TColor.GetColor("#CDCDCD")
prepend_merged = True
# mergeSets = [
    # {'name': 'Other', 'legname': 'CMS / pre-LHC', 'col': 'grey',
     # 'analyses': [
         # 'CDFrun1',
         # 'CDFrun2',
         # 'UA2',
         # 'CMS8scouting18.8fb_manual',
         # 'CMS8dijet20fb',
         # 'CMS13boosted35.9_manual',
         # 'CMS13scouting27_manual',
         # 'CMS13dijet36_manual',
     # ]},
# ]
# bonusname += '_CMSandOld'

mergeSets = []
limits_to_plot = [
    'dijetISRboosted',
    'jjg',
    '3jet',
    'dibjetHighMass',
    'dibjetLowMass',
    'TLA100y06v8',
    'TLA75y03v8',
    'ttbar',
    'dijetFull',
    'dijetAngular',
]

if with8TeV:
    limits_to_plot = ['ATLAS8dijet20.3fb_eps'] + limits_to_plot
    if fillOldObs:
        limits['ATLAS8dijet20.3fb_eps']['fillcol'] = 'lightgrey'
        limits['ATLAS8dijet20.3fb_eps']['fillalpha'] = 1

if not withDijetISR:
    limits_to_plot.remove('jjg')
    limits_to_plot.remove('3jet')
if not withDib:
    limits_to_plot.remove('dibjetHighMass')
    limits_to_plot.remove('dibjetLowMass')
if not withTTbar:
    limits_to_plot.remove('ttbar')
if not withDijetAngular:
    limits_to_plot.remove('dijetAngular')
    
if withPreLHC:
    import ROOT
    limits_to_plot = [
        'UA2',
        'CDFrun1',
        'CDFrun2',
    ] + limits_to_plot

    # extra points hacked in - have to be in this order                                                                                         
    limits['UA2']['extraPointsHack'] = [(260,0.45)]
    limits['UA2']['extraPointsHack'] += [(270,0.5)]
    limits['CDFrun1']['extraPointsHack'] = [(260,0.45)]
    limits['CDFrun1']['extraPointsHack'] += [(250,0.5)]
    
    if not curved:
        verticalLineStyle = ROOT.kDashed
        verticalLineColour = ROOT.TColor.GetColor("#CDCDCD")
        mergeSets = [
            {'name': 'Other', 'analysisname': 'UA2 & CDF',
             'col': 'grey', 'fillcol': 'lightgrey', 'fillalpha': 1,
             'lumi': 'None', 'ref': 'Phys. Rev. D 88, 035021 (2013)',
             'analyses': [
                 'UA2',
                 'CDFrun1',
                 'CDFrun2',
                 # 'CMS8scouting18.8fb_manual',
                 # 'CMS8dijet20fb',
                 # 'CMS13boosted35.9_manual',
                 # 'CMS13scouting27_manual',
                 # 'CMS13dijet36_manual',
             ],
            },
        ]
        if not fillOldObs:
            for i in range(len(mergeSets)):
                mergeSets[i]['fillcol'] = 'white' # None ends up black

    else:
        # don't use mergeSet
        for analysis in ['UA2', 'CDFrun1', 'CDFrun2']:
            limits[analysis]['col'] = 'grey'
            if fillOldObs:
                limits[analysis]['fillcol'] = 'lightgrey'
                limits[analysis]['fillalpha'] = 1
            
            limits[analysis]['analysisname'] = analysis
            limits[analysis]['ref'] = 'some'
            limits[analysis]['lumi'] = None
            limits[analysis]['legname'] = None
            
        limits['UA2']['legname'] = '#lower[-0.2]{#splitline{UA2 & CDF}{#lower[-0.15]{0.106-1.13 fb^{#minus 1}}}}'
        limits['UA2']['ref'] = 'Phys. Rev. D 88, 035021 (2013)'

        limits['CDFrun1']['xrange'] = [0,390]
        limits['CDFrun2']['xrange'] = [330,1000]
        
    bonusname += '_plusPreLHC'


    
    
# change dijet to be darker
limits['dijetFull']['col'] = 'darkBlue'

if newCols:
    limits['dijetISRboosted']['col'] = 'niceGreen'
    # limits['ATLAS8dijet20.3fb_eps']['col'] = 1
    # limits['jjg']['col'] = 'tealyBlue'
    limits['3jet']['col'] = 'darkerPurple'
    # limits['dibjetHighMass']['col'] = 'niceRed'
    # limits['dibjetLowMass']['col'] = 'niceRed'
    limits['TLA75y03v8']['col'] = 'niceBlue'
    limits['TLA100y06v8']['col'] = 'niceBlue'
    # limits['dijetFull']['col'] = 'niceOrange'
    limits['dijetFull']['col'] = 'lighterOrange'
    limits['dijetAngular']['col'] = 'pink' #'darkGreen' # or darkBlue?

    limits['ttbar']['col'] = 'darkGreen'
    pass



# Move labels
textlabelsize = 0.03
limits['TLA75y03v8']['pos'] = [440,0.05]
limits['TLA100y06v8']['pos'] = [980,0.04]
# limits['dibjetHighMass']['pos'] = [1300,0.3]
# limits['dibjetHighMass']['pos'] = [1450,0.33] # no scaling
limits['dibjetHighMass']['pos'] = [2100, 0.315] # with scaling
# limits['dibjetLowMass']['pos'] = [600,0.115]
limits['dibjetLowMass']['pos'] = [500,0.115]
if withTTbar:
    limits['dibjetLowMass']['pos'] = [360,0.128]
if withDijetAngular:
    limits['dibjetHighMass']['pos'] = [2290, 0.21]
    
if zoomOut:
    if withTTbar:
        limits['dibjetLowMass']['pos'] = [320,0.12]
        limits['dibjetHighMass']['pos'] = [2300, 0.2]
    limits['TLA75y03v8']['pos'] = [440,0.055]
    limits['TLA100y06v8']['pos'] = [980,0.045]

# rename
# limits['ATLAS8dijet20.3fb_eps']['analysisname'] = 'Di-jet 8 TeV'
limits['dibjetHighMass']['analysisname'] = 'Dibjet'
# limits['dijetISRboosted']['analysisname'] = 'Boosted di-jet + ISR'
limits['dijetISRboosted']['analysisname'] = 'Boosted dijet + ISR'
# limits['jjg']['analysisname'] = 'Resolved di-jet + ISR (#gamma)'
# limits['3jet']['analysisname'] = 'Resolved di-jet + ISR (j)'
limits['jjg']['analysisname'] = 'Resolved dijet + ISR (#gamma)'
limits['3jet']['analysisname'] = 'Resolved dijet + ISR (j)'
# limits['TLA100y06v8']['analysisname'] = 'TLA di-jet'
# limits['dijetFull']['analysisname'] = 'Di-jet'
# limits['dijetAngular']['analysisname'] = 'Di-jet angular'
limits['dijetAngular']['analysisname'] = 'Dijet angular'

        
bonusTexts = []


nEntries = 5
if with8TeV:
    nEntries += 1
if not withDijetISR:
    nEntries -= 2
if withPreLHC:
    nEntries += 1
if not withDib:
    nEntries -= 1
if withDijetAngular:
    nEntries += 1
if withTTbar:
    nEntries += 1
    
if nEntries < 7:
    setLegBot = setLegBot + (7-nEntries)*0.085

spacing = (setLegTop-setLegBot)/(nEntries+1)
ypos = setLegTop - spacing + 0.009 - 0.01
refxpos = 0.848 - offset




# for nEntries = 5

# setLegBot = 0.15 - (0.02 * (nEntries-5))
# ypos = something
# spacing = 0.0445 

# if with8TeV:
    # setLegBot = 0.13
    # ypos = 0.263
    # spacing = 0.0391
    # if not withDijetISR:
        # setLegBot = 0.324
        # ypos = 0.263
        # spacing = 0.0391
    # elif withPreLHC:
        # spacing = 0.0345
        # ypos = 0.267

# else:
    # if not withDijetISR:
        # if withPreLHC:
            # setLegBot = 0.263
            
            
ypos_0 = ypos
skipped = []
mergeSetI = -1
for analysis in limits_to_plot:
    isMergeSet = False
    for i in range(len(mergeSets)):
        if analysis in mergeSets[i]['analyses']:
            isMergeSet = True
            mergeSetI = i
        
    if isMergeSet:
        if prepend_merged:
            if mergeSetI not in skipped:
                ypos -= spacing
                skipped.append(mergeSetI)
                print "skipping a bit for", analysis, mergeSetI
        continue
    if 'TLA75' in analysis: continue
    if 'dibjetLowMass' in analysis: continue
    if 'CDF' in analysis: continue

    if 'UA2' in analysis:
        if withPreLHC and not curved:
            continue
        else:
            pass
    else:
        # limits[analysis]['legname'] = '#lower[-0.2]{#splitline{'+limits[analysis]['analysisname']+'}{#lower[-0.15]{LUMI fb^{#minus 1}}}}'
        limits[analysis]['legname'] = limits[analysis]['analysisname']
        
        # +'}{#lower[-0.15]{LUMI fb^{#minus 1}}}}'
        # {'text': '#splitline{}{'+ + '}{' + 
    # bonusTexts.append({'text': limits[analysis]['ref'], 'pos': [refxpos, ypos], 'NDC': True, 'size': 0.015})
    bonusTexts.append({'text': '#splitline{'+limits[analysis]['lumi']+' fb^{#minus 1}}{'+limits[analysis]['ref']+'}', 'pos': [refxpos, ypos], 'NDC': True, 'size': 0.015})
    
    ypos -= spacing

    print "\nI just did ref", limits[analysis]['ref'], 'for', analysis, 'at pos', ypos
    
for i in range(len(mergeSets)):
    # mergeSets[i]['legname'] = '#lower[-0.2]{#splitline{'+mergeSets[i]['analysisname']+'}{#lower[-0.15]{0.106-1.13 fb^{#minus 1}}}}'
    mergeSets[i]['legname'] = mergeSets[i]['analysisname']
    thisypos = ypos_0 - spacing*(len(limits_to_plot)-len(mergeSets[i]['analyses'])-2)
    if prepend_merged:
        thisypos = ypos_0 - spacing*(len(mergeSets[i]['analyses'])-3)
    # bonusTexts.append({'text': mergeSets[i]['ref'], 'pos': [refxpos, thisypos], 'NDC': True, 'size': 0.015})
    bonusTexts.append({'text': '#splitline{0.106-1.13 fb^{#minus 1}}{'+mergeSets[i]['ref']+'}', 'pos': [refxpos, thisypos], 'NDC': True, 'size': 0.015})

    # limits[analysis]['legname'] = limits[analysis]['analysisname']
        
    # bonusTexts.append({'text': '#splitline{'+limits[analysis]['lumi']+' fb^{#minus 1}}{'+limits[analysis]['ref']+'}', 'pos': [refxpos, ypos], 'NDC': True, 'size': 0.015})
    

    

if textOnLines:
    limits['dijetISRboosted']['pos'] = (100,0.09)
    for analysis in ['jjg','3jet', 'dijetFull']:
        limits[analysis]['text'] = ''
        
    limits['jjg']['pos'] = (105, 0.27)
    limits['3jet']['pos'] = (280, 0.14)
        
    limits['dibjetLowMass']['pos'] = (560, 0.12)
     # limits['dibjetHighMass']['pos'] = (980, 0.33) # no scaling
    limits['dibjetHighMass']['pos'] = (1500, 0.315) # with scaling
           
    limits['dijetFull']['pos'] = (2400, 0.1)
    limits['ATLAS8dijet20.3fb_eps']['pos'] = (360, 0.31)

    limits['TLA100y06v8']['text'] = 'Dijet TLA, '+ limits['TLA100y06v8']['text']

    limits['dijetAngular']['pos'] = (1520, 0.32)

    if withTTbar:
        limits['dibjetLowMass']['pos'] = (680, 0.19)
        limits['dibjetLowMass']['text'] = '#splitline{Di-b-jet}{#lower[-0.1]{trigger}}'
        limits['ttbar']['pos'] = (300, 0.11)
        limits['dibjetHighMass']['pos'] = (1410, 0.26)
        limits['dibjetHighMass']['text'] = '#splitline{Di-b-jet,}{#lower[-0.1]{Jet trigger}}'

    if withPreLHC:
        preLHCpos = [300, 0.33]
        if not withDijetISR:
            preLHCpos = [160, 0.27]
            if zoomOut:
                preLHCpos = [150, 0.25]
        bonusTexts.append({'text': 'UA2 & CDF', 'pos': preLHCpos, 'NDC': False, 'size': textlabelsize, 'col': 'grey'})

    if zoomOut:
        # limits['3jet']['pos'] = (270, 0.13)
        # limits['jjg']['pos'] = (140, 0.35)

         # split line
        limits['3jet']['pos'] = (315, 0.128)
        
        limits['jjg']['pos'] = (100, 0.3)
        limits['dijetISRboosted']['pos'] = (105,0.09)
        
        limits['dibjetLowMass']['pos'] = (670, 0.185)
        limits['dibjetHighMass']['pos'] = (1400, 0.27)
        limits['dijetAngular']['pos'] = (2200, 0.6)
        if withTTbar:
            # limits['ttbar']['pos'] = (280, 0.105)
            # limits['ttbar']['pos'] = (250, 0.095)
            limits['ttbar']['pos'] = (1600, 0.5)
            limits['3jet']['pos'] = (285, 0.128)
            limits['dijetAngular']['pos'] = (1250, 0.4)
            
    # limits['']['text'] = 'UA2 & CDF'
    
    # limits['dibjetHighMass']
    # limits['dibjetLowMass']
    # limits['TLA100y06v8']
    # limits['TLA75y03v8']

    # limits['dijetFull']


    # for analysis in limits_to_plot:
        # try:
            # limits[analysis]['text'] = limits[analysis]['analysisname']
        # except:
            # print analysis, "does not have a name"
            # pass

    # for analysis in ['jjg','3jet','dijetFull']:
        # if 'pos' not in limits[analysis]:
            # print analysis, 'does not have pos'




if not curved:
    fillsCurved = False
    
# fix curved fill areas
if fillsCurved:
    if with8TeV:
        limits['ATLAS8dijet20.3fb_eps']['hackFillPoints'] = [[[320,0.4]], None]
        if withPreLHC:
            limits['ATLAS8dijet20.3fb_eps']['hackWhiteBox'] = False
        else:
            limits['ATLAS8dijet20.3fb_eps']['hackWhiteBox'] = True

    if withPreLHC:
        if curved:
            limits['CDFrun1']['hackFillPoints'] = [None, [[400, 0.21], [430, 0.21], [450, 0.3]]]
            limits['CDFrun2']['hackFillPoints'] = [[[310, 0.28], [300, 0.3]], None]
            # limits['CDFrun2']['fillalpha'] = 0.4
            # limits['CDFrun2']['fillcol'] = 'red'

from helpers import cols
for analysis in limits:
    if analysis not in limits_to_plot:
        continue
    print analysis, limits[analysis]['col'], cols[limits[analysis]['col']]

