
# copied when split off into separate config

savedir = 'summaryPlots_forCaterina_mergeCollaborations_Jul18'

date = 'July 2018'
dateOnTop = False
dateInLabel = False

newATLASTLA = True
newCMSdijet = True

bonusname = None

if newATLASTLA or newCMSdijet:
    bonusname = ''
if newATLASTLA:
    bonusname += 'newATLASTLA'
if newCMSdijet:
    bonusname += 'newCMSdijet'


lumiInLegend = False
lumiOnPlot = False
drawSqrtsText = False
yearInLegend = False

atlasText = None

logy = False
logx = True

curved = False

setLegLeft = 0.58
setLegBot = 0.65
setLetRight = setLegLeft + 0.2
legtextsize = 0.04
drawLeg2 = False


# get min and max x and y
xmin = 90
xmax = 4200
xmin = 45
ymin = 0.
ymax = 0.5

addMajorTicks = [2000,3000,4000]
addMinorTicks = [1000,4000]
minorTickDivisions = 30

# expected limit line
hatchedOneSidedBand = False
drawBand = False
lineWidth = 2
noOverlap=True

verticalLineStyle = 2
verticalLineColour = 920+2 # kGray

limits = {
    
    # 2016 dijet
    'dijetFull': { 'grname': 'limits_dijets_full2016', 'lumi': '37.0',
                   'legname': '#splitline{Dijet, LUMI fb^{#minus1} 2015+16}{arXiv: 1703.09127}',
                   'text': '|y*#kern[-0.5]{_{12}}| < 0.6', 'col': 'dijet', 'pos': (2000,0.062)},



    # partial 2017 dijet+ISR
    'jjg': {       'grname': 'limits_gjet_DS2p1', 'lumi': '15.5', 
                   'legname': '#splitline{Dijet+ISR (#gamma), LUMI fb^{#minus1} 2015+16}{ATLAS-CONF-2016-070}',
                   'text': '|y*#kern[-0.5]{_{12}}| < 0.8', 'col': 'jjg',   'pos': (120,0.27)},

    '3jet': {      'grname': 'limits_3jet_DS2p1', 'lumi': '15.5',
                   'legname': '#splitline{Dijet+ISR (jet), LUMI fb^{#minus1} 2015+16}{ATLAS-CONF-2016-070}',
                   'text': '|y*#kern[-0.5]{_{23}}| < 0.6', 'col': 'jjj',   'pos': (580,0.17)},



    # 2016 TLA latest as of 5pm 13.12
    'TLA100y06v6': {    'grname': 'limits2016_TLA_J100yStar06_v6', 'lumi': '3.6-29.7',
                        'legname': '#splitline{Dijet TLA, LUMI fb^{#minus1} 2016}{EXOT-2016-20}',
                        'text': '|y*#kern[-0.5]{_{12}}| < 0.6', 'col': 'TLA',   'pos': (900,0.125),
                        'xrange': [700,3000] },
    'TLA75y03v6': {     'grname': 'limits2016_TLA_J75yStar03_v6', 'lumi': '3.6',
                        'legname':  None,
                        'text': '|y*#kern[-0.5]{_{12}}| < 0.3', 'col': 'TLA',   'pos': (357,0.03),
                        'xrange': [0,700] },


    # 2016 dijet+ISR boosted
    'dijetISRboosted' : { 'rootfile': '../data/limits_dijetISRBoosted.root', 'grname': ['Expected','Observed','ExpectedUp','ExpectedDown','Expected2Up','Expected2Down'],
                          'legname': '#splitline{Large-#it{R} jet + ISR, LUMI fb^{#minus1} 2016}{EXOT-2017-01}',
                          'lumi': '36.1', 'col': 'boosted'},
    

    # 2015 TLA
    'TLAys6': {'grname': 'limits_tla_all', 'lumi': '3.4',
            'legname': '#splitline{Dijet TLA, LUMI fb^{#minus1} 2015}{ATLAS-CONF-2016-030}',
            'col': 'TLAnew'},
    
    'TLAys3': {    'grname': 'limits_tla_all_ystar03',
                   'legname': None,
                   'col': 'TLAnew'},

    
    'UA2':                       {'grname': 'UA2',                       'legname': '#splitline{UA2, 10.9 pb^{-1} 0.63 TeV}{Z. Phys. C 49 (1991) 17}',                       'col': 'UA2',                'obsOnly': True},
    'CDFrun1':                   {'grname': 'CDFrun1',                   'legname': '#splitline{CDF run 1, 106 pb^{-1} 1.8 TeV}{arXiv: hep-ex/9702004}',                 'col': 'CDFrun1',            'obsOnly': True},
    'CDFrun2':                   {'grname': 'CDFrun2',                   'legname': '#splitline{CDF run 2, 1.13 fb^{-1} 1.96 TeV}{arXiv: 0812.4036}',                 'col': 'CDFrun2',            'obsOnly': True},
    'CMS8dijet20fb':             {'grname': 'CMS8dijet20fb',             'legname': '#splitline{CMS dijet, 19.7 fb^{-1} 8 TeV}{arXiv: 1501.04198}',       'col': 'CMS8dijet20fb',      'obsOnly': True},
    'CMS8scouting18.8fb_manual': {'grname': 'CMS8scouting18.8fb_manual', 'legname': '#splitline{CMS scouting, 18.8 fb^{-1} 8 TeV}{arXiv: 1604.08907}',  'col': 'CMS8scouting18.8fb', 'obsOnly': True},
    'ATLAS8dijet20.3fb_eps':     {'grname': 'ATLAS8dijet20.3fb_eps',     'legname': '#splitline{ATLAS dijet, 20.3 fb^{-1} 8 TeV}{arXiv: 1407.1376}',   'col': 'ATLAS8dijet20.3fb',  'obsOnly': True},
    'CMS13scouting12.9_manual': {'grname': 'CMS13scouting12.9_manual', 'legname': '#splitline{CMS scouting, 12.9 fb^{-1} 13 TeV}{arXiv: 1611.03568}', 'col': 'CMS13scouting12.9', 'obsOnly': True},
    'CMS13dijet12.9_manual':     {'grname': 'CMS13dijet12.9_manual',     'legname': '#splitline{CMS dijet, 12.9 fb^{-1} 13 TeV}{arXiv: 1611.03568}',    'col': 'CMS13dijet12.9',     'obsOnly': True},
    'CMS13scouting27_prelim_manual': {'grname': 'CMS13scouting27_prelim_manual', 'legname': '#splitline{CMS scouting, 27 fb^{-1} 13 TeV}{new add}', 'col': 'CMS13scouting27', 'obsOnly': True},
    'CMS13dijet36_prelim_manual':     {'grname': 'CMS13dijet36_prelim_manual',     'legname': '#splitline{CMS dijet, 36 fb^{-1} 13 TeV}{new add}',    'col': 'CMS13dijet36',     'obsOnly': True},
    'CMS13scouting27_manual': {'grname': 'CMS13scouting27_manual', 'legname': '#splitline{CMS scouting, 27 fb^{-1} 13 TeV}{new add}', 'col': 'CMS13scouting27', 'obsOnly': True},
    'CMS13dijet36_manual':     {'grname': 'CMS13dijet36_manual',     'legname': '#splitline{CMS dijet, 36 fb^{-1} 13 TeV}{new add}',    'col': 'CMS13dijet36',     'obsOnly': True},
    'CMS13boosted35.9_manual':     {'grname': 'CMS13boosted35.9_manual',     'legname': '#splitline{CMS boosted, 35.9 fb^{-1} 13 TeV}{arXiv: 1710.00159}',    'col': 'CMS13boosted35.9',     'obsOnly': True},

    
    }

drawTexts = False


limits_to_plot = [
    'UA2',
    'CDFrun1',
    'CDFrun2',

    
    'CMS13boosted35.9_manual',
    'CMS8scouting18.8fb_manual',
    # 'CMS13scouting12.9_manual',
    'CMS13scouting27_prelim_manual',
    'CMS8dijet20fb',
    # 'CMS13dijet12.9_manual',
    'CMS13dijet36_prelim_manual',

    'dijetISRboosted',
    'jjg',
    '3jet',
    'TLAys6',
    'TLAys3',
    'ATLAS8dijet20.3fb_eps',
    'dijetFull',
    
    
    # 'TLA100y06v6',
    # 'TLA75y03v6',
]


from limitlines import limits as central_limits
if newATLASTLA:
    limits_to_plot.remove('TLAys6')
    limits_to_plot.remove('TLAys3')
    limits_to_plot.append('TLA100y06v8')
    limits_to_plot.append('TLA75y03v8')

    limits['TLA100y06v8'] = central_limits['TLA100y06v8']
    limits['TLA75y03v8'] = central_limits['TLA75y03v8']

if newCMSdijet:
    limits_to_plot.remove('CMS13scouting27_prelim_manual')
    limits_to_plot.remove('CMS13dijet36_prelim_manual')
    limits_to_plot.append('CMS13scouting27_manual')
    limits_to_plot.append('CMS13dijet36_manual')

for limit in limits_to_plot:
    limits[limit]['obsOnly'] = True

mergeSets = [
    {'name': 'preLHC', 'legname': 'pre-LHC', 'col': 'niceMagenta',
     'analyses': ['CDFrun1', 'CDFrun2', 'UA2']},
    {'name': 'CMS', 'legname': 'CMS', 'col': 'niceOrange',
     'analyses': ['CMS13boosted35.9_manual',
                  'CMS8scouting18.8fb_manual',
                  # 'CMS13scouting12.9_manual',
                  'CMS13scouting27_prelim_manual',
                  'CMS8dijet20fb',
                  # 'CMS13dijet12.9_manual',
                  'CMS13dijet36_prelim_manual',
     ]},
    {'name': 'ATLAS', 'legname': 'ATLAS', 'col': 'tealyBlue',
     'analyses': [
         'dijetISRboosted',
         'jjg',
         '3jet',
         'TLAys6',
         'TLAys3',
         'ATLAS8dijet20.3fb_eps',
         'dijetFull',
     ]},
]

if newATLASTLA:
    mergeSets[2]['analyses'] = [
         'dijetISRboosted',
         'jjg',
         '3jet',
         'TLA100y06v8',
         'TLA75y03v8',
         'ATLAS8dijet20.3fb_eps',
         'dijetFull',        
    ]

if newCMSdijet:
    mergeSets[1]['analyses'] = [
        'CMS13boosted35.9_manual',
        'CMS8scouting18.8fb_manual',
        'CMS13scouting27_manual',
        'CMS8dijet20fb',
        'CMS13dijet36_manual',
    ]
    
for i in range(len(mergeSets)):
    mergeSets[i]['analysisname'] = mergeSets[i]['name']
    mergeSets[i]['fillcol'] = None
    mergeSets[i]['fillalpha'] = 0

# mergeSets = []

# extra points hacked in - have to be in this order
limits['UA2']['extraPointsHack'] = [(260,0.45)]
limits['UA2']['extraPointsHack'] += [(270,0.5)]
limits['CDFrun1']['extraPointsHack'] = [(260,0.45)]
limits['CDFrun1']['extraPointsHack'] += [(250,0.5)]

# for fun, add 13 fb CMS and new ATLAS TLA (comment out the """)
"""
limits_to_plot += [
    'CMS13dijet12.9_manual',
    'CMS13scouting12.9_manual',
    'TLA100y06v6',
    'TLA75y03v6',
    ]
limits['CMS13scouting12.9_manual']['col'] = 'nicePaleGreen'
limits['CMS13dijet12.9_manual']['col'] = 'niceGreen'
limits['CMS13scouting12.9_manual']['legname'] = 'CMS scouting 12.9 fb^{#minus 1}'
limits['CMS13dijet12.9_manual']['legname'] = 'CMS dijet 12.9 fb^{#minus 1}'
limits['TLA100y06v6']['legname'] = 'ATLAS TLA new result'
for limit in limits_to_plot:
    limits[limit]['obsOnly'] = True
setLegBot = 0.6
legtextsize = 0.035
"""

# pre-LHC
# limits['UA2']['drawinterval'] = [339,350]
# limits['CDFrun1']['drawinterval'] = [270.4,340]
# limits['CDFrun2']['drawinterval'] = [338,904]
# for line in ['UA2','CDFrun1','CDFrun2']:
    # limits[line]['col'] = 'CDFrun2'
    # limits[line]['legname'] = None
# limits['UA2']['legname'] = 'pre-LHC (UA2, CDF runs 1 and 2)'


# limits['CMS13boosted35.9_manual']['drawinterval'] = [50,240.5]
# limits['jjg']['drawinterval'] = [240.5,340]

# limits['3jet']['drawinterval'] = [350,450]
# limits['TLAys3']['drawinterval'] = [450,500]
# limits['CMS8scouting18.8fb_manual']['drawinterval'] = [500,775]
# limits['TLAys6']['drawinterval'] = [765,950]
# limits['ATLAS8dijet20.3fb_eps_1']['drawinterval'] = [950,1020]
# limits['CMS13scouting12.9_manual_1']['drawinterval'] = [1010,1190]
# limits['ATLAS8dijet20.3fb_eps_2']['drawinterval'] = [1185,1345] 
# limits['CMS13scouting12.9_manual_2']['drawinterval'] = [1335,1500]
