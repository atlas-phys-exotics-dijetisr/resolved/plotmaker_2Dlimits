
import ROOT
ROOT.gROOT.SetBatch(1)

from prot import utiltools
from prot import canvastools
from prot import plottools

import glob
import re
import os, os.path, sys
import numpy as np
import array

# import math
from math import *


def set_palette(name="palette", ncontours=4):

    from array import array
    from ROOT import TColor, gStyle

    """Set a color palette from a given RGB list
        stops, red, green and blue should all be lists of the same length
        see set_decent_colors for an example"""

    if name == "gray" or name == "grayscale":
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red   = [1.00, 0.84, 0.61, 0.34, 0.00]
        green = [1.00, 0.84, 0.61, 0.34, 0.00]
        blue  = [1.00, 0.84, 0.61, 0.14, 0.00]
    # elif name == "whatever":
    # (define more palettes)
    else:
        # default palette, looks cool
        #jamaica
        # stops = [0.00, 0.20, 0.61, 0.84, 1.00]
        # red   = [0.00, 0.00, 0.87, 1.00, 0.51]
        # green = [0.00, 0.81, 1.00, 0.0, 0.00]
        # blue  = [1.00, 0.20, 0.12, 0.00, 0.00]
        stops = [0.00, 0.20, 0.61, 0.84, 1.00]
        red   = [0.00, 0.00, 0.87, 1.00, 0.51]
        green = [0.00, 0.81, 1.00, 0.20, 0.00]
        blue  = [0.51, 1.00, 0.12, 0.00, 0.00]

    s = array('d', stops)
    r = array('d', red)
    g = array('d', green)
    b = array('d', blue)

    npoints = len(s)
    TColor.CreateGradientColorTable(npoints, s, r, g, b, ncontours)

    # For older ROOT versions
    #gStyle.CreateGradientColorTable(npoints, s, r, g, b, ncontours)
    gStyle.SetNumberContours(ncontours)

def TextLabelBold(x,y,text="",color=ROOT.kBlack, textsize=0.027):
    l = ROOT.TLatex()
    #l.SetNDC()
    l.SetTextFont(42)
    l.SetTextSize(textsize)
    l.SetTextColor(color)
    l.DrawLatex(x,y,text)
    #t=ROOT.TPaveText(x+0.015,y-0.005)

def TextLabelNotBold(x,y,text2="",color=ROOT.kBlack):
    l1 = ROOT.TLatex()
    #l1.SetNDC()
    l1.SetTextFont(42)
    l1.SetTextSize(0.023)
    l1.SetTextColor(color)
    print "text2"
    l1.DrawLatex(x,y,text2)

def bestbins(values):
    #mindiff=None
    print("start of function")
    print(" ")
    diff=[]
    print("values before : ", values)
    mindiff=0
    if len(values)==1:
        return values[0], values[0], 1
    for i in range(len(values)-1):
        diff.append(values[i+1]-values[i])
        print("diff: ", diff)

        width=min(diff)

        print("width in between : ", width)
    #width=mindiff

    print("values after: ", values)
    print("width after: ", width)
    print("width type: ", type(width))
    minval=min(values)-width/2
    maxval=max(values)+width/2
    nbins=int((maxval-minval)/width)
    return minval,maxval,nbins

def bestbins_dijetISR(values,defaultwidth=50):
    mindiff=None
    for i in range(len(values)-1):
        diff=values[i+1]-values[i]
        if mindiff==None or diff<mindiff:
            mindiff=diff
    width=mindiff if mindiff!=None else defaultwidth
    minval=min(values)-width/2
    maxval=max(values)+width/2
    nbins=int((maxval-minval)/width)
    return minval,maxval,nbins


def getOverlaidCurves(inLimits, below950Only=True, nobjets = False):

    # Load the limits
    limits={}
    fh=open(inLimits)
    limits = eval(fh.read())
    fh.close()

    # Determine bins
    gSMs=set()
    mRs=set()
    for gSM,limitsgSM in limits.items():
        gSMs.add(gSM)
        for mR,limit in limitsgSM.items():
            mRs.add(mR)
    gSMs=sorted(list(gSMs))
    mRs =sorted(list(mRs))

    print "gSMs", gSMs
    print "gSMs", mRs

    minmR ,maxmR ,nbinsmR =bestbins_dijetISR(mRs ,defaultwidth=50)
    mingSM,maxgSM,nbinsgSM=bestbins_dijetISR(gSMs,defaultwidth=0.05)
    maxgSM+=0.1
    nbinsgSM+=1

    # Make plot
    ratplots={}
    h2=ROOT.TH2F('excl','Exclusion Plot;m_{R} [GeV];g_{SM}',
                 nbinsmR,minmR*1e3,maxmR*1e3,
                 nbinsgSM,float('%0.3g'%mingSM),float('%0.3g'%maxgSM))

    for gSM,limitsgSM in limits.items():
        for mR,limit in limitsgSM.items():
            xslim_obs=limit.get('obs'   ,None)
            xslim_exp=limit.get('exp'   ,None)
            xstheory =limit.get('theory',None)
            binIdx=h2.FindBin(mR*1000,gSM)
            if xslim_obs==None:
                h2.SetBinContent(binIdx,0)
                continue
            if nobjets : xstheory = xstheory*1.25
            h2.SetBinContent(binIdx,xslim_obs/xstheory)
            ratplots[(mR,gSM)]=(xslim_obs/xstheory,xslim_exp/xstheory)

    # Make graph
    gobs=ROOT.TGraph()
    gexp=ROOT.TGraph()
    gidx=0
    for mR in mRs:
        # Get interesting point
        h=h2.ProjectionY("_py",h2.GetXaxis().FindBin(mR*1000),h2.GetXaxis().FindBin(mR*1000))
        binLim=h.GetNbinsX()
            #was for binIdx in range(h.GetNbinsX(),0,-1):
        for binIdx in range(0, h.GetNbinsX()):

            mu=h.GetBinContent(binIdx)
            print('testing',mR,binIdx,mu)
            if mu==0: continue
            binLim=binIdx
            if mu<1:
                break
        gSMref=float('%0.3g'%h.GetBinCenter(binLim))

        limit=limits.get(gSMref,{}).get(mR,{})
        xsref=limit['theory']
        xsobs=limit['obs']
        xsexp=limit['exp']
        if nobjets : xsref = xsref*1.25

        gSMobs=0
        gSMexp=0
        if xsobs==None:
            continue
        else:
            gSMobs=sqrt(gSMref**2*xsobs/xsref)
            gSMexp=sqrt(gSMref**2*xsexp/xsref)

        print(gSMobs,xsobs,xsref)
        print(gSMexp,xsexp,xsref)

        #avoid showing points above 950 GeV
        if (below950Only and mR*1000 >= 1000) or mR*1000 >= 3600 :
          continue
        gobs.SetPoint(gidx,mR*1000,gSMobs)
        gexp.SetPoint(gidx,mR*1000,gSMexp)
        gidx+=1

    gobs.Print("all")
    gexp.Print("all")

    utiltools.store.append(gobs)
    utiltools.store.append(gexp)

#    markers = []
#    for (mR,gSM),(xsobs,xsexp) in ratplots.items():
#        if gSM < 0.3 : continue
#        if mR < 0.3 : continue
#        if mR > 0.7 and gSM < 0.35 : continue
#        #print(mR,gSM)
#        #Tl.DrawLatex(mR*1000-25,gSM+0.005,'#color[34]{#scale[0.75]{%0.2f}}'  %(xsobs))
#        #Tl.DrawLatex(mR*1000-25,gSM-0.015,'#color[34]{#scale[0.75]{(%0.2f)}}'%(xsexp))
#        marker = ROOT.TMarker(mR*1000, gSM, ROOT.kStar)
#        marker.SetMarkerColor(ROOT.kRed-9)
#        marker.Draw("same")
#        markers.append(marker)


    return gobs, gexp#, markers


# https://meyerweb.com/eric/tools/color-blend
# http://www.december.com/html/spec/color3.html
# https://htmlcolorcodes.com/
cols = {
    'TLAold':   ROOT.kBlue-4,
    'TLA':      ROOT.TColor.GetColor("#003EFF"),
    'TLAb1':    ROOT.TColor.GetColor("#A2B9FF"),
    'TLAb2':    ROOT.TColor.GetColor("#D1DCFF"),
    'jjg':      ROOT.kRed,
    'jjj':      ROOT.kMagenta-2,
    'darkerPurple': ROOT.kMagenta+2,
    'dijetold': ROOT.kBlue+2,
    'darkBlue': ROOT.kBlue+3,
    'dijet':    ROOT.TColor.GetColor("#3F51B5"),
    'dijetb1':  ROOT.TColor.GetColor("#7986CB"),
    'dijetb2':  ROOT.TColor.GetColor("#C5CAE9"),
    'TLAnew':   ROOT.kCyan,

    'boosted':   ROOT.TColor.GetColor("#388E8E"), # kCyan+2
    'boostedb1': ROOT.TColor.GetColor("#A5CCCC"),
    'boostedb2': ROOT.TColor.GetColor("#C9E0E0"),

    'ugly':   ROOT.kBlack,
    'uglyb1': ROOT.kGreen,
    'uglyb2': ROOT.kYellow,

    'UA2':     ROOT.kCyan,
    'CDFrun1': ROOT.kGreen+2,
    'CDFrun2': ROOT.kGreen+1,
    'CMS8dijet20fb':      ROOT.TColor.GetColor("#F14A06"),
    'CMS8scouting18.8fb': ROOT.TColor.GetColor("#E562FF"),
    'ATLAS8dijet20.3fb':  ROOT.TColor.GetColor("#FFD500"),
    'CMS13scouting12.9':  ROOT.TColor.GetColor("#D500FF"),
    'CMS13dijet12.9':     ROOT.TColor.GetColor("#EC6F0C"),
    'CMS13boosted35.9':   ROOT.TColor.GetColor("#62B1FF"),

    'CMS13scouting27':  ROOT.kGreen+3,
    'CMS13dijet36':     ROOT.kGreen+1,

    'red':           ROOT.TColor.GetColor("#FF0000"),
    'niceRed':       ROOT.TColor.GetColor("#F30000"),

    'darkGreen':     ROOT.TColor.GetColor("#096209"),
    'midGreen':      ROOT.TColor.GetColor("#1BBE0A"),
    'niceGreen':     ROOT.TColor.GetColor("#51B816"),
    'nicePaleGreen': ROOT.TColor.GetColor("#8DE319"),

    'darkBlue':      ROOT.TColor.GetColor("#1B4F72"),
    'midBlue':       ROOT.TColor.GetColor("#4189FF"),
    'niceBlue':      ROOT.TColor.GetColor("#377eb8"),
    'tealyBlue':     ROOT.TColor.GetColor("#00BDFF"),

    'niceOrange':    ROOT.TColor.GetColor("#FF8B00"),
    'lighterOrange': ROOT.TColor.GetColor("#FFA004"),
    'midOrange':     ROOT.TColor.GetColor("#F16815"),

    'niceMagenta':   ROOT.TColor.GetColor("#BA3EFC"),
    'pink':          ROOT.TColor.GetColor("#f781bf"),

    'darkPurple':    ROOT.TColor.GetColor("#52009E"),

    'grey':          ROOT.TColor.GetColor("#CDCDCD"),
    'lightgrey':     ROOT.TColor.GetColor("#ECF0F1"),
    'verylightgrey': ROOT.TColor.GetColor("#F7F7F7"), # ROOT can only do some colours, nothing between lightgrey and verylightgrey (which is white)
    'white':         ROOT.kWhite,
}

# http://ksrowell.com/blog-visualizing-data/2012/02/02/optimal-colors-for-graphs/
colourlist = [
    # ROOT.TColor.GetColor(57,106,177), # blue
    # ROOT.TColor.GetColor(218,124,48), # orange
    # ROOT.TColor.GetColor(62,150,81), # green
    # ROOT.TColor.GetColor(204,27,41), # red
    # ROOT.TColor.GetColor(83,81,84), # dark grey
    # ROOT.TColor.GetColor(107,76,154), # purple
    # ROOT.TColor.GetColor(146,36,40), # dark red
    # ROOT.TColor.GetColor(148,139,61), # dark gold
    ROOT.TColor.GetColor('#FF6F0C'), # orange
    ROOT.TColor.GetColor('#FF1D1D'), # red
    ROOT.TColor.GetColor('#FF7BFF'), # pink
    ROOT.TColor.GetColor('#EEE600'), # yellow
    ROOT.TColor.GetColor('#008A19'), # green
    ROOT.TColor.GetColor('#005B8B'), # dark blue
    ROOT.TColor.GetColor('#47BFFF'), # light blue
    ROOT.TColor.GetColor(83,81,84), # dark grey
]

def getCol(colour):
    if type(colour)==str:
        col = cols[colour]
    elif type(colour)==int:
        col = colourlist[colour]
    else:
        print "failed to get colour for", colour
        col = None
    return col

def getHex(colourInt):
    col = ROOT.gROOT.GetColor(colourInt)
    return col.AsHexString()


def addTicks(hist, vals, axis="x", major=True, numDivisions=None):

    if numDivisions==None:
        tickvals = vals

    else:
        if len(vals) != 2:
            sys.exit("you asked for " + str(numDivisions) + "divisions with a list of length "+str(len(vals)) + " - this needs to be 2")
        spacing = int((vals[1]-vals[0])/float(numDivisions))
        print spacing
        tickvals = range(vals[0]+spacing,vals[1],spacing)

    print tickvals

    for val in tickvals:

        if axis=="x":
            x_low = val
            x_high = val
            y_low = hist.GetMinimum()
            y_high = hist.GetMaximum()
            mult = 0.03
            if not major: mult = 0.015

            if ROOT.gPad.GetLogy():
                mult = mult * 0.835 # because the below is wrong in some way
                yb = 10**( log10(y_low) + (log10(y_high)-log10(y_low))*mult )
                yt = 10**( log10(y_high) - (log10(y_high)-log10(y_low))*mult )

            else: # if linear
                length = mult * (y_high-y_low)
                yb = y_low + length
                yt = y_high - length

        else:
            print "adding ticks not implemented for y axis yet"
            sys.exit()

        onTop = (ROOT.gPad.GetTickx()==1)


        tick = ROOT.TLine()
        tick.SetLineWidth(1)
        tick.SetLineColor(1)



        tick.DrawLine(x_low, y_low, x_high, yb)
        if onTop:
            tick.DrawLine(x_low, y_high, x_high, yt)


